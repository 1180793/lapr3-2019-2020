SET SERVEROUTPUT ON;
CREATE OR REPLACE PROCEDURE update_park(p_id IN parks.id%TYPE,
                                      p_latitude IN parks.latitude%TYPE,
                                      p_longitude IN parks.longitude%TYPE,
                                      p_elevation IN parks.elevation%TYPE,
									  p_description IN parks.description%TYPE, 
									  p_max_bike_capacity IN parks.max_bike_capacity%TYPE,
                                      p_max_scooter_capacity IN parks.max_scooter_capacity%TYPE,
                                      p_aB IN parks.availableBikes%TYPE,
                                      p_aS IN parks.availableScooters%TYPE,                              
                                      p_input_voltage IN parks.input_voltage%TYPE,
									  p_input_current IN parks.input_current%TYPE,
                                      idAntigo IN parks.id%TYPE
                                    )IS
BEGIN
        UPDATE parks SET id=p_id
        WHERE id=idAntigo;     
        UPDATE parks SET latitude=p_latitude
        WHERE latitude=p_latitude;
        UPDATE parks SET longitude=p_longitude
        WHERE longitude=p_longitude;
        UPDATE parks SET elevation=p_elevation
        WHERE elevation=p_elevation;
        UPDATE parks SET max_bike_capacity=p_max_bike_capacity
        WHERE max_bike_capacity=p_max_bike_capacity;
        UPDATE parks SET max_scooter_capacity=p_max_scooter_capacity
        WHERE max_scooter_capacity=p_max_scooter_capacity;
        UPDATE parks SET availableBikes=p_aB
        WHERE availableBikes=p_aB;
        UPDATE parks SET availableScooters=p_aS
        WHERE availableScooters=p_aS;
        UPDATE parks SET input_voltage=p_input_voltage
        WHERE input_voltage=p_input_voltage;
		UPDATE parks SET input_current=input_current
        WHERE input_current=input_current;
END;
