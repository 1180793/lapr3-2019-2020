SET SERVEROUTPUT ON;
CREATE OR REPLACE PROCEDURE remove_park(p_id_remove IN parks.id%TYPE
                                        )IS   
BEGIN
   
   UPDATE parks SET max_bike_capacity =0 , max_scooter_capacity =0
        WHERE id=p_id_remove;     
   
   
END;