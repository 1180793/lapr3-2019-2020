CREATE OR REPLACE PROCEDURE add_park (
    p_id                   parks.id%TYPE,
    p_latitude             parks.latitude%TYPE,
    p_longitude            parks.longitude%TYPE,
    p_elevation            parks.elevation%TYPE,
    p_description          parks.description%TYPE,
    p_max_bike_capacity      parks.max_bike_capacity%TYPE,
    p_max_scooter_capacity   parks.max_scooter_capacity%TYPE,
    p_input_voltage         parks.input_voltage%TYPE,
    p_input_current         parks.input_current%TYPE
) AS
BEGIN
    INSERT INTO parks VALUES (  p_id,
                                p_latitude,
                                p_longitude,
                                p_elevation,
                                p_description,
                                p_max_bike_capacity,
                                p_max_scooter_capacity,
                                p_input_voltage,
                                p_input_current,
                                0,
                                0 );
END;
/