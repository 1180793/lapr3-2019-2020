CREATE OR REPLACE FUNCTION get_park (
    p_id IN parks.id%TYPE
) RETURN SYS_REFCURSOR IS
    cur_park SYS_REFCURSOR;
BEGIN
    OPEN cur_park FOR SELECT *
                      FROM parks p
                      WHERE p.id = p_id;
    RETURN cur_park;
END;
/