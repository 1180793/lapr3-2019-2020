CREATE OR REPLACE FUNCTION get_all_parks RETURN SYS_REFCURSOR IS
    cur_parks SYS_REFCURSOR;
BEGIN
    OPEN cur_parks FOR SELECT *
                       FROM parks p
                       WHERE p.max_bike_capacity != 0 AND max_scooter_capacity != 0; --if both capacities equals 0, the park is 'removed'

    RETURN cur_parks;
END;
/