CREATE OR REPLACE FUNCTION validate_park (
    p_id parks.id%TYPE
) RETURN INTEGER IS
    num_results INTEGER;
BEGIN
    SELECT COUNT(*) INTO num_results
    FROM parks p
    WHERE p_id = p.id;

    IF num_results != 0 THEN
        RETURN 0;
    ELSE
        RETURN 1;
    END IF;
END;
/