CREATE OR REPLACE PROCEDURE add_user (
    p_email           users.email%TYPE,
    p_name            users.name%TYPE,
    p_password        users.password%TYPE,
    p_credit_card     users.credit_card%TYPE,
    p_gender          users.gender%TYPE,
    p_height          users.height%TYPE,
    p_weight          users.weight%TYPE,
    p_average_speed   users.average_speed%TYPE
) AS
BEGIN
    INSERT INTO users VALUES (
        p_email,
        p_name,
        p_password,
        p_credit_card,
        p_gender,
        p_height,
        p_weight,
        'N',
        0,
        p_average_speed
    );

END;
/