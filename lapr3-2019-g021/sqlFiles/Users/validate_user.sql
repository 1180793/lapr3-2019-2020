CREATE OR REPLACE FUNCTION validate_user (
    new_email users.email%TYPE, user_name IN USERS.name%type
) RETURN INTEGER IS num_results INTEGER;
BEGIN
    SELECT COUNT(*) INTO num_results
    FROM users u
    WHERE new_email = u.email or user_name = u.name ;

    IF num_results != 0 THEN
        RETURN 0;
    ELSE
        RETURN 1;
    END IF;
END;
/