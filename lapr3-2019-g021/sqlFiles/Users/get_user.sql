CREATE OR REPLACE FUNCTION get_user (
    p_mail users.email%TYPE
) RETURN SYS_REFCURSOR IS
    cur_user       SYS_REFCURSOR;
    number_users   INTEGER;
    ex_not_found EXCEPTION;
BEGIN
    SELECT COUNT(email) INTO number_users
    FROM users u
    WHERE u.email = p_mail;

    IF number_users = 0 THEN
        RAISE ex_not_found;
    END IF;
    OPEN cur_user FOR SELECT *
                      FROM users u
                      WHERE u.email = p_mail or u.name = p_mail;
EXCEPTION
    WHEN ex_not_found THEN
        RETURN cur_user;
END;

