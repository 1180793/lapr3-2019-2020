CREATE OR REPLACE FUNCTION get_all_users 
RETURN SYS_REFCURSOR IS
      cur_user SYS_REFCURSOR;
BEGIN
    OPEN cur_user 
        FOR SELECT *
            FROM users u;
    RETURN cur_user;
END;
/