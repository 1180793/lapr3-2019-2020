CREATE OR REPLACE FUNCTION func_get_user_last_invoice (
    p_username users.name%TYPE
)
RETURN SYS_REFCURSOR IS
    cur_invoice SYS_REFCURSOR;
BEGIN
    OPEN cur_invoice FOR SELECT *
                         FROM invoices i
                         WHERE i.username = p_username AND i.invoice_id = ( SELECT MAX(i.invoice_id)
                                                                            FROM invoices i
                                                                            WHERE i.username = p_username );
    RETURN cur_invoice;
END;
/