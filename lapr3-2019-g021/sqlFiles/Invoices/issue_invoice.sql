CREATE OR REPLACE PROCEDURE issue_invoice (
    p_invoice_id                        IN   invoices.invoice_id%TYPE,
    p_username                          IN   invoices.username%TYPE,
    p_previous_month_points             IN   invoices.previous_month_points%TYPE,
    p_points_earned_this_month          IN   invoices.points_earned_this_month%TYPE,
    p_points_used                       IN   invoices.points_used%TYPE,
    p_points_available_for_next_month   IN   invoices.points_available_for_next_month%TYPE,
    p_total_price                       IN   invoices.total_price%TYPE
) IS
BEGIN
    INSERT INTO invoices VALUES (
        p_invoice_id,
        p_username,
        p_previous_month_points,
        p_points_earned_this_month,
        p_points_used,
        p_points_available_for_next_month,
        p_total_price
    );
END;
/