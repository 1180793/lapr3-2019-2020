CREATE OR REPLACE FUNCTION func_get_new_invoice_number 
RETURN INTEGER IS
    invoicenumber   INTEGER;
    aux             INTEGER;
BEGIN
    SELECT MAX(invoice_id) INTO aux
    FROM invoices i;
    invoicenumber := aux + 1;
    RETURN invoicenumber;
END;
/