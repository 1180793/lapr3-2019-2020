CREATE OR REPLACE FUNCTION get_all_invoices 
RETURN SYS_REFCURSOR IS
      cur_invoice SYS_REFCURSOR;
BEGIN
    OPEN cur_invoice 
        FOR SELECT *
            FROM invoices i;
    RETURN cur_invoice;
END;
/