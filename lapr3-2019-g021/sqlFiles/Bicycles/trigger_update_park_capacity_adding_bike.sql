CREATE OR REPLACE TRIGGER update_park_capacity_adding_bikes BEFORE
INSERT  ON  bicycles  
FOR EACH ROW
DECLARE 
     b_current INTEGER;
    
    b_limit INTEGER;
   
    exeption_capacity EXCEPTION;
    
BEGIN
   
    
     
            SELECT max_bike_capacity INTO b_limit FROM parks   WHERE parks.id = :new.assigned_park;
             SELECT available_bikes INTO b_current FROM PARKS   WHERE parks.id = :new.assigned_park;
             
            IF b_limit  <= b_current THEN
                RAISE exeption_capacity;
         END IF;
           
            UPDATE parks SET available_bikes = b_current+1 WHERE :new.assigned_park = parks.id;
        
      
        
   
  
EXCEPTION
    WHEN exeption_capacity THEN 
        RAISE_APPLICATION_ERROR(-20001,'park capacity dont allow for more bicycles');
         

  END  ;

