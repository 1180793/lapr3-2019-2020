create or replace PROCEDURE validate_update_bicycle (
    id              bicycles.bicycle_id%TYPE,
    result          OUT int  
) IS

BEGIN
  select count(*)
  into   result
  from   bicycles
  where  bicycle_id = id;

END;