create or replace PROCEDURE update_scooter (
    id              scooters.scooter_id%TYPE,
    uweight         scooters.weight%TYPE,
    utype           scooters.type%TYPE,
    park            scooters.assigned_park%TYPE,
    max_battery     scooters.max_battery_capacity%TYPE,
    actual_battery  scooters.actual_battery_capacity%TYPE,
    aero            scooters.aero_coeficient%TYPE,
    front           scooters.frontal_area%TYPE
) IS
BEGIN
    UPDATE scooters SET
        scooter_id=id,
        weight=uweight,
        type=utype,
        assigned_park=park,
        max_battery_capacity=max_battery,
        actual_battery_capacity = actual_battery,
        aero_coeficient = aero,
        frontal_area=front
    ;

END;