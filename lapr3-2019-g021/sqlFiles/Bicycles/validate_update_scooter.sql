create or replace PROCEDURE validate_update_scooter (
    id              scooters.scooter_id%TYPE,
    result          OUT int  
) IS

BEGIN
  select count(*)
  into   result
  from   scooters
  where  scooter_id = id;
  
END;