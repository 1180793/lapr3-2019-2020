CREATE OR REPLACE FUNCTION get_bike (
    p_id IN bicycles.bicycle_id%TYPE
) RETURN SYS_REFCURSOR IS
    cur_bike SYS_REFCURSOR;
BEGIN
    OPEN cur_bike FOR SELECT *
                      FROM bicycles b
                      WHERE b.bicycle_id = p_id;
    RETURN cur_bike;
END;
/
