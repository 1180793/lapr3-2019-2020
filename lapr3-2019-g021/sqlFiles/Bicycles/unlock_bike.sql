CREATE OR REPLACE PROCEDURE update_user_bike(user_emailX IN USERS.email%TYPE,
                                               bike_id IN bicycles.bicycle_id%TYPE)IS
BEGIN
    UPDATE bicycles SET assigned_user=user_emailX
    WHERE bicycles.bicycle_id=bike_id;
END;
