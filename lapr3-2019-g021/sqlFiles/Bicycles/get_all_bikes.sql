CREATE OR REPLACE FUNCTION get_all_bikes RETURN SYS_REFCURSOR IS
    cur_bikes SYS_REFCURSOR;
BEGIN
    OPEN cur_bikes FOR SELECT *
                       FROM bicycles b
                       WHERE ( b.assigned_park IS NOT NULL ) ; --if  attribute is null, the vehicle is 'removed'

    RETURN cur_bikes;
END;
