CREATE OR REPLACE TRIGGER update_park_capacity_unlocking_removing_parking_bikes BEFORE
UPDATE  ON  bicycles  
FOR EACH ROW
DECLARE 
     b_current_next_park INTEGER;
    
    b_limit_next_park INTEGER;
    
     b_current_last_park INTEGER;
    
    b_limit_last_park INTEGER;
   
    exeption_capacity EXCEPTION;
    
BEGIN
 SELECT max_bike_capacity INTO b_limit_last_park FROM parks WHERE parks.id = :old.assigned_park;
                SELECT available_bikes INTO b_current_last_park FROM parks  WHERE parks.id = :old.assigned_park;


               if :new.assigned_park = NULL then --REMOVING VEHICLE
               UPDATE parks SET available_bikes = b_current_last_park-1 WHERE :old.assigned_park = parks.id;
               end if;
               
                if :old.assigned_park = :new.assigned_park then  --unlocking vehicle  parks stays the same, but in that park 1 slot is freed
                UPDATE parks SET available_bikes = b_current_last_park-1 WHERE :old.assigned_park = parks.id;
                end if; 
           
              if :old.assigned_park !=  :new.assigned_park then  -- parking a bike, park changes from old one to the new one
              
                SELECT max_bike_capacity INTO b_limit_next_park FROM parks WHERE parks.id = :new.assigned_park;
                SELECT available_bikes INTO b_current_next_park FROM parks  WHERE parks.id = :new.assigned_park;
               
               
               
                IF b_limit_next_park = b_current_next_park THEN -- if full throws exception
                    RAISE exeption_capacity;
                    END IF;
                    
                    
                    UPDATE parks SET available_bikes = b_current_next_park+1 WHERE :new.assigned_park = parks.id;
                  --  UPDATE parks SET available_bikes = b_current_last_park-1 WHERE :old.assigned_park = parks.id; comentado pois no unlock ja remove um slot ao park antigo
                end if; 
             
               
              
            

   
  
EXCEPTION
    WHEN exeption_capacity THEN 
        RAISE_APPLICATION_ERROR(-20001,'This park dont have more capacity for scooters ');
         

  END  ;
