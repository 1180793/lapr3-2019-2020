CREATE OR REPLACE FUNCTION validate_bike (
    p_id bicycles.bicycle_id%TYPE
) RETURN INTEGER IS
    num_results INTEGER;
BEGIN
    SELECT COUNT(*) INTO num_results
    FROM bicycles b
    WHERE p_id = b.bicycle_id;

    IF num_results != 0 THEN
        RETURN 0;
    ELSE
        RETURN 1;
    END IF;
END;
/
