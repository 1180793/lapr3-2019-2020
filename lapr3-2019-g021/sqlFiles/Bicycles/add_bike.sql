CREATE OR REPLACE PROCEDURE add_bike (
    id_bicycle      bicycles.bicycle_id%TYPE,
    weight          bicycles.weight%TYPE,
    park            bicycles.assigned_park%TYPE,
    aero            bicycles.aero_coeficient%TYPE,
    front           bicycles.frontal_area%TYPE,
    wheel           bicycles.wheel_size%TYPE
) IS
BEGIN
    INSERT INTO bicycles VALUES (
        id_bicycle,
        weight,
        park,
        aero,
        front,
        wheel,
        NULL
    );

END;
/