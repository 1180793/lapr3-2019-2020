create or replace PROCEDURE lock_bike(bike IN bicycles.bicycle_id%TYPE,
                                               park IN parks.id%TYPE)IS
BEGIN
    UPDATE bicycles SET assigned_user=null, assigned_park = park
    WHERE bicycles.bicycle_id=bike and assigned_user is not  null;
END;