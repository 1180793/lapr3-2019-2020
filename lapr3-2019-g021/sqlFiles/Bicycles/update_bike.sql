create or replace PROCEDURE update_bike (
    id_bicycle      bicycles.bicycle_id%TYPE,
    uweight          bicycles.weight%TYPE,
    park            bicycles.assigned_park%TYPE,
    aero            bicycles.aero_coeficient%TYPE,
    front           bicycles.frontal_area%TYPE,
    wheel           bicycles.wheel_size%TYPE
) IS
BEGIN
    UPDATE bicycles SET         
        weight=uweight,
        assigned_park=park,
        aero_coeficient=aero,
        frontal_area=front,
        wheel_size=wheel
    WHERE bicycle_id=id_bicycle
    ;

END;