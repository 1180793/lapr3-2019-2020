CREATE OR REPLACE PROCEDURE remove_bike (
    bike_id IN bicycles.bicycle_id%TYPE
) IS
BEGIN
    UPDATE bicycles b1
    SET assigned_park = NULL
    WHERE b1.bicycle_id = bike_id;

    UPDATE bicycles b2
    SET assigned_user = NULL
    WHERE b2.bicycle_id = bike_id;
END;
/
