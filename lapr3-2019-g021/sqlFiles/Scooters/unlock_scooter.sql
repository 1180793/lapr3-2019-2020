create or replace PROCEDURE update_user_scooter(user_email IN USERS.email%TYPE,
                                               scooter IN scooters.scooter_id%TYPE)IS
BEGIN
    UPDATE scooters SET assigned_user=user_email
    WHERE scooters.scooter_id=scooter;
END;