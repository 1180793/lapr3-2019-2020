CREATE OR REPLACE PROCEDURE remove_scooter (
    scooter_id IN scooters.scooter_id%TYPE
) IS
BEGIN
    UPDATE scooters s1
    SET assigned_park = NULL
    WHERE s1.scooter_id = scooter_id;

    UPDATE scooters s2
    SET assigned_user = NULL
    WHERE s2.scooter_id = scooter_id;
END;
/
