CREATE OR REPLACE FUNCTION get_scooter (
    p_id IN scooters.scooter_id%TYPE
) RETURN SYS_REFCURSOR IS
    cur_scooters SYS_REFCURSOR;
BEGIN
    OPEN cur_scooters FOR SELECT *
                      FROM scooters s
                      WHERE s.scooter_id = p_id;
    RETURN cur_scooters;
END;
/
