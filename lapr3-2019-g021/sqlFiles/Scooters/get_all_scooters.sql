CREATE OR REPLACE FUNCTION get_all_scooters RETURN SYS_REFCURSOR IS
    cur_scooters SYS_REFCURSOR;
BEGIN
    OPEN cur_scooters FOR SELECT *
                       FROM scooters s
                       WHERE ( s.assigned_park IS NOT NULL ); --if  attribute is null, the vehicle is 'removed'

    RETURN cur_scooters;
END;

