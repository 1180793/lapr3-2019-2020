CREATE OR REPLACE TRIGGER update_park_capacity_adding_scooters BEFORE
INSERT  ON  scooters  
FOR EACH ROW
DECLARE 
     s_current INTEGER;
    
    s_limit INTEGER;
   
    exeption_capacity EXCEPTION;
    
BEGIN
   
    
     
            SELECT max_scooter_capacity INTO s_limit FROM parks   WHERE parks.id = :new.assigned_park;
             SELECT availables_cooters INTO s_current FROM PARKS   WHERE parks.id = :new.assigned_park;
             
            IF s_limit  <= s_current THEN
                RAISE exeption_capacity;
         END IF;
           
            UPDATE parks SET availables_cooters = s_current+1 WHERE :new.assigned_park = parks.id;
        
      
        
   
  
EXCEPTION
    WHEN exeption_capacity THEN 
        RAISE_APPLICATION_ERROR(-20001,'park capacity dont allow for more scooters');
         

  END  ;

