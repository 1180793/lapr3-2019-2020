create or replace PROCEDURE lock_scooter(scot IN bicycles.bicycle_id%TYPE,
                                               park IN parks.id%TYPE)IS
BEGIN
    UPDATE scooters SET assigned_user=null, assigned_park = park
    WHERE scooters.scooter_id=scot and assigned_user is not  null;
END;