CREATE OR REPLACE PROCEDURE add_scooter (
    scooter_id      scooters.scooter_id%TYPE,
    weight          scooters.weight%TYPE,
    type            scooters.type%TYPE,
    park            scooters.assigned_park%TYPE,
    max_battery     scooters.max_battery_capacity%TYPE,
    actual_battery  scooters.actual_battery_capacity%TYPE,
    aero            scooters.aero_coeficient%TYPE,
    front           scooters.frontal_area%TYPE,
    motor          scooters.motor%TYPE
) IS
BEGIN
    INSERT INTO scooters VALUES (
        scooter_id,
        weight,
        type,
        park,
        max_battery,
        actual_battery,
        aero,
        front,
        NULL,
        motor
    );

END;
