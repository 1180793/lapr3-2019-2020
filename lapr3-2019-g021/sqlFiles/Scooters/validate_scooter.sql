CREATE OR REPLACE FUNCTION validate_scooter (
    p_id scooters.scooter_id%TYPE
) RETURN INTEGER IS
    num_results INTEGER;
BEGIN
    SELECT COUNT(*) INTO num_results
    FROM scooters s
    WHERE p_id = s.scooter_id;

    IF num_results != 0 THEN
        RETURN 0;
    ELSE
        RETURN 1;
    END IF;
END;
/
