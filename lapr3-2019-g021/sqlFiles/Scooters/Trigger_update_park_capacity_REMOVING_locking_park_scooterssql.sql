create or replace TRIGGER update_park_capacity_unlocking_removing_parking_scooters BEFORE
UPDATE  ON  scooters  
FOR EACH ROW
DECLARE 
     S_current_next_park INTEGER;

    S_limit_next_park INTEGER;

     S_current_last_park INTEGER;

    S_limit_last_park INTEGER;

    exeption_capacity EXCEPTION;

BEGIN
 SELECT max_scooter_capacity INTO s_limit_last_park FROM parks WHERE parks.id = :old.assigned_park;
                SELECT availables_cooters INTO s_current_last_park FROM parks  WHERE parks.id = :old.assigned_park;


               if :new.assigned_park = NULL then --REMOVING VEHICLE
               UPDATE parks SET availables_cooters = s_current_last_park-1 WHERE :old.assigned_park = parks.id;
               end if;

                if :old.assigned_park = :new.assigned_park then  --unlocking vehicle  parks stays the same, but in that park 1 slot is freed
                UPDATE parks SET availables_cooters = s_current_last_park-1 WHERE :old.assigned_park = parks.id;
                end if; 

              if :old.assigned_park !=  :new.assigned_park then  -- parking a bike, park changes from old one to the new one

                SELECT max_scooter_capacity INTO s_limit_next_park FROM parks WHERE parks.id = :new.assigned_park;
                SELECT availables_cooters INTO s_current_next_park FROM parks  WHERE parks.id = :new.assigned_park;



                IF s_limit_next_park = s_current_next_park THEN -- if full throws exception
                    RAISE exeption_capacity;
                    END IF;


                    UPDATE parks SET availables_cooters = s_current_next_park+1 WHERE :new.assigned_park = parks.id;

                end if; 







EXCEPTION
    WHEN exeption_capacity THEN 
        RAISE_APPLICATION_ERROR(-20005,'This park dont have more capacity for scooters ');


  END  ;