--Drop TABLE /table_name/ CASCADE CONSTRAINTS PURGE;
DROP TABLE users CASCADE CONSTRAINTS PURGE;
DROP TABLE parks CASCADE CONSTRAINTS PURGE;
DROP TABLE pointsofinterest CASCADE CONSTRAINTS PURGE;
DROP TABLE scooters CASCADE CONSTRAINTS PURGE;
DROP TABLE bicycles CASCADE CONSTRAINTS PURGE;
DROP TABLE paths CASCADE CONSTRAINTS PURGE;
DROP TABLE invoices CASCADE CONSTRAINTS PURGE;
DROP TABLE trips CASCADE CONSTRAINTS PURGE;

-- USER TABLE --
CREATE TABLE users (
    email           VARCHAR(64),
    name            VARCHAR(64)     CONSTRAINT nn_user_name NOT NULL,
    password        VARCHAR(64)     CONSTRAINT nn_user_password NOT NULL,
    credit_card     CHAR(16),
    gender          CHAR            CONSTRAINT nn_user_gender NOT NULL,
    height          NUMBER(3)       CONSTRAINT ck_user_height CHECK ( height > 0 ),
    weight          NUMBER(5, 2)    CONSTRAINT ck_user_weight CHECK ( weight > 0 ),
    is_admin        CHAR            CONSTRAINT nn_user_is_admin NOT NULL,
    points          INTEGER         CONSTRAINT ck_user_points CHECK ( points >= 0 ),
    average_speed   NUMBER(5, 2)    CONSTRAINT ck_app_user_average_speed CHECK ( average_speed > 0 ),
    CONSTRAINT pk_user_email PRIMARY KEY ( email )
);
ALTER TABLE users ADD CONSTRAINT uk_users_username UNIQUE ( name ) ;

-- PARK TABLE --
CREATE TABLE parks (
    id                   VARCHAR(64),
    latitude             FLOAT,
    longitude            FLOAT,
    elevation            INTEGER,
    description          VARCHAR(64),
    max_bike_capacity    INTEGER,
    max_scooter_capacity INTEGER,
    input_voltage        FLOAT,
    input_current        FLOAT,
    available_bikes      INTEGER,
    availables_cooters   INTEGER,
    CONSTRAINT pk_park PRIMARY KEY ( id )
);

-- POI TABLE --
CREATE TABLE pointsofinterest (
    latitude    NUMBER(9, 6)    CONSTRAINT nn_pois_latitude NOT NULL,
    longitude   NUMBER(9, 6)    CONSTRAINT nn_pois_longitude NOT NULL,
    altitude    INTEGER         CONSTRAINT nn_pois_altitude NOT NULL,
    name        VARCHAR(64),
    CONSTRAINT pk_pois_latitude_longitude PRIMARY KEY ( latitude,
                                                        longitude )
);

-- BICYCLE TABLE --
CREATE TABLE bicycles (
    bicycle_id        	VARCHAR(64),
    weight            	INTEGER,
    assigned_park       VARCHAR(64),
    aero_coeficient   	NUMBER(5, 2),
    frontal_area      	NUMBER(3, 1),
	wheel_size         	INTEGER,
    assigned_user      	VARCHAR(64),
    CONSTRAINT pk_bicycles_id PRIMARY KEY ( bicycle_id )
);
ALTER TABLE bicycles ADD CONSTRAINT bicycles_fk_park FOREIGN KEY (assigned_park) REFERENCES parks (id);
ALTER TABLE bicycles ADD CONSTRAINT bicycles_fk_user FOREIGN KEY (assigned_user) REFERENCES users (email);

-- SCOOTER TABLE --
CREATE TABLE scooters (
    scooter_id              VARCHAR(64),
    weight                  INTEGER,
    type                    INTEGER,
    assigned_park      		VARCHAR(64),
    max_battery_capacity   	FLOAT,
    actual_battery_capacity INTEGER,
    aero_coeficient   		NUMBER(5, 2),
    frontal_area      		NUMBER(3, 1),
    assigned_user       	VARCHAR(64),
    motor                  INTEGER,
    CONSTRAINT pk_scooter_id PRIMARY KEY ( scooter_id )
);
ALTER TABLE scooters ADD CONSTRAINT scooters_fk_park FOREIGN KEY (assigned_park) REFERENCES parks (id);
ALTER TABLE scooters ADD CONSTRAINT scooters_fk_user FOREIGN KEY (assigned_user) REFERENCES users (email);

-- PATH TABLE --
CREATE TABLE paths (
    latitude_a             	NUMBER(9, 6),
    longitude_a            	NUMBER(9, 6),
    latitude_b             	NUMBER(9, 6),
    longitude_b           	NUMBER(9, 6),
    kinetic_coefficient   	FLOAT,
    wind_direction        	FLOAT,
    wind_speed            	FLOAT,
    CONSTRAINT pk_paths_latitudes_longitudes PRIMARY KEY ( latitude_a, longitude_a, latitude_b, longitude_b )
);

-- INVOICE TABLE --
CREATE TABLE invoices (
	invoice_id						INTEGER,
	username						VARCHAR(64)		CONSTRAINT nn_invoice_username NOT NULL,
	previous_month_points           INTEGER         CONSTRAINT nn_invoice_previous_month_points NOT NULL,
    points_earned_this_month        INTEGER         CONSTRAINT nn_invoice_points_earned_this_month NOT NULL,
    points_used                     INTEGER         CONSTRAINT nn_invoice_points_used NOT NULL,
    points_available_for_next_month INTEGER         CONSTRAINT nn_invoice_points_available_for_next_month NOT NULL,
    total_price                     NUMBER(6,2)     CONSTRAINT nn_invoice_total_price NOT NULL,
    CONSTRAINT pk_invoices_invoice_id PRIMARY KEY ( invoice_id )
);
ALTER TABLE invoices ADD CONSTRAINT fk_invoices_username FOREIGN KEY ( username ) REFERENCES users( name );

--TRIP TABLE----
CREATE TABLE trips (
    trip_Number        	INTEGER,
    vehicleId         	VARCHAR(64),
    typeVehicle         CHAR(1),  
	CONSTRAINT typeVehicle_const CHECK (typeVehicle = 'b' OR typeVehicle = 's'),
    dateUnlocked       	TIMESTAMP,
    parkOriginId     	VARCHAR(64),
    assignedUser   		VARCHAR(64),
    parkDestinyId   	VARCHAR(64),
    datelocked    		TIMESTAMP,   
    CONSTRAINT pk_trips_1 PRIMARY KEY ( assignedUser, dateUnlocked ),
    CONSTRAINT datelocked_const check (datelocked > dateUnlocked),
    price    			BINARY_DOUBLE,
    invoice_id          Integer
);
ALTER TABLE trips ADD CONSTRAINT fk_trips1 FOREIGN KEY (parkOriginId) REFERENCES parks (id);
ALTER TABLE trips ADD CONSTRAINT fk_trips2 FOREIGN KEY (parkDestinyId) REFERENCES parks (id);
ALTER TABLE trips ADD CONSTRAINT fk_trips3 FOREIGN KEY (assignedUser) REFERENCES users (email);
