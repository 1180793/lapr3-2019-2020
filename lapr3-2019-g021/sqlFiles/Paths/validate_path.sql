CREATE OR REPLACE FUNCTION validate_path (
    new_latitude_a    paths.latitude_a%TYPE,
    new_longitude_a   paths.longitude_a%TYPE,
    new_latitude_b    paths.latitude_b%TYPE,
    new_longitude_b   paths.longitude_b%TYPE
) RETURN INTEGER AS
    num_results INTEGER;
BEGIN
    SELECT COUNT(*) INTO num_results
    FROM paths p
    WHERE p.latitude_a = new_latitude_a AND p.longitude_a = new_longitude_a AND p.latitude_b = new_latitude_b AND p.longitude_b = new_longitude_b;
    
    IF num_results != 0 THEN
        RETURN 0;
    ELSE
        RETURN 1;
    END IF;
END;
/