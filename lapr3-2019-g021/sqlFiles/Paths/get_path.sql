CREATE OR REPLACE FUNCTION get_path (
    p_latitude_a    IN   paths.latitude_a%TYPE,
    p_longitude_a   IN   paths.longitude_a%TYPE,
    p_latitude_b    IN   paths.latitude_b%TYPE,
    p_longitude_b   IN   paths.longitude_b%TYPE
) RETURN SYS_REFCURSOR IS
    cur_path SYS_REFCURSOR;
BEGIN
    OPEN cur_path FOR SELECT *
                      FROM paths p
                      WHERE p.latitude_a = p_latitude_a
                            AND p.longitude_a = p_longitude_a
                            AND p.latitude_a = p_latitude_b
                            AND p.longitude_b = p_longitude_b;

    RETURN cur_path;
END;
/