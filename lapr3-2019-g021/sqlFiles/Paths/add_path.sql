CREATE OR REPLACE PROCEDURE add_path (
    p_latitude_a            IN   paths.latitude_a%TYPE,
    p_longitude_a           IN   paths.longitude_a%TYPE,
    p_latitude_b            IN   paths.latitude_b%TYPE,
    p_longitude_b           IN   paths.longitude_b%TYPE,
    p_kinetic_coefficient   IN   paths.kinetic_coefficient%TYPE,
    p_wind_direction        IN   paths.wind_direction%TYPE,
    p_wind_speed            IN   paths.wind_speed%TYPE
) IS
BEGIN
    INSERT INTO paths (
        latitude_a,
        longitude_a,
        latitude_b,
        longitude_b,
        kinetic_coefficient,
        wind_direction,
        wind_speed
    ) VALUES (
        p_latitude_a,
        p_longitude_a,
        p_latitude_b,
        p_longitude_b,
        p_kinetic_coefficient,
        p_wind_direction,
        p_wind_speed
    );

END;
/