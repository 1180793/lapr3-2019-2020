CREATE OR REPLACE FUNCTION get_all_paths RETURN SYS_REFCURSOR IS
    cur_paths SYS_REFCURSOR;
BEGIN
    OPEN cur_paths FOR SELECT *
                       FROM paths;
END;
/