CREATE OR REPLACE FUNCTION get_all_trips RETURN SYS_REFCURSOR IS
    cur_trips SYS_REFCURSOR;
BEGIN
    OPEN cur_trips FOR SELECT *
                       FROM trips b;
                       

    RETURN cur_trips;
END;
