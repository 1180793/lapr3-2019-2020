CREATE OR REPLACE PROCEDURE update_trip(parkDestiny IN trips.parkdestinyid%TYPE,
                                               price IN trips.price%TYPE, tripnum IN trips.trip_number%TYPE, dateLock IN trips.datelocked%TYPE)IS
BEGIN
    UPDATE trips SET parkdestinyid=parkDestiny , price=price, datelocked=dateLock
    WHERE trips.trip_number=tripnum;
END;
