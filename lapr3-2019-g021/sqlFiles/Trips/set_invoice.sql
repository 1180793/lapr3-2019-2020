CREATE OR REPLACE PROCEDURE set_invoice (
    trip      IN   trips.trip_number%TYPE,
    invoice   IN   trips.invoice_id%TYPE
) IS
BEGIN
    UPDATE trips SET invoice_id = invoice WHERE trips.trip_number = trip;

END;
/