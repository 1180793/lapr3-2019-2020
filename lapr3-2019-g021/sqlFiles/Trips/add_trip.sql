create or replace PROCEDURE add_trip(vehicle_id IN scooters.scooter_id%TYPE, typeVehicle IN trips.typeVehicle%TYPE, parkOrigin IN parks.id%TYPE, user_email IN USERS.email%TYPE,
                                              tripNumber IN trips.trip_number%type, dateUnlocked IN trips.dateunlocked%TYPE, invoice in trips.invoice_id%TYPE )IS
BEGIN
    INSERT INTO trips VALUES (
    tripNumber, 
        vehicle_id,
        typeVehicle,
         dateUnlocked,
        parkOrigin,
        user_email,
        NULL,
        NULL,
        0,
        invoice
    );
END;


