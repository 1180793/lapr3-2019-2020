CREATE OR REPLACE FUNCTION get_poi (
    latitude    pointsofinterest.latitude%TYPE,
    longitude   pointsofinterest.longitude%TYPE
) RETURN SYS_REFCURSOR IS
    cur_point_location SYS_REFCURSOR;
BEGIN
    OPEN cur_point_location FOR SELECT *
                                FROM pointsofinterest pois
                                WHERE (pois.latitude = latitude AND pois.longitude = longitude );
    RETURN cur_point_location;
END;
/