CREATE OR REPLACE PROCEDURE add_poi (
    poi_latitude    pointsofinterest.latitude%TYPE,
    poi_longitude   pointsofinterest.longitude%TYPE,
    poi_altitude    pointsofinterest.altitude%TYPE,
    poi_name        pointsofinterest.name%TYPE
) AS
BEGIN
    INSERT INTO pointsofinterest VALUES (
        poi_latitude,
        poi_longitude,
        poi_altitude,
        poi_name
    );
END;
/