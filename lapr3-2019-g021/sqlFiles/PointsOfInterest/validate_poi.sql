CREATE OR REPLACE FUNCTION validate_poi (
    new_latitude    pointsofinterest.latitude%TYPE,
    new_longitude   pointsofinterest.longitude%TYPE
) RETURN INTEGER AS
    result INTEGER;
BEGIN
    SELECT COUNT(*) INTO result
    FROM pointsofinterest pois
    WHERE pois.latitude = new_latitude AND pois.longitude = new_longitude;
    
    RETURN result;
END;
/