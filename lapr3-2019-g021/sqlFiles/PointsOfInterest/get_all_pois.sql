CREATE OR REPLACE FUNCTION get_all_pois 
RETURN SYS_REFCURSOR IS
      cur_poi SYS_REFCURSOR;
BEGIN
    OPEN cur_poi 
        FOR SELECT *
            FROM pointsofinterest p;
    RETURN cur_poi;
END;
/