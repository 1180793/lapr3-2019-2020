# Report

# Abstract #
A software company needs to develop a product that supports ride sharing businesses.
This service should allow managing users, bicycles, electric scooters, parks and the pickup and return process.

### Keywords:
* User
* Administrator
* Bicycle
* Scooter
* Park
* Point of Interest (POI)
* Path

# Introduction #
The purpose of this report is to document the entire development process of LAPR3's final project.

The development process of this project was based on the knowledge acquired in the other curricular units of this semester (ESINF, BDDAD and FSIAP) and also on LAPR3's Scrum Methodology and Jira.

Relatively to ESINF we have used the Data Structures , for BDDAD the Databases and also for FSIAP the Applied Physics equations and constants. We applied as well the analysis and design engineering, and object oriented programming patterns learned from last year.

The aim of this project is to create a software which allows an administrator to manage it's parks, bicycles and scooters and the application users. It also allows users to request bicycles to use arround a city, be suggested with different routes and earn points do get discouts on invoices.

This report is divided into 3 parts:
* Problem Statement
* Solution
* Conclusion

# State of the Art #
After research about the state of the art of software related to the one we are developing, we found a lot of simmilar systems in cities all around the world.

* Lime: Ride sharing system available in Lisbon, Portugal which allows users to use electrical scooters, electrically assisted bicycles and normal bicycles [1]

* Circ: Available in OPorto, Portugal and it allows users to rent electrical scooters [2]

# Problem Statement #
In response to LAPR3 Project Assignment, we had to develop a software which allows the administrator the management of a ride sharing business and his vehicles, parks and users and to allow users to unlock vehicles for personal use, gives routes suggestions according to distance, energy efficency routes, etc.
The software must be implemented in JAVA coding language and the data should persist in a Database(SQL).
The software must be capable to supports the management of the ride-sharing business.
In response to these problems, further into the report we will present options that we evaluate as valid solutions.

# Solution #
The solution we came up with is a software that is able to answer the proposed requisits, focusing specially in a correct way to process and store the data on the database.
Our software consists on a service based on unitary and integration tests through the assessment project.

## Administrator

### Vehicles
An admistrator can manage the vehicles in the system:
* Add Bicycles
* Add Scooters
* Update Bicycles
* Update Scooters
* Remove Bicycles
* Remove Scooters
* Get a Vehicle Report stating which vehicles can't perform a trip of X Km in flat road, under perfect circumstances

### Parks and Points of Interest (POIs)
Relatively to Parks and Points of Interest, an administrator can:
* Add Parks
* Add POIs
* Update Parks
* Remove Parks
* Get a Park Report stating the current parked scooters, their battery level and how much time it will take to be fully charged

## User

### Bicycles
Relatively to Vehicles, a User can:
* Unlock a Vehicle
* Lock a Vehicle
* Know all available Vehicles in a Park

### Points of Interest / Parks
Relatively to Parks and Points of Interest, an user can:
* Know the distance to a Park
* Know the closest Parks
* Check if a Park has free slots for a Vehicle
* Calculate the Calories burnt between two Parks

# Conclusion #
In conclusion, throughout this project we were able to apply the acquired knowledge during the semester on all curricular units.

We have reached almost entirely proposed goals to the project. We didn't finish implementing the most energetic efficient route between two parks.
Relatively to the assessment we implemented integration tests to check and demonstrate the operation of our software, including database accesses.
Besides that, we had some errors connecting to DataBase, mostly on these final days, which took more time to finish integration tests than we espected.

In spite of these problems, we managed to implement most of the requisits and present a functional application.

# References #
[1]	"Lime".[Online].Available:https://www.li.me/pt

[2] "Circ".[Online].Available:https://goflash.com/pt/

# US Diagram

![US Diagram](docs/USDiagram.png)

# User Stories Documentation

| User Stories |                   
|:----------------------------------------------------------------------------|
| [US25 - Add Bicycle](docs/US25-AddBicycle/AddBicycle.md) |
| [US25 - Add Scooter](docs/US25-AddScooter/AddScooter.md) |
| [US26 - Remove Bicycle](docs/US26-RemoveBicycle/RemoveBicycle.md) |
| [US26 - Remove Scooter](docs/US26-RemoveScooter/RemoveScooter.md) |
| [US27 - Update Vehicle](docs/US27-UpdateVehicle/UpdateVehicle.md) |
| [US28 - User Registration](docs/US28-UserRegistration/UserRegistration.md) |
| [US29 - Login](docs/US29-Login/Login.md) |
| [US30 - Nearest Parks](docs/US30-NearestParks/NearestParks.md) |
| [US33 - Distance to a Park](docs/US33-DistanceToPark/DistanceToPark.md) |
| [US66 - Add POI](docs/US66-AddPOI/AddPOI.md) |
| [US86 - Add Path](docs/US86-AddPath/AddPath.md) |
| [US91 - Get Park Charging Status Report](docs/US91-GetParkChargingStatusReport/GetParkChargingStatusReport.md) |
| [US134 - Get the Shortest Route between two Parks](docs/US134-GetTheShortestRouteBetweenTwoParks/ShortestRouteTwoParks.md) |
| [US143 - Issue Monthly Invoice](docs/US143-IssueInvoice/IssueInvoice.md) |
| [US103 - Lock Bicycle](docs/US103-LockBicycle/LockBicycle.md) |
| [US103 - Lock Scooter](docs/US103-LockScooter/LockScooter.md) |
| [US103 - Unlock Bicycle](docs/US176-UnlockBicycle/UnlockBicycle.md) |
| [US103 - Unlock Scooter](docs/US176-UnlockScooter/UnlockScooter.md) |

# Relational Model

![Relational Model](docs/RelationalModel.png)


# README #

This is the repository template used for student repositories in LAPR Projets.

#Java source files

Java source and test files are located in folder src.

# Maven files #

Pom.xml file controls the project build.
## Observations
In this file, DO NOT EDIT the following elements:

* groupID
* artifactID
* version
* properties

Also, students can only add dependencies to the specified section on this file.

# Eclipse files #

The following files are solely used by Eclipse IDE:

* .classpath
* .project

# IntelliJ Idea IDE files #

The following folder is solely used by Intellij Idea IDE :

* .idea

## How was the .gitignore file generated? ##
.gitignore file was generated based on https://www.gitignore.io/ with the following keywords:
  - Java
  - Maven
  - Eclipse
  - NetBeans
  - Intellij

## Who do I talk to? ##
In case you have any problem, please email Nuno Bettencourt (nmb@isep.ipp.pt).

## How do I use Maven? ##

### How to run unit tests? ###
Execute the "test" goals.
`$ mvn test`

### How to generate the javadoc for source code? ###
Execute the "javadoc:javadoc" goal.

`$ mvn javadoc:javadoc`

This generates the source code javadoc in folder "target/site/apidocs/index.html".

### How to generate the javadoc for test cases code? ###
Execute the "javadoc:test-javadoc" goal.

`$ mvn javadoc:test-javadoc`

This generates the test cases javadoc in folder "target/site/testapidocs/index.html".

### How to generate Jacoco's Code Coverage Report? ###
Execute the "jacoco:report" goal.

`$ mvn test jacoco:report`

This generates a jacoco code coverage report in folder "target/site/jacoco/index.html".

### How to generate PIT Mutation Code Coverage? ###
Execute the "org.pitest:pitest-maven:mutationCoverage" goal.

`$ mvn test org.pitest:pitest-maven:mutationCoverage`

This generates a PIT Mutation coverage report in folder "target/pit-reports/YYYYMMDDHHMI".

### How to combine different maven goals in one step? ###
You can combine different maven goals in the same command. For example, to locally run your project just like on jenkins, use:

`$ mvn clean test jacoco:report org.pitest:pitest-maven:mutationCoverage`

### How to perform a faster pit mutation analysis ###
Do not clean build => remove "clean"

Reuse the previous report => add "-Dsonar.pitest.mode=reuseReport"

Use more threads to perform the analysis. The number is dependent on each computer CPU => add "-Dthreads=4"

Temporarily remove timestamps from reports.

Example:

`mvn test jacoco:report org.pitest:pitest-maven:mutationCoverage -DhistoryInputFile=target/fasterPitMutationTesting-history.txt -DhistoryOutputFile=target/fasterPitMutationTesting-history.txt -Dsonar.pitest.mode=reuseReport -Dthreads=4 -DtimestampedReports=false`

# Oracle repository

If you get the following error:

```
[ERROR] Failed to execute goal on project
bike-sharing: Could not resolve dependencies for project
lapr3:bike-sharing:jar:1.0-SNAPSHOT:
Failed to collect dependencies at
com.oracle.jdbc:ojdbc7:jar:12.1.0.2:
Failed to read artifact descriptor for
com.oracle.jdbc:ojdbc7:jar:12.1.0.2:
Could not transfer artifact
com.oracle.jdbc:ojdbc7:pom:12.1.0.2
from/to maven.oracle.com (https://maven.oracle.com):
Not authorized , ReasonPhrase:Authorization Required.
-> [Help 1]
```

Follow these steps:

https://blogs.oracle.com/dev2dev/get-oracle-jdbc-drivers-and-ucp-from-oracle-maven-repository-without-ides

You do not need to set a proxy.

You can use existing dummy Oracle credentials available at http://bugmenot.com.
