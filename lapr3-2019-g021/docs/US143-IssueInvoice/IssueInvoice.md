# **US143 - Issue Monthly Invoice**

JIRA Issue: [LAPR3G21-143](https://jira.dei.isep.ipp.pt:8443/browse/G21-143)

## **1. Analysis**

### Brief Description

The administrator requests an Invoice for a User. The system asks for the necessary data (month, username). The administrator enters the data. The system validates and issues the invoice for the specified month and user.

### Main Actor

Administrator

### System Sequence Diagram (SSD)

![IssueInvoiceSSD.png](IssueInvoiceSSD.png)

## **2. Design**

### Sequence Diagram

![Issue Monthly Invoice System Diagram.png](IssueInvoiceSD.png)

### Class Diagram

![Issue Monthly Invoice Class Diagram.png](IssueInvoiceCD.png)
