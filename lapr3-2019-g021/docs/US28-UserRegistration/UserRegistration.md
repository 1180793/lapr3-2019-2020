# **US28 - Register User**

JIRA Issue: [LAPR3G21-28](https://jira.dei.isep.ipp.pt:8443/browse/G21-28)

## **1. Analysis**

### Brief Description

The unregistered user starts the user registration. The system asks for the necessary data (username, email, password, credit card, gender, height, weight). The user enters the data. The system validates and requests for confirmation. The user confirms. The system confirms that the registation was successfully completed.

### Main Actor

Unregistered User

### System Sequence Diagram (SSD)

![UserRegistrationSD.png](UserRegistrationSSD.png)

## **2. Design**

### Sequence Diagram

![User Registration System Diagram.png](UserRegistrationSD.png)

### Class Diagram

![User Registration Class Diagram.png](UserRegistrationCD.png)
