# **US86 - Add a Path to the App**

JIRA Issue: [LAPR3G21-86](https://jira.dei.isep.ipp.pt:8443/browse/G21-86)

## **1. Analysis**

### Brief Description

The Administrator starts the Path adding process. The System asks for the necessary data (latitudeA, longitudeA, latitudeB, longitudeB, kinetic coefficient, wind direction, wind speed). The Administrator enters the data. The System validates and requests for confirmation. The Administrator confirms. The System confirms that the Path was successfully added.

### Main Actor

Administrator

### System Sequence Diagram (SSD)

![AddPathSSD.png](AddPathSSD.png)

## **2. Design**

### Sequence Diagram

![Add Path System Diagram.png](AddPathSD.png)

### Class Diagram

![Add Path Class Diagram.png](AddPathCD.png)
