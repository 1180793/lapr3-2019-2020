# **US25 - Add Scooter to the App**

JIRA Issue: [LAPR3G21-25](https://jira.dei.isep.ipp.pt:8443/browse/LPR19G21-25)

## **1. Analysis**

### Brief Description

The administrator starts the scooter adding process. The system asks for the necessary data. The administrator enters the data. The system validates and confirms that the scooter was successfully added.

### Main Actor

Administrator

### System Sequence Diagram (SSD)

![AddScooterSSD.jpg](AddScooterSSD.jpg)

## **2. Design**

### Sequence Diagram

![AddScooterCD.jpg](AddScooterSD.jpg)

### Class Diagram

![AddScooterCD.jpg](AddScooterCD.jpg)
