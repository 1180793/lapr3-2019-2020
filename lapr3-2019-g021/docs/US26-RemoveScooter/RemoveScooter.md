# **US26 - Remove Scooter**

JIRA Issue: [LAPR3G21-26](https://jira.dei.isep.ipp.pt:8443/projects/LPR19G21/issues/LPR19G21-26)

## **1. Analysis**

### Brief Description

The administrator starts the removal of a scooter process. The system asks for the necessary data. The administrator enters the data. The system validates and confirms that the scooter was successfully removed.

### Main Actor

Administrator

### System Sequence Diagram (SSD)

![RemoveScooterSSD.jpg](RemoveScooterSSD.jpg)

## **2. Design**

### Sequence Diagram

![RemoveScooterSD.jpg](RemoveScooterSD.jpg)

### Class Diagram

![RemoveScooterCD.jpg](RemoveScooterCD.jpg)
