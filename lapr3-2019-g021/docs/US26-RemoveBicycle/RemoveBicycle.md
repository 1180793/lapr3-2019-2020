# **US26 - Remove Bicycle**

JIRA Issue: [LAPR3G21-26](https://jira.dei.isep.ipp.pt:8443/projects/LPR19G21/issues/LPR19G21-26)

## **1. Analysis**

### Brief Description

The administrator starts the removal of a bicycle process. The system asks for the necessary data. The administrator enters the data. The system validates and confirms that the bicycle was successfully removed.

### Main Actor

Administrator

### System Sequence Diagram (SSD)

![RemoveBicycleSSD.jpg](RemoveBicycleSSD.jpg)

## **2. Design**

### Sequence Diagram

![RemoveBicycleSD.jpg](RemoveBicycleSD.jpg)

### Class Diagram

![RemoveBicycleCD.jpg](RemoveBicycleCD.jpg)
