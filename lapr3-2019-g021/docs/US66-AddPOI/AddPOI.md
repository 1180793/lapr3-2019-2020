# **US66 - Add a POI to the App**

JIRA Issue: [LAPR3G21-66](https://jira.dei.isep.ipp.pt:8443/browse/G21-66)

## **1. Analysis**

### Brief Description

The administrator starts the POI adding process. The system asks for the necessary data (latitude, longitude, altitude, name). The administrator enters the data. The system validates and requests for confirmation. The administrator confirms. The system confirms that the POI was successfully added.

### Main Actor

Administrator

### System Sequence Diagram (SSD)

![AddPOISSD.png](AddPOISSD.png)

## **2. Design**

### Sequence Diagram

![Add POI System Diagram.png](AddPOISD.png)

### Class Diagram

![Add POI Class Diagram.png](AddPOICD.png)
