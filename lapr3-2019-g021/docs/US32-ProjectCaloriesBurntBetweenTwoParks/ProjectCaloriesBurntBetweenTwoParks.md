# **US133 - Project Calories Burnt Between Two Parks**

JIRA Issue: [LAPR3G21-133](https://jira.dei.isep.ipp.pt:8443/browse/G21-133)

## **1. Analysis**

### Brief Description

The User starts the process to calculate the projected amount of calories burnt.
The User supplies the latitude and longitude of the origin and destination parks and the bicycle.  
The System validates and displays the result.  


### Main Actor

User

### System Sequence Diagram (SSD)

![ProjectCaloriesBurntBetweenTwoParksSSD.png](ProjectCaloriesBurntBetweenTwoParksSSD.png)

## **2. Design**

### Sequence Diagram

![ProjectCaloriesBurntBetweenTwoParksSD.png](ProjectCaloriesBurntBetweenTwoParksSD.png)

### Class Diagram

![ProjectCaloriesBurntBetweenTwoParksCD.png](ProjectCaloriesBurntBetweenTwoParksCD.png)
