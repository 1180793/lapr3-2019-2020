# **US30 - Get Nearest Parks**

JIRA Issue: [LAPR3G21-30](https://jira.dei.isep.ipp.pt:8443/browse/LPR19G21-30)

## **1. Analysis**

### Brief Description

The user starts the consultation of the nearest parks.The system asks for the user's current location.The user enters the requested data.The system shows the nearest parks.

### Main Actor

User

### System Sequence Diagram (SSD)

![SSD_NearestParks.png](SSD_NearestParks.png)

## **2. Design**

### Sequence Diagram

![SD_NearestParks.png](SD_NearestParks.png)

### Class Diagram

![CD_NearestParks.png](CD_NearestParks.png)
