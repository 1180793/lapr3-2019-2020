# **US29 - Login**

JIRA Issue: [LAPR3G21-29](https://jira.dei.isep.ipp.pt:8443/browse/G21-29)

## **1. Analysis**

### Brief Description

The user/administrator begins authentication process. The system requests email/username and password.The user/administrator enters the requested data. The system validates and confirms that the authentication process was successful.

### Main Actor

User/Administrator

### System Sequence Diagram (SSD)

![SSD_Login.png](SSD_Login.png)

## **2. Design**

### Sequence Diagram

![SD_Login.png](SD_Login.png)

### Class Diagram

![CD_Login.png](CD_Login.png)
