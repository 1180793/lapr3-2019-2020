# **US103 - Lock Bicycle**

JIRA Issue: [LAPR3G21-103](https://jira.dei.isep.ipp.pt:8443/browse/LPR19G21-103)

## **1. Analysis**

### Brief Description

The user stats the process to lock the Bicycle. Thge system validates that the park can handle one more Bicycle and informs sucess to user.

### Main Actor

User

### System Sequence Diagram (SSD)

![LockBicycleSSD.jpg](Lock_Bicycle_ssd.jpg)

## **2. Design**

### Sequence Diagram

![LockBicycleSD.jpg](Lock_Bicycle.jpg)

### Class Diagram

![LockBicycleCD.jpg](lock_Bicycle_cd.jpg)
