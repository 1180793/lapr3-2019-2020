# **US103 - Lock Scooter**

JIRA Issue: [LAPR3G21-103](https://jira.dei.isep.ipp.pt:8443/browse/LPR19G21-103)

## **1. Analysis**

### Brief Description

The user stats the process to lock the Scooter. Thge system validates that the park can handle one more Scooter and informs sucess to user.

### Main Actor

User

### System Sequence Diagram (SSD)

![LockBicycleSSD.jpg](Lock_scooter_ssd.jpg)

## **2. Design**

### Sequence Diagram

![LockBicycleSD.jpg](lock_scooter.jpg)

### Class Diagram

![LockBicycleCD.jpg](Lock_scooter_cd.jpg)
