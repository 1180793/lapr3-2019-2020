# **US32 - Get Park Available Slots**

JIRA Issue: [LAPR3G21-32](https://jira.dei.isep.ipp.pt:8443/browse/G21-32)

## **1. Analysis**

### Brief Description

The Administrator starts the process to get the number of available parking slots.  
The Administrator supplies the parkID, or latitude,longitude.  
The System validates and displays the number of available slots.  


### Main Actor

Administrator

### System Sequence Diagram (SSD)

![GetParkAvailableSlotsSSD.png](GetParkAvailableSlotsSSD.png)

## **2. Design**

### Sequence Diagram

![GetParkAvailableSlotsSD.png](GetParkAvailableSlotsSD.png)

### Class Diagram

![GetParkAvailableSlotsCD.png](GetParkAvailableSlotsCD.png)
