# **US76 - Unlock Scooter**

JIRA Issue: [LAPR3G21-76](https://jira.dei.isep.ipp.pt:8443/browse/LPR19G21-76)

## **1. Analysis**

### Brief Description

The user stats the process to unlock the Scooter. The system validates that the user is available to unlock a vehicle  , unlocks it and informs success to user.

### Main Actor

User

### System Sequence Diagram (SSD)

![LockBicycleSSD.jpg](analise_unlock.jpg)

## **2. Design**

### Sequence Diagram

![LockBicycleSD.jpg](Unlock_Scooter.jpg)

### Class Diagram

![LockBicycleCD.jpg](UnlockScooter_cd.jpg)
