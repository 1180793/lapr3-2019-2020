# **US25 - Add Bicycle to the App**

JIRA Issue: [LAPR3G21-25](https://jira.dei.isep.ipp.pt:8443/browse/LPR19G21-25)

## **1. Analysis**

### Brief Description

The administrator starts the bicycle adding process. The system asks for the necessary data. The administrator enters the data. The system validates and confirms that the bicycle was successfully added.

### Main Actor

Administrator

### System Sequence Diagram (SSD)

![AddBicycleSSD.jpg](AddBicycleSSD.jpg)

## **2. Design**

### Sequence Diagram

![AddBicycleSD.jpg](AddBicycleSD.jpg)

### Class Diagram

![AddBicycleCD.jpg](AddBicycleCD.jpg)
