# **US27 - Update Vehicle**

JIRA Issue: [LAPR3G21-27](https://jira.dei.isep.ipp.pt:8443/browse/G21-27)

## **1. Analysis**

### Brief Description

The Administrator starts the process to update vehicle.  
The Administrator supplies the vehicleID.  
The System shows the current vehicle information and asks for the new data:

**In case of bicicle**: weight, latitude, longitude, aerodynamicCoefficient, frontal_area, wheel size;  
**In case of scooter**: weight, type, latitude, longitude, maxBatteryCap, actualBatteryCap, aerodynamicCoefficient, frontal_area.  

The Administrator enters the data.  
The System validates and requests for confirmation.  
The Administrator confirms.  
The System confirms that the update was successfully completed.


### Main Actor

Administrator

### System Sequence Diagram (SSD)

![UpdateVehicleSSD-Bicycle.png](UpdateVehicleSSD-Bicycle.png)

![UpdateVehicleSSD-Scooter.png](UpdateVehicleSSD-Scooter.png)

## **2. Design**

### Sequence Diagram

![UpdateVehicleSD-Bicycle.png](UpdateVehicleSD-Bicycle.png)

![UpdateVehicleSD-Scooter.png](UpdateVehicleSD-Scooter.png)

### Class Diagram

![UpdateVehicleCD.png](UpdateVehicleCD.png)
