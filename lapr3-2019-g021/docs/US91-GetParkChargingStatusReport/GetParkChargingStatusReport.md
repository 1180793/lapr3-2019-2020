# **US91 - Get Park Charging Status Report**

JIRA Issue: [LAPR3G21-91](https://jira.dei.isep.ipp.pt:8443/browse/G21-91)

## **1. Analysis**

### Brief Description

The Administrator requests a report stating the charging status for each scooter and an estimate projection for how long it would take to reach 100% charge. The System requests a Park ID. The Administrator enters the Park ID. The Administrator returns the charging status report for the requested Park.

### Main Actor

Administrator

### System Sequence Diagram (SSD)

![AddPOISSD.png](GetParkChargingStatusReportSSD.png)

## **2. Design**

### Sequence Diagram

![Get Park Charging Status Report System Diagram.png](GetParkChargingStatusReportSD.png)

### Class Diagram

![Get Park Charging Status Report Class Diagram.png](GetParkChargingStatusReportCD.png)
