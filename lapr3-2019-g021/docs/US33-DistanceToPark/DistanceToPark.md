# **US33 - Calculate Distance to a Park**

JIRA Issue: [LAPR3G21-33](https://jira.dei.isep.ipp.pt:8443/browse/LPR19G21-33)

## **1. Analysis**

### Brief Description

The user starts the consultation to know how far a certair park is.The system requests the park's name/description.The user enters the requested data.The system demands the initial location.The user enters the requested data.The system shows the distance to the park from the location the user inputed.

### Main Actor

User

### System Sequence Diagram (SSD)

![SSD_DistanceToPark.png](SSD_DistanceToPark.png)

## **2. Design**

### Sequence Diagram

![SD_DistanceToPark.png](SD_DistanceToPark.png)

### Class Diagram

![CD_DistanceToPark.png](CD_DistanceToPark.png)
