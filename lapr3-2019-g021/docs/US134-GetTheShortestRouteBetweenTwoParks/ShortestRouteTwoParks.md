# **US134 - Get the Shortest Route between two Parks**

JIRA Issue: [LAPR3G21-134](https://jira.dei.isep.ipp.pt:8443/browse/G21-134)

## **1. Analysis**

### Brief Description

The user requests shortest path. The system demands starting and ending parks.The user enters the requested data. The system returns information regarding shortest path.

### Main Actor

User

### System Sequence Diagram (SSD)

![SSD_ShortestRouteTwoParks.png](SSD_ShortestRouteTwoParks.png)

## **2. Design**

### Sequence Diagram

![SD_ShortestRouteTwoParks.png](SD_ShortestRouteTwoParks.png)

### Class Diagram

![CD_ShortestRouteTwoParks.png](CD_ShortestRouteTwoParks.png)
