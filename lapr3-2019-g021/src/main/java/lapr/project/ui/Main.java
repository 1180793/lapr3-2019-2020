package lapr.project.ui;

import lapr.project.data.DataHandler;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;
import lapr.project.assessment.Facade;

/**
 * @author Nuno Bettencourt <nmb@isep.ipp.pt> on 24/05/16.
 */
class Main {

	/**
	 * Logger class.
	 */
	private static final Logger LOGGER = Logger.getLogger("MainLog");

	/**
	 * Private constructor to hide implicit public one.
	 */
	private Main() {

	}

	/**
	 * Application main method.
	 *
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws IOException, SQLException {

		//load database properties
		try {
			Properties properties
				= new Properties(System.getProperties());
			InputStream input = new FileInputStream("target/classes/application.properties");
			properties.load(input);
			input.close();
			System.setProperties(properties);
		} catch (IOException e) {
			e.printStackTrace();
		}

		//Initial Database Setup
		DataHandler dh = new DataHandler();
		//dh.scriptRunner("sqlFiles/createTables.sql");

              Facade facade = new Facade();
                int nUsers = facade.addUsers("fileTemplates/users.csv");
                System.out.println("There were added " + nUsers + " users to the App.");
//
//                int nPOIs = facade.addPOIs("fileTemplates/pois.csv");
//                System.out.println("There were added " + nPOIs + " POIs to the App.");
//
//                int nParks = facade.addParks("fileTemplates/parks.csv");
//                System.out.println("There were added " + nParks + " Parks to the App.");
//
//                int nBicycles = facade.addBicycles("fileTemplates/bicycles.csv");
//                System.out.println("There were added " + nBicycles + " Bicycles to the App.");
//               
//                int nScooters = facade.addEscooters("fileTemplates/escooters.csv");
//                System.out.println("There were added " + nScooters + " Scooters to the App.");
        }
}
