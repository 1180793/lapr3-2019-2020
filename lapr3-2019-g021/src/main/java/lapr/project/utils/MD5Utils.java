package lapr.project.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class MD5Utils {

	/**
	 * Private constructor to avoid the public default one
	 */
	private MD5Utils() {
	}

	/**
	 * Method to encrypt password.
	 *
	 * @param password: password to be encrypted
	 * @return encrypted password
	 */
	public static String encrypt(String password) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

			StringBuilder sb = new StringBuilder();
			for (byte b : hashInBytes) {
				sb.append(String.format("%02x", b));
			}

			return sb.toString();
		} catch (NoSuchAlgorithmException ex) {
			throw new AssertionError("unreachable", ex);
		}
	}
}
