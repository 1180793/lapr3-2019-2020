package lapr.project.utils;

/**
 *
 * @author Daniel
 */
public final class Constants {

        /**
         * Private constructor to avoid the public default one
         */
        private Constants() {

        }

        public static final int EARTH_RADIUS = 6371;
        public static final int GRAVITY_ACCELERATION = 10;
        public static final int ATHMO_PRESSURE = 1;  	//1 atm
	public static final int USER_REGISTRATION_FEE = 10;  	// 10€
        public static final double FRONTAL_AREA_DROP = 0.32;

}
