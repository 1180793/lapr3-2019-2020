package lapr.project.utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import lapr.project.data.ParkDB;
import lapr.project.data.PathDB;
import lapr.project.data.PointOfInterestDB;
import lapr.project.model.Path;
import lapr.project.model.PointOfInterest;
import lapr.project.model.graph.Graph;
import lapr.project.model.graph.GraphAlgorithms;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class GraphUtil {

	private final boolean testMode;

	/**
	 * Path Database
	 */
	private PathDB pathDb;

	/**
	 * POI Database
	 */
	private PointOfInterestDB poiDb;

	/**
	 * Park Database
	 */
	private ParkDB parkDb;

	/**
	 * Graph Algorithms constructor
	 *
	 * @param test
	 */
	public GraphUtil(boolean test) {
		this.testMode = test;
		this.pathDb = new PathDB(testMode);
		this.poiDb = new PointOfInterestDB(testMode);
		this.parkDb = new ParkDB(testMode);
	}

	/**
	 * Creates a Graph where the weight of the Edges is the distance between the POIs.
	 *
	 * @return the distance graph
	 */
	public Graph<PointOfInterest, Path> createDistanceGraph() {
		List<Path> allPaths = pathDb.getAllPaths();
		List<PointOfInterest> pointsInGraph = new ArrayList<>();
		List<Path> pathsInGraph = new ArrayList<>();
		Graph<PointOfInterest, Path> distanceGraph = new Graph<>(true);

		for (Path path : allPaths) {
			if (!pointsInGraph.contains(path.getPointA())) {
				distanceGraph.insertVertex(path.getPointA());
				pointsInGraph.add(path.getPointA());
			}
			if (!pointsInGraph.contains(path.getPointB())) {
				distanceGraph.insertVertex(path.getPointB());
				pointsInGraph.add(path.getPointB());
			}
			if (!pathsInGraph.contains(path)) {
				distanceGraph.insertEdge(path.getPointA(), path.getPointB(),
					path, Utils.calculateDistance(path.getPointA(), path.getPointB()));
				pathsInGraph.add(path);
			}
		}
		return distanceGraph;
	}

	/**
	 * Gets the shortests paths between two POIs given a sequence of POIs that must be visited and the number of Routes wanted.
	 *
	 * @param g graph
	 * @param vOrig origin vertice
	 * @param vDest destination vertice
	 * @param sequence sequence of points
	 * @param numRoutes number of routes asked
	 * @return returns a list of the shortest paths passing on the sequence
	 */
	public List<LinkedList<Path>> getShortestPathSequence(Graph<PointOfInterest, Path> g, PointOfInterest vOrig, PointOfInterest vDest, List<PointOfInterest> sequence, int numRoutes) {
		List<LinkedList<Path>> finalPaths = new ArrayList<>();
		List<LinkedList<PointOfInterest>> shortestPaths = new ArrayList<>();

		shortestPathSequence(g, vOrig, vDest, sequence, numRoutes, shortestPaths);

		for (int k = 0; k < numRoutes; k++) {
			LinkedList<Path> paths = new LinkedList<>();
			int j = 1;
			for (int i = 0; i < shortestPaths.get(k).size() - 1; i++) {
				Path path = pathDb.getPath(shortestPaths.get(k).get(i).getLatitude(), shortestPaths.get(k).get(i).getLongitude(), shortestPaths.get(k).get(j).getLatitude(), shortestPaths.get(k).get(j).getLongitude());
				paths.add(path);
				j++;

			}
			finalPaths.add(paths);
		}

		return finalPaths;
	}

	/**
	 * Get the shortest path (LinkedList of points)
	 *
	 * @param graph graph
	 * @param vOrig origin point
	 * @param vDest destination point
	 * @param sequence sequence of points
	 * @param numRoutes number of routes requested
	 * @param shortestPaths shortest path result
	 */
	private void shortestPathSequence(Graph<PointOfInterest, Path> graph,
		PointOfInterest vOrig, PointOfInterest vDest, List<PointOfInterest> sequence, int numRoutes, List<LinkedList<PointOfInterest>> shortestPaths) {

		// All Available Routes
		List<LinkedList<PointOfInterest>> availableRoutes = new ArrayList<>();

		// All sequence combinations
		List<LinkedList<PointOfInterest>> combinations = combinations(sequence);
		if (combinations.size() >= numRoutes) {
			ListIterator<LinkedList<PointOfInterest>> permutationIterator = combinations.listIterator();

			while (permutationIterator.hasNext()) {
				LinkedList<PointOfInterest> aux = new LinkedList<>();

				//Sequence
				LinkedList<PointOfInterest> permSequence = permutationIterator.next();
				ListIterator<PointOfInterest> it = permSequence.listIterator();

				//Path Sequence
				LinkedList<PointOfInterest> pathSequence = new LinkedList<>();
				pathSequence.add(vOrig);

				while (it.hasNext()) {
					PointOfInterest auxPOI = it.next();
					GraphAlgorithms.shortestPath(graph, pathSequence.getLast(), auxPOI, aux);

					for (int i = 1; i < aux.size(); i++) {
						pathSequence.add(aux.get(i));
					}
					aux.clear();
				}

				GraphAlgorithms.shortestPath(graph, pathSequence.getLast(), vDest, aux);

				for (int i = 1; i < aux.size(); i++) {
					pathSequence.add(aux.get(i));
				}
				availableRoutes.add(pathSequence);

			}

			//Calculate distances
			List<Double> distances = new ArrayList<>();
			for (int i = 0; i < availableRoutes.size(); i++) {
				double dist = 0;
				int j = 1;
				for (int k = 0; k < availableRoutes.get(i).size() - 1; k++) {
					dist += Utils.calculateDistance(availableRoutes.get(i).get(k), availableRoutes.get(i).get(j));
					j++;
				}
				distances.add(dist);
			}
			for (int i = 0; i < numRoutes; i++) {
				List<PointOfInterest> result = new LinkedList<>();

				double dist = Double.MAX_VALUE;
				int index = 0;
				for (int j = 0; j < distances.size(); j++) {
					if (distances.get(j) < dist) {
						index = j;
						dist = distances.get(j);
					}
				}
				for (int k = 0; k < availableRoutes.get(index).size(); k++) {
					result.add(availableRoutes.get(index).get(k));
				}
				shortestPaths.add((LinkedList<PointOfInterest>) result);
				distances.remove(index);
				availableRoutes.remove(index);
			}
		}
	}

	/**
	 * Returns all the possible combinations with the list of POIs
	 *
	 * @param original list of POIs
	 * @return all the combinations possible
	 */
	private static List<LinkedList<PointOfInterest>> combinations(List<PointOfInterest> original) {
		if (original.isEmpty()) {
			List<LinkedList<PointOfInterest>> result = new ArrayList<>();
			result.add(new LinkedList<>());
			return result;
		}
		PointOfInterest firstElement = original.remove(0);
		List<LinkedList<PointOfInterest>> returnValue = new ArrayList<>();
		List<LinkedList<PointOfInterest>> permutations = combinations(original);
		for (List<PointOfInterest> smallerPermutated : permutations) {
			for (int index = 0; index <= smallerPermutated.size(); index++) {
				LinkedList<PointOfInterest> temp = new LinkedList<>(smallerPermutated);
				temp.add(index, firstElement);
				returnValue.add(temp);
			}
		}
		return returnValue;
	}

//        /**
//         * Gets the most electrically eficient route between two parks for that user
//         *
//         * @param startPark Starting park
//         * @param destPark destination park
//         * @param allPaths list with all the paths
//         * @param scooter the scooter that will be used
//         * @param loggedUser the logged user
//         * @return returns the route
//         */
//        public List<PointOfInterest> getMostEfecienteElectricallyRoute(Park startPark, Park destPark, List<Path> allPaths, Scooter scooter, User loggedUser) {
//                Graph<PointOfInterest, Path> energyGraph = createEletricalEnergyGraph(scooter, allPaths, loggedUser);
//                LinkedList<PointOfInterest> shortPath = new LinkedList<>();
//                double dist = GraphAlgorithms.shortestPath(energyGraph, startPark.getPOI(), destPark.getPOI(), shortPath);
//                return shortPath;
//        }
//
//        public Graph<PointOfInterest, Path> createEletricalEnergyGraph(Scooter scooter, List<Path> allPaths, User loggedUser) {
//                List<PointOfInterest> pointsInGraph = new ArrayList<>();
//                List<Path> pathsInGraph = new ArrayList<>();
//                Graph<PointOfInterest, Path> energyGraph = new Graph<>(true);
//                for (Path path : allPaths) {
//
//                        if (!pointsInGraph.contains(path.getPointA())) {
//                                energyGraph.insertVertex(path.getPointA());
//                                pointsInGraph.add(path.getPointA());
//                        }
//
//                        if (!pointsInGraph.contains(path.getPointB())) {
//                                energyGraph.insertVertex(path.getPointB());
//                                pointsInGraph.add(path.getPointB());
//                        }
//
//                        if (!pathsInGraph.contains(path)) {
//                                energyGraph.insertEdge(path.getPointA(), path.getPointB(), path,
//                                        Utils.getElectricalEnergyBetweenTwoParks(path, scooter, loggedUser));
//                                pathsInGraph.add(path);
//                        }
//
//                }
//                return energyGraph;
//        }
//        
//        /**
//         * Gets the most electrically eficient route between two parks for that user
//         *
//         * @param startPark Starting park
//         * @param destPark destination park
//         * @param allPaths list with all the paths
//         * @param scooter the scooter that will be used
//         * @param loggedUser the logged user
//         * @return returns the route
//         */
//        public List<PointOfInterest> getMostEfecienteElectricallyRoute(Park startPark, Park destPark, List<Path> allPaths, Bicycle bike, User loggedUser) {
//                Graph<PointOfInterest, Path> energyGraph = createEletricalEnergyGraph(bike, allPaths, loggedUser);
//                LinkedList<PointOfInterest> shortPath = new LinkedList<>();
//                double dist = GraphAlgorithms.shortestPath(energyGraph, startPark.getPOI(), destPark.getPOI(), shortPath);
//                return shortPath;
//        }
//
//        public Graph<PointOfInterest, Path> createEletricalEnergyGraph(Bicycle bike, List<Path> allPaths, User loggedUser) {
//                List<PointOfInterest> pointsInGraph = new ArrayList<>();
//                List<Path> pathsInGraph = new ArrayList<>();
//                Graph<PointOfInterest, Path> energyGraph = new Graph<>(true);
//                for (Path path : allPaths) {
//
//                        if (!pointsInGraph.contains(path.getPointA())) {
//                                energyGraph.insertVertex(path.getPointA());
//                                pointsInGraph.add(path.getPointA());
//                        }
//
//                        if (!pointsInGraph.contains(path.getPointB())) {
//                                energyGraph.insertVertex(path.getPointB());
//                                pointsInGraph.add(path.getPointB());
//                        }
//
//                        if (!pathsInGraph.contains(path)) {
//                                energyGraph.insertEdge(path.getPointA(), path.getPointB(), path,
//                                        Utils.getElectricalEnergyBetweenTwoParks(path, bike, loggedUser));
//                                pathsInGraph.add(path);
//                        }
//
//                }
//                return energyGraph;
//        }
}
