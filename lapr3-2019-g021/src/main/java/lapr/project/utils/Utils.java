package lapr.project.utils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lapr.project.model.Path;
import lapr.project.model.PointOfInterest;
import lapr.project.model.Scooter;
import lapr.project.model.Trip;
import lapr.project.model.User;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Utils {

	/**
	 * Private constructor to avoid the public default one
	 */
	private Utils() {
	}

	/**
	 * Calculates the distance in meters between two POIs. Adapted from
	 * https://medium.com/allthingsdata/java-implementation-of-haversine-formula-for-distance-calculation-between-two-points-a3af9562ff1
	 *
	 * @param pointA point A
	 * @param pointB point B
	 * @return the distance between the two POIs
	 */
	public static double calculateDistance(PointOfInterest pointA, PointOfInterest pointB) {
		if (Double.compare(pointA.getLatitude(), pointB.getLatitude()) == 0 && Double.compare(pointA.getLongitude(), pointB.getLongitude()) == 0) {
			return 0.0;
		} else {
			double latDistance = toRad(pointB.getLatitude() - pointA.getLatitude());
			double lonDistance = toRad(pointB.getLongitude() - pointA.getLongitude());

			double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(toRad(pointA.getLatitude())) * Math.cos(toRad(pointB.getLatitude()))
				* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

			double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

			return lapr.project.utils.Constants.EARTH_RADIUS * c * 1000;
		}
	}

	private static double toRad(double value) {
		return value * Math.PI / 180;
	}


	/**
	 * Converts Hours into Seconds.
	 *
	 * @param time in Hours
	 * @return
	 */
	public static int convertHoursToSeconds(double time) {
		double seconds = time * 3600;
		int secondsRounded = (int) Math.round(seconds);
		return secondsRounded;
	}

	/**
	 * Converts Hours into a String (hh:mm:ss)
	 *
	 * @param time
	 * @return
	 */
	public static String convertHoursToString(double time) {
		int hoursAux = (int) time;
		int minutesAux = (int) (time * 60 - hoursAux * 60);
		int secondsAux = (int) (time * 3600 - hoursAux * 3600 - minutesAux * 60);

		String horas = Integer.toString(hoursAux);
		if (horas.length() == 1) {
			horas = "0" + horas;		// e.g. Replaces '3' for '03'
		}

		String minutos = Integer.toString(minutesAux);
		if (minutos.length() == 1) {
			minutos = "0" + minutos;
		}

		String segundos = Integer.toString(secondsAux);
		if (segundos.length() == 1) {
			segundos = "0" + segundos;
		}
		return horas + ":" + minutos + ":" + segundos;
	}

	/**
	 * Orders the passed map by descending time to finish charge in seconds and secondly by ascending Scooter Description.
	 *
	 * @param passedMap
	 * @return
	 */
	public static Map<String, Integer> sortParkChargingReport(Map<String, Integer> passedMap) {
		List<Map.Entry<String, Integer>> list = new ArrayList<>(passedMap.entrySet());

		// Sort list by Charging Time (Value) then by Scooter ID (Key)
		Collections.sort(list, (a, b) -> {
			int cmp1 = a.getValue().compareTo(b.getValue());
			if (cmp1 > 0) {
				return -1;
			} else if (cmp1 < 0) {
				return 1;
			} else {
				return a.getKey().compareTo(b.getKey());
			}
		});

		Map<String, Integer> result = new LinkedHashMap<>();
		for (Map.Entry<String, Integer> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}

		return result;
	}

	/**
	 * Returns the difference in seconds between 2 Timestamps.
	 *
	 * @param lockTime
	 * @param unlockTime
	 * @return
	 */
	public static long getTimestampDifference(Timestamp lockTime, Timestamp unlockTime) {
		long millisecondsUnlock = unlockTime.getTime();
		long millisecondsLock = lockTime.getTime();

		long diff = millisecondsLock - millisecondsUnlock;
		long diffSeconds = diff / 1000;

		return diffSeconds;
	}

	public static int calculateTripPoints(Trip t) {
		int elevation1 = t.getParkOrigin().getElevation();
		int elevation2 = 0;
		if (t.getParkDestiny() != null) {
			elevation2 = t.getParkDestiny().getElevation();
		}
		int relativeElevation = elevation2 - elevation1;
		int tripPoints = 0;
		// Users are given 5 points every time they park a vehicle on a park located 25m higher than the one they picked it, and 15 points when the park is 50m higher.
		if (relativeElevation > 25 && relativeElevation <= 50) {
			tripPoints = tripPoints + 5;
		} else if (relativeElevation > 50) {
			tripPoints = tripPoints + 15;
		}
		// When a user picks-up an electric scooter and leaves it in less that 15 minutes, 5 points are credited to the user
		if (t.getVehicleType().equalsIgnoreCase("S")) {
			long millisecondsUnlocked = t.getDateUnlocked().getTime();
			long millisecondsLocked = t.getDateLocked().getTime();

			long diff = millisecondsLocked - millisecondsUnlocked;
			long minuteDiff = diff / (60 * 1000);
			if (minuteDiff < 15) {
				tripPoints = tripPoints + 5;
			}
		}
		return tripPoints;

	}
}
