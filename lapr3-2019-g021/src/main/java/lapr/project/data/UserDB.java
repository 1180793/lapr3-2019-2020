package lapr.project.data;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lapr.project.data.mock.UsersMock;
import lapr.project.model.User;
import lapr.project.utils.MD5Utils;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Francisco
 */
public class UserDB extends DataHandler {

	private final boolean testMode;

	/**
	 * User database constructor
	 *
	 * @param mock
	 */
	public UserDB(boolean mock) {
		this.testMode = mock;
	}

	public boolean isMock() {
		return testMode;
	}

	/**
	 * Gets all the users on the database
	 *
	 * @return list with all the users
	 */
	public List<User> getAllUsers() {
		if (testMode) {
			UsersMock mock = new UsersMock();
			return mock.getAllUsers();
		}

		List<User> allUsers = new ArrayList<>();

		CallableStatement callStmtAux = null;
		ResultSet rSetAux = null;
		try {
			callStmtAux = getConnection().prepareCall("{ ? = call get_all_users }");

			callStmtAux.registerOutParameter(1, OracleTypes.CURSOR);

			callStmtAux.execute();

			rSetAux = (ResultSet) callStmtAux.getObject(1);

			while (rSetAux.next()) {

				String email = rSetAux.getString(1);
				String name = rSetAux.getString(2);
				String password = rSetAux.getString(3);
				String creditCard = rSetAux.getString(4);
				char gender = rSetAux.getString(5).charAt(0);
				double height = rSetAux.getDouble(6);
				double weight = rSetAux.getDouble(7);
				char isAdmin = rSetAux.getString(8).charAt(0);
				int points = rSetAux.getInt(9);
				double averageSpeed = rSetAux.getDouble(10);

				User user = new User(email, name, password, creditCard, gender, height, weight, averageSpeed);
				if (Character.toUpperCase(isAdmin) == 'S') {
					user.setAdmin(true);
				} else {
					user.setAdmin(false);
				}
				user.setPoints(points);

				allUsers.add(user);
			}
		} catch (SQLException e) {
		} finally {
			closeAll(rSetAux, callStmtAux);
			closeAll();
		}
		return allUsers;
	}

	/**
	 * Validates user globaly (Database)
	 *
	 * @param newUser user to be validated
	 * @return the validation
	 */
	public boolean validateUser(User newUser) {
		if (this.testMode) {
			UsersMock um = new UsersMock();
			return um.validateUser(newUser);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ ? = call validate_user(?,?) }");
			callStmt.registerOutParameter(1, OracleTypes.CHAR);
			callStmt.setString(2, newUser.getEmail());
			callStmt.setString(3, newUser.getName());
			callStmt.execute();
			int result = callStmt.getInt(1);
			callStmt.close();
			return result == 1;	// returns 1 if User is valid
		} catch (SQLException e) {
		} finally {
			closeAll(null, callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * Adds an User to the database
	 *
	 * @param newUser the user to be added
	 * @return if the user has been or not
	 */
	public boolean addUser(User newUser) {
		if (this.testMode) {
			UsersMock um = new UsersMock();
			return um.addUser(newUser);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ call add_user(?,?,?,?,?,?,?,?) }");

			callStmt.setString(1, newUser.getEmail());
			callStmt.setString(2, newUser.getName());
			callStmt.setString(3, newUser.getPassword());
			callStmt.setString(4, newUser.getCreditCard());
			callStmt.setString(5, String.valueOf(newUser.getGender()));
			callStmt.setDouble(6, newUser.getHeight());
			callStmt.setDouble(7, newUser.getWeight());
			callStmt.setDouble(8, newUser.getAverageSpeed());

			callStmt.execute();

			return true;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * Gets an User from the database
	 *
	 * @param email user email
	 * @return retreived user
	 */
	public User getUser(String email) {
		if (this.testMode) {
			UsersMock um = new UsersMock();
			return um.getUserById(email);
		}
		CallableStatement callStmt = null;
		ResultSet rSet = null;
		try {

			callStmt = getConnection().prepareCall("{ ? = call get_user(?) }");
			callStmt.registerOutParameter(1, OracleTypes.CURSOR);
			callStmt.setString(2, email);
			callStmt.execute();
			rSet = (ResultSet) callStmt.getObject(1);
			if (rSet != null) {
				rSet.next();
				String email1 = rSet.getString(1);
				String name = rSet.getString(2);
				String pass = rSet.getString(3);
				String cc = rSet.getString(4);
				char gender = rSet.getString(5).charAt(0);
				double height = rSet.getDouble(6);
				double weight = rSet.getDouble(7);

				char isAdmin = rSet.getString(8).charAt(0);
				int points = rSet.getInt(9);
				double avgSpeed = rSet.getDouble(10);

				User user1 = new User(email1, name, pass, cc, gender, height, weight, avgSpeed);

				if (Character.toUpperCase(isAdmin) == 'S') {
					user1.setAdmin(true);
				} else {
					user1.setAdmin(false);
				}
				user1.setPoints(points);
				return user1;
			}
		} catch (SQLException e) {
		} finally {
			closeAll(rSet, callStmt);
			closeAll();
		}
		return null;
	}

	/**
	 * Gets an User from the database by Username
	 *
	 * @param username user username
	 * @return retreived user
	 */
	public User getUserByUsername(String username) {
		List<User> userList = this.getAllUsers();
		for (User user : userList) {
			if (user.getName().equalsIgnoreCase(username)) {
				return user;
			}
		}
		return null;
	}

	/**
	 * method that reciving the name of the user and his new points update the points
	 *
	 * @param name
	 * @param points
	 * @return
	 */
	public boolean updatePoints(String name, int points) {
		if (testMode) {
			UsersMock um = new UsersMock();
			return um.updatePoints(name, points);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ call update_points(?,?) }");

			callStmt.setString(1, name);
			callStmt.setInt(2, points);

			callStmt.execute();

			return true;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * Validates if the loggin has been successfull
	 *
	 * @param email username or email
	 * @param password password
	 * @return if the loggin was successfull or not
	 */
	public boolean validateLogin(String email, String password) {
		if (testMode) {
			UsersMock um = new UsersMock();
			return um.validateLogin(email, password);
		}
		User oUser = getUser(email);
		if (oUser == null) {
			return false;
		}
		return oUser.getPassword().equals(MD5Utils.encrypt(password));
	}
}
