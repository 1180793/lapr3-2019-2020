package lapr.project.data;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import lapr.project.data.mock.InvoiceMock;
import lapr.project.model.Invoice;
import lapr.project.model.Trip;
import lapr.project.model.TripRegistry;
import lapr.project.model.User;
import oracle.jdbc.OracleTypes;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class InvoiceDB extends DataHandler {

	private final boolean testMode;

	/**
	 * Invoice database constructor
	 *
	 * @param mock
	 */
	public InvoiceDB(boolean mock) {
		this.testMode = mock;
	}

	public boolean isMock() {
		return testMode;
	}

	/**
	 * Gets all the invoices on the database
	 *
	 * @return list with all the users
	 */
	public List<Invoice> getAllInvoices() {
		if (testMode) {
			InvoiceMock mock = new InvoiceMock();
			return mock.getAllInvoices();
		}

		List<Invoice> list = new ArrayList<>();
		TripDB tripDb = new TripDB(testMode);

		CallableStatement callStmt = null;
		ResultSet rSet = null;
		try {
			callStmt = getConnection().prepareCall("{ ? = call get_all_invoices() }");

			callStmt.registerOutParameter(1, OracleTypes.CURSOR);

			callStmt.execute();

			rSet = (ResultSet) callStmt.getObject(1);

			while (rSet.next()) {

				int intID = rSet.getInt(1);
				String strUsername = rSet.getString(2);
				int intPreviousMonthPoints = rSet.getInt(3);
				int intPointsEarnedThisMonth = rSet.getInt(4);
				int intPointsUsed = rSet.getInt(5);
				int intPointsAvailableForNextMonth = rSet.getInt(6);
				double dblTotalPrice = rSet.getDouble(7);

				List<Trip> lstTrips = tripDb.getUserTrips(strUsername);

				Invoice oInvoice = new Invoice(intID, strUsername, intPreviousMonthPoints, intPointsEarnedThisMonth, intPointsUsed, intPointsAvailableForNextMonth, dblTotalPrice, lstTrips);

				list.add(oInvoice);
			}

			callStmt.close();
		} catch (SQLException e) {
		} finally {
			closeAll(rSet, callStmt);
			closeAll();
		}
		return list;
	}

	/**
	 * Issues a new User Invoice for specified month
	 *
	 * @param month
	 * @param user
	 * @return
	 */
	public Invoice issueInvoice(int month, User user) {
		if (testMode) {
			InvoiceMock mock = new InvoiceMock();
			return mock.issueInvoice(month, user);
		}
		Invoice oldInvoice = this.getUserLastInvoice(user);
		TripRegistry oRegistry = new TripRegistry(false);
		TripDB tripDb = new TripDB(false);
		System.out.println("INFO: Searching for User Monthly Trips...");
		List<Trip> lstTrips = oRegistry.getUserMonthTrips(user.getName(), month);
		System.out.println("INFO: Found " + lstTrips.size() + " Trips");
		int intInvoiceID = this.getNewInvoiceNumber();
		String strUsername = user.getName();
		int intPreviousMonthPoints = 0;
		int intPointsEarnedThisMonth = user.getPoints();
		int intPointsUsed = 0;
		int intPointsAvailableNextMonth = user.getPoints();
		double totalPrice = 0;
		System.out.println("INFO: Updating each Trip Invoice ID");
		for (Trip trip : lstTrips) {
			totalPrice += trip.getPrice();
			tripDb.setInvoice(trip.getTripNumber(), intInvoiceID);
		}
		if (oldInvoice != null) {
			intPreviousMonthPoints = oldInvoice.getPointsAvailableForNextMonth();
			intPointsEarnedThisMonth = user.getPoints() - intPreviousMonthPoints;
		}
		System.out.println("INFO: Applying Point Discounts");
		while (totalPrice > 1.00 && intPointsAvailableNextMonth >= 10) {
			totalPrice -= 1;
			intPointsAvailableNextMonth -= 10;
			intPointsUsed += 10;
		}

		Invoice invoice = new Invoice(intInvoiceID, strUsername, intPreviousMonthPoints, intPointsEarnedThisMonth, intPointsUsed, intPointsAvailableNextMonth, totalPrice, lstTrips);
		UserDB userDb = new UserDB(false);
		userDb.updatePoints(strUsername, intPointsAvailableNextMonth);
		CallableStatement callStmt = null;
		try {
			callStmt = getConnection().prepareCall("{ call issue_invoice(?,?,?,?,?,?,?) }");

			callStmt.setInt(1, invoice.getID());
			callStmt.setString(2, invoice.getUsername());
			callStmt.setInt(3, invoice.getPreviousMonthPoints());
			callStmt.setInt(4, invoice.getPointsEarnedThisMonth());
			callStmt.setInt(5, invoice.getPointsUsed());
			callStmt.setInt(6, invoice.getPointsAvailableForNextMonth());
			callStmt.setDouble(7, invoice.getTotalPrice());

			callStmt.execute();

			closeAll();
			System.out.println("INFO: Invoice Issued Successfully");
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return invoice;
	}

	/**
	 * Gets a sequential new number for Invoice ID
	 *
	 * @return
	 */
	public int getNewInvoiceNumber() {
		if (testMode) {
			InvoiceMock mock = new InvoiceMock();
			return mock.getNewInvoiceNumber();
		}
		CallableStatement callStmt = null;
		int res = 0;
		try {
			callStmt = getConnection().prepareCall("{ ? = call get_new_invoice_number() }");

			callStmt.registerOutParameter(1, Types.INTEGER);

			callStmt.execute();

			res = callStmt.getInt(1);

			callStmt.close();
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return res + 1;
	}

	/**
	 * Gets the last Invoice for a User
	 *
	 * @param user
	 * @return
	 */
	public Invoice getUserLastInvoice(User user) {
		if (testMode) {
			InvoiceMock mock = new InvoiceMock();
			return mock.getUserLastInvoice(user);
		}
		CallableStatement callStmt = null;
		ResultSet rSet = null;
		Invoice lastInvoice = null;
		try {
			callStmt = getConnection().prepareCall("{ ? = call get_user_last_invoice(?) }");

			callStmt.registerOutParameter(1, OracleTypes.CURSOR);

			callStmt.setString(2, user.getName());

			callStmt.execute();

			rSet = (ResultSet) callStmt.getObject(1);

			if (rSet != null) {
				while (rSet.next()) {
					int intID = rSet.getInt(1);
					String strUsername = rSet.getString(2);
					int intPreviousMonthPoints = rSet.getInt(3);
					int intPointsEarnedThisMonth = rSet.getInt(4);
					int intPointsUsed = rSet.getInt(5);
					int intPointsAvailableForNextMonth = rSet.getInt(6);
					double dblTotalPrice = rSet.getDouble(7);

					TripDB tripDb = new TripDB(false);
					List<Trip> lstTrips = tripDb.getUserTrips(strUsername);

					lastInvoice = new Invoice(intID, strUsername, intPreviousMonthPoints, intPointsEarnedThisMonth, intPointsUsed, intPointsAvailableForNextMonth, dblTotalPrice, lstTrips);
				}
			}
			callStmt.close();
		} catch (SQLException e) {
		} finally {
			closeAll(rSet, callStmt);
			closeAll();
		}
		return lastInvoice;
	}

}
