package lapr.project.data;

import java.util.List;
import lapr.project.model.Bicycle;

import oracle.jdbc.OracleTypes;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import lapr.project.data.mock.BicycleMock;
import lapr.project.model.Park;
import lapr.project.model.User;

/**
 *
 * @author roger40
 */
public class BicycleDB extends DataHandler {

	private final boolean testMode;

	/**
	 * Bicycle database constructor
	 *
	 * @param mock
	 */
	public BicycleDB(boolean mock) {
		this.testMode = mock;
	}

	public boolean isMock() {
		return testMode;
	}

	/**
	 * Gets all the Bicycles on the database
	 *
	 * @return list with all the Bicycles
	 */
	public List<Bicycle> getAllBicycles() {
		if (testMode) {
			BicycleMock mock = new BicycleMock();
			return mock.getAllBicycles();
		}
		List<Bicycle> allBicycles = new ArrayList<>();
		CallableStatement callStmtAux = null;
		ResultSet rSetAux = null;
		try {
			callStmtAux = getConnection().prepareCall("{ ? = call get_all_bikes }");
			callStmtAux.registerOutParameter(1, OracleTypes.CURSOR);
			callStmtAux.execute();
			rSetAux = (ResultSet) callStmtAux.getObject(1);
			ParkDB parkDb = new ParkDB(testMode);
			UserDB userDb = new UserDB(testMode);
			while (rSetAux.next()) {

				String bicycleId = rSetAux.getString(1);
				System.out.println("bike 1 -" + bicycleId);
				int weight = rSetAux.getInt(2);
				System.out.println("bike 1 -" + weight);
				String parkId = rSetAux.getString(3);
				Park park = parkDb.getPark(parkId);
				double aeroCoefficient = rSetAux.getDouble(4);
				double frontArea = rSetAux.getDouble(5);
				int size = rSetAux.getInt(6);
				Bicycle bicycle = new Bicycle(bicycleId, weight, park, aeroCoefficient, frontArea, size);

				String userEmail = rSetAux.getString(7);
				if (userEmail != null) {
					User user = userDb.getUser(userEmail);
					bicycle.setAssignedUser(user);
				}

				allBicycles.add(bicycle);
			}
		} catch (SQLException e) {
		} finally {
			closeAll(rSetAux, callStmtAux);
			closeAll();
		}
		return allBicycles;
	}

	/**
	 * Validates Bicycle globaly (Database)
	 *
	 * @param newBicycle bicycle to be validated
	 * @return the validation
	 */
	public boolean validateBicycle(Bicycle newBicycle) {
		if (this.testMode) {
			BicycleMock mock = new BicycleMock();
			return mock.validateBicycle(newBicycle);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ ? = call validate_bike(?) }");
			callStmt.registerOutParameter(1, OracleTypes.CHAR);
			callStmt.setString(2, newBicycle.getID());
			callStmt.execute();
			int result = callStmt.getInt(1);
			callStmt.close();
			return result == 1;	// returns 1 if Bicycle is valid
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	public boolean validateUpdateBicycle(Bicycle newBicycle) {
		if (this.testMode) {
			BicycleMock mock = new BicycleMock();
			return mock.validateUpdateBicycle(newBicycle);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ ? = call validate_update_bike(?) }");
			callStmt.registerOutParameter(1, OracleTypes.INTEGER);
			callStmt.setString(2, newBicycle.getID());
			callStmt.execute();
			int result = callStmt.getInt(1);
			callStmt.close();
			return result == 1;	// returns 1 if Bicycle is valid
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * Adds a Bicycle to the database
	 *
	 * @param newBicycle the bicycle to be added
	 * @return if the bicycle has been added or not
	 */
	public boolean addBicycle(Bicycle newBicycle) {
		if (this.testMode) {
			BicycleMock mock = new BicycleMock();
			return mock.addBicycle(newBicycle);
		}
		CallableStatement callStmt = null;
		try {
			openConnection();

			callStmt = getConnection().prepareCall("{ call add_bike(?,?,?,?,?,?) }");

			callStmt.setString(1, newBicycle.getID());
			callStmt.setInt(2, newBicycle.getWeight());
			callStmt.setString(3, newBicycle.getAssignedPark().getID());
			callStmt.setDouble(4, newBicycle.getAerodynamicCoefficient());
			callStmt.setDouble(5, newBicycle.getFrontalArea());
			callStmt.setInt(6, newBicycle.getWheelSize());

			callStmt.execute();

			return true;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * Gets a Bicycle from the database
	 *
	 * @param bicycle_id bicycle id
	 * @return retreived bicycle
	 */
	public Bicycle getBicycle(String bicycle_id) {
		if (this.testMode) {
			BicycleMock mock = new BicycleMock();
			return mock.getBicycle(bicycle_id);
		}
		CallableStatement callStmt = null;
		ResultSet rSet = null;
		try {
			callStmt = getConnection().prepareCall("{ ? = call get_bicycle(?) }");
			callStmt.registerOutParameter(1, OracleTypes.CURSOR);
			callStmt.setString(2, bicycle_id);
			callStmt.execute();
			rSet = (ResultSet) callStmt.getObject(1);
			ParkDB parkDb = new ParkDB(testMode);
			UserDB userDb = new UserDB(testMode);
			if (rSet != null) {
				rSet.next();

				String bicycleId = rSet.getString(1);
				int weight = rSet.getInt(2);
				String parkId = rSet.getString(3);
				Park park = parkDb.getPark(parkId);
				double aeroCoefficient = rSet.getDouble(4);
				double frontArea = rSet.getDouble(5);
				int size = rSet.getInt(6);
				String userEmail = rSet.getString(7);
				Bicycle bicycle = new Bicycle(bicycleId, weight, park, aeroCoefficient, frontArea, size);
				if (userEmail != null) {
					User user = userDb.getUser(userEmail);

					bicycle.setAssignedUser(user);
				}

				return bicycle;
			}
		} catch (SQLException e) {
		} finally {
			closeAll(rSet, callStmt);
			closeAll();
		}
		return null;
	}

	public boolean updateBicycle(Bicycle bike) {
		if (this.testMode) {
			BicycleMock mock = new BicycleMock();
			return mock.updateBicycle(bike);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ call update_bike(?,?,?,?,?,?) }");

			callStmt.setString(1, bike.getID());
			callStmt.setInt(2, bike.getWeight());
			callStmt.setString(3, bike.getAssignedPark().getID());
			callStmt.setDouble(4, bike.getAerodynamicCoefficient());
			callStmt.setDouble(5, bike.getFrontalArea());
			callStmt.setInt(6, bike.getWheelSize());

			callStmt.execute();

			return true;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * 'Removes' a Bicycle
	 *
	 * @param bicycle_id bicycle id
	 * @return true if bicycle is 'removed'
	 */
	public boolean removeBicycle(String bicycle_id) {
		if (this.testMode) {
			BicycleMock mock = new BicycleMock();
			return mock.removeBicycle(bicycle_id);
		}
		for (Bicycle bike : getAllBicycles()) {
			if (bicycle_id.equals(bike.getID()) && bike.getAssignedPark() != null && bike.getAssignedUser() == null) {
				CallableStatement callStmt = null;
				try {
					openConnection();
					callStmt = getConnection().prepareCall("{ call remove_bike(?) }");
					callStmt.setString(1, bicycle_id);
					callStmt.execute();
					return true;

				} catch (SQLException e) {
					return false;
				} finally {
					closeAll(callStmt);
					closeAll();
				}
			}
		}
		return false;

	}

	public List<Bicycle> getAvailableBicycles(String p1) {
		if (this.testMode) {
			BicycleMock mock = new BicycleMock();
			return mock.getAvailableBicycles(p1);

		}
		List<Bicycle> avaibleBicycles = new ArrayList<>();

		for (Bicycle b : getAllBicycles()) {
                    if(b.getAssignedPark()!=null && b.getAssignedUser() ==null){
                    if ( b.getAssignedPark().getID().equals(p1) ){
			avaibleBicycles.add(b);
		}
                }
                }
                return avaibleBicycles;

		
	}

	public boolean unlockBicycle(String id, User us1) {
		if (this.testMode) {
			BicycleMock mock = new BicycleMock();

			return mock.unlockBicycle(id, us1);

		}
		CallableStatement callStmt = null;
		try {
			
			openConnection();

			callStmt = getConnection().prepareCall("{ call update_user_bike(?, ?) }");
			callStmt.setString(1, us1.getEmail());
			callStmt.setString(2, id);

			callStmt.execute();

			closeAll();

			return true;

		} catch (SQLException e) {
			return false;

		} finally {
			closeAll(callStmt);
			closeAll();
		}
	}

	public boolean lockBicycle(Bicycle bike1, Park parkEnd) {
		if (this.testMode) {
			BicycleMock mock = new BicycleMock();

			return mock.lockBicycle(bike1, parkEnd);

		}
		CallableStatement callStmt = null;
		try {
			
			openConnection();

			callStmt = getConnection().prepareCall("{ call lock_bike(?,?) }");

			callStmt.setString(1, bike1.getID());

			callStmt.setString(2, parkEnd.getID());

			callStmt.execute();

			return true;

		} catch (SQLException e) {
			return false;

		} finally {
			closeAll(callStmt);
			closeAll();
		}
	}

}
