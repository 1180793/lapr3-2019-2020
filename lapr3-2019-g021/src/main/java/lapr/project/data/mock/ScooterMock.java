package lapr.project.data.mock;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Park;
import lapr.project.model.Scooter;
import lapr.project.model.User;

/**
 *
 * @author roger40
 */
public class ScooterMock {

        List<Scooter> scooterList = new ArrayList<Scooter>();
	ParkMock parkDb = new ParkMock();

        public ScooterMock() {
                Park park2 = new Park("2", 4.0, -4.0, 65, "parque teste2", 85, 75, 35, 15);
                User user = new User("roger2@gmail.com", "roger2", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);

                Scooter scooter = new Scooter("SCTR001", 20, 1, parkDb.getPark("2"), 0.90, 75, 1.10, 0.3, 250);
                scooter.setAssignedUser(user);

                scooterList.add(new Scooter("SCTR002", 20, 1, parkDb.getPark("1"), 1.0, 15, 1, 0.5, 245));
                scooterList.add(new Scooter("SCTR003", 20, 2, parkDb.getPark("1"), 1.5, 75, 0.90, 0.2, 235));
                scooterList.add(new Scooter("SCTR004", 20, 2, parkDb.getPark("1"), 2.0, 30, 1.2, 0.4, 230));
                scooterList.add(new Scooter("SCTR005", 20, 2, parkDb.getPark("1"), 2.2, 75, 1.30, 0.6, 225));
                scooterList.add(scooter);
                scooterList.add(new Scooter("SCTR007", 20, 2, null, 25.0, 95, 1.30, 0.6, 200));
		parkDb.getPark("1").setAvailableScooters(3);
        }

        /**
         * Gets Scooter by ID
         *
         * @param id
         * @return
         */
        public Scooter getScooter(String id) {
                for (Scooter scooter : scooterList) {
                        if (scooter.getID().equals(id)) {
                                return scooter;
                        }
                }
                return null;
        }
        
        /**
         * Validates a given Scooter
         *
         * @param newScooter
         * @return
         */
        public boolean validateScooter(Scooter newScooter) {
                return !scooterList.contains(newScooter);
        }
        
        public boolean validateUpdateScooter(Scooter newScooter) {
                return scooterList.contains(newScooter);
        }        
        
        /**
         * Adds a given Scooter
         *
         * @param newScooter
         * @return
         */
        public boolean addScooter(Scooter newScooter) {
            newScooter.getAssignedPark().setAvailableScooters(newScooter.getAssignedPark().getAvailableScooters()+1);
                return scooterList.add(newScooter);
        }
        
        public boolean updateScooter(Scooter scooter) {
            for (int i=0;i<scooterList.size();i++) {
                Scooter current=scooterList.get(i);
                if (current.getID().equals(scooter.getID())) {
                    //decrease available bikes in current assigned park
                    Park park=current.getAssignedPark();
                    if (park!=null)
                        park.setAvailableScooters(park.getAvailableScooters()-1);

                    current.setAssignedPark(null);
                    current.setAssignedUser(null);

                    //increase number of bikes in new park
                    park=scooter.getAssignedPark();
                    park.setAvailableScooters(park.getAvailableScooters()+1);

                    scooterList.set(i, scooter);

                    return true;
                }
            }        

            return false;

        }        
        /**
         * Returns a list with all the Scooters
         *
         * @return
         */
        public List<Scooter> getAllScooters() {
                return this.scooterList;
        }

        /**
         * 'Removes' a Scooters
         *
         * @param scooterId
         * @return
         */
        public boolean removeScooter(String scooterId) {
                for (Scooter scooter : scooterList) {
                        if (scooter.getID().equals(scooterId) && scooter.getAssignedPark()!=null && scooter.getAssignedUser() == null) {
                            scooter.getAssignedPark().setAvailableScooters(scooter.getAssignedPark().getAvailableScooters()+1); //atualiza lotaçao do parque
                                scooter.setAssignedPark(null);
                                scooter.setAssignedUser(null);
                                return true;
                        }
                }
                return false;
        }
        
         public boolean unlockScooter(String id,  User us1) {
        for (Scooter s : getAllScooters()) {
            if (s.getID().equals(id) ) {
                s.setAssignedUser(us1);
                return true;
            }
        }
        return false;
    }
        
        
        
        
//        public boolean unlockScooter(String id, User us1, String parkOriginId, String parkEndId) {
//        
//               if (getScooter(id).getAssignedPark().getID().equals(parkOriginId) && getSuggestedScooters(parkOriginId, parkEndId).contains(getScooter(id))) {
//                        getScooter(id).setAssignedUser(us1);
//                }
//                if (getScooter(id).getAssignedUser().equals(us1)) {
//                        return true;
//                }
//                return false;
//
//        }
        
//      public List<Scooter> getSuggestedScooters(int parkId, int parkEndId) {
//               if (parkEndId == 0) {
//                       double usableBat = 0;
//                       double batOfBestScooter=0;
//                      
//
//                       List<Scooter> availableScootersList = new ArrayList<>();
//                      for (Scooter scot : scooterList) {
//
//                               if (scot.getAssignedUser() == null && scot.getAssignedPark() !=null && scot.getAssignedPark().getID() == parkId) {
//                                 
//                                   usableBat = scot.getMaxBatteryCapacity()*scot.getActualBatteryCapacity();
//                                    System.out.println("Usable bat = "+ usableBat+ " id scooter ="+scot.getID());
//                                       if (usableBat >= batOfBestScooter) {
//                                               batOfBestScooter = usableBat;
//                                             
//                                       }
//                               }
//                        }
//                       System.out.println("BEST BAT LEVEL = "+ batOfBestScooter);
//                        for (Scooter scot : scooterList) {
//                  
//                                if (scot.getAssignedUser() == null && scot.getAssignedPark() !=null && scot.getAssignedPark().getID() == parkId) {
//
//                                       usableBat = scot.getMaxBatteryCapacity()*scot.getActualBatteryCapacity();
//                                       System.out.println("USABLE BAT LEVEL  = "+ usableBat + " SCOOTER ID ="+scot.getID());
//                                       if ( usableBat == batOfBestScooter){
//                                             System.out.println("adicionou a scooter á lista suggested"+ scot.getID());
//                                                availableScootersList.add(scot);
//                                        }
//                                }
//                        }
//
//                        return availableScootersList;
//                }
//                return null;  //TO DO
//        }
      
      public List<Scooter> getAvailableScooters(String parkId) {
        List<Scooter> availableScooters_list = new ArrayList<>();

        for (Scooter scooter : scooterList) {
            if (scooter.getAssignedUser() == null && scooter.getAssignedPark()!= null ){
                    if( scooter.getAssignedPark().getID().equals(parkId)) {

                availableScooters_list.add(scooter);
            }
            }
        }
        return availableScooters_list;
    }
      
       public boolean lockScooter(Scooter scot1, Park parkEnd) {
         double parkOriginElevation = scot1.getAssignedPark().getElevation();
         double parkEndElevation = parkEnd.getElevation();
         double relativeElevation = parkEndElevation - parkOriginElevation;
         
        if(relativeElevation > 25 && relativeElevation <= 50 ){                    //            DAR OS PONTOS AO USER SE A ALTITUDE ENTRE OS PARQUES FOR 25 OU MAIOR
            scot1.getAssignedUser().setPoints(scot1.getAssignedUser().getPoints()+5);
         }
        else if(relativeElevation >50){
              scot1.getAssignedUser().setPoints(scot1.getAssignedUser().getPoints()+15);
        }
      
         if(parkEnd.getAvailableScooterParkingSlots()>0){
             scot1.setAssignedUser(null);
             scot1.setAssignedPark(parkEnd);
             return true;
         }
         
         
         return false;
         
    }

}
