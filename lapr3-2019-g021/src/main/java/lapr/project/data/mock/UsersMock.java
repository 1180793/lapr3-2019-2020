package lapr.project.data.mock;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.User;
import lapr.project.utils.MD5Utils;

/**
 *
 * @author Francisco
 */
public class UsersMock {

	List<User> userList = new ArrayList<>();
	
	public UsersMock() {
		String pass = "12345678";

		User user1 = new User("email1@gmail.com", "Francisco Magalhaes", pass, "0000000000000001", 'm', 182, 90, 13);
		user1.setAdmin(false);

		User user2 = new User("email2@gmail.com", "Goncalo Corte Real", pass, "0000000000000002", 'm', 168, 58.9, 12);
		user2.setAdmin(false);

		User user3 = new User("email3@gmail.com", "Daniel Sousa", pass, "0000000000000003", 'm', 186, 82.9, 31);
		user3.setAdmin(false);

		User user4 = new User("email4@gmail.com", "Andre Moreira", pass, "0000000000000004", 'm', 173, 71.3, 25);
		user4.setAdmin(false);

		User user5 = new User("email5@gmail.com", "Joao Mata", pass, "0000000000000005", 'm', 162, 53, 24);
		user5.setAdmin(false);

		User user6 = new User("email6@gmail.com", "Rogerio Alves", pass, "0000000000000006", 'm', 180, 77.6, 18);
		user6.setAdmin(false);

		User user7 = new User("email7@gmail.com", "Ana Santos", pass, "0000000000000007", 'f', 160, 55, 41);
		user7.setAdmin(false);

		User user8 = new User("email8@gmail.com", "Hugo Sancho", pass, "0000000000000008", 'm', 172, 63, 5);
		user8.setAdmin(false);

		User user9 = new User("email9@gmail.com", "Rute Ribeiro", pass, "0000000000000009", 'f', 169, 62.4, 17);
		user9.setAdmin(false);

		userList.add(user1);
		userList.add(user2);
		userList.add(user3);
		userList.add(user4);
		userList.add(user5);
		userList.add(user6);
		userList.add(user7);
		userList.add(user8);
		userList.add(user9);

	}

	/**
	 * gets a user given an id
	 *
	 * @param userEmail
	 * @return
	 */
	public User getUserById(String userEmail) {
		for (User u : userList) {
			if (u.getEmail().equalsIgnoreCase(userEmail)) {
				return u;
			}
		}
		return null;
	}

	/**
	 * Validates a given user
	 *
	 * @param newUser
	 * @return
	 */
        
	public boolean validateUser(User newUser) {
		return !userList.contains(newUser);
	}

	/**
	 * Adds a given user
	 *
	 * @param newUser
	 * @return
	 */
	public boolean addUser(User newUser) {
		return userList.add(newUser);
	}

	/**
	 * Validates a login
	 *
	 * @param userInfo
	 * @param password
	 * @return
	 */
	public boolean validateLogin(String userInfo, String password) {
		for (User u : userList) {
			if (u.getEmail().equals(userInfo)) {
				return MD5Utils.encrypt(password).equals(u.getPassword());
			}
		}
		return false;
	}

	/**
	 * Returns a list with all the users
	 *
	 * @return
	 */
	public List<User> getAllUsers() {
		return this.userList;
	}

	public boolean updatePoints(String name, int points) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}
