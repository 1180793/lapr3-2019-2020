package lapr.project.data.mock;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Park;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ParkMock {

	List<Park> parkList = new ArrayList<>();

	public ParkMock() {
		Park park1 = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 220, 16);
		Park park2 = new Park("2", 3, 4, 4, "Varzim", 1, 2, 220, 16);
		Park park3 = new Park("3", 10, 10, 4, "Porto", 1, 2, 220, 16);
		Park park4 = new Park("4", 10, 30, 4, "Matosinhos", 1, 2, 220, 16);

		parkList.add(park1);
		parkList.add(park2);
		parkList.add(park3);
		parkList.add(park4);
		Park park5 = new Park("5", 100, 300, 4, "Matosinhos2", 0, 0, 220, 16); //PARK SEM CAPACIDADE PARA EFEITOS DE TESTE LOCK VEHICLES  
		parkList.add(park5);
		//Park park6 = new Park("6", 10, 30, 4, "Matosinhos", 1, 2, 220, 16); //PARK Muito lponge dos outros para testar o sugest scooter

		// Parks with real values for Graph testing
		parkList.add(new Park("Park1", 41.123456, 9.153152, 10, "Park 1 for Graph test", 10, 6, 5, 5));
		parkList.add(new Park("Park2", 41.015153, 9.315251, 10, "Park 2 for Graph test", 10, 5, 0, 5));
		parkList.add(new Park("Park3", 41.521512, 9.153120, 50, "Park 3 for Graph test", 10, 3, 0, 3));
		parkList.add(new Park("Park4", 41.222552, 9.153210, 50, "Park 4 for Graph test", 10, 6, 0, 0));
		parkList.add(new Park("Park5", 41.545313, 9.135121, 90, "Park 5 for Graph test", 10, 6, 0, 6));
		parkList.add(new Park("Park6", 41.151313, 9.000000, 10, "Park 6 for Graph test", 10, 2, 10, 1));
		parkList.add(new Park("Park7", 45.000000, 10.014122, 35, "Park 7 for Graph test", 10, 2, 10, 1));
		parkList.add(new Park("Park8", 46.000000, 10.014100, 60, "Park 8 for Graph test", 10, 2, 10, 1));
	}

	/**
	 * Gets Park by ID
	 *
	 * @param id
	 * @return
	 */
	public Park getPark(String id) {
		System.out.println("Searching parks by id.." + id);
		for (Park p : parkList) {
			System.out.println("id --" + p.getID());
			if (p.getID().equals(id)) {
				return p;
			}
		}
		return null;
	}

	/**
	 * Gets Park by Description
	 *
	 * @param desc description
	 * @return
	 */
	public Park getParkByDescription(String desc) {
		for (Park p : parkList) {
			if (p.getDescription().equals(desc)) {
				return p;
			}
		}
		return null;
	}

	public Park getPark(double latitude, double longitude) {
		System.out.println("searching in park by cords WILL SEARCH FOR--  " + latitude + "  --  " + longitude);
		for (Park p : parkList) {
			System.out.println("identifiing--" + p.getID() + "of cords  --  " + p.getLatitude() + " -----  " + p.getLongitude());
			if (p.getLatitude() == latitude && p.getLongitude() == longitude) {
				return p;
			}
		}
		return null;
	}

	/**
	 * Validates a given park
	 *
	 * @param newPark
	 * @return
	 */
	public boolean validatePark(Park newPark) {
		return !parkList.contains(newPark);
	}

	/**
	 * Adds a given Park
	 *
	 * @param newPark
	 * @return
	 */
	public boolean addPark(Park newPark) {
		return parkList.add(newPark);
	}

	/**
	 * Returns a list with all the parks
	 *
	 * @return
	 */
	public List<Park> getAllParks() {
		return this.parkList;
	}

	public boolean removePark(String id) {
		Park park = getPark(id);
		return parkList.remove(park);
	}

	public boolean updatePark(String id, double latitude, double longitude, int elevation, String description,
		int maxBikeCapacity, int maxScooterCapacity, int availableBikes, int availableScooters, double volt, double current, String idAntigo) {
		Park p = getPark(idAntigo);
		if (parkList.contains(p)) {
			if (parkList != null || parkList.remove(p)) {
				Park p1 = new Park(id, latitude, longitude, elevation, description, maxBikeCapacity, maxScooterCapacity, volt, current);
				if (parkList.add(p1)) {
					return true;
				}
			}
		}
		return false;
	}
}
