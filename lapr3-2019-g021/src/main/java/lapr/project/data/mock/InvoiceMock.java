package lapr.project.data.mock;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Invoice;
import lapr.project.model.Park;
import lapr.project.model.Trip;
import lapr.project.model.TripRegistry;
import lapr.project.model.User;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class InvoiceMock {

	/**
	 * List of invoices.
	 */
	private List<Invoice> invoiceList = new ArrayList<>();

	/**
	 * Class constructor.
	 *
	 * Used to instanciate necessary information.
	 */
	public InvoiceMock() {
		UsersMock userDb = new UsersMock();

		User userOne = userDb.getUserById("email1@gmail.com");
		userOne.setPoints(35);

		User userTwo = userDb.getUserById("email2@gmail.com");
		userTwo.setPoints(89);

		ParkMock parkDb = new ParkMock();
		Park park1 = parkDb.getPark("Park1");
		Park park2 = parkDb.getPark("Park2");
		Park park3 = parkDb.getPark("Park3");
		Park park4 = parkDb.getPark("Park4");

		Timestamp tripStartOne = new Timestamp(2019, 10, 3, 10, 0, 0, 0);
		Timestamp tripEndOne = new Timestamp(2019, 10, 3, 12, 0, 0, 0);

		Timestamp tripStartTwo = new Timestamp(2019, 10, 24, 10, 0, 0, 0);
		Timestamp tripEndTwo = new Timestamp(2019, 10, 24, 19, 0, 0, 0);

		Timestamp tripStartThree = new Timestamp(2019, 11, 3, 10, 0, 0, 0);
		Timestamp tripEndThree = new Timestamp(2019, 11, 3, 12, 0, 0, 0);

		Timestamp tripStartFour = new Timestamp(2019, 11, 24, 10, 0, 0, 0);
		Timestamp tripEndFour = new Timestamp(2019, 11, 24, 19, 0, 0, 0);

		Trip tripOne = new Trip("BIKE001", "b", tripStartOne, park1, userOne, park2, tripEndOne, 111, 2.23);

		Trip tripTwo = new Trip("BIKE001", "b", tripStartTwo, park3, userOne, park4, tripEndTwo, 112, 18.23);

		Trip tripThree = new Trip("BIKE002", "b", tripStartThree, park1, userOne, park3, tripEndThree, 113, 18.23);

		Trip tripFour = new Trip("BIKE002", "b", tripStartFour, park4, userOne, park1, tripEndFour, 114, 28.23);

		Trip tripFive = new Trip("BIKE003", "b", tripStartFour, park4, userTwo, park1, tripEndFour, 115, 48.23);

		List<Trip> lstTripsOne = new ArrayList<>();
		lstTripsOne.add(tripOne);
		lstTripsOne.add(tripTwo);
		Invoice invoiceOne = new Invoice(1, userOne.getName(), 55, 30, 80, 5, 12.46, lstTripsOne);

		List<Trip> lstTripsTwo = new ArrayList<>();
		lstTripsTwo.add(tripOne);
		lstTripsTwo.add(tripTwo);
		Invoice invoiceTwo = new Invoice(2, userOne.getName(), 5, 80, 80, 5, 38.46, lstTripsTwo);

		List<Trip> lstTripsThree = new ArrayList<>();
		lstTripsThree.add(tripOne);
		lstTripsThree.add(tripTwo);
		Invoice invoiceThree = new Invoice(3, userTwo.getName(), 65, 10, 40, 5, 41.23, lstTripsThree);

		invoiceList.add(invoiceOne);
		invoiceList.add(invoiceTwo);
		invoiceList.add(invoiceThree);
	}

	/**
	 * Gets all the invoices on the database
	 *
	 * @return list with all the users
	 */
	public List<Invoice> getAllInvoices() {
		return invoiceList;
	}

	/**
	 * simulates getting user's last invoice in sql.
	 *
	 * @param user to find last invoice.
	 * @return user's last invoice.
	 */
	public Invoice getUserLastInvoice(User user) {
		Invoice mostRecentInvoice = null;
		int aux = 0;
		for (Invoice i : invoiceList) {
			if (i.getUsername().equalsIgnoreCase(user.getName())) {
				if (i.getID() > aux) {
					aux = i.getID();
				}
			}
		}

		for (Invoice i : invoiceList) {
			if (i.getID() == aux) {
				mostRecentInvoice = i;
			}
		}

		return mostRecentInvoice;
	}

	/**
	 * Gets a sequential new number for Invoice ID
	 *
	 * @return
	 */
	public int getNewInvoiceNumber() {
		int biggestNumber = 0;

		for (Invoice i : invoiceList) {
			if (i.getID() > biggestNumber) {
				biggestNumber = i.getID();
			}
		}
		return biggestNumber + 1;
	}

	/**
	 * Issues a new User Invoice for specified month
	 *
	 * @param month
	 * @param user
	 * @return
	 */
	public Invoice issueInvoice(int month, User user) {
		Invoice oldInvoice = this.getUserLastInvoice(user);
		TripRegistry oRegistry = new TripRegistry(true);
		List<Trip> lstTrips = oRegistry.getUserMonthTrips(user.getName(), month);
		int intInvoiceID = this.getNewInvoiceNumber();
		String strUsername = user.getName();
		int intPreviousMonthPoints = 0;
		int intPointsEarnedThisMonth = user.getPoints();
		int intPointsUsed = 0;
		int intPointsAvailableNextMonth = user.getPoints();
		double totalPrice = 0;
		for (Trip trip : lstTrips) {
			totalPrice += trip.getPrice();
		}
		if (oldInvoice != null) {
			intPreviousMonthPoints = oldInvoice.getPointsAvailableForNextMonth();
			intPointsEarnedThisMonth = user.getPoints() - intPreviousMonthPoints;
		}

		while (totalPrice > 1.00 && intPointsAvailableNextMonth >= 10) {
			totalPrice -= 1;
			intPointsAvailableNextMonth -= 10;
			intPointsUsed += 10;
		}

		Invoice invoice = new Invoice(intInvoiceID, strUsername, intPreviousMonthPoints, intPointsEarnedThisMonth, intPointsUsed, intPointsAvailableNextMonth, totalPrice, lstTrips);
		UsersMock userDb = new UsersMock();
		userDb.updatePoints(strUsername, intPointsAvailableNextMonth);
		
		invoiceList.add(invoice);
		
		return invoice;
	}
}
