package lapr.project.data.mock;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Park;
import lapr.project.model.Path;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PathMock {

    List<Path> pathList = new ArrayList<>();
    ParkMock parkDb = new ParkMock();

    public PathMock() {
        Park park1 = parkDb.getPark("Park1");
        Park park2 = parkDb.getPark("Park2");
        Park park3 = parkDb.getPark("Park3");
        Park park4 = parkDb.getPark("Park4");

        Path path1 = new Path(park1.getLatitude(), park1.getLongitude(), park2.getLatitude(), park2.getLongitude(), 0.8, 220, 4.1);
        Path path2 = new Path(park2.getLatitude(), park2.getLongitude(), park3.getLatitude(), park3.getLongitude(), 0.8, 50, 3.2);
        Path path3 = new Path(park3.getLatitude(), park3.getLongitude(), park2.getLatitude(), park2.getLongitude(), 0.8, 230, 3.3);
        Path path4 = new Path(park3.getLatitude(), park3.getLongitude(), park4.getLatitude(), park4.getLongitude(), 0.8, 20, 5.4);
        Path path5 = new Path(park4.getLatitude(), park4.getLongitude(), park3.getLatitude(), park3.getLongitude(), 0.8, 200, 5.5);
        Path path6 = new Path(park1.getLatitude(), park1.getLongitude(), park3.getLatitude(), park3.getLongitude(), 0.8, 100, 4.6);
        Path path7 = new Path(park3.getLatitude(), park3.getLongitude(), park1.getLatitude(), park1.getLongitude(), 0.8, 80, 4.7);
        Path path8 = new Path(park2.getLatitude(), park2.getLongitude(), park1.getLatitude(), park1.getLongitude(), 0.8, 40, 4.8);

        pathList.add(path1);
        pathList.add(path2);
        pathList.add(path3);
        pathList.add(path4);
        pathList.add(path5);
        pathList.add(path6);
        pathList.add(path7);
        pathList.add(path8);
    }

    public Path getPath(double latitudeA, double longitudeA, double latitudeB, double longitudeB) {
        for (Path p : pathList) {
            if (p.getLatitudeA() == latitudeA && p.getLongitudeA() == longitudeA && p.getLatitudeB() == latitudeB && p.getLongitudeB() == longitudeB) {
                return p;
            }
        }
        return null;
    }

    /**
     * Validates a given Path
     *
     * @param newPath
     * @return
     */
    public boolean validatePath(Path newPath) {
        return !pathList.contains(newPath);
    }

    /**
     * Adds a given Path
     *
     * @param newPath
     * @return
     */
    public boolean addPath(Path newPath) {
        return pathList.add(newPath);
    }

    /**
     * Returns a list with all the Paths
     *
     * @return
     */
    public List<Path> getAllPaths() {
        return this.pathList;
    }
}
