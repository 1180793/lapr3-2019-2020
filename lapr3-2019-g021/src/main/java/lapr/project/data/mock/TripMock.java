/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.data.mock;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Park;
import lapr.project.model.Trip;
import lapr.project.model.User;

/**
 *
 * @author roger40
 */
public class TripMock {

	List<Trip> tripList;
	Trip trip;
	ParkMock pmcok;
	BicycleMock bMock;
	ScooterMock sMock;

	public TripMock() {
		tripList = new ArrayList<>();

		bMock = new BicycleMock();
		sMock = new ScooterMock();

		User userT = new User("rogerTT@gmail.com", "roger1", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);

		Trip t1 = new Trip(bMock.getBicycle("BIKE006").getID(), "b", bMock.getBicycle("BIKE006").getAssignedPark(), bMock.getBicycle("BIKE006").getAssignedUser(), 1);
		Trip t2 = new Trip(sMock.getScooter("SCTR001").getID(), "s", sMock.getScooter("SCTR001").getAssignedPark(), sMock.getScooter("SCTR001").getAssignedUser(), 2);
		//INSIRO NA LISTA DE TRIPS, AS trips que estao a acontecer no momento

		Trip t3 = new Trip(sMock.getScooter("SCTR001").getID(), "s", sMock.getScooter("SCTR001").getAssignedPark(), userT, 3);
		t3.uppdateTrip(sMock.getScooter("SCTR001").getAssignedPark());
		//esta trip esta concluida  neste caso o user estacioneu a scooter no mesmo parque de onde saiu, o userT como nao tem viagens ativas ( viagem em curso), tambem nao pode fazer update a nenhuma viagem (para efeitos de teste)
		Trip t4 = new Trip(sMock.getScooter("SCTR002").getID(), "s", sMock.getScooter("SCTR001").getAssignedPark(), userT, 4);
		t4.uppdateTrip(sMock.getScooter("SCTR002").getAssignedPark());

		tripList.add(t1);
		tripList.add(t2);
		tripList.add(t3);
		tripList.add(t4);

	}

	public boolean addTrip(String vehicleId, String type, Park parkOrigin, User us1, int tripNumber) {
		trip = new Trip(vehicleId, type, parkOrigin, us1, tripNumber);

		return tripList.add(trip);

	}

	public boolean UpdateTrip(Park parkDestiny, User us1) {
		//COMO 1 USER SO PODE TER UMA VEHICULO UNLOCKED DE MOMENTO, ENTAO BASTAME SABER ESSE USER PARA DESCOBRIR QUAL É A TRIP QUE PRETENDO ATUALIZAR
		System.out.println("update Trip Mock, searching trips size-" + getAllTrips().size() + "user--" + us1);
		for (Trip t : getAllTrips()) {
			System.out.println("...");
			if (t.getAssignedUser() != null && t.getAssignedUser().getEmail().equals(us1.getEmail()) && t.getParkDestiny() == null) {
				System.out.println("descobriu trip");
				t.uppdateTrip(parkDestiny);
				return true;

			}
			System.out.println("ainda n  descobriu trip");
		}
		System.out.println(" n  descobriu trip");
		return false;

	}

	public List<Trip> getAllTrips() {
		return tripList;
	}

	public Trip getTripsFromParkById(String id) {
		for (Trip t : tripList) {
			String id1 = t.getParkOrigin().getID();
			String id2 = t.getParkDestiny().getID();

			if (id.equalsIgnoreCase(id1) || id.equals(id2)) {
				return t;
			}
		}
		return null;
	}

	public boolean setInvoice(int tripn, int invoice) {
		for (Trip t : getAllTrips()) {
			if (t.getTripNumber() == tripn) {
				t.setInvoiceId(invoice);
				return true;
			}

		}
		return false;
	}
}
