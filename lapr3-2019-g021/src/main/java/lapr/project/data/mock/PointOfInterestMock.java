package lapr.project.data.mock;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.PointOfInterest;

/**
 *
 * @author Goncalo
 */
public class PointOfInterestMock {

        List<PointOfInterest> lstPOI;

        public PointOfInterestMock() {
                lstPOI = new ArrayList<>();
                PointOfInterest point1 = new PointOfInterest(41.152712, -8.609297, 13, "Point 1");
                PointOfInterest point2 = new PointOfInterest(42.110212, -8.234267, 12);
                PointOfInterest point3 = new PointOfInterest(42.947574, -8.743839, "Point 3");
                PointOfInterest point4 = new PointOfInterest(43.003479, -8.854783);
                lstPOI.add(point1);
                lstPOI.add(point2);
                lstPOI.add(point3);
                lstPOI.add(point4);
        }

        /**
         * Validates poi
         *
         * @param newPoi
         * @return
         */
        public boolean validatePOI(PointOfInterest newPoi) {
                for (PointOfInterest poi : lstPOI) {
                        if (newPoi.equals(poi)) {
                                return false;
                        }
                }
                return true;
        }

        /**
         * Adds a POI
         *
         * @param point
         */
        public boolean addPoint(PointOfInterest point) {
                return lstPOI.add(point);
        }

        /**
         * Returns a list of all POI registered
         *
         * @return
         */
        public List<PointOfInterest> getAllPOIs() {
                return this.lstPOI;
        }

        /**
         * Get a specific POI given its coordinates
         *
         * @param latitude
         * @param longitude
         * @return
         */
        public PointOfInterest getPOI(double latitude, double longitude) {
                for (PointOfInterest poi : this.lstPOI) {
                        PointOfInterest test = new PointOfInterest(latitude, longitude);
                        if (test.equals(poi)) {
                                return poi;
                        }
                }
                return null;
        }

}
