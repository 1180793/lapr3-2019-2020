package lapr.project.data.mock;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.User;

/**
 *
 * @author roger40
 */
public class BicycleMock {

    List<Bicycle> bikeList = new ArrayList<>();

    public BicycleMock() {
        Park park1 = new Park("1", 3.0, -3, 45, "parque teste1", 100, 100, 45, 25);
        Park park2 = new Park("2", 4.0, -4.0, 65, "parque teste2", 85, 75, 35, 15);
        User user = new User("roger1@gmail.com", "roger1", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);

        Bicycle bike1 = new Bicycle("BIKE006", 19, park1, 20, 1.5, 17);
        bike1.setAssignedUser(user);

        bikeList.add(new Bicycle("BIKE001", 15, park1, 20, 1.05, 15));
        bikeList.add(new Bicycle("BIKE002", 15, park2, 20, 1.5, 15));
        bikeList.add(new Bicycle("BIKE003", 16, park2, 20, 1.2, 13));
        bikeList.add(new Bicycle("BIKE004", 17, park1, 20, 1.3, 17));
        bikeList.add(new Bicycle("BIKE005", 18, park1, 20, 1.4, 15));
        bikeList.add(bike1);
          bikeList.add(new Bicycle("BIKE007", 15, null, 20, 1.5, 15));
    }

    /**
     * Gets Bicycle by ID
     *
     * @param id
     * @return
     */
    public Bicycle getBicycle(String id) {
        for (Bicycle bike : bikeList) {
            if (bike.getID().equals(id)) {
                return bike;
            }
        }
        return null;
    }

    /**
     * Validates a given Bicycle
     *
     * @param newBicycle
     * @return
     */
    public boolean validateBicycle(Bicycle newBicycle) {
        return !bikeList.contains(newBicycle);
    }
    
    /**
     * validates an update
     * @param newBicycle
     * @return true if the bike exists
     */
    public boolean validateUpdateBicycle(Bicycle newBicycle) {
        return bikeList.contains(newBicycle);
    }    

    /**
     * Adds a given Bicycle
     *
     * @param newBicycle
     * @return
     */
    public boolean addBicycle(Bicycle newBicycle) {
        newBicycle.getAssignedPark().setAvailableBikes(newBicycle.getAssignedPark().getAvailableBikes()+1);  
        return bikeList.add(newBicycle);
    }
    
    /**
     * replaces the existing bike in the list with the bicycle with the updated data
     * @param bicycle
     * @return true if bike found, false if not
     */
    public boolean updateBicycle(Bicycle bicycle) {
        for (int i=0;i<bikeList.size();i++) {
            Bicycle bike=bikeList.get(i);
            if (bike.getID().equals(bicycle.getID())) {
                //decrease available bikes in current assigned park
                Park park=bike.getAssignedPark();
                if (park!=null)
                    park.setAvailableBikes(park.getAvailableBikes()-1);
                
                bike.setAssignedPark(null);
                bike.setAssignedUser(null);
                
                //increase number of bikes in new park
                park=bicycle.getAssignedPark();
                park.setAvailableBikes(park.getAvailableBikes()+1);
                
                bikeList.set(i, bicycle);
                
                return true;
            }
        }        
        
        return false;
        
    }
    
    /**
     * Returns a list with all the Bicycles
     *
     * @return
     */
    public List<Bicycle> getAllBicycles() {
        return this.bikeList;
    }

    /**
     * 'Removes' a Bicycle
     *
     * @param bikeId
     * @return
     */
   public boolean removeBicycle(String bikeId) {
        for (Bicycle bike : bikeList) {
            if (bike.getID().equals(bikeId) && bike.getAssignedUser()==null && bike.getAssignedPark() !=null) {
             bike.getAssignedPark().setAvailableBikes(   bike.getAssignedPark().getAvailableBikes()-1); //atualiza lotaçao do parque
                bike.setAssignedPark(null);
                bike.setAssignedUser(null);
                return true;
            }
        }
        return false;
    }

    public boolean unlockBicycle(String id,  User us1) {
        for (Bicycle bikes : getAllBicycles()) {
            if (bikes.getID().equals(id) ) {
                bikes.setAssignedUser(us1);
                return true;
            }
        }
        return false;
    }

    public List<Bicycle> getAvailableBicycles(String parkId) {
        List<Bicycle> availableBicycles_list = new ArrayList<>();

        for (Bicycle bike : bikeList) {
            if (bike.getAssignedUser() == null && bike.getAssignedPark()!=null){
                    if( bike.getAssignedPark().getID().equals(parkId)) {

                availableBicycles_list.add(bike);

            }
            }
        }
        return availableBicycles_list;
    }
    
    
    
     public boolean lockBicycle(Bicycle bike1, Park parkEnd) {
         double parkOriginElevation = bike1.getAssignedPark().getElevation();
         double parkEndElevation = parkEnd.getElevation();
         double relativeElevation = parkEndElevation - parkOriginElevation;
         
       if(relativeElevation > 25 && relativeElevation <= 50 ){                    //            DAR OS PONTOS AO USER SE A ALTITUDE ENTRE OS PARQUES FOR 25 OU MAIOR
            bike1.getAssignedUser().setPoints(bike1.getAssignedUser().getPoints()+5);
         }
        else if(relativeElevation >50){
              bike1.getAssignedUser().setPoints(bike1.getAssignedUser().getPoints()+15);
        }
      
         if(parkEnd.getAvailableBicycleParkingSlots()>0){
             bike1.setAssignedUser(null);
             bike1.setAssignedPark(parkEnd);
             return true;
         }
         
         
         return false;
         
    }

}
