package lapr.project.data;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lapr.project.data.mock.TripMock;
import lapr.project.model.Park;
import lapr.project.model.Trip;
import lapr.project.model.User;
import lapr.project.utils.Utils;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author roger40
 */
public class TripDB extends DataHandler {

	private final boolean testMode;
	UserDB udb;

	/**
	 * Bicycle database constructor
	 *
	 * @param mock
	 */
	public TripDB(boolean mock) {
		this.testMode = mock;
		udb = new UserDB(mock);
	}

	public boolean isMock() {
		return testMode;
	}

	public List<Trip> getAllTrips() {
		if (testMode) {
			TripMock mock = new TripMock();
			return mock.getAllTrips();
		}
		List<Trip> allTrips = new ArrayList<>();

		CallableStatement callStmtAux = null;
		ResultSet rSetAux = null;
		try {

			callStmtAux = getConnection().prepareCall("{ ? = call get_all_trips }");
			callStmtAux.registerOutParameter(1, OracleTypes.CURSOR);
			callStmtAux.execute();
			rSetAux = (ResultSet) callStmtAux.getObject(1);
			ParkDB parkDb = new ParkDB(testMode);
			UserDB userDb = new UserDB(testMode);
			while (rSetAux.next()) {
				int tripnum = rSetAux.getInt(1);

				String vehicleId = rSetAux.getString(2);

				String typeVehicle = rSetAux.getString(3);

				Timestamp unlockedtm = rSetAux.getTimestamp(4);

				String parOriginkId = rSetAux.getString(5);

				String userEmail = rSetAux.getString(6);

				String parDestinyId = rSetAux.getString(7);

				Timestamp lockedtm = rSetAux.getTimestamp(8);

				double price = rSetAux.getDouble(9);

				int invoiceId = rSetAux.getInt(10);

				Park parkOrigin = parkDb.getPark(parOriginkId);

				Park parkDestiny = null;

				if (parDestinyId != null) {
					parkDestiny = parkDb.getPark(parDestinyId);
				}

				User user = userDb.getUser(userEmail);

				Trip trip = new Trip(vehicleId, typeVehicle, unlockedtm, parkOrigin, user, parkDestiny, lockedtm, tripnum, price, invoiceId);

				allTrips.add(trip);

			}
		} catch (SQLException e) {
		} finally {
			closeAll(rSetAux, callStmtAux);
			closeAll();
		}
		return allTrips;
	}

	public List<Trip> getUserTrips(String username) {
		List<Trip> allTrips = this.getAllTrips();
		List<Trip> userTrips = new ArrayList<>();
		for (Trip trip : allTrips) {
			if (trip.getAssignedUser().getName().equalsIgnoreCase(username)) {
				userTrips.add(trip);
			}
		}
		return userTrips;
	}

	public boolean addTrip(Trip trip) {
		if (testMode) {

			TripMock mock = new TripMock();
			return mock.addTrip(trip.getVehicleId(), trip.getVehicleType(), trip.getParkOrigin(), trip.getAssignedUser(), trip.getTripNumber());
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ call add_trip(?,?,?,?,?,?,?) }");

			callStmt.setString(1, trip.getVehicleId());
			callStmt.setString(2, trip.getVehicleType());
			callStmt.setString(3, trip.getParkOrigin().getID());
			callStmt.setString(4, trip.getAssignedUser().getEmail());
			callStmt.setInt(5, trip.getTripNumber());
			callStmt.setTimestamp(6, trip.getDateUnlocked());
			callStmt.setInt(7, trip.getInvoiceId());
			//REST OF THE FIELDS WILL BE NULL OR 0

			callStmt.execute();

			closeAll();
			return true;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	public boolean updateTrip(Park pdest, User us1) {

		if (testMode) {
			TripMock mock = new TripMock();
			return mock.UpdateTrip(pdest, us1);
		}

		for (Trip t : getAllTrips()) {

			if (t.getAssignedUser().getEmail().equals(us1.getEmail()) && t.getParkDestiny() == null) { // descobre qual trip atualizar, sabendo que um user so tem no maximo uma trip ativa

				t.uppdateTrip(pdest);
				int points = Utils.calculateTripPoints(t);
				int currentPoints = udb.getUser(us1.getEmail()).getPoints();
				udb.updatePoints(us1.getName(), currentPoints + points);

				CallableStatement callStmt = null;
				try {

					callStmt = getConnection().prepareCall("{ call update_trip(?,?,?,?) }");

					callStmt.setString(1, pdest.getID()); // USO ESTE VALOR PARA SER INSERIDO NA TRIPS CORRESPONDENTE

					callStmt.setDouble(2, t.getPrice());  // USO ESTE VALOR PARA SER INSERIDO NA TRIPS CORRESPONDENTE
					callStmt.setInt(3, t.getTripNumber());  // USO ESTE VALOR PARA Procurar a trip a atualizar
					callStmt.setTimestamp(4, t.getDateLocked());
					callStmt.execute();

					closeAll();
					return true;
				} catch (SQLException e) {
				} finally {
					closeAll(callStmt);
					closeAll();
				}

			}

		}

		return false;

	}

	public boolean setInvoice(int tripId, int invoiceID) {
		int numberUpdatedTrips = 0;

		if (testMode) {
			TripMock mock = new TripMock();
			return mock.setInvoice(tripId, invoiceID);
		}

		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ call set_invoice(?,?) }");

			callStmt.setInt(1, tripId); // USO ESTE VALOR PARA procurar a trip CORRESPONDENTE

			callStmt.setInt(2, invoiceID);  // USO ESTE VALOR PARA SER INSERIDO NA TRIPS CORRESPONDENTE

			callStmt.execute();

			closeAll();
			
			return true;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;

	}

	public long getUnlockedTime(String vehicleId) {
		Date date = new Date();  //get current date in timestamp format
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);

		for (Trip t : getAllTrips()) {
			if (t.getVehicleId().equalsIgnoreCase(vehicleId) && t.getDateLocked() == null) { //find the trip with same vehicle Id and at same time must be unlocked
				return ts.getTime() - t.getDateUnlocked().getTime();

			}
		}
		return 0;

	}

	public List<Trip> getUnpaidTrips(String username) {
		List<Trip> lstTrips = this.getAllTrips();
		List<Trip> lstUnpaidTrips = new ArrayList<>();
		for (Trip trip : lstTrips) {
			if (trip.getInvoiceId() == 0 && trip.getParkDestiny()!=null) {
				lstUnpaidTrips.add(trip);
			}
		}
		return lstUnpaidTrips;
	}

	public List<Trip> getAllTripsFromUser(String username) {
		List<Trip> lstTrips = this.getAllTrips();
		List<Trip> lstUserTrips = new ArrayList<>();
		for (Trip trip : lstTrips) {
			if (trip.getAssignedUser().getName().equalsIgnoreCase(username) && trip.getParkDestiny()!=null) {
				lstUserTrips.add(trip);
			}
		}
		return lstUserTrips;
	}
}
