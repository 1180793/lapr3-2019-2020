package lapr.project.data;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lapr.project.data.mock.ParkMock;
import lapr.project.model.Park;
import oracle.jdbc.OracleTypes;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ParkDB extends DataHandler {

	private final boolean testMode;

	/**
	 * Park database constructor
	 *
	 * @param mock
	 */
	public ParkDB(boolean mock) {
		this.testMode = mock;
	}

	public boolean isMock() {
		return testMode;
	}

	/**
	 * Gets all the Parks on the database
	 *
	 * @return list with all the Parks
	 */
	public List<Park> getAllParks() {
		if (testMode) {
			ParkMock mock = new ParkMock();
			return mock.getAllParks();
		}
		List<Park> allParks = new ArrayList<>();
		CallableStatement callStmtAux = null;
		ResultSet rSetAux = null;
		try {
			callStmtAux = getConnection().prepareCall("{ ? = call get_all_parks }");
			callStmtAux.registerOutParameter(1, OracleTypes.CURSOR);
			callStmtAux.execute();
			rSetAux = (ResultSet) callStmtAux.getObject(1);
			while (rSetAux.next()) {

				String id = rSetAux.getString(1);
				double latitude = rSetAux.getDouble(2);
				double longitude = rSetAux.getDouble(3);
				int elevation = rSetAux.getInt(4);
				String description = rSetAux.getString(5);
				int maxBikeCapacity = rSetAux.getInt(6);
				int maxScooterCapacity = rSetAux.getInt(7);
				double inputVoltage = rSetAux.getDouble(8);
				double inputCurrent = rSetAux.getDouble(9);
				int availableBikes = rSetAux.getInt(10);
				int availableScooters = rSetAux.getInt(11);

				Park park = new Park(id, latitude, longitude, elevation, description, maxBikeCapacity, maxScooterCapacity, inputVoltage, inputCurrent);
				park.setAvailableBikes(availableBikes);
				park.setAvailableScooters(availableScooters);

				allParks.add(park);
			}
		} catch (SQLException e) {
		} finally {
			closeAll(rSetAux, callStmtAux);
			closeAll();
		}
		return allParks;
	}

	/**
	 * Validates Park globaly (Database)
	 *
	 * @param newPark park to be validated
	 * @return the validation
	 */
	public boolean validatePark(Park newPark) {
		if (this.testMode) {
			ParkMock um = new ParkMock();
			return um.validatePark(newPark);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ ? = call validate_park(?) }");
			callStmt.registerOutParameter(1, OracleTypes.CHAR);
			callStmt.setString(2, newPark.getID());
			callStmt.execute();
			int result = callStmt.getInt(1);
			callStmt.close();
			return result == 1;	// returns 1 if Park is valid
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * Adds a Park to the database
	 *
	 * @param newPark the park to be added
	 * @return if the park has been added or not
	 */
	public boolean addPark(Park newPark) {
		if (this.testMode) {
			ParkMock pm = new ParkMock();
			return pm.addPark(newPark);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ call add_park(?,?,?,?,?,?,?,?,?) }");

			callStmt.setString(1, newPark.getID());
			callStmt.setDouble(2, newPark.getLatitude());
			callStmt.setDouble(3, newPark.getLongitude());
			callStmt.setInt(4, newPark.getElevation());
			callStmt.setString(5, newPark.getDescription());
			callStmt.setInt(6, newPark.getMaxBikeCapacity());
			callStmt.setInt(7, newPark.getMaxScooterCapacity());
			callStmt.setDouble(8, newPark.getInputVoltage());
			callStmt.setDouble(9, newPark.getInputCurrent());

			callStmt.execute();

			closeAll();
			return true;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	public Park getPark(String id) {
		if (testMode) {
			
			ParkMock mock = new ParkMock();
			return mock.getPark(id);
		}
		List<Park> parkList = this.getAllParks();
		for (Park park : parkList) {
			if (id.equalsIgnoreCase(park.getID())) {
				return park;
			}
		}
		return null;
	}

	public Park getPark(double latitude, double longitude) {
		if (testMode) {
			ParkMock mock = new ParkMock();
			return mock.getPark(latitude, longitude);
		}
		List<Park> parkList = this.getAllParks();
		for (Park park : parkList) {
			if (latitude == park.getLatitude() && longitude == park.getLongitude()) {
				return park;
			}
		}
		return null;
	}

	public Park getParkByDescription(String description) {

		if (testMode) {
			ParkMock mock = new ParkMock();
			return mock.getParkByDescription(description);
		}
		List<Park> parkList = this.getAllParks();
		for (Park park : parkList) {
			if (park.getDescription().equalsIgnoreCase(description)) {
				return park;
			}
		}
		return null;
	}

	//---------------------------------------------------------------------------
	public boolean removePark(String id) {
		if (testMode) {
			ParkMock pm = new ParkMock();
			return pm.removePark(id);
		}
		for (Park p1 : getAllParks()) {
			if (p1.getID().equals(id)) {
				CallableStatement callStmt = null;
				try {
					openConnection();

					callStmt = getConnection().prepareCall("{ remove_park(?) }");

					callStmt.setString(1, id);
					callStmt.execute();
					closeAll();

					return true;
				} catch (SQLException e) {
				} finally {
					closeAll(callStmt);
					closeAll();
				}
			}
			return false;
		}
		return false;
	}

	public boolean updatePark(Park oldPark, Park p) {
		if (p.getMaxBikeCapacity() >= oldPark.getAvailableBikes()
			&& p.getMaxScooterCapacity() >= oldPark.getAvailableScooters()) {
			if (p.getAvailableBikes() <= p.getMaxBikeCapacity()
				&& p.getAvailableScooters() <= p.getMaxScooterCapacity()) {
				return updatePark(p.getID(), p.getLatitude(), p.getLongitude(),
					p.getElevation(), p.getDescription(), p.getMaxBikeCapacity(),
					p.getMaxScooterCapacity(), p.getAvailableBikes(), p.getAvailableScooters(), p.getInputVoltage(), p.getInputCurrent(),
					oldPark.getID());
			}
		}
		return false;
	}

	public boolean updatePark(String id, double latitude, double longitude,
		int elevation, String description, int maxBikeCapacity, int maxScooterCapacity, int availableBikes, int availableScooters, double volt, double current, String idAntigo) {
		if (testMode) {
			ParkMock pm = new ParkMock();
			return pm.updatePark(id, latitude, longitude, elevation, description, maxBikeCapacity, maxScooterCapacity, availableBikes, availableScooters, volt, current, idAntigo);
		}
		CallableStatement callStmt = null;
		try {
			callStmt = getConnection().prepareCall("{ call update_park(?,?,?,?,?,?,?,?,?,?,?) }");

			callStmt.setString(1, id);
			callStmt.setDouble(2, latitude);
			callStmt.setDouble(3, longitude);
			callStmt.setDouble(4, elevation);
			callStmt.setString(5, description);
			callStmt.setInt(6, maxBikeCapacity);
			callStmt.setInt(7, maxScooterCapacity);
			callStmt.setInt(8, availableBikes);
			callStmt.setInt(9, availableScooters);
			callStmt.setDouble(10, volt);
			callStmt.setDouble(11, current);

			callStmt.execute();

			closeAll();
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}
}
