package lapr.project.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.controller.AddBicycleController;
import lapr.project.controller.AddPOIController;
import lapr.project.controller.AddParkController;
import lapr.project.controller.AddPathController;
import lapr.project.controller.AddScooterController;
import lapr.project.controller.UserRegistrationController;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DataLoader {

	/**
	 * Loads Users into the DB
	 *
	 * @param filePath
	 * @param fieldSeparator
	 * @return
	 */
	public int loadUsers(String filePath, String fieldSeparator) {
		int nUsers = 0;
		List<ArrayList<String>> list = getDataFromFile(filePath, fieldSeparator);
		int n = 0;
		for (ArrayList<String> data : list) {
			if (n > 0) {
				// name;email;height;weight;cycling average speed;visa;gender;password
				String strName = data.get(0);
				String strEmail = data.get(1);
				double dblHeight = Double.parseDouble(data.get(2));
				double dblWeight = Double.parseDouble(data.get(3));
				double dblAverageSpeed = Double.parseDouble(data.get(4));
				String strCreditCard = data.get(5);
				char charGender = data.get(6).charAt(0);
				String strPassword = data.get(7);

				UserRegistrationController oController = new UserRegistrationController(false);
				oController.newUser(strEmail, strName, strPassword, strCreditCard, charGender, dblHeight, dblWeight, dblAverageSpeed);
				if (oController.registerUser()) {
					nUsers++;
				}
			}
			n++;
		}
		return nUsers;
	}

	/**
	 * Loads Parks into the DB
	 *
	 * @param filePath
	 * @param fieldSeparator
	 * @return
	 */
	public int loadParks(String filePath, String fieldSeparator) {
		int nParks = 0;
		List<ArrayList<String>> list = getDataFromFile(filePath, fieldSeparator);
		int n = 0;
		for (ArrayList<String> data : list) {
			if (n > 0) {
				// park identification;latitude;longitude;elevation;park description;max number of bicycles;max number of escooters;park input voltage;park input current
				String intID = data.get(0);
				double dblLatitude = Double.parseDouble(data.get(1));
				double dblLongitude = Double.parseDouble(data.get(2));
				int intElevation = 0;
				String strElevation = data.get(3);
				if (strElevation != null && !strElevation.isEmpty()) {
					intElevation = Integer.parseInt(strElevation);
				}
				String strDescription = data.get(4);
				int intMaxBikeCapacity = Integer.parseInt(data.get(5));
				int intMaxScooterCapacity = Integer.parseInt(data.get(6));
				double dblInputVoltage = Double.parseDouble(data.get(7));
				double dblInputCurrent = Double.parseDouble(data.get(8));

				AddParkController oController = new AddParkController(false);
				oController.newPark(intID, dblLatitude, dblLongitude, intElevation, strDescription, intMaxBikeCapacity, intMaxScooterCapacity, dblInputVoltage, dblInputCurrent);
				if (oController.addPark()) {
					nParks++;
				}
			}
			n++;

		}
		return nParks;
	}

	/**
	 * Loads Bicycles into the DB
	 *
	 * @param filePath
	 * @param fieldSeparator
	 * @return
	 */
	public int loadBicycles(String filePath, String fieldSeparator) {
		int nBicycles = 0;
		List<ArrayList<String>> list = getDataFromFile(filePath, fieldSeparator);
		int n = 0;
		for (ArrayList<String> data : list) {
			if (n > 0) {
				// bicycle description;weight;park latitude;park longitude;aerodynamic coefficient;frontal area;wheel size
				String strID = data.get(0);
				int intWeight = Integer.parseInt(data.get(1));
				double dblLatitude = Double.parseDouble(data.get(2));
				double dblLongitude = Double.parseDouble(data.get(3));
				double dblAerodynamicCoefficient = Double.parseDouble(data.get(4));
				double dblFrontalArea = Double.parseDouble(data.get(5));
				int intWheelSize = Integer.parseInt(data.get(6));

				AddBicycleController oController = new AddBicycleController(false);

				oController.newBicycle(strID, intWeight, dblLatitude, dblLongitude, dblAerodynamicCoefficient, dblFrontalArea, intWheelSize);
				if (oController.addBicycle()) {
					nBicycles++;
				}
			}
			n++;
		}
		return nBicycles;
	}

	/**
	 * Loads Scooters into the DB
	 *
	 * @param filePath
	 * @param fieldSeparator
	 * @return
	 */
	public int loadScooters(String filePath, String fieldSeparator) {
		int nScooters = 0;
		List<ArrayList<String>> list = getDataFromFile(filePath, fieldSeparator);
		int n = 0;
		for (ArrayList<String> data : list) {
			if (n > 0) {
				// escooter description;weight;type;park latitude;park longitude;max battery capacity;actual battery capacity;aerodynamic coefficient;frontal area
				String strID = data.get(0);
				int intWeight = Integer.parseInt(data.get(1));
				String strType = data.get(2);
				int intType;
				if (strType.equalsIgnoreCase("city")) {
					intType = 1;
				} else if (strType.equalsIgnoreCase("off-road")) {
					intType = 2;
				} else {
					intType = -1;
				}
				double dblLatitude = Double.parseDouble(data.get(3));
				double dblLongitude = Double.parseDouble(data.get(4));
				double dblMaxBatteryCapacity = Double.parseDouble(data.get(5));
				int intActualBatteryCapacity = Integer.parseInt(data.get(6));

				double dblAerodynamicCoefficient = Double.parseDouble(data.get(7));
				double dblFrontalArea = Double.parseDouble(data.get(8));
				int dblMotor = Integer.parseInt(data.get(9));

				AddScooterController oController = new AddScooterController(false);
				oController.newScooter(strID, intWeight, intType, dblLatitude, dblLongitude, dblMaxBatteryCapacity, intActualBatteryCapacity, dblAerodynamicCoefficient, dblFrontalArea, dblMotor);
				if (oController.addScooter()) {
					nScooters++;
				}
			}
			n++;
		}
		return nScooters;
	}

	/**
	 * Loads POIs into the DB
	 *
	 * @param filePath
	 * @param fieldSeparator
	 * @return
	 */
	public int loadPOIs(String filePath, String fieldSeparator) {
		int nPOIs = 0;
		List<ArrayList<String>> list = getDataFromFile(filePath, fieldSeparator);
		int n = 0;
		for (ArrayList<String> data : list) {
			if (n > 0) {
				// latitude;longitude;elevation;poi description
				double dblLatitude = Double.parseDouble(data.get(0));
				double dblLongitude = Double.parseDouble(data.get(1));
				int intAltitude = 0;
				String strName = "";
				if (data.size() >= 3) {
					String strAltitude = data.get(2);
					if (strAltitude != null && !strAltitude.isEmpty()) {
						intAltitude = Integer.parseInt(strAltitude);
					}
					if (data.size() >= 4) {
						strName = data.get(3);
						if (strName == null) {
							strName = "";
						}
					}
				}
				AddPOIController oController = new AddPOIController(false);
				oController.newPOI(dblLatitude, dblLongitude, intAltitude, strName);
				if (oController.addPOI()) {
					nPOIs++;
				}
			}
			n++;
		}
		return nPOIs;
	}

	/**
	 * Loads Paths into the DB
	 *
	 * @param filePath
	 * @param fieldSeparator
	 * @return
	 */
	public int loadPaths(String filePath, String fieldSeparator) {
		int nPaths = 0;
		List<ArrayList<String>> list = getDataFromFile(filePath, fieldSeparator);
		int n = 0;
		for (ArrayList<String> data : list) {
			if (n > 0) {
				// latitudeA;longitudeA;latitudeB;longitudeB;kinetic coefficient;wind direction;wind speed
				double dblLatitudeA = Double.parseDouble(data.get(0));
				double dblLongitudeA = Double.parseDouble(data.get(1));
				double dblLatitudeB = Double.parseDouble(data.get(2));
				double dblLongitudeB = Double.parseDouble(data.get(3));
				double dblKineticCoefficient = 0;
				double dblWindDirection = 0;
				double dblWindSpeed = 0;

				if (data.size() >= 5) {
					String strKineticCoefficient = data.get(4);
					if (strKineticCoefficient != null && !strKineticCoefficient.isEmpty()) {
						dblKineticCoefficient = Double.parseDouble(strKineticCoefficient);
					}
				}

				if (data.size() >= 6) {
					String strWindDirection = data.get(5);
					if (strWindDirection != null && !strWindDirection.isEmpty()) {
						dblWindDirection = Double.parseDouble(strWindDirection);
					}
				}

				if (data.size() == 7) {
					dblWindSpeed = Double.parseDouble(data.get(6));
				}

				AddPathController oController = new AddPathController(false);
				oController.newPath(dblLatitudeA, dblLongitudeA, dblLatitudeB, dblLongitudeB, dblKineticCoefficient, dblWindDirection, dblWindSpeed);
				if (oController.addPath()) {
					nPaths++;
				}
			}
			n++;
		}
		return nPaths;
	}

	/**
	 * Gets data from a file and returns a List with String for each line
	 *
	 * @param filePath
	 * @param fieldSeparator
	 * @return
	 */
	private List<ArrayList<String>> getDataFromFile(String filePath, String fieldSeparator) {
		List<ArrayList<String>> data = new ArrayList<>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(filePath));
			String line = reader.readLine();
			while (line != null) {
				if (!line.startsWith("#") && !line.isEmpty()) {
					String[] lineData = line.split(fieldSeparator);
					ArrayList<String> lineElem = new ArrayList<>();
					for (String item : lineData) {
						lineElem.add(item.trim());
					}
					data.add(lineElem);
				}
				line = reader.readLine();
			}
		} catch (IOException e) {
			return null;
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ex) {
				}
			}
		}
		return data;
	}
}
