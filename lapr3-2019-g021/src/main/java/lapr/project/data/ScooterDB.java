package lapr.project.data;

import java.util.List;

import oracle.jdbc.OracleTypes;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import lapr.project.data.mock.ScooterMock;
import lapr.project.model.Park;
import lapr.project.model.Scooter;
import lapr.project.model.User;
import lapr.project.utils.Utils;

/**
 *
 * @author roger40
 */
public class ScooterDB extends DataHandler {

	public static final int CHARGING_POINT_MAX = 3000;	// 3kWh
	public static final double EFFICENCY_CHARGE = 0.90;

	private final boolean testMode;

	/**
	 * Scooter database contructor
	 *
	 * @param mock
	 */
	public ScooterDB(boolean mock) {
		this.testMode = mock;
	}

	public boolean isMock() {
		return testMode;
	}

	/**
	 * Gets all the Scooters on the database
	 *
	 * @return list with all the Scooters
	 */
	public List<Scooter> getAllScooters() {
		if (testMode) {
			ScooterMock mock = new ScooterMock();
			return mock.getAllScooters();
		}
		List<Scooter> allScooters = new ArrayList<>();
		CallableStatement callStmtAux = null;
		ResultSet rSetAux = null;
		try {
			openConnection();

			callStmtAux = getConnection().prepareCall("{ ? = call get_all_scooters }");
			callStmtAux.registerOutParameter(1, OracleTypes.CURSOR);
			callStmtAux.execute();
			rSetAux = (ResultSet) callStmtAux.getObject(1);
			ParkDB parkDb = new ParkDB(testMode);
			UserDB userDb = new UserDB(testMode);
			while (rSetAux.next()) {

				String scooterId = rSetAux.getString(1);
				int weight = rSetAux.getInt(2);
				int type = rSetAux.getInt(3);
				String parkId = rSetAux.getString(4);
				Park park = parkDb.getPark(parkId);
				double maxBatteryCap = rSetAux.getDouble(5);
				int actualBatteryCap = rSetAux.getInt(6);
				double aeroCoefficient = rSetAux.getDouble(7);
				double frontArea = rSetAux.getDouble(8);
				String userEmail = rSetAux.getString(9);
				int motor = rSetAux.getInt(10);
				User user = userDb.getUser(userEmail);

				Scooter scooter = new Scooter(scooterId, weight, type, park, maxBatteryCap, actualBatteryCap, aeroCoefficient, frontArea, motor);
				scooter.setAssignedUser(user);

				allScooters.add(scooter);
			}
		} catch (SQLException e) {
		} finally {
			closeAll(rSetAux, callStmtAux);
			closeAll();
		}
		return allScooters;
	}

	public List<Scooter> getAllScooters(Park oPark) {
		List<Scooter> lstAllScooters = getAllScooters();
		List<Scooter> lstScootersFromPark = new ArrayList<>();

		for (Scooter scooter : lstAllScooters) {
			if (scooter.getAssignedPark() != null && scooter.getAssignedUser() == null && scooter.getAssignedPark().equals(oPark)) {
				lstScootersFromPark.add(scooter);
			}
		}
		return lstScootersFromPark;
	}

	/**
	 * Validates Scooter globaly (Database)
	 *
	 * @param newScooter scooter to be validated
	 * @return the validation
	 */
	public boolean validateScooter(Scooter newScooter) {
		if (this.testMode) {
			ScooterMock mock = new ScooterMock();
			return mock.validateScooter(newScooter);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ ? = call validate_scooter(?) }");
			callStmt.registerOutParameter(1, OracleTypes.CHAR);
			callStmt.setString(2, newScooter.getID());
			callStmt.execute();
			int result = callStmt.getInt(1);
			callStmt.close();
			return result == 1;	// returns 1 if Scooter is valid
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	public boolean validateUpdateScooter(Scooter newScooter) {
		if (this.testMode) {
			ScooterMock mock = new ScooterMock();
			return mock.validateUpdateScooter(newScooter);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ ? = call validate_update_scooter(?) }");
			callStmt.registerOutParameter(1, OracleTypes.INTEGER);
			callStmt.setString(2, newScooter.getID());
			callStmt.execute();
			int result = callStmt.getInt(1);
			callStmt.close();
			return result == 1;	// returns 1 if Scooter is valid
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * Adds a Scooter to the database
	 *
	 * @param newScooter the scooter to be added
	 * @return if the scooter has been added or not
	 */
	public boolean addScooter(Scooter newScooter) {
		if (this.testMode) {
			ScooterMock mock = new ScooterMock();
			return mock.addScooter(newScooter);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ call add_scooter(?,?,?,?,?,?,?,?,?) }");

			callStmt.setString(1, newScooter.getID());
			callStmt.setInt(2, newScooter.getWeight());
			callStmt.setInt(3, newScooter.getType());
			callStmt.setString(4, newScooter.getAssignedPark().getID());
			callStmt.setDouble(5, newScooter.getMaxBatteryCapacity());
			callStmt.setInt(6, newScooter.getActualBatteryCapacity());
			callStmt.setDouble(7, newScooter.getAerodynamicCoefficient());
			callStmt.setDouble(8, newScooter.getFrontalArea());
			callStmt.setInt(9, newScooter.getMotor());

			callStmt.execute();

			closeAll();
			return true;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	public boolean updateScooter(Scooter scooter) {
		if (this.testMode) {
			ScooterMock mock = new ScooterMock();
			return mock.updateScooter(scooter);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ call update_scooter(?,?,?,?,?,?,?,?) }");

			callStmt.setString(1, scooter.getID());
			callStmt.setInt(2, scooter.getWeight());
			callStmt.setInt(3, scooter.getType());
			callStmt.setString(4, scooter.getAssignedPark().getID());
			callStmt.setDouble(5, scooter.getMaxBatteryCapacity());
			callStmt.setInt(6, scooter.getActualBatteryCapacity());
			callStmt.setDouble(7, scooter.getAerodynamicCoefficient());
			callStmt.setDouble(8, scooter.getFrontalArea());

			callStmt.execute();

			closeAll();
			return true;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * Gets a Scooter from the database
	 *
	 * @param scooter_id scooter id
	 * @return retreived scooter
	 */
	public Scooter getScooter(String scooter_id) {
		if (this.testMode) {
			ScooterMock mock = new ScooterMock();
			return mock.getScooter(scooter_id);
		}
		CallableStatement callStmt = null;
		ResultSet rSet = null;
		try {

			callStmt = getConnection().prepareCall("{ ? = call get_scooter(?) }");
			callStmt.registerOutParameter(1, OracleTypes.CURSOR);
			callStmt.setString(2, scooter_id);
			callStmt.execute();
			rSet = (ResultSet) callStmt.getObject(1);
			ParkDB parkDb = new ParkDB(testMode);
			UserDB userDb = new UserDB(testMode);
			if (rSet != null) {
				rSet.next();

				String scooterId = rSet.getString(1);
				int weight = rSet.getInt(2);
				int type = rSet.getInt(3);
				String parkId = rSet.getString(4);
				Park park = parkDb.getPark(parkId);
				double maxBatteryCap = rSet.getDouble(5);
				int actualBatteryCap = rSet.getInt(6);
				double aeroCoefficient = rSet.getDouble(7);
				double frontArea = rSet.getDouble(8);
				String userEmail = rSet.getString(9);
				int motor = rSet.getInt(10);
				User user = userDb.getUser(userEmail);

				Scooter scooter = new Scooter(scooterId, weight, type, park, maxBatteryCap, actualBatteryCap, aeroCoefficient, frontArea, motor);
				scooter.setAssignedUser(user);

				return scooter;
			}
		} catch (SQLException e) {
		} finally {
			closeAll(rSet, callStmt);
			closeAll();
		}
		return null;
	}

	/**
	 * 'Removes' a Scooter
	 *
	 * @param scooter_id scooter id
	 * @return true if scooter is 'removed'
	 */
	public boolean removeScooter(String scooter_id) {
		if (this.testMode) {
			ScooterMock mock = new ScooterMock();
			return mock.removeScooter(scooter_id);
		}
		for (Scooter s1 : getAllScooters()) {
			if (s1.getID().equals(scooter_id) && s1.getAssignedPark() != null && s1.getAssignedUser() == null) {
				CallableStatement callStmt = null;
				try {
					callStmt = getConnection().prepareCall("{ call remove_scooter(?) }");
					callStmt.setString(1, scooter_id);
					callStmt.execute();
					closeAll();
					return true;
				} catch (SQLException e) {
				} finally {
					closeAll(callStmt);
					closeAll();
				}
				return false;
			}
			return false;
		}
		return false;
	}

	public boolean unlockScooter(String id, User us1) {
		
		if (this.testMode) {
			ScooterMock mock = new ScooterMock();

			return mock.unlockScooter(id, us1);

		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ call update_user_scooter(?, ?) }");
			callStmt.setString(1, us1.getEmail());
			callStmt.setString(2, id);

			callStmt.execute();

			closeAll();

			return true;

		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}


	public boolean getChargingStatusReport(Park oPark, LinkedHashMap<String, Integer> report) {

		List<Scooter> lstScooters = getAllScooters(oPark);

		double parkVoltage = oPark.getInputVoltage();			// V
		double parkCurrent = oPark.getInputCurrent();			// I
		int nScooters = lstScooters.size();

		double parkPower = parkVoltage * parkCurrent;			// P = V * I

		if (nScooters == 0) {
			return false;
		}

		double chargingPointPower = Math.min(parkPower / nScooters, CHARGING_POINT_MAX);

		for (Scooter scooter : lstScooters) {
			int remainingTime;

			if (scooter.getActualBatteryCapacity() < 100) {
				double notCharged = ((100.0 - scooter.getActualBatteryCapacity()) / 100.0) * scooter.getMaxBatteryCapacity() * 1000;	// kWh * 1000 = Wh
				double hoursLeft = notCharged / (EFFICENCY_CHARGE * chargingPointPower);
				remainingTime = Utils.convertHoursToSeconds(hoursLeft);
			} else {
				remainingTime = 0;                          // The Scooter is already fully Charged
			}
			String scooterInfo = scooter.getID() + ";" + scooter.getActualBatteryCapacity();
			report.put(scooterInfo, remainingTime);
		}
		return true;
	}

	public List<Scooter> getAvailableScooters(Park p1) {
		if (this.testMode) {
			ScooterMock mock = new ScooterMock();
			return mock.getAvailableScooters(p1.getID());
		}

		List<Scooter> availableScooters = new ArrayList<>();
		for (Scooter s : getAllScooters()) {
			 if(s.getAssignedPark()!=null && s.getAssignedUser() ==null){
			if (s.getAssignedPark().getID().equals(p1) ) {
				availableScooters.add(s);
			}
		}
                }
		return availableScooters;

	}
        
        public List<Scooter> getAvailableScooters(String p1) {
		if (this.testMode) {
			ScooterMock mock = new ScooterMock();
			return mock.getAvailableScooters(p1);
		}

		List<Scooter> availableScooters = new ArrayList<>();
		for (Scooter s : getAllScooters()) {
                     if(s.getAssignedPark()!=null && s.getAssignedUser() ==null){
			if (s.getAssignedPark().getID().equals(p1) ) {
				availableScooters.add(s);
			}
		}
                }
		return availableScooters;

	}

	public boolean lockScooter(Scooter scot1, Park parkEnd) {
		if (this.testMode) {
			ScooterMock mock = new ScooterMock();

			return mock.lockScooter(scot1, parkEnd);

		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ call lock_scooter(?,?) }");

			callStmt.setString(1, scot1.getID());

			callStmt.setString(2, parkEnd.getID());

			callStmt.execute();

			closeAll();

			return true;

		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

}
