package lapr.project.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;
import java.util.List;
import java.util.Map;
import lapr.project.model.Invoice;
import lapr.project.model.Trip;
import lapr.project.model.User;
import lapr.project.model.UserRegistry;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DataOutput {

	/**
	 * Exports the Park Charging Report to a specified output file.
	 *
	 * @param result map contain the report data
	 * @param outputFileName outputfile name
	 * @return
	 */
	public int exportParkChargingReport(Map<String, Integer> result, String outputFileName) {
		Formatter outputFormatter;
		int nScooters = 0;
		try {
			outputFormatter = new Formatter(new File(outputFileName));
			outputFormatter.format("%s", "escooter description;"
				+ "actual battery capacity;"
				+ "time to finish charge in seconds");
			for (Map.Entry<String, Integer> entry : result.entrySet()) {
				outputFormatter.format("\n%s;%d", entry.getKey(), entry.getValue());
				if (entry.getValue() != 0) {
					nScooters++;
				}
			}
			outputFormatter.close();
		} catch (FileNotFoundException ex) {
			return -1;
		}
		return nScooters;
	}

	public double exportUserDebt(List<Trip> lstTrips, String outputFileName) {
		DecimalFormat df = new DecimalFormat("#.00");
		Formatter outputFormatter;
		double unpaidValue = 0;
		Collections.sort(lstTrips, new OrderTripsByUnlockTime());
		try {
			outputFormatter = new Formatter(new File(outputFileName));
			outputFormatter.format("%s", "vehicle description;"
				+ "vehicle unlock time;"
				+ "vehicle lock time;"
				+ "origin park latitude;"
				+ "origin park longitude;"
				+ "destination park latitude;"
				+ "destination park longitude;"
				+ "total time spent in seconds;"
				+ "charged value");
			for (Trip trip : lstTrips) {
				long tripDuration = trip.getDateLocked().getTime() - trip.getDateUnlocked().getTime();
				outputFormatter.format("\n%s", trip.getVehicleId()
					+ ";" + trip.getDateUnlocked().getTime()
					+ ";" + trip.getDateLocked().getTime()
					+ ";" + trip.getParkOrigin().getLatitude()
					+ ";" + trip.getParkOrigin().getLongitude()
					+ ";" + trip.getParkDestiny().getLatitude()
					+ ";" + trip.getParkDestiny().getLongitude()
					+ ";" + tripDuration
					+ ";" + df.format(trip.getPrice()));
				unpaidValue = unpaidValue + trip.getPrice();
			}
			outputFormatter.close();
		} catch (FileNotFoundException ex) {
			return -1;
		}
		return unpaidValue;
	}

	public int exportUserPoints(List<Trip> lstTrips, String username, String outputFileName) {
		Formatter outputFormatter;
		UserRegistry oRegistry = new UserRegistry(false);
		User user = oRegistry.getUserByUsername(username);
		Collections.sort(lstTrips, new OrderTripsByUnlockTime());
		try {
			outputFormatter = new Formatter(new File(outputFileName));
			outputFormatter.format("%s", "vehicle description;"
				+ "vehicle unlock time;"
				+ "vehicle lock time;"
				+ "origin park latitude;"
				+ "origin park longitude;"
				+ "origin park elevation;"
				+ "destination park latitude;"
				+ "destination park longitude;"
				+ "destination park elevation;"
				+ "elevation difference;"
				+ "points");
			for (Trip trip : lstTrips) {
				int elevationDifference = trip.getParkDestiny().getElevation() - trip.getParkOrigin().getElevation();
				outputFormatter.format("\n%s", trip.getVehicleId()
					+ ";" + trip.getDateUnlocked().getTime()
					+ ";" + trip.getDateLocked().getTime()
					+ ";" + trip.getParkOrigin().getLatitude()
					+ ";" + trip.getParkOrigin().getLongitude()
					+ ";" + trip.getParkOrigin().getElevation()
					+ ";" + trip.getParkDestiny().getLatitude()
					+ ";" + trip.getParkDestiny().getLongitude()
					+ ";" + trip.getParkDestiny().getElevation()
					+ ";" + elevationDifference
					+ ";" + Utils.calculateTripPoints(trip));
			}
			outputFormatter.close();
		} catch (FileNotFoundException ex) {
			return -1;
		}
		return user.getPoints();
	}

	public double exportInvoice(Invoice invoice, String outputFileName) {
		Formatter outputFormatter;
		try {
			outputFormatter = new Formatter(new File(outputFileName));
			outputFormatter.format("%s", invoice.toString());
			outputFormatter.close();
		} catch (FileNotFoundException ex) {
			return -1;
		}
		double price = invoice.getTotalPrice();
		BigDecimal bd = new BigDecimal(price).setScale(2, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	class OrderTripsByUnlockTime implements Comparator<Trip> {

		@Override
		public int compare(Trip o1, Trip o2) {
			if (o1.getDateUnlocked().before(o2.getDateUnlocked())) {
				return -1;
			} else if (o1.getDateUnlocked().after(o2.getDateUnlocked())) {
				return 1;
			} else {
				return 0;
			}
		}
	}
}
