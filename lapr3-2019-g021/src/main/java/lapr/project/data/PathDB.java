package lapr.project.data;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lapr.project.data.mock.PathMock;
import lapr.project.model.Path;
import oracle.jdbc.OracleTypes;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PathDB extends DataHandler {

	private final boolean testMode;

	/**
	 * Path Database Constructor
	 *
	 * @param mock
	 */
	public PathDB(boolean mock) {
		this.testMode = mock;
	}

	public boolean isMock() {
		return testMode;
	}

	/**
	 * Gets all the Paths on the database
	 *
	 * @return list with all the Paths
	 */
	public List<Path> getAllPaths() {
		if (testMode) {
			PathMock mock = new PathMock();
			return mock.getAllPaths();
		}
		List<Path> allPaths = new ArrayList<>();
		CallableStatement callStmtAux = null;
		ResultSet rSetAux = null;
		try {
			callStmtAux = getConnection().prepareCall("{ ? = call get_all_paths }");
			callStmtAux.registerOutParameter(1, OracleTypes.CURSOR);
			callStmtAux.execute();
			rSetAux = (ResultSet) callStmtAux.getObject(1);
			while (rSetAux.next()) {

				double latitudeA = rSetAux.getDouble(1);
				double longitudeA = rSetAux.getDouble(2);
				double latitudeB = rSetAux.getDouble(3);
				double longitudeB = rSetAux.getDouble(4);
				double kineticCoefficient = rSetAux.getDouble(5);
				double windDirection = rSetAux.getDouble(6);
				double windSpeed = rSetAux.getDouble(7);

				Path path = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);

				allPaths.add(path);
			}
		} catch (SQLException e) {
		} finally {
			closeAll(rSetAux, callStmtAux);
			closeAll();
		}
		return allPaths;
	}

	/**
	 * Validates Path globaly (Database)
	 *
	 * @param newPath path to be validated
	 * @return the validation
	 */
	public boolean validatePath(Path newPath) {
		if (this.testMode) {
			PathMock mock = new PathMock();
			return mock.validatePath(newPath);
		}
		CallableStatement callStmt = null;
		try {
			callStmt = getConnection().prepareCall("{ ? = call validate_path(?,?,?,?) }");
			callStmt.registerOutParameter(1, OracleTypes.CHAR);
			callStmt.setDouble(2, newPath.getLatitudeA());
			callStmt.setDouble(3, newPath.getLongitudeA());
			callStmt.setDouble(4, newPath.getLatitudeB());
			callStmt.setDouble(5, newPath.getLongitudeB());
			callStmt.execute();
			int result = callStmt.getInt(1);
			callStmt.close();
			return result == 1;	// returns 1 if Path is valid
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * Adds a Path to the Database
	 *
	 * @param newPath the path to be added
	 * @return if the path has been added or not
	 */
	public boolean addPath(Path newPath) {
		if (this.testMode) {
			PathMock pm = new PathMock();
			return pm.addPath(newPath);
		}
		CallableStatement callStmt = null;
		try {
			callStmt = getConnection().prepareCall("{ call add_path(?,?,?,?,?,?,?) }");

			callStmt.setDouble(1, newPath.getLatitudeA());
			callStmt.setDouble(2, newPath.getLongitudeA());
			callStmt.setDouble(3, newPath.getLatitudeB());
			callStmt.setDouble(4, newPath.getLongitudeB());
			callStmt.setDouble(5, newPath.getKineticCoefficient());
			callStmt.setDouble(6, newPath.getWindDirection());
			callStmt.setDouble(7, newPath.getWindSpeed());

			callStmt.execute();

			closeAll();
			return true;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	public Path getPath(double latitudeA, double longitudeA, double latitudeB, double longitudeB) {
		if (testMode) {
			PathMock mock = new PathMock();
			return mock.getPath(latitudeA, longitudeA, latitudeB, longitudeB);
		}
		List<Path> pathList = this.getAllPaths();
		for (Path path : pathList) {
			if (latitudeA == path.getLatitudeA() && longitudeA == path.getLongitudeA() && latitudeB == path.getLatitudeB() && longitudeB == path.getLongitudeB()) {
				return path;
			}
		}
		return null;
	}

}
