package lapr.project.data;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lapr.project.data.mock.PointOfInterestMock;
import lapr.project.model.PointOfInterest;
import oracle.jdbc.OracleTypes;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PointOfInterestDB extends DataHandler {

	private final boolean testMode;

	/**
	 * User database contructor
	 *
	 * @param mock
	 */
	public PointOfInterestDB(boolean mock) {
		this.testMode = mock;
	}

	public boolean isMock() {
		return this.testMode;
	}

	/**
	 * Add a POI to the database
	 *
	 * @param point point to be added
	 */
	public void addPOI(PointOfInterest point) {
		if (testMode) {
			PointOfInterestMock poiMock = new PointOfInterestMock();
			poiMock.addPoint(point);
		} else {
			CallableStatement callStmt = null;
			try {

				callStmt = getConnection().prepareCall("{ call add_poi(?,?,?,?) }");

				callStmt.setDouble(1, point.getLatitude());
				callStmt.setDouble(2, point.getLongitude());
				callStmt.setInt(3, point.getAltitude());
				callStmt.setString(4, point.getName());
				callStmt.execute();

				closeAll();
			} catch (SQLException e) {
			} finally {
				closeAll(callStmt);
				closeAll();
			}

		}
	}

	/**
	 * Validates if the POI is valid (Global Validation)
	 *
	 * @param newPoi name of the point
	 * @return if is valid or not
	 */
	public boolean validatePOI(PointOfInterest newPoi) {
		if (testMode) {
			PointOfInterestMock poiMock = new PointOfInterestMock();
			return poiMock.validatePOI(newPoi);
		}
		CallableStatement callStmt = null;
		try {

			callStmt = getConnection().prepareCall("{ ? = call validate_poi(?,?) }");

			callStmt.registerOutParameter(1, OracleTypes.INTEGER);
			callStmt.setDouble(2, newPoi.getLatitude());
			callStmt.setDouble(3, newPoi.getLongitude());
			callStmt.execute();

			return callStmt.getInt(1) == 0;
		} catch (SQLException e) {
		} finally {
			closeAll(callStmt);
			closeAll();
		}
		return false;
	}

	/**
	 * Gets a POI given its coordinates
	 *
	 * @param latitude
	 * @param longitude
	 * @return the point
	 */
	public PointOfInterest getPOI(double latitude, double longitude) {
		if (testMode) {
			PointOfInterestMock poiMock = new PointOfInterestMock();
			return poiMock.getPOI(latitude, longitude);

		}
		CallableStatement callStmt = null;
		ResultSet rSet = null;
		try {
			
			callStmt = getConnection().prepareCall("{ ? = call get_poi(?,?) }");

			callStmt.registerOutParameter(1, oracle.jdbc.OracleTypes.CURSOR);
			callStmt.setDouble(2, latitude);
			callStmt.setDouble(3, longitude);
			callStmt.execute();
			rSet = (ResultSet) callStmt.getObject(1);
			if (rSet.next()) {
				int altitude = rSet.getInt(3);
				String name = rSet.getString(4);
				return new PointOfInterest(latitude, longitude, altitude, name);
			}
			callStmt.close();
		} catch (SQLException e) {
		} finally {
			closeAll(rSet, callStmt);
			closeAll();
		}
		return null;
	}

	/**
	 * Gets all points from the database
	 *
	 * @return a list with all points
	 */
	public List<PointOfInterest> getAllPOIs() {
		List<PointOfInterest> lstPOIs = new ArrayList<>();
		if (testMode) {
			PointOfInterestMock poiMock = new PointOfInterestMock();
			return poiMock.getAllPOIs();
		}
		CallableStatement callStmt = null;
		ResultSet rSet = null;
		try {
			callStmt = getConnection().prepareCall("{ ? = call get_all_pois() }");
			callStmt.registerOutParameter(1, oracle.jdbc.OracleTypes.CURSOR);
			callStmt.execute();
			rSet = (ResultSet) callStmt.getObject(1);
			while (rSet.next()) {
				double latitude = rSet.getDouble(1);
				double longitude = rSet.getDouble(2);
				int altitude = rSet.getInt(3);
				String name = rSet.getString(4);
				PointOfInterest poi = new PointOfInterest(latitude, longitude, altitude, name);
				lstPOIs.add(poi);
			}
		} catch (SQLException e) {
		} finally {
			closeAll(rSet, callStmt);
			closeAll();
		}

		return lstPOIs;
	}
}
