package lapr.project.assessment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.controller.AvailableVehiclesController;
import lapr.project.controller.FindVehicleUnlockByUserController;
import lapr.project.controller.GetParkAvailableSlotsController;
import lapr.project.controller.GetParkChargingStatusReportController;
import lapr.project.controller.IssueInvoiceController;
import lapr.project.controller.LockBicycleController;
import lapr.project.controller.LockScooterController;
import lapr.project.controller.NearestParksController;
import lapr.project.controller.ProjectTotalAmountCaloriesController;
import lapr.project.controller.RemoveParkController;
import lapr.project.controller.ShortestRouteTwoParksController;
import lapr.project.controller.ShortestRouteTwoParksWithSequenceController;
import lapr.project.controller.SuggestScooterController;
import lapr.project.controller.TripController;
import lapr.project.controller.UnlockBicycleController;
import lapr.project.controller.UnlockScooterController;
import lapr.project.controller.UpdateBicycleController;
import lapr.project.controller.UpdateScooterController;
import lapr.project.controller.UserRegistrationController;
import lapr.project.controller.getNumberOfVehiclesAtParkController;
import lapr.project.data.DataLoader;
import lapr.project.data.DataOutput;
import lapr.project.data.ParkDB;
import lapr.project.data.PointOfInterestDB;
import lapr.project.model.Bicycle;
import lapr.project.model.Invoice;
import lapr.project.model.Park;
import lapr.project.model.Path;
import lapr.project.model.PointOfInterest;
import lapr.project.model.Scooter;
import lapr.project.model.Trip;
import lapr.project.model.TripRegistry;
import lapr.project.utils.Utils;

public class Facade implements Serviceable {

	DataLoader dataLoader = new DataLoader();

	/**
	 * Add Bicycles to the system.
	 *
	 * Basic: Add one bicycle to one park. Intermediate: Add several bicycles to one park. Advanced: Add several bicycles to several parks.
	 *
	 * @param inputFile Path to file with bicycles to add, according to input/bicycles.csv.
	 * @return Number of added bicycles.
	 */
	@Override
	public int addBicycles(String inputFile) {
		return dataLoader.loadBicycles(inputFile, ";");
	}

	/**
	 * Add Escooters to the system.
	 *
	 * Basic: Add one Escooter to one park. Intermediate: Add several Escooters to one park. Advanced: Add several Escooters to several parks.
	 *
	 * @param inputFile Path to file with Escooters to add, according to input/escooters.csv.
	 * @return Number of added escooters.
	 */
	@Override
	public int addEscooters(String inputFile) {
		return dataLoader.loadScooters(inputFile, ";");
	}

	/**
	 * Assigned to: Gonçalo Corte Real <1180793@isep.ipp.pt>
	 * Add Parks to the system.
	 *
	 * Basic: Add one Park. Intermediate: Add several Parks.
	 *
	 * @param inputFile Path to file that contains the parks, according to file input/parks.csv.
	 * @return The number of added parks.
	 */
	@Override
	public int addParks(String inputFile) {
		return dataLoader.loadParks(inputFile, ";");
	}

	/**
	 * Remove a park from the system. Assigned to: Daniel Sousa <1171073@isep.ipp.pt>
	 *
	 * @param parkIdentification Park to be removed from the system.
	 * @return The number of removed parks.
	 */
	@Override
	public int removePark(String parkIdentification) {
		RemoveParkController rpc = new RemoveParkController(false);
		return rpc.removePark(parkIdentification);
	}

	/**
	 * Assigned to: Gonçalo Corte Real <1180793@isep.ipp.pt>
	 * Add POIs to the system.
	 *
	 * Basic: Add one POI. Intermediate: Add several POIs.
	 *
	 * @param inputFile Path to file that contains the POIs, according to file input/pois.csv.
	 * @return The number of added POIs.
	 */
	@Override
	public int addPOIs(String inputFile) {
		return dataLoader.loadPOIs(inputFile, ";");
	}

	/**
	 * Assigned to: Gonçalo Corte Real <1180793@isep.ipp.pt>
	 * Add Users to the system.
	 *
	 * Basic: Add one User. Intermediate: Add several Users. Advanced: Add several Users transactionally.
	 *
	 * @param inputFile Path to file that contains the Users, according to file input/users.csv.
	 * @return The number of added users.
	 */
	@Override
	public int addUsers(String inputFile) {
		return dataLoader.loadUsers(inputFile, ";");
	}

	/**
	 * Assigned to: Gonçalo Corte Real <1180793@isep.ipp.pt>
	 * Add Paths to the system.
	 *
	 * @param inputFile Path to file that contains the Paths, according to file input/paths.csv.
	 * @return The number of added Paths.
	 */
	@Override
	public int addPaths(String inputFile) {
		return dataLoader.loadPaths(inputFile, ";");
	}

	/**
	 * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Get the list of bicycles parked at a given park.
	 *
	 * @param parkLatitudeInDegrees Park latitude in Decimal degrees.
	 * @param parkLongitudeInDegrees Park Longitude in Decimal degrees.
	 * @param outputFileName Path to file where output should be written, according to file output/bicycles.csv. Sort in ascending order by bike
	 * description.
	 * @return The number of bicycles at a given park.
	 */
	@Override
	public int getNumberOfBicyclesAtPark(double parkLatitudeInDegrees, double parkLongitudeInDegrees, String outputFileName) {
		getNumberOfVehiclesAtParkController gtvp = new getNumberOfVehiclesAtParkController(false);
		List<Bicycle> lb = gtvp.getNumberOfBicyclesAtPark(parkLatitudeInDegrees, parkLongitudeInDegrees);

		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(outputFileName));

			writer.write("bicycle description;wheel size");
			for (Bicycle b : lb) {
				writer.newLine();
				writer.write(b.getID() + ";" + b.getWheelSize());
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lb.size();

	}

	/**
	 * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Get the list of bicycles parked at a given park.
	 *
	 * @param parkIdentification The Park Identification.
	 * @param outputFileName Path to file where output should be written, according to file output/bicycles.csv. Sort in ascending order by bike
	 * description.
	 * @return The number of bicycles at a given park.
	 */
	@Override
	public int getNumberOfBicyclesAtPark(String parkIdentification, String outputFileName) {
		getNumberOfVehiclesAtParkController gtvp = new getNumberOfVehiclesAtParkController(false);
		List<Bicycle> lb = gtvp.getNumberOfBicyclesAtPark(parkIdentification);

		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(outputFileName));

			writer.write("bicycle description;wheel size");
			for (Bicycle b : lb) {
				writer.newLine();
				writer.write(b.getID() + ";" + b.getWheelSize());
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lb.size();
	}

	/**
	 * Assigned to: André Moreira <1141236@isep.ipp.pt>
	 * Get the list of escooters parked at a given park.
	 *
	 * @param parkLatitudeInDegrees Park latitude in Decimal degrees.
	 * @param parkLongitudeInDegrees Park Longitude in Decimal degrees.
	 * @param outputFileName Path to file where output should be written, according to file output/escooters.csv. Sort in ascending order by bike
	 * description.
	 * @return The number of escooters at a given park.
	 */
	@Override
	public int getNumberOfEscootersAtPark(double parkLatitudeInDegrees, double parkLongitudeInDegrees, String outputFileName) {
		getNumberOfVehiclesAtParkController gtvp = new getNumberOfVehiclesAtParkController(false);
		List<Scooter> lb = gtvp.getNumberOfScootersAtPark(parkLatitudeInDegrees, parkLongitudeInDegrees);

		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(outputFileName));

			writer.write("escooter description;type;actual battery capacity");
			for (Scooter b : lb) {
				writer.newLine();
				writer.write(b.getID() + ";" + b.getType() + ";" + b.getActualBatteryCapacity());
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lb.size();
	}

	/**
	 * Assigned to: André Moreira <1141236@isep.ipp.pt>
	 * Get the list of escooters parked at a given park.
	 *
	 * @param parkIdentification The Park Identification.
	 * @param outputFileName Path to file where output should be written, according to file output/escooters.csv. Sort in ascending order by bike
	 * description.
	 * @return The number of escooters at a given park.
	 */
	@Override
	public int getNumberOfEScootersAtPark(String parkIdentification, String outputFileName) {
		getNumberOfVehiclesAtParkController gtvp = new getNumberOfVehiclesAtParkController(false);
		List<Scooter> lb = gtvp.getNumberOfScootersAtPark(parkIdentification);

		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(outputFileName));

			writer.write("escooter description;type;actual battery capacity");
			for (Scooter b : lb) {
				writer.newLine();
				writer.write(b.getID() + ";" + b.getType() + ";" + b.getActualBatteryCapacity());
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lb.size();
	}

	/**
	 * Get a list of the nearest parks to the user.
	 *
	 * @param userLatitudeInDegrees User latitude in Decimal Degrees.
	 * @param userLongitudeInDegrees User longitude in Decimal Degrees.
	 * @param outputFileName Path to file where output should be written, according to file output/pois.csv. Sort by distance in ascending order.
	 */
	@Override
	public void getNearestParks(double userLatitudeInDegrees, double userLongitudeInDegrees, String outputFileName) {
		NearestParksController npc = new NearestParksController(false);
		npc.newPointOfInterest(userLatitudeInDegrees, userLongitudeInDegrees);
		Map<Park, Double> dp = npc.getNearestParksByCoordinates();

		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(outputFileName));

			writer.write("latitude;longitude;distance in meters");
			writer.newLine();
			for (Park p : dp.keySet()) {
				double dist = dp.get(p);
				writer.write(p.getLatitude() + ";" + p.getLongitude() + ";" + dist);
			}
			//writer.write("41.15227;-8.60929;465");
			// writer.newLine();
			// writer.write("41.145883,-8.610680,282");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 * Get a list of the nearest parks to the user.
	 *
	 * @param userLatitudeInDegrees User latitude in Decimal Degrees.
	 * @param userLongitudeInDegrees User longitude in Decimal Degrees.
	 * @param outputFileName Path to file where output should be written, according to file output/locations.csv. Sort by distance in ascending
	 * order.
	 * @param radius The radius in meters to which extent the user desires the results to be returned within.
	 */
	@Override
	public void getNearestParks(double userLatitudeInDegrees, double userLongitudeInDegrees, String outputFileName, int radius) {
		NearestParksController npc = new NearestParksController(false, radius);
		npc.newPointOfInterest(userLatitudeInDegrees, userLongitudeInDegrees);
		Map<Park, Double> dp = npc.getNearestParksByCoordinates();

		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(outputFileName));

			writer.write("latitude;longitude;distance in meters");
			writer.newLine();
			for (Park p : dp.keySet()) {
				double dist = dp.get(p);
				writer.write(p.getLatitude() + ";" + p.getLongitude() + ";" + dist);
			}
			//writer.write("41.15227;-8.60929;465");
			// writer.newLine();
			// writer.write("41.145883,-8.610680,282");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 *  * Assigned to: Joao Mata <1151352@isep.ipp.pt>
	 * Updates the information of a Bicycle.
	 *
	 * @param bicycleId The Bicycle Identification.
	 * @param weight The Weight of the Bicycle.
	 * @param latitude The Latitude of the Bicycle location.
	 * @param longitude The Longitude of the Bicycle location.
	 * @param aeroCoefficient The aeroCoefficient of the Bicycle
	 * @param frontArea The Frontal Area of the Bicycle.
	 * @param size The size of the Bicycle wheel
	 *
	 * @return if the Bicycle was updated.
	 */
	public boolean updateBicycle(String bicycleId, int weight, double latitude, double longitude, double aeroCoefficient, double frontArea, int size) {
		UpdateBicycleController controller = new UpdateBicycleController(false);
		return controller.updateBicycle(bicycleId, weight, latitude, longitude, aeroCoefficient, frontArea, size);

	}

	/**
	 *  * Assigned to: Joao Mata <1151352@isep.ipp.pt>
	 * Updates the information of a Scooter.
	 *
	 * @param bicycleId The Scooter Identification.
	 * @param weight The Weight of the Scooter.
	 * @param type The Type of the Scooter.
	 * @param latitude The Latitude of the Scooter location.
	 * @param longitude The Longitude of the Scooter location.
	 * @param maxBatteryCap The Maximum Battery Capacity of the Scooter.
	 * @param actualBatteryCap The Actual Battery Capacity of the Scooter.
	 * @param aeroCoefficient The AeroCoefficient of the Scooter
	 * @param frontArea The Frontal Area of the Scooter
	 *
	 * @return if the Scooter was updated.
	 */
	public boolean updateScooter(String scooterId, int weight, int type, double latitude, double longitude, double maxBatteryCap, int actualBatteryCap, double aeroCoefficient, double frontArea) {
		UpdateScooterController controller = new UpdateScooterController(false);
		return controller.updateScooter(scooterId, weight, type, latitude, longitude, maxBatteryCap, actualBatteryCap, aeroCoefficient, frontArea);
	}

	/**
	 *  * Assigned to: Joao Mata <1151352@isep.ipp.pt>
	 * Get the number of free bicycle parking places at a given park for the loaned bicycle.
	 *
	 * @param parkIdentification The Park Identification.
	 * @param username The username that has unlocked it.
	 *
	 * @return The number of free slots at a given park for the user's bicycle type.
	 */
	@Override
	public int getFreeBicycleSlotsAtPark(String parkIdentification, String username) {
		GetParkAvailableSlotsController gasc = new GetParkAvailableSlotsController(false);
		return gasc.getAvailableBicycleParkingSlots(parkIdentification);
	}

	/**
	 *  * Assigned to: Joao Mata <1151352@isep.ipp.pt>
	 * Get the number of free escooters parking places at a given park for the loaned scooter.
	 *
	 * @param parkIdentification The Park Identification.
	 * @param username The username that has unlocked it.
	 *
	 * @return The number of free slots at a given park for the user's Scooter.
	 *
	 */
	@Override
	public int getFreeEscooterSlotsAtPark(String parkIdentification, String username) {
		GetParkAvailableSlotsController gasc = new GetParkAvailableSlotsController(false);
		return gasc.getAvailableScooterParkingSlots(parkIdentification);
	}

	/**
	 *  * Assigned to: Joao Mata <1151352@isep.ipp.pt>
	 * Get the number of free bicycle parking places at a given park for the loaned bicycle.
	 *
	 * @param latitude Latitude of Park.
	 * @param longitude Longitude of Park.
	 * @param username The username that has unlocked it.
	 *
	 * @return The number of free slots at a given park for the user's bicycle type.
	 */
	public int getFreeBicycleSlotsAtPark(double latitude, double longitude, String username) {
		GetParkAvailableSlotsController gasc = new GetParkAvailableSlotsController(false);
		return gasc.getAvailableBicycleParkingSlots(latitude, longitude);
	}

	/**
	 *  * Assigned to: Joao Mata <1151352@isep.ipp.pt>
	 * Get the number of free escooters parking places at a given park for the loaned scooter.
	 *
	 * @param latitute Latitude of Park.
	 * @param longitude Longitude of Park.
	 * @param username The username that has unlocked it.
	 *
	 * @return The number of free slots at a given park for the user's vehicle.
	 *
	 */
	public int getFreeEscooterSlotsAtPark(double latitude, double longitude, String username) {
		GetParkAvailableSlotsController gasc = new GetParkAvailableSlotsController(false);
		return gasc.getAvailableScooterParkingSlots(latitude, longitude);
	}

	/**
	 *  * Assigned to: Joao Mata <1151352@isep.ipp.pt>
	 * Get the number of free parking places at a given park for the user's loaned vehicle.
	 *
	 * @param username The username that has unlocked it.
	 * @param parkIdentification The Park Identification.
	 *
	 * @return The number of free slots at a given park for the user's vehicle.
	 *
	 */
	@Override
	public int getFreeSlotsAtParkForMyLoanedVehicle(String username, String parkIdentification) {
		GetParkAvailableSlotsController gasc = new GetParkAvailableSlotsController(false);
		return gasc.getAvailableLoanedVehicleParkingSlots(username, parkIdentification);
	}

	/**
	 *  * Assigned to: Joao Mata <1151352@isep.ipp.pt>
	 * Calculates the Total Amount of Calories Burnt.
	 *
	 * @param bicycleId The Bicycle Identification.
	 * @param latitudeOrigin The Latitude of the Origin Park.
	 * @param longitudeOrigin The Longitude of the Origin Park.
	 * @param latitudeDest The Latitude of the Destination Park.
	 * @param longitudeDest The Longitude of the Destination Park.
	 * @param user The User of the vehicle.
	 *
	 * @return the calories burnt riding the vehicle.
	 */
	public double calculateTotalAmountCalories(String bicycleId, double latitudeOrigin, double longitudeOrigin, double latitudeDest, double longitudeDest, String user) {
		ProjectTotalAmountCaloriesController controller = new ProjectTotalAmountCaloriesController(false);
		return controller.calculateTotalAmountCalories(bicycleId, latitudeOrigin, longitudeOrigin, latitudeDest, longitudeDest, user);

	}

	/**
	 * Assigned to: Gonçalo Corte Real <1180793@isep.ipp.pt>
	 * Get the linear distance from one location to another.
	 *
	 * @param originLatitudeInDegrees Origin latitude in Decimal Degrees.
	 * @param originLongitudeInDegrees Origin longitude in Decimal Degrees.
	 * @param destinyLatitudeInDegrees Destiny latitude in Decimal Degrees.
	 * @param destinyLongitudeInDegrees Destiny longitude in Decimal Degrees.
	 * @return Returns the distance in meters from one location to another.
	 */
	@Override
	public int linearDistanceTo(double originLatitudeInDegrees, double originLongitudeInDegrees, double destinyLatitudeInDegrees, double destinyLongitudeInDegrees) {
		double distance = Utils.calculateDistance(new PointOfInterest(originLatitudeInDegrees, originLongitudeInDegrees), new PointOfInterest(destinyLatitudeInDegrees, destinyLongitudeInDegrees));
		int intDistance = (int) distance;
		double diff = distance - intDistance;
		if (diff >= 0.4) {
			return intDistance + 1;
		} else {
			return intDistance;
		}
	}

	/**
	 * Get the shortest path distance from one location to another.
	 *
	 * @param originLatitudeInDegrees Origin latitude in Decimal Degrees.
	 * @param originLongitudeInDegrees Origin longitude in Decimal Degrees.
	 * @param destinyLatitudeInDegrees Destiny latitude in Decimal Degrees.
	 * @param destinyLongitudeInDegrees Destiny longitude in Decimal Degrees.
	 * @return Returns the distance in meters from one location to another.
	 */
	@Override
	public int pathDistanceTo(double originLatitudeInDegrees, double originLongitudeInDegrees, double destinyLatitudeInDegrees, double destinyLongitudeInDegrees) {
		ShortestRouteTwoParksController controller = new ShortestRouteTwoParksController(false);
		controller.insertCoordinates(originLatitudeInDegrees, originLongitudeInDegrees, destinyLatitudeInDegrees, destinyLongitudeInDegrees);
		List<PointOfInterest> route = controller.getShortestRoute();
		return (int) (controller.getMinDistance() * 1000);
	}

	/**
	 *  * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Unlocks a specific bicycle.
	 *
	 * @param username User that requested the unlock.
	 * @param bicycleDescription Bicycle description to unlock.
	 * @return The time in milliseconds at which it was unlocked.
	 */
	@Override
	public long unlockBicycle(String username, String bicycleDescription) {
		Date date = new Date();

		long timeMilli = date.getTime();
		UnlockBicycleController ubc = new UnlockBicycleController(username, false);
		if (ubc.unlockBicycleByUser(bicycleDescription)) {
			Date date2 = new Date();

			long timeMilli2 = date2.getTime();
			return timeMilli2 - timeMilli;
		}
		return 0;
	}

	/**
	 *  * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Unlocks a specific escooter.
	 *
	 * @param username User that requested the unlock.
	 * @param escooterDescription Escooter description to unlock.
	 * @return The time in milliseconds at which it was unlocked.
	 */
	@Override
	public long unlockEscooter(String username, String escooterDescription) {
		Date date = new Date();

		long timeMilli = date.getTime();
		UnlockScooterController usc = new UnlockScooterController(username, false);
		if (usc.unlockSpecificScooter(escooterDescription)) {
			Date date2 = new Date();

			long timeMilli2 = date2.getTime();
			return timeMilli2 - timeMilli;
		}
		return 0;
	}

	/**
	 *  * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Lock a specific bicycle at a park.
	 *
	 * Basic: Lock a specific bicycle at a park. Intermediate: Create an invoice line for the loaned vehicle. Advanced: Add points to user.
	 *
	 * @param bicycleDescription Bicycle to lock.
	 * @param parkLatitudeInDegrees Park latitude in Decimal degrees.
	 * @param parkLongitudeInDegrees Park Longitude in Decimal degrees.
	 * @param username User that requested the unlock.
	 * @return The time in milliseconds at which the bicycle was locked.
	 */
	@Override
	public long lockBicycle(String bicycleDescription, double parkLatitudeInDegrees, double parkLongitudeInDegrees, String username) {
		Date date = new Date();

		long timeMilli = date.getTime();
		LockBicycleController lbc = new LockBicycleController(username, parkLatitudeInDegrees, parkLongitudeInDegrees, false);
		if (lbc.lockBicycleGivenParkCords()) {
			Date date2 = new Date();

			long timeMilli2 = date2.getTime();
			return timeMilli2 - timeMilli;
		}
		return 0;
	}

	/**
	 *  * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Lock a specific bicycle at a park.
	 *
	 * Basic: Lock a specific bicycle at a park. Intermediate: Create an invoice line for the loaned vehicle. Advanced: Add points to user.
	 *
	 * @param bicycleDescription Bicycle to lock.
	 * @param parkIdentification The Park Identification.
	 * @param username User that requested the unlock.
	 * @return The time in milliseconds at which the bicycle was locked.
	 */
	@Override
	public long lockBicycle(String bicycleDescription, String parkIdentification, String username) {
		Date date = new Date();

		long timeMilli = date.getTime();

		LockBicycleController lbc = new LockBicycleController(username, parkIdentification, false);
		if (lbc.lockBicycleGivenParkId()) {
			Date date2 = new Date();

			long timeMilli2 = date2.getTime();
			return timeMilli2 - timeMilli;

		}
		return 0;
	}

	/**
	 *  * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Lock a specific escooter at a park.
	 *
	 * Basic: Lock a specific escooter at a park. Intermediate: Create an invoice line for the loaned vehicle. Advanced: Add points to user.
	 *
	 * @param escooterDescription Escooter to lock.
	 * @param parkLatitudeInDegrees Park latitude in Decimal degrees.
	 * @param parkLongitudeInDegrees Park Longitude in Decimal degrees.
	 * @param username User that requested the unlock.
	 * @return The time in milliseconds at which it was locked.
	 */
	@Override
	public long lockEscooter(String escooterDescription, double parkLatitudeInDegrees, double parkLongitudeInDegrees, String username) {

		Date date = new Date();

		long timeMilli = date.getTime();

		LockScooterController lsc = new LockScooterController(username, parkLatitudeInDegrees, parkLongitudeInDegrees, false);
		if (lsc.lockScooterGivenParkCords()) {
			Date date2 = new Date();

			long timeMilli2 = date2.getTime();
			return timeMilli2 - timeMilli;
		}
		return 0;
	}

	/**
	 * Lock a specific escooter at a park. * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Basic: Lock a specific escooter at a park. Intermediate: Create an invoice line for the loaned vehicle. Advanced: Add points to user.
	 *
	 * @param escooterDescription Escooter to lock.
	 * @param parkIdentification The Park Identification.
	 * @param username User that requested the unlock.
	 * @return The time in milliseconds at which it was locked.
	 */
	@Override
	public long lockEscooter(String escooterDescription, String parkIdentification, String username) {
		LockScooterController lsc = new LockScooterController(username, parkIdentification, false);
		if (lsc.lockScooterGivenParkId()) {
			Date date = new Date();
			//This method returns the time in millis
			long timeMilli = date.getTime();
			return timeMilli;
		}
		return 0;
	}

	/**
	 * Assigned to: Gonçalo Corte Real <1180793@isep.ipp.pt>
	 * Register a user on the system.
	 *
	 * @param username User's username.
	 * @param email User's email.
	 * @param password User's desired password.
	 * @param visaCardNumber User's Visa Card number.
	 * @param height User's height in cm.
	 * @param weight User's weight in kg.
	 * @param averageCyclingSpeed User's average speed in m/s with two decimal places e.g 4.17.
	 * @param gender User's gender in text.
	 * @return Return 1 if a user is successfully registered.
	 */
	@Override
	public int registerUser(String username, String email, String password, String visaCardNumber, int height, int weight, BigDecimal averageCyclingSpeed, String gender) {
		UserRegistrationController oController = new UserRegistrationController(false);
		oController.newUser(email, username, password, visaCardNumber, gender.charAt(0), height, weight, averageCyclingSpeed.doubleValue());
		if (oController.registerUser()) {
			return 1;
		}
		return 0;
	}

	/**
	 *  * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Unlocks any escooter at one park. It should unlock the one with higher battery capacity.
	 *
	 * @param parkIdentification Park Identification where to unlock escooter.
	 * @param username User that requested the unlock.
	 * @param outputFileName Write the unlocked vehicle information to a file, according to file output/escooters.csv.
	 * @return The time in milliseconds at which it was unlocked.
	 */
	@Override
	public long unlockAnyEscooterAtPark(String parkIdentification, String username, String outputFileName) {
		FindVehicleUnlockByUserController ubc = new FindVehicleUnlockByUserController(false);
		Date date = new Date();

		long timeMilli = date.getTime();
		UnlockScooterController usc = new UnlockScooterController(username, false);
		if (usc.unlockAnyScooterAtPark(parkIdentification)) {
			Date date2 = new Date();

			long timeMilli2 = date2.getTime();
			long timetoUnlock = timeMilli2 - timeMilli;
			//  escooter description;type;actual battery capacity

			Scooter s1 = ubc.findUnlockedScooterByUser(username);

			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter(outputFileName));

				writer.write("escooter description;type;actual battery capacity");
				writer.newLine();
				writer.write(s1.getID() + ";" + s1.getType() + ";" + s1.getActualBatteryCapacity());
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return timetoUnlock;

		}
		return 0;
	}

	/**
	 *  * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Unlocks any escooter at one park that allows travelling to the destination.
	 *
	 * @param parkIdentification Park Identification where to unlock escooter.
	 * @param username User that requested the unlock.
	 * @param destinyLatitudeInDegrees Destiny latitude in Decimal Degrees.
	 * @param destinyLongitudeInDegrees Destiny longitude in Decimal Degrees.
	 * @param outputFileName Write the unlocked vehicle information to a file, according to file output/escooters.csv.
	 * @return The time in milliseconds at which it was unlocked.
	 */
	@Override
	public long unlockAnyEscooterAtParkForDestination(String parkIdentification, String username, double destinyLatitudeInDegrees, double destinyLongitudeInDegrees, String outputFileName) {
		FindVehicleUnlockByUserController ubc = new FindVehicleUnlockByUserController(false);
		Date date = new Date();

		long timeMilli = date.getTime();
		UnlockScooterController usc = new UnlockScooterController(username, false);
		try {
			if (usc.unlockAnyScooterGivenParkDestination(parkIdentification, username, destinyLatitudeInDegrees, destinyLongitudeInDegrees)) {
				Date date2 = new Date();

				long timeMilli2 = date2.getTime();
				long timetoUnlock = timeMilli2 - timeMilli;
				//  escooter description;type;actual battery capacity

				Scooter s1 = ubc.findUnlockedScooterByUser(username);

				BufferedWriter writer = null;
				try {
					writer = new BufferedWriter(new FileWriter(outputFileName));

					writer.write("escooter description;type;actual battery capacity");
					writer.newLine();
					writer.write(s1.getID() + ";" + s1.getType() + ";" + s1.getActualBatteryCapacity());
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return timetoUnlock;

			}
		} catch (SQLException ex) {
			Logger.getLogger(Facade.class.getName()).log(Level.SEVERE, null, ex);
		}
		return 0;
	}

	/**
	 *  * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Suggest escooters with enough energy + 10% to go from one Park to another.
	 *
	 * @param parkIdentification Park Identification where to unlock escooter.
	 * @param username Username.
	 * @param destinationParkLatitudeInDegrees Destination Park latitude in Decimal degrees.
	 * @param destinationParkLongitudeInDegrees Destination Park Longitude in Decimal degrees.
	 *
	 * @param outputFileName Write the escooters information to a file, according to file output/escooters.csv.
	 * @return The number of suggested vehicles.
	 */
	@Override
	public int suggestEscootersToGoFromOneParkToAnother(String parkIdentification, String username, double destinationParkLatitudeInDegrees, double destinationParkLongitudeInDegrees, String outputFileName) {

		SuggestScooterController usc = new SuggestScooterController(false);
		try {
			List<Scooter> listSuggested = usc.suggestScooters(parkIdentification, destinationParkLatitudeInDegrees, destinationParkLongitudeInDegrees);
			if (listSuggested != null) {
				int numberOfSuggestedScooters = listSuggested.size();
				//  escooter description;type;actual battery capacity

				BufferedWriter writer = null;
				try {
					writer = new BufferedWriter(new FileWriter(outputFileName));

					writer.write("escooter description;type;actual battery capacity");
					for (Scooter sg : listSuggested) {

						writer.newLine();
						writer.write(sg.getID() + ";" + sg.getType() + ";" + sg.getActualBatteryCapacity());
						writer.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				return numberOfSuggestedScooters;

			}
			BufferedWriter writer = null;

			try {
				writer = new BufferedWriter(new FileWriter(outputFileName));

				writer.write("escooter description;type;actual battery capacity");

				writer.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
			return 0;
		} catch (SQLException ex) {

			Logger.getLogger(Facade.class.getName()).log(Level.SEVERE, null, ex);

			BufferedWriter writer = null;

			try {
				writer = new BufferedWriter(new FileWriter(outputFileName));

				writer.write("escooter description;type;actual battery capacity");

				writer.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return 0;
	}

	/**
	 * Calculate the most energy efficient route from one park to another.
	 *
	 * Basic: Does not consider wind. Intermediate: Considers wind. Advanced: Considers the different mechanical and aerodynamic coefficients.
	 *
	 * @param originParkIdentification Origin Park Identification.
	 * @param destinationParkIdentification Destination Park Identification.
	 *
	 * @param typeOfVehicle The type of vehicle required e.g. "bicycle" or "escooter".
	 * @param vehicleSpecs The specs for the vehicle e.g. "16", "19", "27" or any other number for bicyles and "city" or "off-road" for any
	 * escooter.
	 * @param username The username.
	 * @param outputFileName Write to the file the Route between two parks according to file output/paths.csv. More than one path may exist. If
	 * so, sort routes by the ascending number of points between the parks and by ascending order of elevation difference.
	 * @return The distance in meters for the most energy efficient path.
	 */
	@Override
	public long mostEnergyEfficientRouteBetweenTwoParks(String originParkIdentification, String destinationParkIdentification, String typeOfVehicle, String vehicleSpecs, String username, String outputFileName) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 * Assigned to: Gonçalo Corte Real <1180793@isep.ipp.pt>
	 * Return the current debt for the user.
	 *
	 * @param username The username.
	 * @param outputFileName The path for the file to output the debt, according to file output/balance.csv. Sort the information by unlock time
	 * in ascending order (oldest to newest).
	 * @return The User's current debt in euros, rounded to two decimal places.
	 */
	@Override
	public double getUserCurrentDebt(String username, String outputFileName) {
		TripRegistry oRegistry = new TripRegistry(false);
		List<Trip> lstTrips = oRegistry.getUnpaidTrips(username);
		DataOutput output = new DataOutput();
		double debt = output.exportUserDebt(lstTrips, outputFileName);
		return debt;
	}

	/**
	 * Assigned to: Gonçalo Corte Real <1180793@isep.ipp.pt>
	 * Return the current points for the user.
	 *
	 * @param username The user to get the points report from.
	 * @param outputFileName The path for the file to output the points, according to file output/points.csv. Sort the information by unlock time
	 * in ascenind order (oldest to newest).
	 * @return The User's current points.
	 */
	@Override
	public double getUserCurrentPoints(String username, String outputFileName) {
		TripRegistry oRegistry = new TripRegistry(false);
		List<Trip> lstTrips = oRegistry.getAllTripsFromUser(username);
		DataOutput output = new DataOutput();
		int userPoints = output.exportUserPoints(lstTrips, username, outputFileName);
		return userPoints;
	}

	/**
	 * Calculate the amount of electrical energy required to travel from one park to another.
	 *
	 * @param originLatitudeInDegrees Origin latitude in Decimal degrees.
	 * @param originLongitudeInDegrees Origin Longitude in Decimal degrees.
	 * @param destinationLatitudeInDegrees Destination Park latitude in Decimal degrees.
	 * @param destinationLongitudeInDegrees Destination Park Longitude in Decimal degrees.
	 * @param username Username.
	 * @return The electrical energy required in kWh, rounded to two decimal places.
	 */
	@Override
	public double calculateElectricalEnergyToTravelFromOneLocationToAnother(double originLatitudeInDegrees, double originLongitudeInDegrees, double destinationLatitudeInDegrees, double destinationLongitudeInDegrees, String username) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 *  * Assigned to: Rogério Alves <1171250@isep.ipp.pt>
	 * Get for how long has a vehicle has been unlocked.
	 *
	 * @param vehicleDescription Vehicle description.
	 * @return The time in seconds since the vehicle was unlocked.
	 */
	@Override
	public long forHowLongAVehicleIsUnlocked(String vehicleDescription) {
		TripController tpc = new TripController(false);
		return tpc.getUnlockedTime(vehicleDescription);
	}

	/**
	 * Calculate the shortest Route from one park to another.
	 *
	 * Basic: Only one shortest Route between two Parks is available. Advanced: More than one Route between two parks are available with different
	 * number of POIs (limit to a maximum of two) inbetween and different evelations difference.
	 *
	 * @param originLatitudeInDegrees Origin latitude in Decimal degrees.
	 * @param originLongitudeInDegrees Origin Longitude in Decimal degrees.
	 * @param destinationLatitudeInDegrees Destination Park latitude in Decimal degrees.
	 * @param destinationLongitudeInDegrees Destination Park Longitude in Decimal degrees.
	 * @param numberOfPOIs The number of POIs that should be included in the path. Default can be 0.
	 * @param outputFileName Write to the file the Route between two parks according to file output/paths.csv. More than one path may exist. If
	 * so, sort routes by the ascending number of points between the parks and by ascending order of elevation difference.
	 * @return The distance in meters for the shortest path.
	 */
	@Override
	public long shortestRouteBetweenTwoParks(double originLatitudeInDegrees, double originLongitudeInDegrees, double destinationLatitudeInDegrees, double destinationLongitudeInDegrees, int numberOfPOIs, String outputFileName) {

		ShortestRouteTwoParksController controller = new ShortestRouteTwoParksController(false);
		controller.insertCoordinates(originLatitudeInDegrees, originLongitudeInDegrees, destinationLatitudeInDegrees, destinationLongitudeInDegrees);
		List<PointOfInterest> list = controller.getShortestRoute();

		if (controller.getMinDistance() != 0) {

			ParkDB pdb = new ParkDB(false);
			PointOfInterestDB pntdb = new PointOfInterestDB(false);

			Park iPark = pdb.getPark(list.get(0).getLatitude(), list.get(0).getLongitude());
			PointOfInterest iPoi;
			if (iPark == null) {
				iPoi = pntdb.getPOI(list.get(0).getLatitude(), list.get(0).getLongitude());
			} else {
				iPoi = new PointOfInterest(iPark.getLatitude(), iPark.getLongitude(), iPark.getElevation());
			}

			Park ePark = pdb.getPark(list.get(list.size() - 1).getLatitude(), list.get(list.size() - 1).getLongitude());
			PointOfInterest ePoi;
			if (ePark == null) {
				ePoi = pntdb.getPOI(list.get(list.size() - 1).getLatitude(), list.get(list.size() - 1).getLongitude());
			} else {
				ePoi = new PointOfInterest(ePark.getLatitude(), ePark.getLongitude(), ePark.getElevation());
			}

			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter(outputFileName));

				writer.write("Path: 001");
				writer.write("total_distance: " + Math.round(controller.getMinDistance() * 1000));
				writer.write("total_energy:TOTAL_ENERGY");
				writer.write("elevation: " + Math.round(iPoi.getAltitude() - ePoi.getAltitude()));
				for (PointOfInterest p : list) {
					writer.write(p.getLatitude() + " ; " + p.getLongitude());
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return (long) controller.getMinDistance();
		}

		return 0;
	}

	/**
	 * Calculate the shortest Route from one park to another.
	 *
	 * Basic: Only one shortest Route between two Parks is available. Advanced: More than one Route between two parks are available with different
	 * number of POIs (limit to a maximum of two) inbetween and different evelations difference.
	 *
	 * @param originParkIdentification Origin Park Identification.
	 * @param destinationParkIdentification Destination Park Identification.
	 * @param numberOfPOIs The number of POIs that should be included in the path. Default can be 0.
	 * @param outputFileName Write to the file the Route between two parks according to file output/paths.csv. More than one path may exist. If
	 * so, sort routes by the ascending number of points between the parks and by ascending order of elevation difference.
	 * @return The distance in meters for the shortest path.
	 */
	@Override
	public long shortestRouteBetweenTwoParks(String originParkIdentification, String destinationParkIdentification, int numberOfPOIs, String outputFileName) {

		try {
			ShortestRouteTwoParksController controller = new ShortestRouteTwoParksController(false);
			controller.validateParks(originParkIdentification, destinationParkIdentification);
			List<PointOfInterest> list = controller.getShortestRoute();

			if (controller.getMinDistance() != 0) {

				ParkDB pdb = new ParkDB(false);
				PointOfInterestDB pntdb = new PointOfInterestDB(false);

				Park iPark = pdb.getPark(list.get(0).getLatitude(), list.get(0).getLongitude());
				PointOfInterest iPoi;
				if (iPark == null) {
					iPoi = pntdb.getPOI(list.get(0).getLatitude(), list.get(0).getLongitude());
				} else {
					iPoi = new PointOfInterest(iPark.getLatitude(), iPark.getLongitude(), iPark.getElevation());
				}

				Park ePark = pdb.getPark(list.get(list.size() - 1).getLatitude(), list.get(list.size() - 1).getLongitude());
				PointOfInterest ePoi;
				if (ePark == null) {
					ePoi = pntdb.getPOI(list.get(list.size() - 1).getLatitude(), list.get(list.size() - 1).getLongitude());
				} else {
					ePoi = new PointOfInterest(ePark.getLatitude(), ePark.getLongitude(), ePark.getElevation());
				}

				BufferedWriter writer = null;
				try {
					writer = new BufferedWriter(new FileWriter(outputFileName));

					writer.write("Path: 001");
					writer.write("total_distance: " + Math.round(controller.getMinDistance() * 1000));
					writer.write("total_energy:TOTAL_ENERGY");
					writer.write("elevation: " + Math.round(iPoi.getAltitude() - ePoi.getAltitude()));
					for (PointOfInterest p : list) {
						writer.write(p.getLatitude() + " ; " + p.getLongitude());
					}
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

				return (long) controller.getMinDistance();
			}

		} catch (SQLException ex) {
			Logger.getLogger(Facade.class.getName()).log(Level.SEVERE, null, ex);
		}

		return 0;
	}

	/**
	 * Calculate the shortest Route from one park to another.
	 *
	 * Basic: Only one shortest Route between two Parks is available. Advanced: More than one Route between two parks are available with different
	 * number of points inbetween and different evelations difference.
	 *
	 * @param originParkIdentification Origin Park Identification.
	 * @param destinationParkIdentification Destination Park Identification.
	 * @param inputPOIs Path to file that contains the POIs that the route must go through, according to file input/pois.csv.
	 * @param outputFileName Write to the file the Route between two parks according to file output/paths.csv. More than one path may exist. If
	 * so, sort routes by the ascending number of points between the parks and by ascending order of elevation difference.
	 * @return The distance in meters for the shortest path.
	 */
	@Override
	public long shortestRouteBetweenTwoParksForGivenPOIs(String originParkIdentification, String destinationParkIdentification, String inputPOIs, String outputFileName) {

		ShortestRouteTwoParksWithSequenceController controller = new ShortestRouteTwoParksWithSequenceController(false);

		List<ArrayList<String>> data = new ArrayList<>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(inputPOIs));
			String line = reader.readLine();
			while (line != null) {
				if (!line.startsWith("#") && !line.isEmpty()) {
					String[] lineData = line.split(";");
					ArrayList<String> lineElem = new ArrayList<>();
					for (String item : lineData) {
						lineElem.add(item.trim());
					}
					data.add(lineElem);
				}
				line = reader.readLine();
			}
		} catch (IOException e) {

		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ex) {
				}
			}
		}

		List<String> sequence = new ArrayList<>();
		for (ArrayList<String> list : data) {
			sequence.add(list.get(3));
		}

		controller.validateParks(originParkIdentification, destinationParkIdentification, sequence, 1);
		List<LinkedList<Path>> list = controller.calculateShortestPath();

		if (!list.isEmpty()) {

			ParkDB pdb = new ParkDB(false);
			PointOfInterestDB pntdb = new PointOfInterestDB(false);

			Park iPark = pdb.getPark(list.get(0).get(0).getLatitudeA(), list.get(0).get(0).getLongitudeA());
			PointOfInterest iPoi;
			if (iPark == null) {
				iPoi = pntdb.getPOI(list.get(0).get(0).getLatitudeA(), list.get(0).get(0).getLongitudeA());
			} else {
				iPoi = new PointOfInterest(iPark.getLatitude(), iPark.getLongitude(), iPark.getElevation());
			}

			Park ePark = pdb.getPark(list.get(0).get(list.get(0).size() - 1).getLatitudeB(), list.get(0).get(list.size() - 1).getLongitudeB());
			PointOfInterest ePoi;
			if (ePark == null) {
				ePoi = pntdb.getPOI(list.get(0).get(list.get(0).size() - 1).getLatitudeB(), list.get(0).get(list.size() - 1).getLongitudeB());
			} else {
				ePoi = new PointOfInterest(ePark.getLatitude(), ePark.getLongitude(), ePark.getElevation());
			}

			double dist = 0;
			for (Path path : list.get(0)) {
				dist = dist + Utils.calculateDistance(new PointOfInterest(path.getLatitudeA(), path.getLongitudeA()), new PointOfInterest(path.getLatitudeB(), path.getLongitudeB()));
			}

			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter(outputFileName));

				writer.write("Path: 001");
				writer.write("total_distance: " + Math.round(dist));
				writer.write("total_energy:TOTAL_ENERGY");
				writer.write("elevation: " + Math.round(iPoi.getAltitude() - ePoi.getAltitude()));
				for (Path path : list.get(0)) {
					writer.write(path.getLatitudeA() + " ; " + path.getLongitudeA());
				}
				writer.write(ePoi.getLatitude() + " ; " + ePoi.getLongitude());
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return (long) dist;
		}

		return 0;
	}

	/**
	 * Calculate the shortest Route from one park to another.
	 *
	 * Basic: Only one shortest Route between two Parks is available. Advanced: More than one Route between two parks are available with different
	 * number of points inbetween and different evelations difference.
	 *
	 * @param originLatitudeInDegrees Origin latitude in Decimal degrees.
	 * @param originLongitudeInDegrees Origin Longitude in Decimal degrees.
	 * @param destinationLatitudeInDegrees Destination Park latitude in Decimal degrees.
	 * @param destinationLongitudeInDegrees Destination Park Longitude in Decimal degrees.
	 * @param inputPOIs Path to file that contains the POIs that the route must go through, according to file input/pois.csv.
	 * @param outputFileName Write to the file the Route between two parks according to file output/paths.csv. More than one path may exist. If
	 * so, sort routes by the ascending number of points between the parks and by ascending order of elevation difference.
	 * @return The distance in meters for the shortest path.
	 */
	@Override
	public long shortestRouteBetweenTwoParksForGivenPOIs(double originLatitudeInDegrees, double originLongitudeInDegrees, double destinationLatitudeInDegrees, double destinationLongitudeInDegrees, String inputPOIs, String outputFileName) {

		ShortestRouteTwoParksWithSequenceController controller = new ShortestRouteTwoParksWithSequenceController(false);

		List<ArrayList<String>> data = new ArrayList<>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(inputPOIs));
			String line = reader.readLine();
			while (line != null) {
				if (!line.startsWith("#") && !line.isEmpty()) {
					String[] lineData = line.split(";");
					ArrayList<String> lineElem = new ArrayList<>();
					for (String item : lineData) {
						lineElem.add(item.trim());
					}
					data.add(lineElem);
				}
				line = reader.readLine();
			}
		} catch (IOException e) {

		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ex) {
				}
			}
		}

		List<String> sequence = new ArrayList<>();
		for (ArrayList<String> list : data) {
			sequence.add(list.get(3));
		}

		controller.insertPointOfInterest(originLatitudeInDegrees, originLongitudeInDegrees, destinationLatitudeInDegrees, destinationLongitudeInDegrees, sequence);
		List<LinkedList<Path>> list = controller.calculateShortestPath();

		if (!list.isEmpty()) {

			ParkDB pdb = new ParkDB(false);
			PointOfInterestDB pntdb = new PointOfInterestDB(false);

			Park iPark = pdb.getPark(list.get(0).get(0).getLatitudeA(), list.get(0).get(0).getLongitudeA());
			PointOfInterest iPoi;
			if (iPark == null) {
				iPoi = pntdb.getPOI(list.get(0).get(0).getLatitudeA(), list.get(0).get(0).getLongitudeA());
			} else {
				iPoi = new PointOfInterest(iPark.getLatitude(), iPark.getLongitude(), iPark.getElevation());
			}

			Park ePark = pdb.getPark(list.get(0).get(list.get(0).size() - 1).getLatitudeB(), list.get(0).get(list.size() - 1).getLongitudeB());
			PointOfInterest ePoi;
			if (ePark == null) {
				ePoi = pntdb.getPOI(list.get(0).get(list.get(0).size() - 1).getLatitudeB(), list.get(0).get(list.size() - 1).getLongitudeB());
			} else {
				ePoi = new PointOfInterest(ePark.getLatitude(), ePark.getLongitude(), ePark.getElevation());
			}

			double dist = 0;
			for (Path path : list.get(0)) {
				dist = dist + Utils.calculateDistance(new PointOfInterest(path.getLatitudeA(), path.getLongitudeA()), new PointOfInterest(path.getLatitudeB(), path.getLongitudeB()));
			}

			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter(outputFileName));

				writer.write("Path: 001");
				writer.write("total_distance: " + Math.round(dist));
				writer.write("total_energy:TOTAL_ENERGY");
				writer.write("elevation: " + Math.round(iPoi.getAltitude() - ePoi.getAltitude()));
				for (Path path : list.get(0)) {
					writer.write(path.getLatitudeA() + " ; " + path.getLongitudeA());
				}
				writer.write(ePoi.getLatitude() + " ; " + ePoi.getLongitude());
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return (long) dist;
		}

		return 0;
	}

	/**
	 * Assigned to: Gonçalo Corte Real <1180793@isep.ipp.pt>
	 * Get a report for the escooter charging status at a given park.
	 *
	 * @param parkIdentification Park Identification.
	 * @param outputFileName Path to file where vehicles information should be written, according to file output/chargingReport .csv. Sort items
	 * by descending order of time to finish charge in seconds and secondly by ascending escooter description order.
	 * @return The number of escooters charging at the moment that are not 100% fully charged.
	 */
	@Override
	public long getParkChargingReport(String parkIdentification, String outputFileName) {
		GetParkChargingStatusReportController oController = new GetParkChargingStatusReportController(false);
		Map<String, Integer> map = oController.getChargingStatusReport(parkIdentification);
		Map<String, Integer> sortedMap = Utils.sortParkChargingReport(map);
		DataOutput dataOutput = new DataOutput();
		int nChargingScooters = dataOutput.exportParkChargingReport(sortedMap, outputFileName);
		return nChargingScooters;
	}

	/**
	 * Calculate the most energetically efficient route from one park to another with sorting options.
	 *
	 * @param originParkIdentification Origin Park Identification.
	 * @param destinationParkIdentification Destination Park Identification.
	 *
	 * @param typeOfVehicle The type of vehicle required e.g. "bicycle" or "escooter".
	 * @param vehicleSpecs The specs for the vehicle e.g. "16", "19", "27" or any other number for bicyles and "city" or "off-road" for any
	 * escooter.
	 * @param username The user that asked for the routes.
	 * @param maxNumberOfSuggestions The maximum number of suggestions to provide.
	 * @param ascendingOrder If routes should be ordered by ascending or descending order
	 * @param sortingCriteria The criteria to use for ordering "energy", "shortest_distance", "number_of_points".
	 * @param inputPOIs Path to file that contains the POIs that the route must go through, according to file input/pois.csv. By default, the file
	 * is empty.
	 * @param outputFileName Write to the file the Route between two parks according to file output/paths.csv. More than one path may exist.
	 * @return The number of suggestions
	 */
	@Override
	public int suggestRoutesBetweenTwoLocations(String originParkIdentification, String destinationParkIdentification, String typeOfVehicle, String vehicleSpecs, String username, int maxNumberOfSuggestions, boolean ascendingOrder, String sortingCriteria, String inputPOIs, String outputFileName) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 * Assigned to: Gonçalo Corte Real <1180793@isep.ipp.pt>
	 * Get the current invoice for the current month, for a specific user. This should include all loans that were charged the user, the number of
	 * points the user had before the actual month, the number of points earned during the month, the number of points converted to euros.
	 *
	 * @param month The month of the invoice e.g. 1 for January.
	 * @param username The user for which the invoice should be created.
	 * @param outputPath Path to file where the invoice should be written, according to file output/invoice.csv.
	 * @return User debt in euros rounded to two decimal places.
	 */
	@Override
	public double getInvoiceForMonth(int month, String username, String outputPath) {
		IssueInvoiceController oController = new IssueInvoiceController(false);
		Invoice invoice = oController.issueInvoice(month, username);
		if (invoice == null) {
			return -1;
		}
		DataOutput output = new DataOutput();
		double userDebt = output.exportInvoice(invoice, outputPath);
		return userDebt;
	}

}
