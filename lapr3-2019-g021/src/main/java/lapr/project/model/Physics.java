/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.data.UserDB;
import static lapr.project.utils.Constants.GRAVITY_ACCELERATION;

/**
 *
 * @author danie
 */
public class Physics {

    private double aeroDynCof;
    


    public Physics(double aeroDynCof) {

        this.aeroDynCof = aeroDynCof;

   }
    
    public Physics(){
       
        
    }
  

    /**
     * Calculate the frictional force
     *
     * @param bikeWeight
     * @param inclination
     * @param userWeight
     * @return
     */
    public double calculateFAtrito(double bikeWeight, double inclination, double userWeight) {
        return (userWeight + bikeWeight) * GRAVITY_ACCELERATION * Math.cos(Math.toRadians(inclination)) * this.aeroDynCof;
    }
//
//    /**
//     * force applied to the user and bicycle by gravity
//     *
//     * @param bikeWeight
//     * @param inclination
//     * @param userWeight
//     * @return
//     */
    public double calculateFGravitica(double bikeWeight, double inclination, double userWeight) {
        return (userWeight + bikeWeight) * GRAVITY_ACCELERATION * Math.sin(Math.toRadians(inclination));
    }
    
     public boolean calculateAutonomyOfScooter(Scooter s,double dist) {
        
         
     double actualBatteryChargeKWh = ((s.getMaxBatteryCapacity() * s.getActualBatteryCapacity()))/100;
     double actualBatteryChargeWh = actualBatteryChargeKWh * 1000;
     double enginePower = s.getMotor();
     int speed = 20;  //assumo que a velocidade média é 20km/h como diz no forum
     double timeInHours = actualBatteryChargeWh / enginePower;
     double maximumDistance =(timeInHours* speed )*0.7; //0.7 is becouse 70% efficiency
      System.out.println("distance to make "+dist);
     System.out.println("max distance of scooter --"+s.getID()+ " = "+maximumDistance);
     if (dist <= maximumDistance){
         return true;
     }
     return false;
         
         

    }
     
     
     
     UserDB userDB;
     ParkDB parkDb;
     BicycleDB bikeDb;
     
     
     /**
      * Gets a Bicycle by its ID
      *
      * @param id
      * @return
      */
      public Bicycle getBicycle(String id) {
        return this.bikeDb.getBicycle(id);
      }
      
      /**
       * Gets a Park from the DB by its coordinates
       *
       * @param latitude
       * @param longitude
       * @return
       */
       private Park getPark(double latitude, double longitude) {
            return this.parkDb.getPark(latitude, longitude);
        }
     
     
     
     
     public double calculateTotalAmountCalories(String bicycleId, double latitudeOrigin, double longitudeOrigin, double latitudeDest, double longitudeDest, String userEmail) {
                Bicycle bicycle = getBicycle(bicycleId);
                if (bicycle == null) {
                        return -1;
                }

                Park parkOrigin = getPark(latitudeOrigin, longitudeOrigin);
                if (parkOrigin == null) {
                        return -1;
                }

                Park parkDest = getPark(latitudeDest, longitudeDest);
                if (parkDest == null) {
                        return -1;
                }

                User user = userDB.getUser(userEmail);
                if (user == null) {
                        return -1;
                }

                PointOfInterest poi1 = new PointOfInterest(latitudeOrigin, longitudeOrigin, parkOrigin.getElevation(), "");
                PointOfInterest poi2 = new PointOfInterest(latitudeDest, longitudeDest, parkDest.getElevation(), "");

                double distance = poi1.calculateDistance(poi2);

                double angle = Math.atan((parkDest.getElevation() - parkOrigin.getElevation()) / distance);
                Physics physics = new Physics(bicycle.getAerodynamicCoefficient());
                double atrito = physics.calculateFAtrito(bicycle.getWeight(), angle, user.getWeight());
                double calorias = distance / 1000 / 20 * 6 * user.getWeight();
                calorias = calorias * (1 + atrito) * (1 + Math.cos(angle));

                return calorias;
        }
}
