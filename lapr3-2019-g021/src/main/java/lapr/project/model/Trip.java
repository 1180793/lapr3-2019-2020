/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author roger40
 */
public class Trip {

	private String vehicleId;
	private String vehicleType;
	private Timestamp dateUnlocked;
	private Park parkOrigin;
	private User assignedUser;
	private Park parkDestinaton;
	private Timestamp dateLocked;
	private int tripNumber;
	private int invoiceId;

	private double price;

	//when getting  a full trip  from db
	public Trip(String vehicleId, String vehicleType, Timestamp dateUnlocked, Park parkOrigin, User assignedUser, Park parkDestination, Timestamp dateLocked, int tripNumber, double price) {

		this.vehicleId = vehicleId;
		this.vehicleType = vehicleType;
		this.dateUnlocked = dateUnlocked;
		this.parkOrigin = parkOrigin;
		this.assignedUser = assignedUser;
		this.parkDestinaton = parkDestination;
		this.dateLocked = dateLocked;

		this.tripNumber = tripNumber;

		this.price = price;
		this.invoiceId = 0;

	}

	public Trip(String vehicleId, String vehicleType, Timestamp dateUnlocked, Park parkOrigin, User assignedUser, Park parkDestination, Timestamp dateLocked, int tripNumber, double price, int invoice) {

		this.vehicleId = vehicleId;
		this.vehicleType = vehicleType;
		this.dateUnlocked = dateUnlocked;
		this.parkOrigin = parkOrigin;
		this.assignedUser = assignedUser;
		this.parkDestinaton = parkDestination;
		this.dateLocked = dateLocked;

		this.tripNumber = tripNumber;

		this.price = price;
		this.invoiceId = invoice;

	}

	//when creating a new  a full trip  to add in the  db
	public Trip(String vehicleId, String vehicleType, Park parkOrigin, User assignedUser, int tripNumber) {
		Date date = new Date();  //get current date in timestamp format
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);

		this.vehicleId = vehicleId;
		this.vehicleType = vehicleType;
		this.dateUnlocked = ts;
		this.parkOrigin = parkOrigin;
		this.assignedUser = assignedUser;
		this.tripNumber = tripNumber;
		this.parkDestinaton = null;
		this.dateLocked = null;
		this.price = 0;
	}

	public void uppdateTrip(Park parkDestination) {
		Date date = new Date();  //get current date in timestamp format
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);

		this.parkDestinaton = parkDestination;
		this.dateLocked = ts;

		long milliseconds1 = this.dateUnlocked.getTime();
		long milliseconds2 = this.dateLocked.getTime();

		long diff = milliseconds2 - milliseconds1;
		long diffHours = diff / (60 * 60 * 1000);

		if (diffHours >= 1) {
			this.price = (diffHours * 1.5);
		}

	}

	public double getPrice() {
		return price;
	}

	public int getTripNumber() {
		return tripNumber;
	}

	public void setUnlockDate(Timestamp du) {  //for Test proposes
		this.dateUnlocked = du;
	}

	public void setInvoiceId(int invoice) {  //for Test proposes
		this.invoiceId = invoice;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public int getInvoiceId() {
		return this.invoiceId;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public Timestamp getDateUnlocked() {
		return dateUnlocked;
	}

	public Park getParkOrigin() {
		return parkOrigin;
	}

	public User getAssignedUser() {
		return assignedUser;
	}

	public Park getParkDestiny() {
		return parkDestinaton;
	}

	public Timestamp getDateLocked() {
		return dateLocked;
	}

}
