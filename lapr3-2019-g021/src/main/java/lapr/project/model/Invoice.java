package lapr.project.model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Invoice {

	/**
	 * Invoice ID to make invoice unique.
	 */
	private int invoiceId;

	/**
	 * User's username.
	 */
	private String username;

	/**
	 * Points available from lastMonth.
	 */
	private int previousMonthPoints;

	/**
	 * Points earned this month.
	 */
	private int pointsEarnedThisMonth;

	/**
	 * Points used to make discount.
	 */
	private int pointsUsed;

	/**
	 * Points left after discount.
	 */
	private int pointsAvailableForNextMonth;

	/**
	 * Total price before discount.
	 */
	private double totalPrice;

	/**
	 * Decimal format for monetary value
	 */
	private DecimalFormat df = new DecimalFormat("#.00");

	/**
	 * List with each invoice month's trips.
	 */
	private List<Trip> tripList;

	/**
	 * Constructor for Invoice Instance.
	 *
	 * @param intID
	 * @param strUsername
	 * @param intPreviousMonthPoints
	 * @param intPointsEarnedThisMonth
	 * @param intPointsUsed
	 * @param intPointsAvailableForNextMonth
	 * @param dblTotalPrice
	 * @param lstTrips
	 */
	public Invoice(int intID, String strUsername, int intPreviousMonthPoints, int intPointsEarnedThisMonth, int intPointsUsed, int intPointsAvailableForNextMonth, double dblTotalPrice, List<Trip> lstTrips) {
		tripList = new ArrayList<>();
		this.invoiceId = intID;
		this.username = strUsername;
		this.previousMonthPoints = intPreviousMonthPoints;
		this.pointsEarnedThisMonth = intPointsEarnedThisMonth;
		this.pointsUsed = intPointsUsed;
		this.pointsAvailableForNextMonth = intPointsAvailableForNextMonth;
		this.totalPrice = dblTotalPrice;
		this.tripList = lstTrips;
	}

	/**
	 * @return the invoiceNumber
	 */
	public int getID() {
		return this.invoiceId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * @return the previousMonthPoints
	 */
	public int getPreviousMonthPoints() {
		return this.previousMonthPoints;
	}

	/**
	 * @return the pointsEarnedThisMonth
	 */
	public int getPointsEarnedThisMonth() {
		return this.pointsEarnedThisMonth;
	}

	/**
	 * @return the pointsUsed
	 */
	public int getPointsUsed() {
		return this.pointsUsed;
	}

	/**
	 * @return the pointsAvailableForNextMonth
	 */
	public int getPointsAvailableForNextMonth() {
		return this.pointsAvailableForNextMonth;
	}

	/**
	 * @return the totalPrice
	 */
	public double getTotalPrice() {
		return this.totalPrice;
	}

	/**
	 * @return the tripInfoList
	 */
	public List<Trip> getTripList() {
		return this.tripList;
	}

	/**
	 * HashCode generated for the equals method.
	 *
	 * @return
	 */
	@Override
	public int hashCode() {
		return Objects.hashCode(getID());
	}

	/**
	 * Compares the invoice with the object received.
	 *
	 * @param obj the object to compare with invoice.
	 * @return true if the object received represents other invoice equivelent to invoice. Otherwise, returns false.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Invoice other = (Invoice) obj;
		return other.getID() == this.getID();
	}

	/**
	 * Returns the text description of the invoice.
	 *
	 * @return the invoice's description.
	 */
	@Override
	public String toString() {
		return getUsername()
			+ "\nPrevious points:" + getPreviousMonthPoints()
			+ "\nEarned points:" + getPointsEarnedThisMonth()
			+ "\nDiscounted points:" + getPointsUsed()
			+ "\nActual points:" + getPointsAvailableForNextMonth()
			+ "\nCharged Value:" + df.format(getTotalPrice())
			+ tripInfoToString();
	}

	/**
	 * Returns the text description of each Trip.
	 *
	 * @return the trip's info.
	 */
	private String tripInfoToString() {
		StringBuilder strBuilder = new StringBuilder();
		for (Trip trip : tripList) {
			String aux = "\n" + trip.getVehicleId() + ";" // Vehicle Description
				+ trip.getDateUnlocked().getTime() + ";" // Vehicle Unlock Time
				+ trip.getDateLocked().getTime() + ";" // Vehicle Lock Time
				+ trip.getParkOrigin().getLatitude() + ";" // Origin Park Latitude
				+ trip.getParkOrigin().getLongitude() + ";" // Origin Park Longitude
				+ trip.getParkDestiny().getLatitude() + ";" // Destination Park Latitude
				+ trip.getParkDestiny().getLongitude() + ";" // Destination Park Longitude
				+ Utils.getTimestampDifference(trip.getDateLocked(), trip.getDateUnlocked()) + ";" // Total Time Spent in Seconds
				+ df.format(trip.getPrice());				// Charged Value
			strBuilder.append(aux);
		}
		return strBuilder.toString();
	}

}
