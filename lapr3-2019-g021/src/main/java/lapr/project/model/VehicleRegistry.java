package lapr.project.model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.controller.SuggestScooterController;
import lapr.project.data.BicycleDB;
import lapr.project.data.ParkDB;
import lapr.project.data.ScooterDB;
import lapr.project.data.UserDB;

/**
 *
 * @author roger40
 */
public class VehicleRegistry {

	/**
	 * Connection to the Bicycle Database
	 */
	TripRegistry tripreg;
	BicycleDB bikeDb;
	SuggestScooterController ssc;
	/**
	 * Connection to the Scooter Database
	 */
	ScooterDB scooterDb;

	/**
	 * Connection to the Park Database
	 */
	ParkDB parkDb;
	UserDB userDB;
	boolean mock;
	private static final Logger LOGGER = Logger.getLogger("VehicleRegistryLog");

	/**
	 * Park Registry Constructor
	 *
	 * @param mock
	 */
	public VehicleRegistry(boolean mock) {
		this.bikeDb = new BicycleDB(mock);
		this.scooterDb = new ScooterDB(mock);
		this.parkDb = new ParkDB(mock);
		this.tripreg = new TripRegistry(mock);
		this.userDB = new UserDB(mock);
		this.mock = mock; // save the mock state to instanciate suggestscooter
		// this.ssc = new SuggestScooterController(mock);
	}

	/**
	 * Gets all Registered Bicycles
	 *
	 * @return list with all bicycles
	 */
	public List<Bicycle> getAllBicycles() {
		return this.bikeDb.getAllBicycles();
	}

	/**
	 * Gets all Registered Scooters
	 *
	 * @return list with all scooters
	 */
	public List<Scooter> getAllScooters() {
		return this.scooterDb.getAllScooters();
	}

	/**
	 * Gets a Bicycle by its ID
	 *
	 * @param id
	 * @return
	 */
	public Bicycle getBicycle(String id) {
		return this.bikeDb.getBicycle(id);
	}

	/**
	 * Gets a Scooter by its ID
	 *
	 * @param id
	 * @return
	 */
	public Scooter getScooter(String id) {
		return this.scooterDb.getScooter(id);

	}

	/**
	 * Gets a Park from the DB by its coordinates
	 *
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	private Park getPark(double latitude, double longitude) {
		return this.parkDb.getPark(latitude, longitude);
	}

	/**
	 * Creates a new Bicycle instance to register and validates it
	 *
	 * @param bicycleId
	 * @param weight
	 * @param latitude
	 * @param longitude
	 * @param aeroCoefficient
	 * @param frontArea
	 * @param size
	 * @return
	 */
	public Bicycle newBicycle(String bicycleId, int weight, double latitude, double longitude, double aeroCoefficient, double frontArea, int size) {
		Park park = getPark(latitude, longitude);
		Bicycle oBicycle = new Bicycle(bicycleId, weight, park, aeroCoefficient, frontArea, size);
		if (oBicycle.validateBicycle()) {
			return oBicycle;
		}
		return null;
	}

	/**
	 * Creates a new Scooter instance to register and validates it
	 *
	 * @param scooterId
	 * @param weight
	 * @param type
	 * @param latitude
	 * @param longitude
	 * @param maxBatteryCap
	 * @param aeroCoefficient
	 * @param actualBatteryCap
	 * @param frontArea
	 * @return
	 */
	public Scooter newScooter(String scooterId, int weight, int type, double latitude, double longitude, double maxBatteryCap, int actualBatteryCap, double aeroCoefficient, double frontArea, int motor) {
		Park park = getPark(latitude, longitude);
		Scooter oScooter = new Scooter(scooterId, weight, type, park, maxBatteryCap, actualBatteryCap, aeroCoefficient, frontArea, motor);
		if (oScooter.validateScooter()) {
			return oScooter;
		}
		return null;
	}

	/**
	 * Adds a given Bicycle instance to the Database
	 *
	 * @param oBicycle
	 * @return
	 */
	public boolean addBicycle(Bicycle oBicycle) {
		if (oBicycle == null) {
			LOGGER.info("Error adding Bicycle: Local Validation Failed");
			return false;
		}
		if (bikeDb.validateBicycle(oBicycle)) {
			bikeDb.addBicycle(oBicycle);
			LOGGER.log(Level.INFO, "Bicycle({0}) Added", oBicycle.getID());
			return true;
		} else {
			LOGGER.log(Level.INFO, "Error adding Bicycle({0}): Global Validation Failed", oBicycle.getID());
			return false;
		}
	}

	/**
	 * Adds a given Scooter instance to the Database
	 *
	 * @param oScooter
	 * @return
	 */
	public boolean addScooter(Scooter oScooter) {
		if (oScooter == null) {
			LOGGER.info("Error adding Scooter: Local Validation Failed");
			return false;
		}
		if (scooterDb.validateScooter(oScooter)) {
			scooterDb.addScooter(oScooter);
			LOGGER.log(Level.INFO, "Scooter({0}) Added", oScooter.getID());
			return true;
		} else {
			LOGGER.log(Level.INFO, "Error adding Scooter({0}): Global Validation Failed", oScooter.getID());
			return false;
		}
	}

	public boolean removeBicycle(String id) {
		return bikeDb.removeBicycle(id);
	}

	public boolean removeScooter(String id) {
		return scooterDb.removeScooter(id);
	}

	public List<Scooter> getSuggestedScooters(Park parkOrigin, Park parkEnd, double totalDistKm) {

		this.ssc = new SuggestScooterController(this.mock); // not initialized in construtor becouse of stack memory overflow error when running multiples tests
//              double totalDistMeters = Utils.calculateDistance(new PointOfInterest(parkOrigin.getLatitude(), parkOrigin.getLongitude()), new PointOfInterest(parkEnd.getLatitude(), parkEnd.getLongitude()));
//             double totalDistKm = totalDistMeters /1000;
		double distPlus10Percent = totalDistKm + ((totalDistKm * 10) / 100);
		List<Scooter> listSugScooter = new ArrayList<>();
		Physics phis = new Physics();
		for (Scooter s : getAllScooters()) {
			if (phis.calculateAutonomyOfScooter(s, distPlus10Percent)) {
				listSugScooter.add(s);
			}
		}
		return listSugScooter;

	}

	public List<Bicycle> getAvailableBicycles(String parkOrigin) {

		return bikeDb.getAvailableBicycles(parkOrigin);
	}

	public List<Scooter> getAvailableScooters(String parkOrigin) {

		return scooterDb.getAvailableScooters(parkOrigin);
	}

	public List<Bicycle> getAvailableBicycles(double lat, double longi) {
		Park p = parkDb.getPark(lat, longi);
		if (p != null) {
			return bikeDb.getAvailableBicycles(p.getID());
		}
		return null;
	}

	public List<Scooter> getAvailableScooters(double lat, double longi) {

		Park p = parkDb.getPark(lat, longi);
		if (p != null) {
			return scooterDb.getAvailableScooters(p.getID());

		}
		return null;

	}

//    public List<Scooter> getSuggestedScooters(String parkOrigin, String parkEnd) {
//
//        return scooterDb.getSuggestedScooters(parkOrigin, parkEnd);
//    }
	public boolean verifyUserUnlock(String us1) {

		for (Bicycle bikes : bikeDb.getAllBicycles()) {

			if (bikes.getAssignedUser() != null && (bikes.getAssignedUser().getEmail().equals(us1) || bikes.getAssignedUser().getName().equals(us1))) {

				return false;

			}
		}

		for (Scooter scoot : scooterDb.getAllScooters()) {

			if (scoot.getAssignedUser() != null && (scoot.getAssignedUser().getEmail().equals(us1) || scoot.getAssignedUser().getName().equals(us1))) {

				return false;

			}
		}

		return true;
	}

	public boolean unlockBicycle(String bike1, String us1) {

		if (verifyUserUnlock(us1)) {

			for (Bicycle b : getAllBicycles()) {

				if (b.getID().equals(bike1)) {

					if (b.getAssignedUser() != null) {  // se a bicicleta ja estiver em utilizaçao nao deixa fazer esse unlock

						return false;
					}

					return (bikeDb.unlockBicycle(bike1, userDB.getUser(us1)) && tripreg.addTrip(bike1, "b", b.getAssignedPark(), userDB.getUser(us1)));
//                                        tripreg.addTrip(bike1, "b", b.getAssignedPark(), userDB.getUser(us1));
//                                        for(User us : userDB.getAllUsers()){
//                                            if (us.getEmail().equals(us1)){
//                                                // tripreg.addTrip(bike1, "b", b.getAssignedPark(), us);
//                                                 return bikeDb.unlockBicycle(bike1, us);
//                                            }
//                                        }
//                                        
//                                          //encontrando a bike para unlock crio uma nova trip;

				}
			}

		}
		return false;
	}

	public boolean unlockSpecificScooter(String scotId, String us1) {

		if (verifyUserUnlock(us1)) {
			Park parkOfBicycle;
			for (Scooter s : getAllScooters()) {
				if (s.getID().equals(scotId)) {
					if (s.getAssignedUser() != null) {  // se a scooter ja estiver em utilizaçao nao deixa fazer esse unlock
						return false;
					}
					return (scooterDb.unlockScooter(scotId, userDB.getUser(us1)) && tripreg.addTrip(scotId, "s", s.getAssignedPark(), userDB.getUser(us1)));
					//tripreg.addTrip(scotId, "s", s.getAssignedPark(), userDB.getUser(us1));  // encontrando a scooter para unlock crio uma nova trip;

				}
			}

			// return (scooterDb.unlockScooter(scotId, userDB.getUser(us1)) &&  tripreg.addTrip(scotId, "s", s.getAssignedPark(), userDB.getUser(us1)));
		}
		return false;
	}

	public Scooter CheckHigherBatteryScooterAtPark(String parkId) {

		double capacityOfScooter;
		double bestCapacity = 0;

		for (Scooter s : getAllScooters()) {

			if (s.getAssignedPark() != null && s.getAssignedPark().getID().equals(parkId) && s.getAssignedUser() == null) { // scooter tem de estar no park

				capacityOfScooter = s.getActualBatteryCapacity() * s.getMaxBatteryCapacity();
				if (capacityOfScooter > bestCapacity) {
					bestCapacity = capacityOfScooter;

				}

			}

		}

		for (Scooter s : getAllScooters()) {

			if (s.getAssignedPark() != null && s.getAssignedPark().getID().equals(parkId) && s.getAssignedUser() == null && s.getActualBatteryCapacity() * s.getMaxBatteryCapacity() == bestCapacity) { // scooter tem de estar no park

				//System.out.println("encontrou melhor scooter" + s.getID());
				return s;

			}
		}

		return null;

	}

	public boolean unlockAnyScooterAtPark(String parkId, String us1) {  // nlock da scooter com mais bateria no park

		if (verifyUserUnlock(us1)) {

			Scooter s1 = null;
//                       

			s1 = CheckHigherBatteryScooterAtPark(parkId);
			if (s1 != null) {

				return (scooterDb.unlockScooter(s1.getID(), userDB.getUser(us1)) && tripreg.addTrip(s1.getID(), "s", s1.getAssignedPark(), userDB.getUser(us1)));

			}

		}

		return false;
	}

//        
//
//	public boolean unlockScooter(Scooter scot1, String us1, Park parkOrigin, Park parkEnd) {
//		if (verifyUserUnlock(us1)) {
//                    tripreg.addTrip(scot1.getID(), "s", parkOrigin, us1);  //crio uma nova trip;
//			return scooterDb.unlockScooter(scot1.getID(), us1, parkOrigin, parkEnd);
//		}
//		return false;
//
//	}
	public boolean unlockAnyScooterGivenParkDestination(Scooter s1, String user1) throws SQLException {
		User us1 = userDB.getUser(user1);
		if (us1 != null) {
			return (scooterDb.unlockScooter(s1.getID(), us1) && tripreg.addTrip(s1.getID(), "s", s1.getAssignedPark(), us1));
		}
		return false;

	}

	public Bicycle updateBicycle(String bicycleId, int weight, double latitude, double longitude, double aeroCoefficient, double frontArea, int size) {
		Park park = getPark(latitude, longitude);

		Bicycle oBicycle = getBicycle(bicycleId);
		//if bicycle is not in the database return null
		if (oBicycle == null) {
			return null;
		}

		oBicycle.setWeight(weight);
		oBicycle.setAssignedPark(park);
		oBicycle.setAerodynamicCoefficient(aeroCoefficient);
		oBicycle.setFrontalArea(frontArea);
		oBicycle.setWheelSize(size);

		if (oBicycle.validateBicycle()) {
			return oBicycle;
		}
		return null;
	}

	public boolean updateBicycle(Bicycle oBicycle) {
		if (oBicycle == null) {
			LOGGER.info("Error updating Bicycle: Local Validation Failed");
			return false;
		}
		if (bikeDb.validateUpdateBicycle(oBicycle)) {
			bikeDb.updateBicycle(oBicycle);
			LOGGER.log(Level.INFO, "Bicycle({0}) updated", oBicycle.getID());
			return true;
		} else {
			LOGGER.log(Level.INFO, "Error updating Bicycle({0}): Global Validation Failed", oBicycle.getID());
			return false;
		}
	}

	public Scooter updateScooter(String scooterId, int weight, int type, double latitude, double longitude, double maxBatteryCap, int actualBatteryCap, double aeroCoefficient, double frontArea) {
		Park park = getPark(latitude, longitude);

		Scooter oScooter = getScooter(scooterId);
		//if bicycle is not in the database return null
		if (oScooter == null) {
			return null;
		}

		oScooter.setWeight(weight);
		oScooter.setType(type);
		oScooter.setAssignedPark(park);
		oScooter.setMaximumBatteryCapacity(maxBatteryCap);
		oScooter.setActualBatteryCapacity(actualBatteryCap);
		oScooter.setAerodynamicCoefficient(aeroCoefficient);
		oScooter.setFrontalArea(frontArea);

		if (oScooter.validateScooter()) {
			return oScooter;
		}
		return null;
	}

	public boolean updateScooter(Scooter oScooter) {
		if (oScooter == null) {
			LOGGER.info("Error updating Scooter: Local Validation Failed");
			return false;
		}
		if (scooterDb.validateUpdateScooter(oScooter)) {
			scooterDb.updateScooter(oScooter);
			LOGGER.log(Level.INFO, "Scooter({0}) updated", oScooter.getID());
			return true;
		} else {
			LOGGER.log(Level.INFO, "Error updating Scooter({0}): Global Validation Failed", oScooter.getID());
			return false;
		}

	}

	public Map<String, Integer> getChargingStatusReport(String parkID) {
		Park oPark = this.parkDb.getPark(parkID);
		LinkedHashMap<String, Integer> report = new LinkedHashMap<>();
		if (oPark == null) {
			LOGGER.info("Error Getting Charging Status Report: Invalid Park ID");
			return null;
		}
		if (scooterDb.getChargingStatusReport(oPark, report)) {
			LOGGER.log(Level.INFO, "Charging Status Report Generated for Park({0})", oPark.getID());
			return report;
		} else {
			LOGGER.log(Level.INFO, "Error Getting Charging Status Report for Park({0}): No Scooters available in Park", oPark.getID());
			return null;
		}

	}

	public Map<String, String> GetCurrentUnlockedBicyclesReport() {

		LinkedHashMap<String, String> unlcokedBikesReport = new LinkedHashMap<>();

		for (Bicycle bike : bikeDb.getAllBicycles()) {
			if (bike.getAssignedUser() != null) {

				unlcokedBikesReport.put(bike.getAssignedUser().getEmail(), bike.getID());
			}
		}
		return unlcokedBikesReport;

	}

	public Map<String, String> GetCurrentUnlockedScootersReport() {

		LinkedHashMap<String, String> unlcokedscotReport = new LinkedHashMap<>();

		for (Scooter scot : scooterDb.getAllScooters()) {
			if (scot.getAssignedUser() != null) {

				unlcokedscotReport.put(scot.getAssignedUser().getEmail(), scot.getID());
			}
		}
		return unlcokedscotReport;

	}

	public List<Scooter> getScooterCapacityEstimatedTripReport(int distanceKm) {

		Physics phis = new Physics();
		List<Scooter> scooterCapacityEstimatedTripReport = new ArrayList<>();

		for (Scooter scooter : scooterDb.getAllScooters()) {
			if (!phis.calculateAutonomyOfScooter(scooter, distanceKm)) {
				scooterCapacityEstimatedTripReport.add(scooter);
			}
		}
		return scooterCapacityEstimatedTripReport;
	}

	public boolean lockBicycle(String us1Email, double parkLat, double parkLong) {

		Park parkEnd = parkDb.getPark(parkLat, parkLong);

		if (parkEnd != null && parkEnd.getAvailableBicycleParkingSlots() > 0) {

			for (Bicycle b1 : getAllBicycles()) {
				if (b1.getAssignedUser() != null && b1.getAssignedUser().getEmail().equals(us1Email)) {

					User uss = b1.getAssignedUser();
					return (bikeDb.lockBicycle(b1, parkEnd) && tripreg.updateTrip(parkEnd, uss));
				}
			}
		}
		return false;

	}

	public boolean lockBicycle(String us1Email, String parkId) {

		Park parkEnd = parkDb.getPark(parkId);

		if (parkEnd != null && parkEnd.getAvailableBicycleParkingSlots() > 0) {

			for (Bicycle b1 : getAllBicycles()) {
				if (b1.getAssignedUser() != null && b1.getAssignedUser().getEmail().equals(us1Email)) {

					User uss = b1.getAssignedUser();
					return (bikeDb.lockBicycle(b1, parkEnd) && tripreg.updateTrip(parkEnd, uss));

				}
			}
		}
		return false;

	}

	public boolean lockScooter(String us1Email, double parkLat, double parkLong) {

		Park parkEnd = parkDb.getPark(parkLat, parkLong);

		if (parkEnd != null && parkEnd.getAvailableScooterParkingSlots() > 0) {

			for (Scooter s1 : getAllScooters()) {
				if (s1.getAssignedUser() != null && s1.getAssignedUser().getEmail().equals(us1Email)) {
					User uss = s1.getAssignedUser();
					return (scooterDb.lockScooter(s1, parkEnd) && tripreg.updateTrip(parkEnd, uss));
				}
			}
		}
		return false;

	}

	public boolean lockScooter(String us1Email, String parkId) {

		Park parkEnd = parkDb.getPark(parkId);

		if (parkEnd != null && parkEnd.getAvailableScooterParkingSlots() > 0) {

			for (Scooter s1 : getAllScooters()) {
				if (s1.getAssignedUser() != null && s1.getAssignedUser().getEmail().equals(us1Email)) {
					User uss = s1.getAssignedUser();
					return (scooterDb.lockScooter(s1, parkEnd) && tripreg.updateTrip(parkEnd, uss));
				}
			}
		}
		return false;

	}

	public double calculateTotalAmountCalories(String bicycleId, double latitudeOrigin, double longitudeOrigin, double latitudeDest, double longitudeDest, String userEmail) {
		Physics physics = new Physics();
		return physics.calculateTotalAmountCalories(bicycleId, latitudeOrigin, longitudeOrigin, latitudeDest, longitudeDest, userEmail);
	}

	public Bicycle findUnlockedBicycleByUser(String user) {

		for (Bicycle b : bikeDb.getAllBicycles()) {

			if (b.getAssignedUser() != null) {

				if (b.getAssignedUser().getName().equals(user) || b.getAssignedUser().getEmail().equals(user)) {
					return b;
				}
			}
		}
		return null;

	}

	public Scooter findUnlockedScooterByUser(String user) {
		for (Scooter s : scooterDb.getAllScooters()) {
			if (s.getAssignedUser() != null) {
				if (s.getAssignedUser().getName().equals(user) || s.getAssignedUser().getEmail().equals(user)) {
					return s;
				}
			}
		}
		return null;

	}

	public List<Bicycle> getNumberOfBicyclesAtPark(String park) {

		List<Bicycle> lb = new ArrayList<>();

		for (Bicycle b : getAllBicycles()) {

			if (b.getAssignedPark() != null) {
				if (b.getAssignedPark().getID().equals(park) && b.getAssignedUser() == null) {

					lb.add(b);
				}
			}
		}

		return lb;

	}

	public List<Bicycle> getNumberOfBicyclesAtPark(double lat, double lon) {
		List<Bicycle> lb = new ArrayList<>();
		Park p = parkDb.getPark(lat, lon);
		if (p != null) {
			for (Bicycle b : getAllBicycles()) {
				if (b.getAssignedPark() != null) {
					if (b.getAssignedPark().getID().equals(p.getID()) && b.getAssignedUser() == null) {

						lb.add(b);
					}
				}
			}

			return lb;

		}
		return lb;

	}

	public List<Scooter> getNumberOfScootersAtPark(String park) {
		List<Scooter> lb = new ArrayList<>();
		for (Scooter s : getAllScooters()) {
			if (s.getAssignedPark() != null) {
				if (s.getAssignedPark().getID().equals(park) && s.getAssignedUser() == null) {

					lb.add(s);
				}
			}
		}

		return lb;

	}

	public List<Scooter> getNumberOfScootersAtPark(double lat, double lon) {
		List<Scooter> lb = new ArrayList<>();
		Park p = parkDb.getPark(lat, lon);
		if (p != null) {
			for (Scooter s : getAllScooters()) {
				if (s.getAssignedPark() != null) {
					if (s.getAssignedPark().getID().equals(p.getID()) && s.getAssignedUser() == null) {

						lb.add(s);
					}
				}
			}

			return lb;

		}
		return lb;

	}

}
