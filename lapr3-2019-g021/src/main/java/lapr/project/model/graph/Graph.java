package lapr.project.model.graph;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author DEI-ESINF
 * @param <V>
 * @param <E>
 */
public class Graph<V, E> implements GraphInterface<V, E> {

        private int numVert;
        private int numEdge;
        private final boolean isDirected;
        private final Map<V, Vertex<V, E>> vertices;  //All Vertices of the graph 

        // Constructs an empty graph (either undirected or directed)
        public Graph(boolean directed) {
                numVert = 0;
                numEdge = 0;
                isDirected = directed;
                vertices = new LinkedHashMap<>();
        }

        @Override
        public int numVertices() {
                return numVert;
        }

        @Override
        public Iterable<V> vertices() {
                return vertices.keySet();
        }

        public boolean validVertex(V vert) {
                return vertices.get(vert) != null;
        }

        public int getKey(V vert) {
                return vertices.get(vert).getKey();
        }

        public V[] allkeyVerts() {
                V vertElem = null;
                for (Vertex<V, E> vert : vertices.values()) {
                        vertElem = vert.getElement();            // To get type
                }
                if (vertElem == null) {
                        return null;
                }
                @SuppressWarnings("unchecked")
                V[] keyverts = (V[]) Array.newInstance(vertElem.getClass(), numVert);
                for (Vertex<V, E> vert : vertices.values()) {
                        keyverts[vert.getKey()] = vert.getElement();
                }
                return keyverts;
        }

        public Iterable<V> adjVertices(V vert) {
                if (!validVertex(vert)) {
                        return null;
                }
                Vertex<V, E> vertex = vertices.get(vert);
                return vertex.getAllAdjVerts();
        }

        @Override
        public int numEdges() {
                return numEdge;
        }

        @Override
        public Iterable<Edge<V, E>> edges() {
                ArrayList<Edge<V, E>> listEdges = new ArrayList<>();
                for (Vertex<V, E> v : this.vertices.values()) {
                        listEdges.addAll((Collection<? extends Edge<V, E>>) v.getAllOutEdges());
                }
                return listEdges;
        }

        @Override
        public Edge<V, E> getEdge(V vOrig, V vDest) {
                if (!validVertex(vOrig) || !validVertex(vDest)) {
                        return null;
                }
                Vertex<V, E> vorig = vertices.get(vOrig);
                return vorig.getEdge(vDest);
        }

        @Override
        public V[] endVertices(Edge<V, E> edge) {
                if (edge == null) {
                        return null;
                }
                if (!validVertex(edge.getVOrig()) || !validVertex(edge.getVDest())) {
                        return null;
                }
                Vertex<V, E> vorig = vertices.get(edge.getVOrig());
                if (!edge.equals(vorig.getEdge(edge.getVDest()))) {
                        return null;
                }
                return edge.getEndpoints();
        }

        @Override
        public V opposite(V vert, Edge<V, E> edge) {
                if (!validVertex(vert)) {
                        return null;
                }
                Vertex<V, E> vertex = vertices.get(vert);
                return vertex.getAdjVert(edge);
        }

        @Override
        public int outDegree(V vert) {
                if (!validVertex(vert)) {
                        return -1;
                }
                Vertex<V, E> vertex = vertices.get(vert);
                return vertex.numAdjVerts();
        }

        @Override
        public int inDegree(V vert) {
                if (!validVertex(vert)) {
                        return -1;
                }
                int degree = 0;
                for (V otherVert : vertices.keySet()) {
                        if (getEdge(otherVert, vert) != null) {
                                degree++;
                        }
                }
                return degree;
        }

        @Override
        public Iterable<Edge<V, E>> outgoingEdges(V vert) {
                if (!validVertex(vert)) {
                        return null;
                }
                Vertex<V, E> vertex = vertices.get(vert);

                return vertex.getAllOutEdges();
        }

        @Override
        public Iterable<Edge<V, E>> incomingEdges(V v) {
                if (!this.isDirected) {
                        return this.outgoingEdges(v);
                }
                ArrayList<Edge<V, E>> listEdges = new ArrayList<>();
                for (V vert : this.vertices.keySet()) {
                        Edge<V, E> v2 = this.getEdge(vert, v);
                        if (v2 != null) {
                                listEdges.add(v2);
                        }
                }
                return listEdges;
        }

        @Override
        public boolean insertVertex(V vert) {
                if (validVertex(vert)) {
                        return false;
                }
                Vertex<V, E> vertex = new Vertex<>(numVert, vert);
                vertices.put(vert, vertex);
                numVert++;
                return true;
        }

        @Override
        public boolean insertEdge(V vOrig, V vDest, E eInf, double eWeight) {
                if (getEdge(vOrig, vDest) != null) {
                        return false;
                }
                if (!validVertex(vOrig)) {
                        insertVertex(vOrig);
                }
                if (!validVertex(vDest)) {
                        insertVertex(vDest);
                }
                Vertex<V, E> vorig = vertices.get(vOrig);
                Vertex<V, E> vdest = vertices.get(vDest);
                Edge<V, E> newEdge = new Edge<>(eInf, eWeight, vorig, vdest);
                vorig.addAdjVert(vDest, newEdge);
                numEdge++;
                //if graph is not direct insert other edge in the opposite direction 
                if (!isDirected) {// if vDest different vOrig
                        if (getEdge(vDest, vOrig) == null) {
                                Edge<V, E> otherEdge = new Edge<>(eInf, eWeight, vdest, vorig);
                                vdest.addAdjVert(vOrig, otherEdge);
                                numEdge++;
                        }
                }
                return true;
        }

        @Override
        public boolean removeVertex(V vert) {
                if (!validVertex(vert)) {
                        return false;
                }
                //remove all edges that point to vert
                for (Edge<V, E> edge : incomingEdges(vert)) {
                        V vadj = edge.getVOrig();
                        removeEdge(vadj, vert);
                }
                Vertex<V, E> vertex = vertices.get(vert);
                //update the keys of subsequent vertices in the map
                for (Vertex<V, E> v : vertices.values()) {
                        int keyVert = v.getKey();
                        if (keyVert > vertex.getKey()) {
                                keyVert = keyVert - 1;
                                v.setKey(keyVert);
                        }
                }
                //The edges that live from vert are removed with the vertex    
                vertices.remove(vert);
                numVert--;
                return true;
        }

        @Override
        public boolean removeEdge(V vOrig, V vDest) {
                if (!validVertex(vOrig) || !validVertex(vDest)) {
                        return false;
                }
                Edge<V, E> edge = getEdge(vOrig, vDest);
                if (edge == null) {
                        return false;
                }
                Vertex<V, E> vorig = vertices.get(vOrig);
                vorig.remAdjVert(vDest);
                numEdge--;
                //if graph is not direct 
                if (!isDirected) {
                        edge = getEdge(vDest, vOrig);
                        if (edge != null) {
                                Vertex<V, E> vdest = vertices.get(vDest);
                                vdest.remAdjVert(vOrig);
                                numEdge--;
                        }
                }
                return true;
        }

        //Returns a clone of the graph 
        @Override
        public Graph<V, E> clone() {
                Graph<V, E> newObject = new Graph<>(this.isDirected);
                //insert all vertices
                for (V vert : vertices.keySet()) {
                        newObject.insertVertex(vert);
                }
                //insert all edges
                for (V vert1 : vertices.keySet()) {
                        for (Edge<V, E> e : this.outgoingEdges(vert1)) {
                                if (e != null) {
                                        V vert2 = this.opposite(vert1, e);
                                        newObject.insertEdge(vert1, vert2, e.getElement(), e.getWeight());
                                }
                        }
                }
                return newObject;
        }

}
