package lapr.project.model;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.data.PathDB;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PathRegistry {

	/**
	 * Connection to the Path Database
	 */
	PathDB pathDb;

	private static final Logger LOGGER = Logger.getLogger("PathRegistryLog");

	/**
	 * Path Registry Constructor
	 *
	 * @param mock
	 */
	public PathRegistry(boolean mock) {
		this.pathDb = new PathDB(mock);
	}

	/**
	 * Gets a Path by its Points
	 *
	 * @param poiA
	 * @param poiB
	 * @return
	 */
	public Path getPath(PointOfInterest poiA, PointOfInterest poiB) {
		return this.getPath(poiA.getLatitude(), poiA.getLongitude(), poiB.getLatitude(), poiB.getLongitude());
	}

	/**
	 * Gets a Path by its Points
	 *
	 * @param latitudeA
	 * @param longitudeA
	 * @param latitudeB
	 * @param longitudeB
	 * @return
	 */
	public Path getPath(double latitudeA, double longitudeA, double latitudeB, double longitudeB) {
		return this.pathDb.getPath(latitudeA, longitudeA, latitudeB, longitudeB);
	}

	/**
	 * Creates a new Path instance to register and validates it
	 *
	 * @param latitudeA
	 * @param longitudeA
	 * @param latitudeB
	 * @param longitudeB
	 * @param kineticCoefficient
	 * @param windDirection
	 * @param windSpeed
	 * @return
	 */
	public Path newPath(double latitudeA, double longitudeA, double latitudeB, double longitudeB, double kineticCoefficient, double windDirection, double windSpeed) {
		Path oPath = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		if (oPath.validatePath()) {
			return oPath;
		}
		return null;
	}

	/**
	 * Adds a given Path instance to the database
	 *
	 * @param oPath
	 * @return
	 */
	public boolean addPath(Path oPath) {
		if (oPath == null) {
			LOGGER.info("Error adding Path: Local Validation Failed");
			return false;
		}
		if (pathDb.validatePath(oPath)) {
			pathDb.addPath(oPath);
			LOGGER.log(Level.INFO, "Path Added");
			return true;
		} else {
			LOGGER.log(Level.INFO, "Error adding Path: Global Validation Failed");
			return false;
		}
	}
	
	public List<Path> getAllPaths() {
		return this.pathDb.getAllPaths();
	}
}
