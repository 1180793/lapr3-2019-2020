package lapr.project.model;

import java.util.Objects;
import static lapr.project.utils.Constants.FRONTAL_AREA_DROP;
import static lapr.project.utils.Constants.ATHMO_PRESSURE;
import static lapr.project.utils.Constants.GRAVITY_ACCELERATION;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Path {

    /**
     * Latitude for the first point.
     */
    private double latitudeA;

    /**
     * Longitude for the first point
     */
    private double longitudeA;

    /**
     * Latitude for the second point.
     */
    private double latitudeB;

    /**
     * Longitude for the second point.
     */
    private double longitudeB;

    /**
     * Kinetic Coefficient.
     */
    private double kineticCoefficient;

    /**
     * Wind Direction.
     */
    private double windDirection;

    /**
     * Wind Speed.
     */
    private double windSpeed;

    private double heightDifference;

    /**
     * Constructor of Path.
     *
     * @param latitudeA
     * @param longitudeA
     * @param latitudeB
     * @param longitudeB
     * @param kineticCoefficient
     * @param windDirection
     * @param windSpeed
     */
    public Path(double latitudeA, double longitudeA, double latitudeB, double longitudeB, double kineticCoefficient, double windDirection, double windSpeed) {
        this.latitudeA = latitudeA;
        this.longitudeA = longitudeA;
        this.latitudeB = latitudeB;
        this.longitudeB = longitudeB;
        this.kineticCoefficient = kineticCoefficient;
        this.windDirection = windDirection;
        this.windSpeed = windSpeed;
    }

    /**
     * @return the latitude for point A
     */
    public double getLatitudeA() {
        return this.latitudeA;
    }

    /**
     * @return the longitude for point A
     */
    public double getLongitudeA() {
        return this.longitudeA;
    }

    /**
     * @return the a POI instance for point A
     */
    public PointOfInterest getPointA() {
        return new PointOfInterest(latitudeA, longitudeA);
    }

    /**
     * @return the latitude for point B
     */
    public double getLatitudeB() {
        return this.latitudeB;
    }

    /**
     * @return the longitude for point B
     */
    public double getLongitudeB() {
        return this.longitudeB;
    }

    /**
     * @return the a POI instance for point B
     */
    public PointOfInterest getPointB() {
        return new PointOfInterest(latitudeB, longitudeB);
    }

    /**
     * @return the kinetic coefficient
     */
    public double getKineticCoefficient() {
        return kineticCoefficient;
    }

    /**
     * @return the wind direction
     */
    public double getWindDirection() {
        return windDirection;
    }

    /**
     * @return the wind speed
     */
    public double getWindSpeed() {
        return this.windSpeed;
    }

    /**
     * Local validation for Path instance
     *
     * @return true if the path is valid
     */
    public boolean validatePath() {
        return validateLatitudes() && validateLongitudes();
    }

    /**
     * Checks if the given latitudes are valid
     *
     * @return true if the latitudes are valid
     */
    private boolean validateLatitudes() {
        return !(this.latitudeA > 90 || this.latitudeA < -90) && !(this.latitudeB > 90 || this.latitudeB < -90);
    }

    /**
     * Checks if the given longitudes are valid
     *
     * @return true if the longitudes are valid
     */
    private boolean validateLongitudes() {
        return !(this.longitudeA > 180 || this.longitudeA < -180) && !(this.longitudeB > 180 || this.longitudeB < -180);
    }

    /**
     * Hash Code method
     *
     * @return hashed path
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.latitudeA, this.longitudeA, this.latitudeB, this.longitudeB);
    }

    /**
     * Equals method
     *
     * @param obj object to be compared
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Path other = (Path) obj;
        return (this.latitudeA == other.latitudeA && this.longitudeA == other.longitudeA && this.latitudeB == other.latitudeB && this.longitudeB == other.longitudeB);
    }
//
//    /**
//     * Calculates the amount of calories burnt given two parks
//     *
//     * @param conn
//     * @param bike
//     * @param user
//     * @return
//     */
//    public double calculateCaloriesBetWeenTwoParks(Path path, Bicycle bike, User user) {
//
//        double userWeight = user.getWeight();
//        double bikeWeight = bike.getWeight();
//        double fR;
//        double energy;
//        double angleDiference;
//        double projection;
//
//        double ckVelvento = this.windSpeed / 3.6;             //vel em m/s
//        double ckVelBicycle = user.getAverageSpeed() / 3.6;
//
//        double distances = Utils.calculateDistance(this.getPointA(), this.getPointB());
//
//        //double hipotenusa = Math.sqrt(Math.pow(this.altitudeDif,2) + Math.pow(distances,2));
//        double inclination = Math.toDegrees(Math.atan(this.heightDifference / distances));
//
//        double brng = calculateBearingBetweenTwoParks(this.getPointA(), this.getPointB());
//
//        angleDiference = Math.abs(brng - this.windDirection);
//        projection = calculateProjection(angleDiference, ckVelvento);
//
//        fR = calculateFAt(bikeWeight, inclination, userWeight) + calculateFG(bikeWeight, inclination, userWeight) + calculateFDrag(projection, ckVelBicycle, bike);
//
//        energy = fR * distances;
//
//        return energy;
//    }
//
//    /**
//     * Calculates the amount of calories burnt given two parks
//     *
//     * @param path
//     * @param scooter
//     * @param user
//     * @return
//     */
//    public double calculateCaloriesBetWeenTwoParks(Path path, Scooter scooter, User user) {
//
//        double userWeight = user.getWeight();
//        double bikeWeight = scooter.getWeight();
//        double fR;
//        double energy;
//        double angleDiference;
//        double projection;
//
//        double ckVelvento = this.windSpeed / 3.6;             //vel em m/s
//        double ckVelBicycle = user.getAverageSpeed() / 3.6;
//
//        double distances = Utils.calculateDistance(this.getPointA(), this.getPointB());    //km para metros
//
//        //double hipotenusa = Math.sqrt(Math.pow(this.altitudeDif,2) + Math.pow(distances,2));
//        double inclination = Math.toDegrees(Math.atan(this.heightDifference / distances));
//
//        double brng = calculateBearingBetweenTwoParks(this.getPointA(), this.getPointB());
//
//        angleDiference = Math.abs(brng - this.windDirection);
//        projection = calculateProjection(angleDiference, ckVelvento);
//
//        fR = calculateFAt(bikeWeight, inclination, userWeight) + calculateFG(bikeWeight, inclination, userWeight) + calculateFDrag(projection, ckVelBicycle, scooter);
//
//        energy = fR * distances * ELET_BICYCLE_PROP;
//
//        return energy;
//    }
//
//    /**
//     * Calculates the bearing between two points
//     *
//     * @param pointOrig
//     * @param pointDest
//     * @return
//     */
//    private double calculateBearingBetweenTwoParks(PointOfInterest pointOrig, PointOfInterest pointDest) {
//
//        double dLng = Math.abs(pointDest.getLongitude() - pointOrig.getLongitude());
//        double y = Math.sin(dLng) * Math.cos(pointDest.getLatitude());
//        double x = Math.cos(pointOrig.getLatitude()) * Math.sin(pointDest.getLatitude()) - Math.sin(pointOrig.getLatitude()) * Math.cos(pointDest.getLatitude()) * Math.cos(dLng);
//        double brng = Math.toDegrees((Math.atan2(y, x)));
//
//        if (brng < 0) {
//            brng = 360 + brng;
//        }
//
//        return brng;
//    }
//
//    /**
//     * Calculates a projection of vetor wind in vetor bicycle
//     *
//     * @param angleDiference
//     * @param ckVelvento
//     * @return
//     */
//    private double calculateProjection(double angleDiference, double ckVelvento) {
//
//        double projection = ckVelvento * (Math.cos(Math.toRadians(angleDiference)));
//        return projection;
//    }
//
//    /**
//     * Calculate the frictional force
//     *
//     * @param bikeWeight
//     * @param inclination
//     * @param userWeight
//     * @return
//     */
//    private double calculateFAt(double bikeWeight, double inclination, double userWeight) {
//        return (userWeight + bikeWeight) * GRAVITY_ACCELERATION * Math.cos(Math.toRadians(inclination)) * this.kineticCoefficient;
//    }
//
//    /**
//     * Calculates the force of drag
//     *
//     * @param projection
//     * @param ckVelBicycle
//     * @param bike
//     * @return
//     */
//    private double calculateFDrag(double projection, double ckVelBicycle, Bicycle bike) {
//        return 0.5 * ATHMO_PRESSURE * FRONTAL_AREA_DROP * bike.getAerodynamicCoefficient() * Math.pow((ckVelBicycle - projection), 2);
//    }
//
//    /**
//     * Calculates the force of drag
//     *
//     * @param projection
//     * @param ckVelBicycle
//     * @param bike
//     * @return
//     */
//    private double calculateFDrag(double projection, double ckVelBicycle, Scooter scooter) {
//        return 0.5 * ATHMO_PRESSURE * FRONTAL_AREA_DROP * scooter.getAerodynamicCoefficient() * Math.pow((ckVelBicycle - projection), 2);
//    }
//
//    /**
//     * force applied to the user and bicycle by gravity
//     *
//     * @param weight
//     * @param inclination
//     * @param userWeight
//     * @return
//     */
//    private double calculateFG(double weight, double inclination, double userWeight) {
//        return (userWeight + weight) * GRAVITY_ACCELERATION * Math.sin(Math.toRadians(inclination));
//    }
}
