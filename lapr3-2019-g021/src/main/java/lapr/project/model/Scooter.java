package lapr.project.model;

import java.util.Objects;

/**
 *
 * @author roger
 */
public class Scooter {

        /**
         * Scooter id
         */
        private String scooterId;

        /**
         * Scooter weight
         */
        private int weight;

        /**
         * Scooter type Types of Scooter: 1. city, 2. off-road
         */
        private int type;

        /**
         * Scooter assigned park
         */
        private Park assignedPark;

        /**
         * Scooter maximum battery capacity
         */
        private double maximumBatteryCapacity;

        /**
         * Scooter actual battery capacity
         */
        private int actualBatteryCapacity;

        /**
         * Scooter aerodynamic coefficient
         */
        private double aerodynamicCoefficient;

        /**
         * Scooter frontal area
         */
        private double frontalArea;

        /**
         * Scooter assigned user
         */
        private User assignedUser;
        
        private int motor;

        public Scooter(String scooterId, int weight, int type, Park park, double maxBatteryCap, int actualBatteryCap, double aeroCoefficient, double frontArea, int motor) {
                this.scooterId = scooterId;
                this.weight = weight;
                this.type = type;
                this.assignedPark = park;
                this.maximumBatteryCapacity = maxBatteryCap;
                this.actualBatteryCapacity = actualBatteryCap;
                this.aerodynamicCoefficient = aeroCoefficient;
                this.frontalArea = frontArea;
                this.assignedUser = null;
                this.motor = motor;
        }
        
        
         /**
         * @return the motor power
         */
        public int getMotor() {
                return this.motor;
        }

        /**
         * @return the id
         */
        public String getID() {
                return this.scooterId;
        }

        /**
         * @return the weight
         */
        public int getWeight() {
                return this.weight;
        }

        /**
         * @return the weight
         */
        public int getType() {
                return this.type;
        }

        /**
         * @return the assigned park
         */
        public Park getAssignedPark() {
                return this.assignedPark;
        }

        /**
         * @return the maximum battery capacity
         */
        public double getMaxBatteryCapacity() {
                return this.maximumBatteryCapacity;
        }

        /**
         * @return the actual battery capacity
         */
        public int getActualBatteryCapacity() {
                return this.actualBatteryCapacity;
        }

        /**
         * @return the aerodynamic coefficient
         */
        public double getAerodynamicCoefficient() {
                return this.aerodynamicCoefficient;
        }

        /**
         * @return the frontal area
         */
        public double getFrontalArea() {
                return this.frontalArea;
        }

        /**
         * @return the assigned user
         */
        public User getAssignedUser() {
                return this.assignedUser;
        }


        /**
         * Sets the assigned user
         *
         * @param newUser
         */
        public void setAssignedUser(User newUser) {
                this.assignedUser = newUser;
        }

        /**
         * Sets the assigned park
         *
         * @param newPark
         */
        public void setAssignedPark(Park newPark) {
                this.assignedPark = newPark;
        }

        public void setWeight(int weight) {
                this.weight = weight;
        }

        public void setType(int type) {
                this.type = type;
        }

        public void setMaximumBatteryCapacity(double maximumBatteryCapacity) {
                this.maximumBatteryCapacity = maximumBatteryCapacity;
        }

        public void setActualBatteryCapacity(int actualBatteryCapacity) {
                this.actualBatteryCapacity = actualBatteryCapacity;
        }

        public void setAerodynamicCoefficient(double aerodynamicCoefficient) {
                this.aerodynamicCoefficient = aerodynamicCoefficient;
        }

        public void setFrontalArea(double frontalArea) {
                this.frontalArea = frontalArea;
        }
        
        public void setMotor(int motor) {
                 this.motor = motor;
        }

        /**
         * Local validation for Scooter instance
         *
         * @return true if the scooter is valid
         */
        public boolean validateScooter() {
                return validateWeight() && validateType() && validateBatteryMaxCapacapity()
                        && validateBatteryActualCapacapity() && validateAerodynamicCoefficient()
                        && validateFrontalArea() && validatePark();
        }

        /**
         * Checks if given weight is valid
         *
         * @return true if the weight is valid
         */
        private boolean validateWeight() {
                return this.weight > 0;
        }

        private boolean validateType() {
                return this.type == 1 || this.type == 2;
        }

        /**
         * Checks if given aerodynamic coefficient is valid
         *
         * @return true if the aerodynamic coefficient is valid
         */
        private boolean validateAerodynamicCoefficient() {
                return this.aerodynamicCoefficient > 0;
        }

        /**
         * Checks if given frontal area is valid
         *
         * @return true if the frontal area is valid
         */
        private boolean validateFrontalArea() {
                return this.frontalArea > 0;
        }

        /**
         * Checks if given maximum battery capacity is valid
         *
         * @return true if the maximum battery capacity is valid
         */
        private boolean validateBatteryMaxCapacapity() {
                return this.maximumBatteryCapacity > 0;

        }

        /**
         * Checks if given actual battery capacity is valid
         *
         * @return true if the actual battery capacity is valid
         */
        private boolean validateBatteryActualCapacapity() {
                return this.actualBatteryCapacity >= 0 && this.actualBatteryCapacity <= 100;
        }

        /**
         * Checks if given park is valid
         *
         * @return true if the park is valid
         */
        private boolean validatePark() {
                return this.assignedPark != null;
        }

        /**
         * Hash Code method
         *
         * @return hashed park
         */
        @Override
        public int hashCode() {
                return Objects.hash(this.scooterId);
        }

        /**
         * Equals method
         *
         * @param obj object to be compared
         * @return boolean
         */
        @Override
        public boolean equals(Object obj) {
                if (this == obj) {
                        return true;
                }
                if (obj == null) {
                        return false;
                }
                if (getClass() != obj.getClass()) {
                        return false;
                }
                final Scooter other = (Scooter) obj;
                return this.scooterId.equals(other.scooterId);
        }

}
