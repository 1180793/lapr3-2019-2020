package lapr.project.model;

import java.util.Objects;
import lapr.project.utils.MD5Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class User {

	/**
	 * User's email
	 */
	private final String email;

	/**
	 * User's name
	 */
	private final String name;

	/**
	 * User's password
	 */
	private final String password;

	/**
	 * User's credit card information
	 */
	private final String creditCard;

	/**
	 * User's gender
	 */
	private final char gender;

	/**
	 * User's height on centimeters
	 */
	private final double height;
	/**
	 * User's weight on kilograms
	 */
	private final double weight;

	/**
	 * Char that represents if the user has Administrator permissions
	 */
	private char isAdmin;

	/**
	 * User's points
	 */
	private int points;

	/**
	 * User's points
	 */
	private final double avgSpeed;

	/**
	 * Constructor for user
	 *
	 * @param strName name
	 * @param strEmail email
	 * @param strPassword password
	 * @param strCreditCard creditcard
	 * @param gender gender
	 * @param dblHeight height
	 * @param dblWeight weight
	 * @param dblAvgSpeed average speed
	 */
	public User(String strEmail, String strName, String strPassword, String strCreditCard, char gender, double dblHeight, double dblWeight, double dblAvgSpeed) {
		this.email = strEmail;
		this.name = strName;
		this.password = MD5Utils.encrypt(strPassword);
		this.creditCard = strCreditCard;
		this.gender = gender;
		this.height = dblHeight;
		this.weight = dblWeight;
		this.isAdmin = 'N';
		this.points = 0;
		this.avgSpeed = dblAvgSpeed;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * @return the creditCard
	 */
	public String getCreditCard() {
		return this.creditCard;
	}

	/**
	 * @return the gender
	 */
	public char getGender() {
		return this.gender;
	}

	/**
	 * @return the height
	 */
	public double getHeight() {
		return this.height;
	}

	/**
	 * @return the weight
	 */
	public double getWeight() {
		return this.weight;
	}

	/**
	 * @return the average speed
	 */
	public double getAverageSpeed() {
		return this.avgSpeed;
	}

	/**
	 * Checks if the User has admin permissions
	 *
	 * @return the isAdmin
	 */
	public boolean isAdmin() {
		return Character.toUpperCase(this.isAdmin) == 'S';
	}

	/**
	 * @return the points
	 */
	public int getPoints() {
		return this.points;
	}

	/**
	 * @param isAdmin the isAdmin to set
	 */
	public void setAdmin(boolean isAdmin) {
		if (isAdmin == true) {
			this.isAdmin = 'S';
		} else {
			this.isAdmin = 'N';
		}
	}

	/**
	 * @param points the points to set
	 */
	public void setPoints(int points) {
		this.points = points;
	}

	/**
	 * Local validation for User instance
	 *
	 * @return true if the user is valid
	 */
	public boolean validateUser() {
		return isValidEmail() && isValidName() && isValidCreditCard()
			&& isValidGender() && isValidPassword() && isValidHeight()
			&& isValidWeight() && isValidAverageSpeed();
	}

	/**
	 * Checks if given username is valid
	 *
	 * @return true if the username is valid
	 */
	private boolean isValidName() {
		return this.name.length() > 0;
	}

	/**
	 * Checks if given email is valid
	 *
	 * @return true if the email is valid
	 */
	private boolean isValidEmail() {
		for (int i = 0; i < this.email.length(); i++) {
			if (this.email.charAt(i) == '@') {
				for (int j = i + 1; j < this.email.length(); j++) {

					if (this.email.charAt(j) == '.') {
						return true;
					} else if (this.email.charAt(j) == '@') {
						return false;
					}
				}
				return false;
			}
		}
		return false;
	}

	/**
	 * Checks if the given password is valid
	 *
	 * @return true if the password is valid
	 */
	private boolean isValidPassword() {
		return !this.password.equals(MD5Utils.encrypt(""));
	}

	/**
	 * Checks if the given creditCard is valid
	 *
	 * @return true if the creditCard is valid
	 */
	private boolean isValidCreditCard() {
		if (this.creditCard.length() != 16) {
			return false;
		}
		for (int i = 0; i < this.creditCard.length(); i++) {
			char c = this.creditCard.charAt(i);
			if (!Character.isDigit(c)) {
				return false;

			}
		}
		return true;
	}

	/**
	 * Checks if the given gender is valid
	 *
	 * @return true if the gender is valid
	 */
	private boolean isValidGender() {
		return Character.toUpperCase(this.gender) == 'F' || Character.toUpperCase(this.gender) == 'M';
	}

	/**
	 * Checks if the given height is valid
	 *
	 * @return true if the height is valid
	 */
	private boolean isValidHeight() {
		return this.height > 0;
	}

	/**
	 * Checks if the given weight is valid
	 *
	 * @return true if the weight is valid
	 */
	private boolean isValidWeight() {
		return this.weight > 0;
	}

	/**
	 * Checks if the given average speed is valid
	 *
	 * @return true if the average speed is valid
	 */
	private boolean isValidAverageSpeed() {
		return this.avgSpeed > 0;
	}

	/**
	 * Hash Code method
	 *
	 * @return hashed user
	 */
	@Override
	public int hashCode() {
		return Objects.hash(this.email);
	}

	/**
	 * Equals method
	 *
	 * @param obj object to be compared
	 * @return boolean
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final User other = (User) obj;
		return this.email.equals(other.email);
	}

}
