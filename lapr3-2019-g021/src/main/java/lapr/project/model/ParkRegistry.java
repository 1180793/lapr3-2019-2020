package lapr.project.model;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.data.ParkDB;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ParkRegistry {

	/**
	 * Connection to the Park Database
	 */
	ParkDB parkDb;

	private static final Logger LOGGER = Logger.getLogger("ParkRegistryLog");

	/**
	 * Park Registry Constructor
	 *
	 * @param mock
	 */
	public ParkRegistry(boolean mock) {
		this.parkDb = new ParkDB(mock);
	}

	/**
	 * Gets a Park by its ID
	 *
	 * @param id
	 * @return
	 */
	public Park getPark(String id) {
		return this.parkDb.getPark(id);
	}

	/**
	 * Gets a Park by its Coordinates
	 *
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	public Park getPark(double latitude, double longitude) {
		return this.parkDb.getPark(latitude, longitude);
	}

	/**
	 * Gets a park by its description.
	 *
	 * @param description
	 * @return
	 */
	public Park getParkByDescription(String description) {
		return this.parkDb.getParkByDescription(description);
	}

	/**
	 * Creates a new Park instance to register and validates it
	 *
	 * @param id
	 * @param latitude
	 * @param description
	 * @param longitude
	 * @param elevation
	 * @param inputCurrent
	 * @param maxBikeCapacity
	 * @param inputVoltage
	 * @param maxScooterCapacity
	 * @return
	 */
	public Park newPark(String id, double latitude, double longitude, int elevation, String description, int maxBikeCapacity, int maxScooterCapacity, double inputVoltage, double inputCurrent) {
		Park oPark = new Park(id, latitude, longitude, elevation, description, maxBikeCapacity, maxScooterCapacity, inputVoltage, inputCurrent);
		if (oPark.validatePark()) {
			return oPark;
		}
		return null;
	}

	/**
	 * Adds a given Park instance to the database
	 *
	 * @param oPark
	 * @return
	 */
	public boolean addPark(Park oPark) {
		if (oPark == null) {
			LOGGER.info("Error adding Park: Local Validation Failed");
			return false;
		}
		if (parkDb.validatePark(oPark)) {
			parkDb.addPark(oPark);
			LOGGER.log(Level.INFO, "Park Added");
			return true;
		} else {
			LOGGER.log(Level.INFO, "Error adding Park: Global Validation Failed");
			return false;
		}
	}

	public boolean removePark(String id) {
                List<Park> parkList = this.getAllParks();
		for (Park park : parkList) {
			if (id.equals(park.getID())) {
                            if(park.getAvailableBikes()==0 && park.getAvailableScooters()==0){
                                return parkDb.removePark(id);
                            }
			}
		}
		return false;
	}

	public List<Park> getAllParks() {

		return this.parkDb.getAllParks();
	}

	public boolean updatePark(Park oldPark, Park p) {
		return parkDb.updatePark(oldPark, p);
	}
}
