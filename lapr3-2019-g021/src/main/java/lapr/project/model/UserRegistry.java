package lapr.project.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.data.UserDB;

/**
 *
 * @author Francisco
 */
public class UserRegistry {

        /**
         * Connection to the User Database
         */
        UserDB udb;

        private static final Logger LOGGER = Logger.getLogger("UserRegistryLog");

        /**
         * User Registry Constructor
         *
         * @param mock
         */
        public UserRegistry(boolean mock) {
                this.udb = new UserDB(mock);
        }

        public User getUserByEmail(String email) {
                for (User user : this.udb.getAllUsers()) {
                        if (user.getEmail().equalsIgnoreCase(email)) {
                                return user;
                        }
                }
                return null;
        }

	public User getUserByUsername(String username) {
                for (User user : this.udb.getAllUsers()) {
                        if (user.getName().equalsIgnoreCase(username)) {
                                return user;
                        }
                }
                return null;
        }
	
        /**
         * Validates if the login has been successful
         *
         * @param email email
         * @param password password
         * @return if the login was successful or not
         */
        public boolean validateLogin(String email, String password) {
                if (udb.validateLogin(email, password)) {
                        LOGGER.info("User Logged In Sucessfully!");
                        return true;
                } else {
                        LOGGER.info("Error Authenticating User!");
                        return false;
                }
        }

        /**
         * Creates a new User instance to register and validates it
         *
         * @param email
         * @param name
         * @param password
         * @param creditCard
         * @param gender
         * @param height
         * @param weight
         * @param avgSpeed
         * @return
         */
        public User newUser(String email, String name, String password, String creditCard, char gender, double height, double weight, double avgSpeed) {
                User oUser = new User(email, name, password, creditCard, gender, height, weight, avgSpeed);
                if (oUser.validateUser()) {
                        return oUser;
                }
                return null;
        }

        /**
         * Registers a given User instance
         *
         * @param oUser
         * @return
         */
        public boolean registerUser(User oUser) {
                if (oUser == null) {
                        LOGGER.info("Error registering User: Local Validation Failed");
                        return false;
                }
                if (udb.validateUser(oUser)) {
                        udb.addUser(oUser);
                        LOGGER.log(Level.INFO, "User(''{0}'') Registered", oUser.getEmail());
                        return true;
                } else {
                        LOGGER.log(Level.INFO, "Error registering User(''{0}''): Global Validation Failed", oUser.getEmail());
                        return false;
                }
        }
}
