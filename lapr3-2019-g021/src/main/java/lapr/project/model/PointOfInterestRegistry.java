package lapr.project.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.data.PointOfInterestDB;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PointOfInterestRegistry {
        
        /**
         * Connection to the User Database
         */
        PointOfInterestDB poidb;

        private static final Logger LOGGER = Logger.getLogger("POIRegistryLog");
        
        /**
         * User Registry Constructor
         *
         * @param mock
         */
        public PointOfInterestRegistry(boolean mock) {
                this.poidb = new PointOfInterestDB(mock);
        }
        
        public PointOfInterest getPOI(double latitude, double longitude) {
                return this.poidb.getPOI(latitude, longitude);
        }
        
        /**
         * Creates a new POI instance to register and validates it
         *
         * @param latitude
         * @param longitude
         * @param altitude
         * @param name
         * @return
         */
        public PointOfInterest newPOI(double latitude, double longitude, int altitude, String name) {
                PointOfInterest oPOI = new PointOfInterest(latitude, longitude, altitude, name);
                if (oPOI.validatePOI()) {
                        return oPOI;
                }
                return null;
        }
        
        /**
         * Adds a given POI instance to the database
         *
         * @param oPOI
         * @return
         */
        public boolean addPOI(PointOfInterest oPOI) {
                if (oPOI == null) {
                        LOGGER.info("Error adding POI: Local Validation Failed");
                        return false;
                }
                if (poidb.validatePOI(oPOI)) {
                        poidb.addPOI(oPOI);
                        LOGGER.log(Level.INFO, "POI Added");
                        return true;
                } else {
                        LOGGER.log(Level.INFO, "Error adding POI: Global Validation Failed");
                        return false;
                }
        }
}
