package lapr.project.model;

import java.util.Objects;

public class Bicycle {

    /**
     * Bicycle id
     */
    private String bicycleId;

    /**
     * Bicycle weight
     */
    private int weight;

    /**
     * Bicycle assigned park
     */
    private Park assignedPark;

    /**
     * Bicycle aerodynamic coefficient
     */
    private double aerodynamicCoefficient;

    /**
     * Bicycle frontal area
     */
    private double frontalArea;

    /**
     * Bicycle wheel size
     */
    private int wheelSize;

    /**
     * Bicycle assigned user
     */
    private User assignedUser;

    public Bicycle(String bicycleId, int weight, Park park, double aeroCoefficient, double frontArea, int size) {
        this.bicycleId = bicycleId;
        this.weight = weight;
        this.assignedPark = park;
        this.aerodynamicCoefficient = aeroCoefficient;
        this.frontalArea = frontArea;
        this.wheelSize = size;
        this.assignedUser = null;
    }

    /**
     * @return the id
     */
    public String getID() {
        return this.bicycleId;
    }

    /**
     * @return the weight
     */
    public int getWeight() {
        return this.weight;
    }

    /**
     * @return the assigned park
     */
    public Park getAssignedPark() {
        return this.assignedPark;
    }

    /**
     * @return the aerodynamic coefficient
     */
    public double getAerodynamicCoefficient() {
        return this.aerodynamicCoefficient;
    }

    /**
     * @return the frontal area
     */
    public double getFrontalArea() {
        return this.frontalArea;
    }

    /**
     * @return the wheel size
     */
    public int getWheelSize() {
        return this.wheelSize;
    }

    /**
     * @return the assigned user
     */
    public User getAssignedUser() {
        return this.assignedUser;
    }

    /**
     * Sets the assigned user
     *
     * @param newUser
     */
    public void setAssignedUser(User newUser) {
        this.assignedUser = newUser;
    }

    /**
     * Sets the assigned park
     *
     * @param newPark
     */
    public void setAssignedPark(Park newPark) {
        this.assignedPark = newPark;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setAerodynamicCoefficient(double aerodynamicCoefficient) {
        this.aerodynamicCoefficient = aerodynamicCoefficient;
    }

    public void setFrontalArea(double frontalArea) {
        this.frontalArea = frontalArea;
    }

    public void setWheelSize(int wheelSize) {
        this.wheelSize = wheelSize;
    }

    /**
     * Local validation for Bicycle instance
     *
     * @return true if the bicycle is valid
     */
    public boolean validateBicycle() {
        return validateWeight() && validateAerodynamicCoefficient()
                && validateFrontalArea() && validateSize()
                && validatePark();
    }

    /**
     * Checks if given weight is valid
     *
     * @return true if the weight is valid
     */
    private boolean validateWeight() {
        return this.weight > 0;
    }

    /**
     * Checks if given aerodynamic coefficient is valid
     *
     * @return true if the aerodynamic coefficient is valid
     */
    private boolean validateAerodynamicCoefficient() {
        return this.aerodynamicCoefficient > 0;
    }

    /**
     * Checks if given frontal area is valid
     *
     * @return true if the frontal area is valid
     */
    private boolean validateFrontalArea() {
        return this.frontalArea > 0;
    }

    /**
     * Checks if given wheel size is valid
     *
     * @return true if the wheel size is valid
     */
    private boolean validateSize() {
        return this.wheelSize > 0;
    }

    /**
     * Checks if given park is valid
     *
     * @return true if the park is valid
     */
    private boolean validatePark() {
        return this.assignedPark != null;
    }

    /**
     * Hash Code method
     *
     * @return hashed park
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.bicycleId);
    }

    /**
     * Equals method
     *
     * @param obj object to be compared
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bicycle other = (Bicycle) obj;
        return this.bicycleId.equals(other.bicycleId);
    }

//    /**
//     * calculates the ammount of energy needed to travel beetewen two points
//     *
//     * @param connection path with point origin and destination
//     * @param user user to travel
//     * @return
//     */
//    public double getElectricalEnergyBetweenTwoParks(Path path, User user) {
//        double energy = path.calculateCaloriesBetWeenTwoParks(path, this, user);
//
//        double energyBikeHasToMake = (energy / (1 - Constants.ELET_BICYCLE_PROP)) * Constants.ELET_BICYCLE_PROP;
//        return (energyBikeHasToMake / ((double) Constants.BICYCLE_EFFICIENCY / 100));
//
//    }
}
