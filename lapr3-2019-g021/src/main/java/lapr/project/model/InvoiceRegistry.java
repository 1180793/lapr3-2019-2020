package lapr.project.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.data.UserDB;
import lapr.project.data.InvoiceDB;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class InvoiceRegistry {
	
	/**
	 * Connection to the Invoice Database
	 */
	InvoiceDB invoiceDb;
	
	/**
	 * Connection to the User Database
	 */
	UserDB userDb;

	private static final Logger LOGGER = Logger.getLogger("InvoiceRegistryLog");

	/**
	 * Invoice Registry Constructor
	 *
	 * @param mock
	 */
	public InvoiceRegistry(boolean mock) {
		this.invoiceDb = new InvoiceDB(mock);
		this.userDb = new UserDB(mock);
	}

	/**
	 * Gets a sequential new number for Invoice ID
	 *
	 * @return
	 */
	public int getNewInvoiceNumber() {
		return this.invoiceDb.getNewInvoiceNumber();
	}

	/**
	 * Issues a new User Invoice for specified month
	 *
	 * @param month
	 * @param username
	 * @return
	 */
	public Invoice issueInvoice(int month, String username) {
		User user = this.userDb.getUserByUsername(username);
		if (user == null) {
			LOGGER.info("Error issuing Invoice: User not Found!");
			return null;
		}
		LOGGER.log(Level.INFO, "Found User: {0}", user.getName());
		return this.invoiceDb.issueInvoice(month, user);
	}
	
}
