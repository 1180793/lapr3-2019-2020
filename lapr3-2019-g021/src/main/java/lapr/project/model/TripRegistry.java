/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import lapr.project.data.TripDB;
import lapr.project.utils.Utils;

/**
 *
 * @author roger40
 */
public class TripRegistry {

	TripDB tripdb;

	private static final Logger LOGGER = Logger.getLogger("TripRegistry");

	/**
	 * Park Registry Constructor
	 *
	 * @param mock
	 */
	public TripRegistry(boolean mock) {
		this.tripdb = new TripDB(mock);

	}

	public List<Trip> getAllTrips() {
		return tripdb.getAllTrips();

	}

	public boolean addTrip(String vehID, String type, Park porigin, User us1) { // CREATES A NEW TRIP when vehicle gets unlocked
		
		Trip trip = new Trip(vehID, type, porigin, us1, generateTripNumber());
		
		return tripdb.addTrip(trip);

	}

	public boolean updateTrip(Park pdest, User us1) {
		return tripdb.updateTrip(pdest, us1);
	}

	public long getUnlockedTime(String vehicleId) {
		return tripdb.getUnlockedTime(vehicleId);

	}

	public int generateTripNumber() {
		return tripdb.getAllTrips().size() + 1;

	}

	public List<Trip> getUserMonthTrips(String username, int month) {
		List<Trip> listOfTripsByUserAndMonth = new ArrayList<>();
		for (Trip t : getAllTrips()) {
			if (t.getDateLocked() != null && t.getDateLocked().getMonth() + 1 == month && t.getAssignedUser().getName().equalsIgnoreCase(username) && t.getInvoiceId() == 0) {
				listOfTripsByUserAndMonth.add(t);
			}
		}
		return listOfTripsByUserAndMonth;
	}
	
	public List<Trip> getUnpaidTrips(String username) {
		return tripdb.getUnpaidTrips(username);
	}

	public List<Trip> getAllTripsFromUser(String username) {
		return tripdb.getAllTripsFromUser(username);
	}
}
