package lapr.project.model;

import java.util.Objects;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Park {

	/**
	 * Park id
	 */
	private String id;

	/**
	 * Park latitude
	 */
	private double latitude;

	/**
	 * Park longitude
	 */
	private double longitude;

	/**
	 * Park elevation
	 */
	private int elevation;

	/**
	 * Park description
	 */
	private String description;

	/**
	 * Park maximum bicycle capacity
	 */
	private int maxBikeCapacity;

	/**
	 * Park maximum scooter capacity
	 */
	private int maxScooterCapacity;

	/**
	 * Park input voltage
	 */
	private double inputVoltage;

	/**
	 * Park input current
	 */
	private double inputCurrent;

	/**
	 * Park available bikes
	 */
	private int availableBikes;

	/**
	 * Park available scooters
	 */
	private int availableScooters;

	public Park(String id, double lat, double lon, int elev, String desc, int maxBCap, int maxSCap, double volt, double current) {
		this.id = id;
		this.latitude = lat;
		this.longitude = lon;
		this.elevation = elev;
		this.description = desc;
		this.maxBikeCapacity = maxBCap;
		this.maxScooterCapacity = maxSCap;
		this.inputVoltage = volt;
		this.inputCurrent = current;
		this.availableBikes = 0;
		this.availableScooters = 0;
	}

	/**
	 * @return a POI instance with the Park data
	 */
	public PointOfInterest getPOI() {
		return new PointOfInterest(this.latitude, this.longitude, this.elevation, this.description);
	}

	/**
	 * @return the id
	 */
	public String getID() {
		return this.id;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return this.latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return this.longitude;
	}

	/**
	 * @return the elevation
	 */
	public int getElevation() {
		return this.elevation;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * @return the max bicycle capacity
	 */
	public int getMaxBikeCapacity() {
		return this.maxBikeCapacity;
	}

	/**
	 * @return the max scooter capacity
	 */
	public int getMaxScooterCapacity() {
		return this.maxScooterCapacity;
	}

	/**
	 * @return the input voltage
	 */
	public double getInputVoltage() {
		return this.inputVoltage;
	}

	/**
	 * @return the input current
	 */
	public double getInputCurrent() {
		return this.inputCurrent;
	}

	/**
	 * @return the available bicycles
	 */
	public int getAvailableBikes() {
		return this.availableBikes;
	}

	/**
	 * @return the available scooters
	 */
	public int getAvailableScooters() {
		return this.availableScooters;
	}

	/**
	 * Sets the available bicycles
	 *
	 * @param availableBikes
	 */
	public void setAvailableBikes(int availableBikes) {
		this.availableBikes = availableBikes;
	}

	/**
	 * Sets the available bicycles
	 *
	 * @param availableScooters
	 */
	public void setAvailableScooters(int availableScooters) {
		this.availableScooters = availableScooters;
	}

	/**
	 * Local validation for Park instance
	 *
	 * @return true if the park is valid
	 */
	public boolean validatePark() {
		return validateLatitude() && validateLongitude() && validateDescription()
			&& validateBikeMaxCapacity() && validateScooterMaxCapacity() && validateInputVoltage() && validateInputCurrent();
	}

	/**
	 * Checks if given latitude is valid
	 *
	 * @return true if the latitude is valid
	 */
	private boolean validateLatitude() {
		return !(this.latitude > 90 || this.latitude < -90);
	}

	/**
	 * Checks if given longitude is valid
	 *
	 * @return true if the longitude is valid
	 */
	private boolean validateLongitude() {
		return !(this.longitude > 180 || this.longitude < -180);
	}

	/**
	 * Checks if given description is valid
	 *
	 * @return true if the description is valid
	 */
	private boolean validateDescription() {
		return !this.description.equalsIgnoreCase("");
	}

	/**
	 * Checks if given bicycle max capacity is valid
	 *
	 * @return true if the bicycle max capacity is valid
	 */
	private boolean validateBikeMaxCapacity() {
		return this.maxBikeCapacity >= 0;
	}

	/**
	 * Checks if given scooter max capacity is valid
	 *
	 * @return true if the scooter max capacity is valid
	 */
	private boolean validateScooterMaxCapacity() {
		return this.maxScooterCapacity >= 0;
	}

	/**
	 * Checks if given input voltage is valid
	 *
	 * @return true if the input voltage is valid
	 */
	private boolean validateInputVoltage() {
		return this.inputVoltage >= 0;
	}

	/**
	 * Checks if given input current is valid
	 *
	 * @return true if the input current is valid
	 */
	private boolean validateInputCurrent() {
		return this.inputCurrent >= 0;
	}

	/**
	 * Hash Code method
	 *
	 * @return hashed park
	 */
	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

	/**
	 * Equals method
	 *
	 * @param obj object to be compared
	 * @return boolean
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Park other = (Park) obj;
		return this.id.equals(other.id);
	}

	/**
	 * @return the number of available bicycles parking slots
	 */
	public int getAvailableBicycleParkingSlots() {
		return maxBikeCapacity - availableBikes;
	}

	/**
	 * @return the number of available scooters parking slots
	 */
	public int getAvailableScooterParkingSlots() {
		return maxScooterCapacity - availableScooters;
	}
}
