package lapr.project.model;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PointOfInterest {

        /**
         * Point's location
         */
        private final double latitude;

        /**
         * Point's longitude
         */
        private final double longitude;

        /**
         * Point's altitude
         */
        private final int altitude;

        /**
         * Point's name
         */
        private final String name;

        /**
         * Constructor of Point Of Interest
         *
         * @param name point name
         * @param latitude point latitude
         * @param longitude point longitude
         * @param altitude point altitude
         */
        public PointOfInterest(double latitude, double longitude, int altitude, String name) {
                this.latitude = latitude;
                this.longitude = longitude;
                this.altitude = altitude;
                this.name = name;
        }

        /**
         * Constructor of Point Of Interest without altitude
         *
         * @param latitude point latitude
         * @param longitude point longitude
         * @param name point name
         */
        public PointOfInterest(double latitude, double longitude, String name) {
                this.latitude = latitude;
                this.longitude = longitude;
                this.altitude = 0;
                this.name = name;
        }

        /**
         * Constructor of Point Of Interest without name
         *
         * @param latitude point latitude
         * @param longitude point longitude
         * @param altitude point altitude
         */
        public PointOfInterest(double latitude, double longitude, int altitude) {
                this.latitude = latitude;
                this.longitude = longitude;
                this.altitude = altitude;
                this.name = "";
        }

        /**
         * Constructor of Point Of Interest without name and altitude
         *
         * @param latitude point latitude
         * @param longitude point longitude
         */
        public PointOfInterest(double latitude, double longitude) {
                this.latitude = latitude;
                this.longitude = longitude;
                this.altitude = 0;
                this.name = "";
        }

        /**
         * @return the name
         */
        public String getName() {
                return this.name;
        }

        /**
         * @return the latitude
         */
        public double getLatitude() {
                return this.latitude;
        }

        /**
         * @return the longitude
         */
        public double getLongitude() {
                return this.longitude;
        }

        /**
         * @return the altitude
         */
        public int getAltitude() {
                return this.altitude;
        }

        /**
         * Validate Point Of Interest
         *
         * @return boolean representing the validation
         */
        public boolean validatePOI() {
                return validateLatitude() && validateLongitude();
        }

        /**
         * Validate latitude
         *
         * @return boolean
         */
        private boolean validateLatitude() {
                return this.latitude >= -90 && this.latitude <= 90;
        }

        /**
         * Validate name
         *
         * @return boolean
         */
        private boolean validateLongitude() {
                return this.longitude >= -180 && this.longitude <= 180;
        }

        /**
         * Hash Code method
         *
         * @return hashed POI
         */
        @Override
        public int hashCode() {
                int hash = 7;
                hash = 47 * hash + (int) (Double.doubleToLongBits(this.latitude) ^ (Double.doubleToLongBits(this.latitude) >>> 32));
                hash = 47 * hash + (int) (Double.doubleToLongBits(this.longitude) ^ (Double.doubleToLongBits(this.longitude) >>> 32));
                return hash;
        }

        /**
         * Equals method
         *
         * @param obj object to be compared
         * @return boolean
         */
        @Override
        public boolean equals(Object obj) {
                if (this == obj) {
                        return true;
                }
                if (obj == null) {
                        return false;
                }
                if (getClass() != obj.getClass()) {
                        return false;
                }
                final PointOfInterest other = (PointOfInterest) obj;
                return Double.compare(this.latitude, other.latitude) == 0 && Double.compare(this.longitude, other.longitude) == 0;
        }

        /**
         * Calculates the distance between two points on earth using haversine formula Retired and adapted from https://medium.com/allthingsdata/java-implementation-of-haversine-formula-for-distance-calculation-between-two-points-a3af9562ff1
         *
         * @param l the final desired location
         * @return the distance between the two points
         */
        public double calculateDistance(PointOfInterest point) {

                if (Double.compare(this.latitude, point.latitude) == 0 && Double.compare(this.longitude, point.longitude) == 0) {
                        return 0.0;
                } else {
                        double latDistance = toRad(point.latitude - this.latitude);
                        double lonDistance = toRad(point.longitude - this.longitude);

                        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                                + Math.cos(toRad(this.latitude)) * Math.cos(toRad(point.latitude))
                                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

                        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

                        return 6371 * c;
                }
        }

        /**
         * this method converts degrees to radians
         *
         * @param value angle in degrees
         * @return angle in radians
         */
        private static double toRad(double value) {
                return value * Math.PI / 180;
        }

}
