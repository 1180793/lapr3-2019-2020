package lapr.project.controller;

import lapr.project.model.Park;
import lapr.project.model.ParkRegistry;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AddParkController {

	/**
	 * Park Registry
	 */
	ParkRegistry parkRegistry;

	/**
	 * New Park instance
	 */
	Park oPark;

	/**
	 * Constructor for the Controller
	 *
	 * @param mock represents if it is for testing purposes
	 */
	public AddParkController(boolean mock) {
		parkRegistry = new ParkRegistry(mock);
	}

	/**
	 * Sets the data for the Park to be added
	 *
	 * @param id
	 * @param latitude
	 * @param description
	 * @param longitude
	 * @param elevation
	 * @param inputCurrent
	 * @param maxBikeCapacity
	 * @param inputVoltage
	 * @param maxScooterCapacity
	 * @return true if the Park passed the local validation
	 */
	public boolean newPark(String id, double latitude, double longitude, int elevation, String description, int maxBikeCapacity, int maxScooterCapacity, double inputVoltage, double inputCurrent) {
		this.oPark = parkRegistry.newPark(id, latitude, longitude, elevation, description, maxBikeCapacity, maxScooterCapacity, inputVoltage, inputCurrent);
		return this.oPark != null;
	}

	/**
	 * Adds the Park to the database
	 *
	 * @return if the Park was added or not
	 */
	public boolean addPark() {
		return parkRegistry.addPark(this.oPark);
	}
}
