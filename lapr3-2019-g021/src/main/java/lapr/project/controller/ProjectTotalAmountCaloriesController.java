package lapr.project.controller;

import lapr.project.model.Bicycle;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author Joao Mata <1151352@isep.ipp.pt>
 */
public class ProjectTotalAmountCaloriesController {

    /**
     * Vehicle Registry
     */
    VehicleRegistry vehicleRegistry;

    /**
     * Constructor for the Controller
     *
     * @param mock represents if it is for testing purposes
     */
    public ProjectTotalAmountCaloriesController(boolean mock) {
        vehicleRegistry = new VehicleRegistry(mock);
    }

    public double calculateTotalAmountCalories(String bicycleId, double latitudeOrigin, double longitudeOrigin, double latitudeDest, double longitudeDest, String user) {
        return vehicleRegistry.calculateTotalAmountCalories(bicycleId, latitudeOrigin, longitudeOrigin, latitudeDest, longitudeDest, user);
    }
    
    
}
