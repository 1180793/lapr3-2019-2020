package lapr.project.controller;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import lapr.project.model.Park;
import lapr.project.model.ParkRegistry;
import lapr.project.model.PointOfInterest;

/**
 *
 * @author Francisco
 */
public class NearestParksController {

    private PointOfInterest point;
    private final ParkRegistry reg;
    private final int radius ;

    /**
     * Constructor of the Nearest Parks Controller.
     *
     * @param mock
     */
    public NearestParksController(boolean mock) {
        this.reg = new ParkRegistry(mock);
        this.radius = 1000;//Integer.MAX_VALUE;//1000; //1km
    }

    /**
     * Constructor of the Nearest Parks Controller with radius.
     *
     * @param mock
     * @param radius
     */
    public NearestParksController(boolean mock, int radius) {
        this.reg = new ParkRegistry(mock);
        this.radius = radius;
    }

    /**
     * Creates a new point of interest with only latitude and longitude.
     *
     * @param lat
     * @param lon
     */
    public void newPointOfInterest(double lat, double lon) {
        point = new PointOfInterest(lat, lon);
    }

    /**
     * this method returns the list with the maximum of 5 nearest parks from the
     * user location
     *
     * @return list with nearest parks
     */
    public Map<Park, Double> getNearestParksByCoordinates() {

        List<Park> allParks = reg.getAllParks();
        Map<Park, Double> map = new HashMap<>();

        for (Park park : allParks) {
            PointOfInterest pnt = new PointOfInterest(park.getLatitude(), park.getLongitude());
            double dist = point.calculateDistance(pnt);

            if (dist <= radius) {
                map.put(park, dist);
            }
        }

        return getSortedMap(map);
    }

    /**
     * sorts the map by ascending order of distance and returns a map with the
     * maximum of 5 nearest parks
     *
     * @param parksDistance map unsorted with the park and their distances
     * @return sorted map with the maximum of 5 parks sorted by distance
     */
    private static Map<Park, Double> getSortedMap(Map<Park, Double> parksDistance) {
        SortedMap v = new SortedMap(parksDistance);
        Map<Park, Double> sortedParks = new TreeMap<>(v);
        sortedParks.putAll(parksDistance);

        Iterator<Park> iterator = sortedParks.keySet().iterator();

        Map<Park, Double> res = new TreeMap<>(v);
        for (int i = 0; i < 5; i++) {
            if (iterator.hasNext()) {
                Park p = iterator.next();

                res.put(p, Math.round(parksDistance.get(p) * 1000.0) / 1000.0);

            }

        }
        return res;
    }

    /**
     * nested class that implements Comparator to sort the map with the parks
     * and their distances
     */
    public static class SortedMap implements Comparator<Park> {

        private Map<Park, Double> base;

        /**
         * constructor of the class
         *
         * @param base base map to sort
         */
        public SortedMap(Map<Park, Double> base) {
            this.base = base;

        }

        /**
         * override of compare
         *
         * @param a park to compare with other
         * @param b park to compare with other
         * @return if the parks are sorted
         */
        @Override
        public int compare(Park a, Park b) {
            if (base.get(a) <= base.get(b)) {
                return -1;
            } else {
                return 1;
            }
        }

    }

    /**
     * @return point of interest.
     */
    public PointOfInterest getPointOfInterest() {
        return point;
    }
}
