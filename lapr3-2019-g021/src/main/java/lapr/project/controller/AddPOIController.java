package lapr.project.controller;

import lapr.project.model.PointOfInterest;
import lapr.project.model.PointOfInterestRegistry;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AddPOIController {

        /**
         * POI Registry
         */
        PointOfInterestRegistry poiRegistry;

        /**
         * New POI instance
         */
        PointOfInterest oPoi;

        /**
         * Constructor for the Controller
         *
         * @param mock represents if it is for testing purposes
         */
        public AddPOIController(boolean mock) {
                poiRegistry = new PointOfInterestRegistry(mock);
        }

        /**
         * Sets the data for the POI to be added
         *
         * @param latitude
         * @param longitude
         * @param altitude
         * @param name
         * @return
         */
        public boolean newPOI(double latitude, double longitude, int altitude, String name) {
                this.oPoi = poiRegistry.newPOI(latitude, longitude, altitude, name);
                return this.oPoi != null;
        }

        /**
         * Adds the POI to the database
         *
         * @return if the POI was added or not
         */
        public boolean addPOI() {
                return poiRegistry.addPOI(this.oPoi);
        }
}
