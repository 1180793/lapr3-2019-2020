
package lapr.project.controller;

import lapr.project.model.User;
import lapr.project.model.UserRegistry;

/**
 *
 * @author Francisco
 */
public class LoginController {
    
    /**
     * Email of the user
     */
    String email;
    
    /**
     * Password of the user
     */
    String password;
    
    /**
     * Registry of users
     */
    UserRegistry reg;

    /**
     * Constructor of the Login controller
     *
     * @param email
     * @param password
     * @param mock
     */
    public LoginController(String email, String password, boolean mock)
    {
        this.email = email;
        this.password = password;
        reg = new UserRegistry(mock);
    }

    /**
     * Gets the User that is being logged in
     * @return the user logged in
     */
    public User loginUser() {
       
        boolean test = reg.validateLogin(email, password);

        if (test) {
            return reg.getUserByEmail(email);
        }
        
        return null;
    }
}
