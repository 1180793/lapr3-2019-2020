package lapr.project.controller;

import java.util.List;
import lapr.project.model.Bicycle;
import lapr.project.model.ParkRegistry;
import lapr.project.model.Scooter;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author Omen
 */
public class AvailableVehiclesController {
    
    /**
     * Park Registry
     */
    ParkRegistry parkRegistry;
    
    VehicleRegistry vehicleRegistry;

    /**
     * Constructor for the Controller
     *
     * @param mock represents if it is for testing purposes
     */
    public AvailableVehiclesController(boolean mock) {
        parkRegistry = new ParkRegistry(mock);
        vehicleRegistry = new VehicleRegistry(mock);
    }
    
    public List<Bicycle> getAvailableBicycles(String id){
        List<Bicycle> list = vehicleRegistry.getAvailableBicycles(id);
        return list;
    }
    
    public List<Scooter> getAvailableScooters(String id){
        List<Scooter> list = vehicleRegistry.getAvailableScooters(id);
        return list;
    }
    
    public List<Bicycle> getAvailableBicycles(double lat, double lon){
        List<Bicycle> list = vehicleRegistry.getAvailableBicycles(lat, lon);
        return list;
    }
    
    public List<Scooter> getAvailableScooters(double lat, double lon){
        List<Scooter> list = vehicleRegistry.getAvailableScooters(lat, lon);
        return list;
    } 
}
