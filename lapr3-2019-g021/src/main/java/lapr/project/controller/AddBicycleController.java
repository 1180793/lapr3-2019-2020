package lapr.project.controller;

import lapr.project.model.Bicycle;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author roger40
 */
public class AddBicycleController {

        /**
         * Vehicle Registry
         */
        VehicleRegistry vehicleRegistry;

        /**
         * New Bicycle instance
         */
        Bicycle oBicycle;
        
        /**
         * Constructor for the Controller
         *
         * @param mock represents if it is for testing purposes
         */
        public AddBicycleController(boolean mock) {
             
                vehicleRegistry = new VehicleRegistry(mock);
        }

        /**
         * Sets the data for the Bicycle to be added
         *
         * @param bicycleId
         * @param weight
         * @param latitude
         * @param longitude
         * @param aeroCoefficient
         * @param frontArea
         * @param size
         * @return true if the Bicycle passed the local validation
         */
        public boolean newBicycle(String bicycleId, int weight, double latitude, double longitude, double aeroCoefficient, double frontArea, int size) {
                this.oBicycle = vehicleRegistry.newBicycle(bicycleId, weight, latitude, longitude, aeroCoefficient, frontArea, size);
                return this.oBicycle != null;
        }
        
        /**
         * Adds the Bicycle to the database
         *
         * @return if the Bicycle was added or not
         */
        public boolean addBicycle() {
                return vehicleRegistry.addBicycle(this.oBicycle);
        }

}
