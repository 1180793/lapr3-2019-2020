package lapr.project.controller;

import java.sql.SQLException;
import lapr.project.model.ParkRegistry;
import lapr.project.model.Park;

/**
 *
 * @author danie
 */
public class UpdateParkController {

        private ParkRegistry pr;
        private Park oldPark;

        /**
         *
         * @param teste
         */
        public UpdateParkController(boolean teste) {
                pr = new ParkRegistry(teste);
        }

        /**
         * giving the park id the method returns the park
         *
         * @param oldName
         * @return
         * @throws SQLException
         */
        public Park getParkById(String idAntigo) throws SQLException {
                oldPark = pr.getPark(idAntigo);
                return oldPark;
        }
        
         public void newPark(String id, double lat, double lon, int elev, String desc, int maxBCap, int maxSCap, double volt, double current) {
                Park p = new Park(id, lat, lon, elev, desc, maxBCap, maxSCap, volt, current);
                pr.updatePark(oldPark, p);
        }  
}
