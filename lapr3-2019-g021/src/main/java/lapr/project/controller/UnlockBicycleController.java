package lapr.project.controller;
import lapr.project.model.VehicleRegistry;


import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Scooter;
import lapr.project.model.User;
import lapr.project.model.VehicleRegistry;
//
///**
// *
// * @author roger40
// */
public class UnlockBicycleController {

    VehicleRegistry vhr;

    private final String user1;
    private final VehicleRegistry ver;
    private final  Park parkOrigin;

    
      public UnlockBicycleController(String us1, boolean mock) {
        this.user1 = us1;
        this.parkOrigin=null;
        ver = new VehicleRegistry(mock);

    }
    
    
    public List<Bicycle> suggestBicycles(String parkOrigin) {
        return ver.getAvailableBicycles(parkOrigin);

    }

 

    public boolean unlockBicycleByUser(String bike1) {         //unlocm specific bike
         
        
         return  ver.unlockBicycle(bike1, user1);
          
    
    }

    
    
    
    
    
}
