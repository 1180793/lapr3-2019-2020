package lapr.project.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import lapr.project.model.Park;
import lapr.project.model.ParkRegistry;
import lapr.project.model.Path;
import lapr.project.model.PointOfInterest;
import lapr.project.model.graph.*;
import lapr.project.utils.GraphUtil;

/**
 *
 * @author Francisco
 */
public class ShortestRouteTwoParksController {

	private PointOfInterest initialPOI;
	private PointOfInterest finalPOI;
	private final GraphUtil algorithm;
	private final ParkRegistry pdb;
	private final Graph<PointOfInterest, Path> graph;
	private double dist;

	public ShortestRouteTwoParksController(boolean mock) {

		this.algorithm = new GraphUtil(mock);
		this.pdb = new ParkRegistry(mock);
		this.graph = algorithm.createDistanceGraph();

	}

	public boolean validateParks(String startPark, String endPark) throws SQLException {
		boolean res = true;

		Park parkStart = pdb.getPark(startPark);
		Park parkEnd = pdb.getPark(endPark);

		if (parkStart == null) {
			return false;
		}

		if (parkEnd == null) {
			return false;
		}

		initialPOI = new PointOfInterest(parkStart.getLatitude(), parkStart.getLongitude());
		finalPOI = new PointOfInterest(parkEnd.getLatitude(), parkEnd.getLongitude());

		return res;
	}

	public void insertCoordinates(double originLatitudeInDegrees, double originLongitudeInDegrees, double destinyLatitudeInDegrees, double destinyLongitudeInDegrees) {

		initialPOI = new PointOfInterest(originLatitudeInDegrees, originLongitudeInDegrees);
		finalPOI = new PointOfInterest(destinyLatitudeInDegrees, destinyLongitudeInDegrees);

	}

	public List<PointOfInterest> getShortestRoute() {
		LinkedList<PointOfInterest> shortPath = new LinkedList<>();
		dist = GraphAlgorithms.shortestPath(graph, initialPOI, finalPOI, shortPath);
		return shortPath;
	}

	public double getMinDistance() {
		return dist / 1000; // togive kilometers
	}

}
