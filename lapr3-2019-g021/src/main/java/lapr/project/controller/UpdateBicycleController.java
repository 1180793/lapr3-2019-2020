package lapr.project.controller;

import lapr.project.model.Bicycle;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author Joao Mata <1151352@isep.ipp.pt>
 */
public class UpdateBicycleController {

    /**
     * Vehicle Registry
     */
    VehicleRegistry vehicleRegistry;

    /**
     * New Bicycle instance
     */
    Bicycle oBicycle;

    /**
     * Constructor for the Controller
     *
     * @param mock represents if it is for testing purposes
     */
    public UpdateBicycleController(boolean mock) {
        vehicleRegistry = new VehicleRegistry(mock);
    }

    /**
     * gets the bicycle with the given Id
     *
     * @param id
     * @return the Bicycle or null
     */
    public Bicycle getBicycleById(String id) {
        return vehicleRegistry.getBicycle(id);
    }

    /**
     * gets the current bicycle
     * 
     * @return the current Bicycle
     */
    public Bicycle getBicycle() {
        return oBicycle;
    }

    /**
     * Sets the updated data for the Bicycle
     *
     * @param bicycleId
     * @param weight
     * @param latitude
     * @param longitude
     * @param aeroCoefficient
     * @param frontArea
     * @param size
     * 
     * @return true if the updated data passed the local validation
     */
    public boolean updateBicycle(String bicycleId, int weight, double latitude, double longitude, double aeroCoefficient, double frontArea, int size) {
        this.oBicycle = vehicleRegistry.updateBicycle(bicycleId, weight, latitude, longitude, aeroCoefficient, frontArea, size);
        return this.oBicycle != null;
    }

    /**
     * Updates the Bicycle to the database
     *
     * @return if the Bicycle was updated or not
     */
    public boolean updateBicycle() {
        return vehicleRegistry.updateBicycle(this.oBicycle);
    }

}
