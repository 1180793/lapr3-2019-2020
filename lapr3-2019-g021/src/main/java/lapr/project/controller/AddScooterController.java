package lapr.project.controller;

import lapr.project.model.Scooter;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author roger40
 */
public class AddScooterController {

        /**
         * Vehicle Registry
         */
        VehicleRegistry vehicleRegistry;

        /**
         * New Scooter instance
         */
        Scooter oScooter;

        /**
         * Constructor for the Controller
         *
         * @param mock represents if it is for testing purposes
         */
        public AddScooterController(boolean mock) {
                vehicleRegistry = new VehicleRegistry(mock);
        }

        /**
         * Sets the data for the Scooter to be added
         *
         * @param scooterId
         * @param weight
         * @param type
         * @param latitude
         * @param longitude
         * @param maxBatteryCap
         * @param actualBatteryCap
         * @param aeroCoefficient
         * @param frontArea
         * @return true if the Scooter passed the local validation
         */
        public boolean newScooter(String scooterId, int weight, int type, double latitude, double longitude, double maxBatteryCap, int actualBatteryCap, double aeroCoefficient, double frontArea, int motor) {
                this.oScooter = vehicleRegistry.newScooter(scooterId, weight, type, latitude, longitude, maxBatteryCap, actualBatteryCap, aeroCoefficient, frontArea, motor);
                return this.oScooter != null;
        }
        
        /**
         * Adds the Scooter to the database
         *
         * @return if the Scooter was added or not
         */
        public boolean addScooter() {
                return vehicleRegistry.addScooter(this.oScooter);
        }

}
