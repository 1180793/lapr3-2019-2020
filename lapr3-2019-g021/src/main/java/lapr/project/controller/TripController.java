/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.List;
import lapr.project.model.Trip;
import lapr.project.model.TripRegistry;

/**
 *
 * @author roger40
 */
public class TripController {
    
    
    
    
     TripRegistry tripreg;

  
    public TripController( boolean mock) {
        
        tripreg = new TripRegistry(mock);

    }

//    /**
//     *
//     * @param parkEnd
//     * @return
//     */
    public List<Trip> getAllTrips() {
        return tripreg.getAllTrips();

    }

    
     public long getUnlockedTime(String vehicleId) {
        return tripreg.getUnlockedTime(vehicleId);

    }
 

    
}
