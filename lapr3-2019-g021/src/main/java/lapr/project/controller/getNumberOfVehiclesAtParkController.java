/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.List;
import lapr.project.model.Bicycle;
import lapr.project.model.ParkRegistry;
import lapr.project.model.Scooter;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author roger40
 */
public class getNumberOfVehiclesAtParkController {
    
    
    ParkRegistry preg;
    VehicleRegistry vreg;
    
    public getNumberOfVehiclesAtParkController(boolean mock){
        preg = new ParkRegistry(mock);
        vreg = new VehicleRegistry(mock);
        
    }
    
    
    
    public List<Bicycle>  getNumberOfBicyclesAtPark(String parkId){
        
        return vreg.getNumberOfBicyclesAtPark(parkId);
        
        
    }
    
      public List<Bicycle>  getNumberOfBicyclesAtPark(double lat, double lon){
        return vreg.getNumberOfBicyclesAtPark(lat, lon);
        
        
    }
      
        public List<Scooter>  getNumberOfScootersAtPark(String parkId){
        return vreg.getNumberOfScootersAtPark(parkId);
        
        
    }
        
          public List<Scooter>  getNumberOfScootersAtPark(double lat, double lon){
        return vreg.getNumberOfScootersAtPark(lat, lon);
        
        
    }
    
    
    
    
}
