package lapr.project.controller;

import lapr.project.model.ParkRegistry;
import lapr.project.model.TripRegistry;

/**
 *
 * @author Daniel
 */
public class RemoveParkController {

    ParkRegistry pr;
    /**
     *
     * @param teste
     */
    public RemoveParkController(boolean teste) {
        
        pr = new ParkRegistry(teste);
    }
    
    public int removePark(String id) {
       
        if (pr.removePark(id)){
            return 1;
        }
        return 0;
    }
    
}