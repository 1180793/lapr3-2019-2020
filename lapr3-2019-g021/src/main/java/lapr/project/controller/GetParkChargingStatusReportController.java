package lapr.project.controller;

import java.util.Map;
import lapr.project.model.VehicleRegistry;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class GetParkChargingStatusReportController {

	/**
	 * Vehicle Registry
	 */
	VehicleRegistry vehicleRegistry;

	/**
	 * Constructor for the Controller
	 *
	 * @param mock represents if it is for testing purposes
	 */
	public GetParkChargingStatusReportController(boolean mock) {
		vehicleRegistry = new VehicleRegistry(mock);
	}

	/**
	 * Method that returns a report that has the ID of each
	 * scooter, their current battery level and how long it
	 * will take to fully charge each of them
	 *
	 * @param parkID
	 * @return
	 */
	public Map<String, Integer> getChargingStatusReport(String parkID) {
		return this.vehicleRegistry.getChargingStatusReport(parkID);
	}

}
