/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.sql.SQLException;
import java.util.List;
import lapr.project.model.Park;
import lapr.project.model.Scooter;
import lapr.project.model.User;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author roger40
 */
public class UnlockScooterController {

    VehicleRegistry vhr;

    private final String user1;
    private final VehicleRegistry ver;
    private final String parkOrigin;
    private SuggestScooterController ssc;
    private boolean mock;

    public UnlockScooterController(String user, boolean mock) {
        this.user1 = user;
        this.parkOrigin = null;
        ver = new VehicleRegistry(mock);
        this.mock = mock;

    }

//    public List<Scooter> suggestScooters(String parkEnd) {
//
//        return ver.getSuggestedScooters(this.parkOrigin, parkEnd);
//
//    }

    public boolean unlockSpecificScooter(String scotId) { // unlocks a specific scooter
           return ver.unlockSpecificScooter(scotId, user1);

    }
    
  
    public boolean unlockAnyScooterAtPark(String ParkOriginId) {  // unlocks a scoote rwith higher batery capacity

      
            

            return ver.unlockAnyScooterAtPark(ParkOriginId, user1 );
        
    }
    
    
   // unlockAnyEscooterAtParkForDestination(String parkIdentification, String username, double destinyLatitudeInDegrees, double destinyLongitudeInDegrees, String outputFileName) {
    
     public boolean unlockAnyScooterGivenParkDestination(String ParkOriginId, String username, double parkDestinyLat, double parkDestinyLong) throws SQLException {  // unlocks a scoote rwith higher batery capacity
      ssc = new SuggestScooterController(this.mock);
        if (ver.verifyUserUnlock(this.user1) ) {
            
         List <Scooter> ls = ssc.suggestScooters(ParkOriginId, parkDestinyLat, parkDestinyLong);
            if (ls!=null){
       
       for (Scooter sc :ls ){
          
           if (sc !=null){
           return ver.unlockAnyScooterGivenParkDestination(sc, user1 );
        }
           return false;
    
    }
        return false;
     }
         return false;
        }
        return false;
}
}
