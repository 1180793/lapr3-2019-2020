package lapr.project.controller;

import lapr.project.model.VehicleRegistry;

/**
 *
 * @author roger40
 */
public class RemoveBicycleController {

        /**
         * Vehicle Registry
         */
        VehicleRegistry vehicleRegistry;

        /**
         * Constructor for the Controller
         *
         * @param mock represents if it is for testing purposes
         */
        public RemoveBicycleController(boolean mock) {
                vehicleRegistry = new VehicleRegistry(mock);
        }

        /**
         * 'Removes' the Bicycle from the database
         *
         * @param id
         * @return if the Bicycle was removed or not
         */
        public boolean removeBicycle(String id) {
                return vehicleRegistry.removeBicycle(id);
        }
}
