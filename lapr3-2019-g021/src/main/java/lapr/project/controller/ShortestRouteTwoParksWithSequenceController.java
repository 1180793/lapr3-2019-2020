package lapr.project.controller;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import lapr.project.model.Park;
import lapr.project.model.ParkRegistry;
import lapr.project.model.Path;
import lapr.project.model.PointOfInterest;
import lapr.project.model.graph.Graph;
import lapr.project.utils.GraphUtil;

/**
 *
 * @author Francisco
 */
public class ShortestRouteTwoParksWithSequenceController {

    private GraphUtil util;

    private ParkRegistry parkRegistry;

    private Park parkStart;

    private Park parkEnd;

    private LinkedList<PointOfInterest> sequence;

    private Graph<PointOfInterest, Path> graph;

    /**
     * Number of requested routes
     */
    private int numRoutes;

    private boolean mock;

    public ShortestRouteTwoParksWithSequenceController(boolean mock) {
        this.util = new GraphUtil(mock);
        this.parkRegistry = new ParkRegistry(mock);
        this.graph = util.createDistanceGraph();
        this.mock = mock;
    }

    /**
     * Validates the Parks by knowing if the exist on the database
     *
     * @param startingPark
     * @param endingPark
     * @param sequence
     * @param numRoutes
     * @return the validation
     * @throws SQLException
     */
    public boolean validateParks(String startPark, String endPark, List<String> sequence, int numRoutes) {
        boolean res = true;

        parkStart = parkRegistry.getPark(startPark);
        parkEnd = parkRegistry.getPark(endPark);
        this.numRoutes = numRoutes;
        Iterator<String> it = sequence.iterator();
        this.sequence = new LinkedList<>();

        res = getPointOfInterestSequence(it);

        if (parkStart == null) {
            res = false;
        }
        if (parkEnd == null) {
            res = false;
        }

        return res;
    }
    
    public boolean insertPointOfInterest(double originLatitudeInDegrees, double originLongitudeInDegrees, double destinationLatitudeInDegrees, double destinationLongitudeInDegrees, List<String> sequence) {
        parkStart = new Park("Park1",originLatitudeInDegrees, originLongitudeInDegrees , 10, "Park 1 for Graph test", 10, 6, 5, 5);
        parkEnd = new Park("Park1",destinationLatitudeInDegrees, destinationLongitudeInDegrees , 10, "Park 1 for Graph test", 10, 6, 5, 5);
        this.numRoutes = 1;
        
        Iterator<String> it = sequence.iterator();
        this.sequence = new LinkedList<>();

        return getPointOfInterestSequence(it);
    }

    /**
     * Gets the point sequence from the database
     *
     * @param it
     * @return if all the points exist
     * @throws SQLException
     */
    private boolean getPointOfInterestSequence(Iterator<String> it) {
        while (it.hasNext()) {
            Park aux = parkRegistry.getPark(it.next());
            if (aux == null) {
                return false;
            }
            this.sequence.add(new PointOfInterest(aux.getLatitude(), aux.getLongitude()));
        }
        return true;
    }

    /**
     * Calculates the shortests routes
     *
     * @return a list with the shortests routes
     */
    public List<LinkedList<Path>> calculateShortestPath() {
        return util.getShortestPathSequence(graph, new PointOfInterest(parkStart.getLatitude(), parkStart.getLongitude()), new PointOfInterest(parkEnd.getLatitude(), parkEnd.getLongitude()), this.sequence, this.numRoutes);
    }

}
