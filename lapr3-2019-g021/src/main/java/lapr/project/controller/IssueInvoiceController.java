package lapr.project.controller;

import lapr.project.model.Invoice;
import lapr.project.model.InvoiceRegistry;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class IssueInvoiceController {

        /**
         * Invoice Registry
         */
        InvoiceRegistry invoiceRegistry;

        /**
         * New Invoice instance
         */
        Invoice oInvoice;
	
	/**
         * Constructor for the Controller
         *
         * @param test represents if it is for testing purposes
         */
        public IssueInvoiceController(boolean test) {
                invoiceRegistry = new InvoiceRegistry(test);
        }

	/**
         * Issues a Monthly Invoice for an User
         *
	 * @param month
	 * @param username
	 * @return 
         */
	public Invoice issueInvoice(int month, String username) {
		return this.invoiceRegistry.issueInvoice(month, username);
	}

}
