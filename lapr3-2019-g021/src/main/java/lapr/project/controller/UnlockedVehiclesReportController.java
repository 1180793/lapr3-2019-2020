/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.HashMap;
import java.util.Map;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author roger40
 */
public class UnlockedVehiclesReportController {
    
    
     VehicleRegistry vhr;
     
     
     public UnlockedVehiclesReportController(boolean mock){
         
         vhr = new VehicleRegistry(mock);
         
         
     }
     
     private Map<String, String> getUnlockedBicyclesReport(){
         
        return vhr.GetCurrentUnlockedBicyclesReport();
     }
     
     
      private Map<String, String> getUnlockedScootersReport(){
         
        return vhr.GetCurrentUnlockedScootersReport();
     }
      
      
         
      public Map<String, String> getUnlockedVehiclesReport(){
          
        Map <String, String> unlockedVehiclesReport = new HashMap<String, String>();
        
        unlockedVehiclesReport.putAll(getUnlockedBicyclesReport());
        unlockedVehiclesReport.putAll(getUnlockedScootersReport());
        return unlockedVehiclesReport;
        
        
     }
    
}
