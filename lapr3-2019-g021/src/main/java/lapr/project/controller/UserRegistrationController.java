package lapr.project.controller;

import lapr.project.model.User;
import lapr.project.model.UserRegistry;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UserRegistrationController {

	/**
	 * User Registry
	 */
	UserRegistry userRegistry;
	
	/**
	 * New User instance
	 */
	User oUser;
	
	/**
	 * Constructor for the Controller
	 *
	 * @param mock represents if it is for testing purposes
	 */
	public UserRegistrationController(boolean mock) {
		userRegistry = new UserRegistry(mock);
	}

	/**
	 * Sets the data for the user to be registered
	 *
	 * @param email
	 * @param name
	 * @param password
	 * @param creditCard
	 * @param gender
	 * @param height
	 * @param weight
	 * @param avgSpeed
	 * @return 
	 */
	public boolean newUser(String email, String name, String password, String creditCard, char gender, double height, double weight, double avgSpeed) {
		this.oUser = userRegistry.newUser(email, name, password, creditCard, gender, height, weight, avgSpeed);
		return this.oUser != null;
	}

	/**
	 * Regists the user to the database
	 *
	 * @return if the user was registered or not
	 */
	public boolean registerUser() {
		return userRegistry.registerUser(this.oUser);
	}
}
