package lapr.project.controller;

import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.ParkRegistry;
import lapr.project.model.Scooter;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author Joao Mata <1151352@isep.ipp.pt>
 */
public class GetParkAvailableSlotsController {

    /**
     * Park Registry
     */
    ParkRegistry parkRegistry;
    VehicleRegistry vehicleRegistry;

    /**
     * Constructor for the Controller
     *
     * @param mock represents if it is for testing purposes
     */
    public GetParkAvailableSlotsController(boolean mock) {
        parkRegistry = new ParkRegistry(mock);
        vehicleRegistry = new VehicleRegistry(mock);
    }

    /**
     * For the park specified by the id, returns the available parking slots
     *
     * @param id
     * @return the available parking slots or -1 if the park is not found
     */
    public int getAvailableBicycleParkingSlots(String id) {
        Park park = parkRegistry.getPark(id);
        if (park == null) {
            return -1;
        }
        return park.getAvailableBicycleParkingSlots();

    }

    /**
     * For the park specified by the id, returns the available parking slots
     *
     * @param id
     * @return the available parking slots or -1 if the park is not found
     */
    public int getAvailableScooterParkingSlots(String id) {
        Park park = parkRegistry.getPark(id);
        if (park == null) {
            return -1;
        }
        return park.getAvailableScooterParkingSlots();
    }

    /**
     * For the park at the specified location, returns the available parking
     * slots
     *
     * @param id
     * @return the available parking slots or -1 if the park is not found
     */
    public int getAvailableBicycleParkingSlots(double latitutude, double longitude) {
        Park park = parkRegistry.getPark(latitutude, longitude);
        if (park == null) {
            return -1;
        }
        return park.getAvailableBicycleParkingSlots();

    }

    /**
     * For the park at the specified location, returns the available parking
     * slots
     *
     * @param id
     * @return the available parking slots or -1 if the park is not found
     */
    public int getAvailableScooterParkingSlots(double latitutude, double longitude) {
        Park park = parkRegistry.getPark(latitutude, longitude);
        if (park == null) {
            return -1;
        }
        return park.getAvailableScooterParkingSlots();
    }

    /**
     * For the park at the specified location, returns the available parking
     * slots for the specified vehicle
     *
     * @param id
     * @return the available parking slots or -1 if the park is not found
     */
    public int getAvailableLoanedVehicleParkingSlots(String useremail, String parkId) {
        for (Bicycle b : vehicleRegistry.getAllBicycles()) {
            if (b.getAssignedUser() != null && b.getAssignedUser().getEmail().equals(useremail)) {
                Park park = parkRegistry.getPark(parkId);
                if(park !=null)
                return park.getAvailableBicycleParkingSlots();
            }
        }
        for (Scooter s : vehicleRegistry.getAllScooters()) {
            if (s.getAssignedUser() != null && s.getAssignedUser().getEmail().equals(useremail)) {
                Park park = parkRegistry.getPark(parkId);
                 if(park !=null)
                return park.getAvailableScooterParkingSlots();
            }

        }
        return -1;
    }

}
