/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.Park;
import lapr.project.model.User;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author roger40
 */
public class LockScooterController {
    
    

    private final String userEmail;
    private final VehicleRegistry ver;
    private double  parkEndLat;
    private double  parkEndLong;
    private final String  parkId;
   
    
    
    
    
    
    
    public LockScooterController(String userEmail, double parkLat, double parkLong, boolean mock){
         this.userEmail = userEmail;
        this.parkEndLat = parkLat;
        this.parkEndLong = parkLong;
        ver = new VehicleRegistry(mock);
        
        parkId = null;
        
        
        
    }
    
    
     public LockScooterController(String userEmail, String parkId, boolean mock){
         this.userEmail = userEmail;
        this.parkId = parkId;
        
        ver = new VehicleRegistry(mock);
        
        
        
        
    }
    
    
    
    public boolean lockScooterGivenParkCords(){
         
        return ver.lockScooter(this.userEmail, this.parkEndLat, this.parkEndLong);
        
        
        
    }
    
    public boolean lockScooterGivenParkId(){
          
        return ver.lockScooter(this.userEmail,this.parkId);
        
        
        
    }
    
}
    