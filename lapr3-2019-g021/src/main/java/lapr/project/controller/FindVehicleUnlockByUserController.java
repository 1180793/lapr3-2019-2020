/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.Bicycle;
import lapr.project.model.Scooter;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author roger40
 */
public class FindVehicleUnlockByUserController {
    
    VehicleRegistry vhr;
     public FindVehicleUnlockByUserController (boolean mock){
         vhr = new VehicleRegistry(mock);
     }
            
    public Bicycle findUnlockedBicycleByUser(String user){
      
       return vhr.findUnlockedBicycleByUser(user);
    }
    
    public Scooter findUnlockedScooterByUser(String user){
       return vhr.findUnlockedScooterByUser(user);
    }
    
    
    
}
