package lapr.project.controller;

import lapr.project.model.Park;
import lapr.project.model.ParkRegistry;
import lapr.project.model.PointOfInterest;

/**
 *
 * @author Francisco
 */
public class DistanceToParkController {

    private final ParkRegistry reg;
    private PointOfInterest curLocation;
    private PointOfInterest parkLocation;

    /**
     * Constructor.
     *
     * @param mock
     */
    public DistanceToParkController(boolean mock) {
        reg = new ParkRegistry(mock);
    }

    /**
     * Returns a park by it's name/description.
     *
     * @param description
     * @return
     */
    public Park getParkByDescription(String description) {
        Park p = this.reg.getParkByDescription(description);
        if (p != null) {
            parkLocation = new PointOfInterest(p.getLatitude(), p.getLongitude());
        }
        return p;
    }

    /**
     * creates a new PointOfInterest.
     *
     * @param lat
     * @param lon
     */
    public void newPointOfInterest(double lat, double lon) {
        curLocation = new PointOfInterest(lat, lon);
    }

    /**
     * calculates the distance between the park and the user's current location.
     *
     * @return the distance
     */
    public double calculateDistance() {
        return curLocation.calculateDistance(parkLocation);
    }

    public PointOfInterest getPointOfInterest() {
        return curLocation;
    }

}
