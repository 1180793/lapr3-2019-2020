/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.List;
import lapr.project.model.Scooter;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author danie
 */
public class GetScooterCapacityEstimatedTripReportController {
    
    /**
	 * Vehicle Registry
	 */
	VehicleRegistry vehicleRegistry;

	/**
	 * Constructor for the Controller
	 *
	 * @param mock represents if it is for testing purposes
	 */
	public GetScooterCapacityEstimatedTripReportController(boolean mock) {
		vehicleRegistry = new VehicleRegistry(mock);
	}
        
	public List<Scooter> GetScooterCapacityEstimatedTripReport(int distanceKm) {
		return this.vehicleRegistry.getScooterCapacityEstimatedTripReport(distanceKm);
	}
    
}
