/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.sql.SQLException;
import java.util.List;
import lapr.project.model.Park;
import lapr.project.model.ParkRegistry;
import lapr.project.model.PointOfInterest;
import lapr.project.model.Scooter;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author roger40
 */
public class SuggestScooterController {
    ShortestRouteTwoParksController srtpc;
    ParkRegistry preg;
    VehicleRegistry vreg;
    public SuggestScooterController(boolean mock){
        preg = new ParkRegistry(mock);
        vreg = new VehicleRegistry(mock);
        srtpc = new ShortestRouteTwoParksController(mock);
        
        
    }
    
    
    
     public List<Scooter> suggestScooters(String parkOrigin, double destinationParkLatitude, double destinationParkLongitude) throws SQLException {
        
     Park  parkEnd = preg.getPark(destinationParkLatitude, destinationParkLongitude);
   
      Park  parkOriginal = preg.getPark(parkOrigin);
     
     if (parkEnd !=null && parkOriginal !=null){
      
         
      boolean tt = srtpc.validateParks(parkOriginal.getID(), parkEnd.getID());
      
      
       
     List<PointOfInterest> poL = srtpc.getShortestRoute();
     if (poL ==null){
        return null;
     }
     
     
     
      if (  parkOriginal.getID().equals(parkEnd.getID())){ // no caso de ele viajar de um park origem para um park destino que é o mesmo ele calcula na mesma e vai ter na lista todas as scooters
     
      
       return vreg.getSuggestedScooters(parkOriginal, parkEnd, 0 ); //devolve tudo
      
     }
      
       Double  totalDist = srtpc.getMinDistance();
       if (totalDist !=0){
             return vreg.getSuggestedScooters(parkOriginal, parkEnd, totalDist );
       }
      
    
     }
    
        return null;
    
}
}
