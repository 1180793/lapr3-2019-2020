package lapr.project.controller;

import lapr.project.model.Path;
import lapr.project.model.PathRegistry;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AddPathController {

	/**
	 * Path Registry
	 */
	PathRegistry pathRegistry;

	/**
	 * New Path instance
	 */
	Path oPath;

	/**
	 * Constructor for the Controller
	 *
	 * @param mock represents if it is for testing purposes
	 */
	public AddPathController(boolean mock) {
		pathRegistry = new PathRegistry(mock);
	}

	/**
	 * Sets the data for the Path to be added
	 *
	 * @param latitudeA
	 * @param longitudeA
	 * @param latitudeB
	 * @param longitudeB
	 * @param kineticCoefficient
	 * @param windDirection
	 * @param windSpeed
	 * @return true if the Path passed the local validation
	 */
	public boolean newPath(double latitudeA, double longitudeA, double latitudeB, double longitudeB, double kineticCoefficient, double windDirection, double windSpeed) {
		this.oPath = pathRegistry.newPath(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		return this.oPath != null;
	}

	/**
	 * Adds the Path to the database
	 *
	 * @return if the Path was added or not
	 */
	public boolean addPath() {
		return pathRegistry.addPath(this.oPath);
	}
}
