package lapr.project.controller;

import lapr.project.model.Scooter;
import lapr.project.model.VehicleRegistry;

/**
 *
 * @author Joao Mata <1151352@isep.ipp.pt>
 */
public class UpdateScooterController {

    /**
     * Vehicle Registry
     */
    VehicleRegistry vehicleRegistry;

    /**
     * updated Scooter instance
     */
    Scooter oScooter;

    /**
     * Constructor for the Controller
     *
     * @param mock represents if it is for testing purposes
     */
    public UpdateScooterController(boolean mock) {
        vehicleRegistry = new VehicleRegistry(mock);
    }

    /**
     * gets the scooter with the given Id
     *
     * @param id
     * 
     * @return the Scooter or null
     */
    public Scooter getScooterById(String id) {
        return vehicleRegistry.getScooter(id);
    }

    /**
     * gets the current updated Scooter
     * 
     * @return the current updated Scooter
     */
    public Scooter getScooter() {
        return oScooter;
    }

    /**
     * Sets the updated data for the Scooter
     *
     * @param scooterId
     * @param weight
     * @param type
     * @param latitude
     * @param longitude
     * @param maxBatteryCap
     * @param actualBatteryCap
     * @param aeroCoefficient
     * @param frontArea
     * 
     * @return true if the updated data passed the local validation
     */
    public boolean updateScooter(String scooterId, int weight, int type, double latitude, double longitude, double maxBatteryCap, int actualBatteryCap, double aeroCoefficient, double frontArea) {
        this.oScooter = vehicleRegistry.updateScooter(scooterId, weight, type, latitude, longitude, maxBatteryCap, actualBatteryCap, aeroCoefficient, frontArea);
        return this.oScooter != null;
    }

    /**
     * Updates the Scooter to the database
     *
     * @return if the Scooter was updated or not
     */
    public boolean updateScooter() {
        return vehicleRegistry.updateScooter(this.oScooter);
    }

}
