package lapr.project.controller;

import lapr.project.model.VehicleRegistry;

/**
 *
 * @author roger40
 */
public class RemoveScooterController {

        /**
         * Vehicle Registry
         */
        VehicleRegistry vehicleRegistry;

        /**
         * Constructor for the Controller
         *
         * @param mock represents if it is for testing purposes
         */
        public RemoveScooterController(boolean mock) {
                vehicleRegistry = new VehicleRegistry(mock);
        }

        /**
         * 'Removes' the Scooter from the database
         *
         * @param id
         * @return if the Scooter was removed or not
         */
        public boolean removeScooter(String id) {
                return vehicleRegistry.removeScooter(id);
        }
}
