/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author roger40
 */


/**
 *
 * @author roger40
 */
public class LockScooterControllerTest {
    
        LockScooterController lbc;
    LockScooterController lbc2;
      LockScooterController lbc3;
     LockScooterController lbc4;
    
    public LockScooterControllerTest() {
      
    }
   
    @Test
    public void testLockScooterGivenParkCords() {
         lbc = new LockScooterController("roger2@gmail.com", 2, 3, true); // user tem scooter e o park de destino existe (o park de destino tem de existir ou na bd ou na parkMock)
     lbc2 = new LockScooterController("rogerXXX@gmail.com", 3.0, -3, true); //user nao tem scooter
     lbc3 = new LockScooterController("roger2@gmail.com", 4, 100, true);  // user tem scooter e o park de destino NAO existe  (o park de destino tem de existir ou na bd ou na parkMock)
     lbc4 = new LockScooterController("roger1@gmail.com", 100, 300, true);   //park de destino nao tem mais lugares
     
     
     System.out.println("LockBicycle");
         assertEquals(lbc.lockScooterGivenParkCords(), true);
         assertEquals(lbc2.lockScooterGivenParkCords(), false);
           assertEquals(lbc3.lockScooterGivenParkCords(), false);
             assertEquals(lbc4.lockScooterGivenParkCords(), false);
    }

    /**
     * Test of lockScooterGivenParkId method, of class LockScooterController.
     */
    @Test
    public void testLockScooterGivenParkId() {
       lbc = new LockScooterController("roger2@gmail.com", "1", true); // user tem bicicleta e o park de destino existe (o park de destino tem de existir ou na bd ou na parkMock)
     lbc2 = new LockScooterController("rogerXXX@gmail.com", "1", true); //user nao tem bicicleta
     lbc3 = new LockScooterController("roger2@gmail.com", "NOexists", true);  // user tem bicicleta e o park de destino NAO existe  (o park de destino tem de existir ou na bd ou na parkMock)
     lbc4 = new LockScooterController("roger2@gmail.com", "5", true);  // user tem bicicleta e o park de destino  existe MAS o park nao tem capacidade para mais bikes 
        
        
        
        
        
        System.out.println("LockBicycle");
         assertEquals(lbc.lockScooterGivenParkId(), true);
         assertEquals(lbc2.lockScooterGivenParkId(), false);
           assertEquals(lbc3.lockScooterGivenParkId(), false);
             assertEquals(lbc4.lockScooterGivenParkId(), false);
    }
    
}
