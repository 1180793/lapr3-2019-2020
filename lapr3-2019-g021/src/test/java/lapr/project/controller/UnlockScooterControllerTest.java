/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

//
import java.sql.SQLException;
import java.util.List;
import lapr.project.data.ParkDB;
import lapr.project.data.ScooterDB;
import lapr.project.data.mock.BicycleMock;
import lapr.project.data.mock.ParkMock;
import lapr.project.data.mock.ScooterMock;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Scooter;
import lapr.project.model.User;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
//

/**
 *
 * @author roger40
 */
public class UnlockScooterControllerTest {

    UnlockScooterController uvc;
   UnlockScooterController uvc2;
    UnlockScooterController uvc3;
    User user1;
    User user2;
    User user3;
    BicycleMock mockBikes;
    ScooterMock mockScooter;
    ScooterDB scooterdata;
    Bicycle bikex;
    List<Bicycle> listbikes;
    List<Scooter> listscot;
    Scooter scotx;
    ParkDB parkdata;

    public UnlockScooterControllerTest() {
        scooterdata = new ScooterDB (true);
        parkdata = new ParkDB(true);
        mockBikes = new BicycleMock();
        mockScooter = new ScooterMock();
        listscot = mockScooter.getAllScooters();
        listbikes = mockBikes.getAllBicycles();
       
        user1 = new User("noscooter@gmail.com", "roger", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);  //crio um user que nao tem bikes requesitadas
        user2 = new User("roger1@gmail.com", "roger1", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25); //crio um user que ja tem bikes requesitadas (email ja esta atribuido a uma bike
        user3 = new User("roger2@gmail.com", "roger2", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);

       

       // scotx = new Scooter("SCTR005", 25, 1, parkMock.getAllParks().get(0), 200.0, 100, 1.22, 2.1);
        uvc = new UnlockScooterController("email6@gmail.com", true);
         uvc2 = new UnlockScooterController("roger1@gmail.com", true);
        
    }


    /**
     * Test of unlockScooterByUser method, of class UnlockScooterController.
     */
    @Test
    public void testUnlockAnyScooterScooter() {
        System.out.println("testUnlockAnyScooter");

        

        boolean test1 = uvc.unlockAnyScooterAtPark("1");
         assertEquals(test1, true);  // atribui a scooter com mais bateria ao user que iniciou o controller

         boolean test2 = uvc2.unlockAnyScooterAtPark("1");
         assertEquals(test2, false);  //user ja tem veiculo

        
         
         
         
         
    }
         
         @Test
    public void testUnlockSpecificScooterScooter() {
         uvc = new UnlockScooterController("email6@gmail.com", true);
           boolean test1 = uvc.unlockSpecificScooter("SCTR001");
         assertEquals(test1, false);   //FALSE PORQUE a scooter ja esta a ser utilizada
         
         
         
           uvc2 = new UnlockScooterController("roger2@gmail.com", true);
           boolean test2= uvc2.unlockSpecificScooter("SCTR002");
         assertEquals(test2, false);   //FALSE PORQUE user ja tem vehicuclo
         
          uvc = new UnlockScooterController("email6@gmail.com", true);
           boolean test3= uvc.unlockSpecificScooter("SCTR0000000");
         assertEquals(test3, false);   //FALSE PORQUE veiculo nao existe
         
         uvc = new UnlockScooterController("email6@gmail.com", true);
           boolean test4 = uvc.unlockSpecificScooter("SCTR002");
         assertEquals(test4, true);   
         
  

    }
      
         @Test
    public void unlockAnyScooterGivenParkDestination() throws SQLException {
         uvc = new UnlockScooterController("email6@gmail.com", true);
         boolean test1 = uvc.unlockAnyScooterGivenParkDestination("Park1", "email1@gmail.com", 41.015153, 9.315251); // do Park1 até ao Park2 da mock
        assertEquals(test1, true);   //true pois o user e valido e parks tambem e ha path dum park ao outro
        
         boolean test2 = uvc.unlockAnyScooterGivenParkDestination("Park111111", "email1@gmail.com", 41.015153, 9.315251); // do ParkOrigem invalido até ao Park2 da mock
        assertEquals(test2, false);   
        
        
           boolean test3 = uvc.unlockAnyScooterGivenParkDestination("Park1", "email1@gmail.com", 411.015153, 9.315251); // park destino invalido
        assertEquals(test3, false);  
        
    }
    
   

}
