package lapr.project.controller;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UserRegistrationControllerTest {
	
	UserRegistrationController oController;
	
	public void setUp() {
		oController = new UserRegistrationController(true);
	}
	
	public UserRegistrationControllerTest() {
	}

	/**
	 * Test of newUser method, of class UserRegistrationController.
	 */
	@Test
	public void testNewValidUser() {
		setUp();
		System.out.println("testNewValidUser");
		boolean expResult = true;
		boolean result = oController.newUser("email@valid.com", "Teste 1", "teste123", "1111222233334444", 'M', 1.43, 65, 13);
		assertEquals(expResult, result);
	}
	
	@Test
	public void testNewInvalidUser() {
		setUp();
		System.out.println("testNewInvalidUser");
		boolean expResult = false;
		boolean result = oController.newUser("email_invalid.com", "Teste 1", "teste123", "1111222233334444", 'M', 1.43, 65, 13);
		assertEquals(expResult, result);
	}

	/**
	 * Test of registerUser method, of class UserRegistrationController.
	 */
	@Test
	public void testRegisterValidUser() {
		setUp();
		System.out.println("testRegisterValidUser");
		boolean expResult = true;
		oController.newUser("email@valid.com", "Teste 1", "teste123", "1111222233334444", 'M', 1.43, 65, 13);
		boolean result = oController.registerUser();
		assertEquals(expResult, result);
	}
	
	@Test
	public void testRegisterInvalidUser() {
		setUp();
		System.out.println("testRegisterInvalidUser");
		boolean expResult = false;
		oController.newUser("email1@gmail.com", "Teste 1", "teste123", "1111222233334444", 'M', 1.43, 65, 13);
		boolean result = oController.registerUser();
		assertEquals(expResult, result);
	}
}
