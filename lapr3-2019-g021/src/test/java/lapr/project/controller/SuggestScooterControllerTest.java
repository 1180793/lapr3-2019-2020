/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 *
 * @author roger40
 */
public class SuggestScooterControllerTest {
    SuggestScooterController ssc;
    
    public SuggestScooterControllerTest() {
        ssc = new SuggestScooterController(true);
    }
    
  
    @Test
    public void testSuggestScooters() throws Exception {
       
       
        
      
        assertNull( ssc.suggestScooters("Park1", 333333, 4)); //park destino nao existe
        assertNull( ssc.suggestScooters("11111",41.015153, 9.315251)); // park origem nao existe
         assertNull( ssc.suggestScooters("111111", 4000, 9.315251)); //park destino e origem nao existe
        
     

           assertNotNull( ssc.suggestScooters("Park1", 41.015153, 9.315251));
           
           assertNotNull( ssc.suggestScooters("Park1",  41.123456, 9.153152)); //park de destino é o mesmo que park origem, devolve todas as scooters (todas sao capazes)
           assertEquals( ssc.suggestScooters("Park1",  41.123456, 9.153152).size(), 6);
           
            assertNull( ssc.suggestScooters("1",  41.123456, 9.153152)); //Nao ha path entre os dois paths
           
           assertEquals(ssc.suggestScooters("Park1", 41.015153, 9.315251).size(),5);
       
    }
    
}
