package lapr.project.controller;

import lapr.project.model.Bicycle;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Joao Mata <1151352@isep.ipp.pt>
 */
public class GetParkAvailableSlotsControllerTest {

    GetParkAvailableSlotsController oController;

    public void setUp() {
        oController = new GetParkAvailableSlotsController(true);
    }

    public GetParkAvailableSlotsControllerTest() {
    }

    @Test
    public void testGetAvailableBicycleAtExistingPark() {
        setUp();
        System.out.println("testGetAvailableBicycleAtExistingPark");
        int expResult = 1;
        int result = oController.getAvailableBicycleParkingSlots("1");
        
        assertEquals(expResult, result);
    }

    @Test
    public void testGetAvailableBicycleAtNonExistingPark() {
        setUp();
        System.out.println("testGetAvailableBicycleAtNonExistingPark");
        int expResult = -1;
        int result = oController.getAvailableBicycleParkingSlots("99");
        
        assertEquals(expResult, result);
    }
   
        @Test
    public void testGetAvailableScooterAtExistingPark() {
        setUp();
        System.out.println("testGetAvailableScooterAtExistingPark");
        int expResult = 2;
        int result = oController.getAvailableScooterParkingSlots("1");
        
        assertEquals(expResult, result);
    }

    @Test
    public void testGetAvailableScooterAtNonExistingPark() {
        setUp();
        System.out.println("testGetAvailableScooterAtNonExistingPark");
        int expResult = -1;
        int result = oController.getAvailableScooterParkingSlots("99");
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetAvailableBicycleAtExistingParkByLocation() {
        setUp();
        System.out.println("testGetAvailableBicycleAtExistingParkByLocation");
        int expResult = 1;
        int result = oController.getAvailableBicycleParkingSlots(2,3);
        
        assertEquals(expResult, result);
    }

    @Test
    public void testGetAvailableBicycleAtNonExistingParkByLocation() {
        setUp();
        System.out.println("testGetAvailableBicycleAtNonExistingParkByLocation");
        int expResult = -1;
        int result = oController.getAvailableBicycleParkingSlots(99,99);
        
        assertEquals(expResult, result);
    }
   
        @Test
    public void testGetAvailableScooterAtExistingParkByLocation() {
        setUp();
        System.out.println("testGetAvailableScooterAtExistingParkByLocation");
        int expResult = 2;
        int result = oController.getAvailableScooterParkingSlots(2,3);
        
        assertEquals(expResult, result);
    }

    @Test
    public void testGetAvailableScooterAtNonExistingParkByLocation() {
        setUp();
        System.out.println("testGetAvailableScooterAtNonExistingParkByLocation");
        int expResult = -1;
        int result = oController.getAvailableScooterParkingSlots(99,99);
        
        assertEquals(expResult, result);
    }
    
    
      @Test
    public void testGetAvailableLoanedVehiclesSlots() {
        setUp();
        System.out.println("testGetAvailableLoanedVehiclesSlots");
        int expResult = 10;
        int result = oController.getAvailableLoanedVehicleParkingSlots("roger1@gmail.com", "Park2"); //uaer have bike
        
        assertEquals(expResult, result);
        
        
         int expResult2 = 5;
        int result2 = oController.getAvailableLoanedVehicleParkingSlots("roger2@gmail.com", "Park2"); //user have scooter
        
        assertEquals(expResult2, result2);
        
         int expResult3 = -1;
        int result3 = oController.getAvailableLoanedVehicleParkingSlots("rogerNNNN@gmail.com", "Park2"); //user dont have unlocked vehicles
        
        assertEquals(expResult3, result3);
        
        
        int expResult4 = -1;
        int result4 = oController.getAvailableLoanedVehicleParkingSlots("roger2@gmail.com", "ParkNNNN"); //upark dont exist
        
        assertEquals(expResult4, result4);
    }
}
