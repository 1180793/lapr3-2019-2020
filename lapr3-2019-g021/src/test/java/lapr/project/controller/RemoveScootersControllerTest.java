package lapr.project.controller;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author roger40
 */
public class RemoveScootersControllerTest {

        RemoveScooterController oController;

        public RemoveScootersControllerTest() {
                oController = new RemoveScooterController(true);
        }

        /**
         * Test of removeBicycle method, of class RemoveBicyclesController.
         */
        @Test
        public void testRemoveScooter() {
                 assertEquals(oController.removeScooter("SCTR001"), false ); //scooter tem user atribuido
             
                        assertEquals(oController.removeScooter("SCTR002"), true);
                assertEquals(oController.removeScooter("SCTR007"), false); //ja se encontra removida
                assertEquals(oController.removeScooter("SCTR099"), false); // Scooter doesn't exist
        }
}
