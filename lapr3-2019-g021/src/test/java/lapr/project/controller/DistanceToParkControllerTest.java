package lapr.project.controller;

import lapr.project.model.Park;
import lapr.project.model.PointOfInterest;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Francisco
 */
public class DistanceToParkControllerTest {

    DistanceToParkController controller;

    public void setUp() {
        controller = new DistanceToParkController(true);
    }

    /**
     * Test of getParkByDescription method, of class DistanceToParkController.
     */
    @Test
    public void testGetParkByDescription() {
        System.out.println("getParkByDescription");

        setUp();

        String description = "Porto";
        Park expResult = new Park("3", 10, 10, 4, "Porto", 1, 2, 3, 4);
        Park result = controller.getParkByDescription(description);
        assertEquals(expResult, result);
        
        description = "Lisboa";
        expResult = null;
        result = controller.getParkByDescription(description);
        assertEquals(expResult, result);
    }

    /**
     * Test of newPointOfInterest method, of class DistanceToParkController.
     */
    @Test
    public void testNewPointOfInterest() {
        System.out.println("newPointOfInterest");

        setUp();

        double lat = 40.0;
        double lon = 10.0;
        controller.newPointOfInterest(lat, lon);
        PointOfInterest expResult = new PointOfInterest(lat, lon);
        PointOfInterest result = controller.getPointOfInterest();
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateDistance method, of class DistanceToParkController.
     */
    @Test
    public void testCalculateDistance() {
        System.out.println("calculateDistance");
        
        setUp();
        
        controller.newPointOfInterest(10, 20);
        Park p = controller.getParkByDescription("Porto");
        
        double expResult = 1095.0142245900295;
        double result = controller.calculateDistance();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of getPointOfInterest method, of class DistanceToParkController.
     */
    @Test
    public void testGetPointOfInterest() {
        System.out.println("getPointOfInterest");

        setUp();

        double lat = 40.0;
        double lon = 10.0;
        controller.newPointOfInterest(lat, lon);
        PointOfInterest expResult = new PointOfInterest(lat, lon);
        PointOfInterest result = controller.getPointOfInterest();
        assertEquals(expResult, result);
    }

}
