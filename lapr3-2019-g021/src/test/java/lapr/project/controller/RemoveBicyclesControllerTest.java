package lapr.project.controller;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author roger40
 */
public class RemoveBicyclesControllerTest {

        RemoveBicycleController oController;

        public RemoveBicyclesControllerTest() {
                oController = new RemoveBicycleController(true);
        }

        /**
         * Test of removeBicycle method, of class RemoveBicycleController.
         */
        @Test
        public void testRemoveBicycle() {
                assertEquals(oController.removeBicycle("BIKE001"), true);
                 assertEquals(oController.removeBicycle("BIKE007"), false);// bibicleta ja esta removida
                assertEquals(oController.removeBicycle("BIKE006"), false); // bibicleta tem um user atribuido
                assertEquals(oController.removeBicycle("BIKE099"), false); // Bicycle doesn't exist
        }

}
