package lapr.project.controller;

import lapr.project.model.Scooter;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Joao Mata <1151352@isep.ipp.pt>
 */
public class UpdateScooterControllerTest {

    UpdateScooterController oController;

    public void setUp() {
        oController = new UpdateScooterController(true);
    }

    public UpdateScooterControllerTest() {
    }

    @Test
    public void testUpdateExistingScooter() {
        setUp();
        System.out.println("testUpdateExistingScooter");
        boolean expResult = true;
        Scooter scooter = oController.getScooterById("SCTR001");
        assertNotNull(scooter);

        boolean result = oController.updateScooter("SCTR001", 15, 1, 2, 3, 10, 5, 2, 3);

        assertEquals(expResult, result);
    }

    @Test
    public void testUpdateToDbExistingScooter() {
        setUp();
        System.out.println("testUpdateToDbExistingScooter");
        boolean expResult = true;
        Scooter scooter = oController.getScooterById("SCTR001");
        assertNotNull(scooter);

        boolean result = oController.updateScooter("SCTR001", 15, 1, 2, 3, 10, 5, 2, 3);

        result = oController.updateScooter();
        assertEquals(expResult, result);

        expResult = false;
        result = oController.updateScooter("Test", 0, 0, 0, 0, 0, 0, 0, 0);
        assertEquals(expResult, result);
    }

    @Test
    public void testUpdateNonExistingScooter() {
        setUp();
        System.out.println("testUpdateNonExistingScooter");
        Scooter scooter = oController.getScooterById("XPTO");
        assertNull(scooter);
    }

    /**
     * Test of getScooter method, of class UpdateScooterController.
     */
    @Test
    public void testGetScooter() {
        System.out.println("getScooter");
        setUp();
        Scooter expResult = null;
        Scooter result = oController.getScooter();
        assertEquals(expResult, result);
    }

    /**
     * Test of getScooter method, of class UpdateScooterController.
     */
    @Test
    public void testGetScooterNotNull() {
        System.out.println("getScooterNotNull");
        setUp();
        oController.updateScooter("SCTR001", 5, 2, 2, 3, 11, 1, 1.0, 2);
        Scooter result = oController.getScooter();
        assertNotNull(result);
    }

    @Test
    public void testUpdateScooterInvalidWeight() {
        System.out.println("testUpdateScooterInvalidWeight");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateScooter("SCTR001", 0, 2, 2, 3, 11, 1, 1.0, 2);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateScooterInvalidAeroCoeficient() {
        System.out.println("testUpdateScooterInvalidAeroCoeficient");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateScooter("SCTR001", 5, 2, 2, 3, 11, 1, 0, 2);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateScooterInvalidFrontalArea() {
        System.out.println("testUpdateScooterInvalidFrontalArea");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateScooter("SCTR001", 5, 2, 2, 3, 11, 1, 1.0, 0);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateScooterInvalidType() {
        System.out.println("testUpdateScooterInvalidType");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateScooter("SCTR001", 5, 0, 2, 3, 11, 1, 1.0, 2);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateScooterInvalidPark() {
        System.out.println("testUpdateScooterInvalidPark");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateScooter("SCTR001", 5, 2, 0, 0, 11, 1, 1.0, 2);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateScooterInvalidMaxCap() {
        System.out.println("testUpdateScooterInvalidMaxCap");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateScooter("SCTR001", 5, 2, 2, 3, -1, 1, 1.0, 2);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateScooterInvalidActualCapMin() {
        System.out.println("testUpdateScooterInvalidActualCapMin");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateScooter("SCTR001", 5, 2, 2, 3, 11, -1, 1.0, 2);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateScooterInvalidActualCapMax() {
        System.out.println("testUpdateScooterInvalidActualCapMax");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateScooter("SCTR001", 5, 2, 2, 3, 11, 101, 1.0, 2);
        assertEquals(result, expResult);
    }
}
