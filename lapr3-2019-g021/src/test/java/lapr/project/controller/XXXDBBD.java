/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;
import lapr.project.data.mock.BicycleMock;
import lapr.project.data.mock.ParkMock;
import lapr.project.data.mock.ScooterMock;
import lapr.project.data.mock.UsersMock;
import lapr.project.model.Bicycle;
import lapr.project.model.ParkRegistry;
import lapr.project.model.User;
import lapr.project.model.UserRegistry;
import lapr.project.model.VehicleRegistry;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author roger40
 */
public class XXXDBBD {
    
    AddBicycleController abc;
    VehicleRegistry vhr;
    BicycleMock bmock;
    ScooterMock smock;
    UsersMock umock;
    ParkMock pmock;
    ParkRegistry pr;
    UserRegistry ur;
    
    public XXXDBBD() {
        
    abc = new AddBicycleController(false);
    vhr = new VehicleRegistry(false);
    pr = new ParkRegistry(false);
     bmock = new BicycleMock();
     smock = new ScooterMock();
     umock =  new UsersMock();
     pmock = new ParkMock();
     ur = new UserRegistry(false);
    
    
}
    
    
//@Test
    public void test1AddBike(){
        pr.addPark(bmock.getAllBicycles().get(0).getAssignedPark());
        pr.addPark(bmock.getAllBicycles().get(1).getAssignedPark());
        vhr.addBicycle(bmock.getAllBicycles().get(0));
    vhr.addScooter(smock.getAllScooters().get(0));
    vhr.addScooter(smock.getAllScooters().get(1));
         User user1 = new User("email1@gmail.com", "Francisco Magalhaes", "12345678", "0000000000000001", 'm', 182, 90, 13);
		user1.setAdmin(false);
       ur.registerUser(user1);
       
        User user2 = new User("email2@gmail.com", "roger", "12345678", "0000000000000002", 'm', 182, 90, 13);
		user2.setAdmin(false);
       ur.registerUser(user2);
       
        User user3 = new User("email3@gmail.com", "roger2", "12345678", "0000000000000003", 'm', 182, 90, 13);
		user3.setAdmin(false);
       ur.registerUser(user3);
        
        
    }
    
    
//@Test
    public void test2UnlockBike(){
       
         assertEquals(vhr.unlockBicycle("BIKE001", "email1@gmail.com"), true);
      assertEquals(vhr.unlockAnyScooterAtPark("1", "email1@gmail.com"), false); // user ja tem bike
     
            assertEquals(vhr.unlockAnyScooterAtPark("1", "email2@gmail.com"), true); // vai fazer unlock a scooter id - SCT 003
            
            
             assertEquals(vhr.unlockSpecificScooter("SCTR002", "email3@gmail.com"), true);
        
    }
    
    
    
  // @Test
    public void test2LockBike(){
       
        assertEquals(vhr.lockBicycle("email1@gmail.com", "2"), true);
   // // // // assertEquals(vhr.lockBicycle( "emailNNNN@gmail.com", 3, -3), false); // user nao existe
    
          assertEquals(vhr.lockScooter("email2@gmail.com", "2"), true); // vai fazer lock a scooter id - SCT 002
           
            
             assertEquals(vhr.lockScooter( "email3@gmail.com", 4, -4), true);
        
    }
    
    
    
    
}
