package lapr.project.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AddParkControllerTest {
	
	AddParkController oController = new AddParkController(true);
	
	public void setUp() {
		oController = new AddParkController(true);
	}
	
	public AddParkControllerTest() {
	}

	/**
	 * Test of newValidPark method, of class AddParkController.
	 */
	@Test
	public void testNewValidPark() {
		setUp();
		System.out.println("testNewValidPark");
		boolean expResult = true;
		boolean result = oController.newPark("1", -4, 2, 2, "Park Test 1", 20, 10, 1.4,0.5);
		assertEquals(expResult, result);
	}
	
	@Test
	public void testNewInvalidPark() {
		setUp();
		System.out.println("testNewInvalidPark");
		boolean expResult = false;
		boolean result = oController.newPark("1", -999, -999, 2, "Park Test 1", 20, 10, 1.4,0.5);
		assertEquals(expResult, result);
	}

	/**
	 * Test of addValidPark method, of class AddParkController.
	 */
	@Test
	public void testAddValidPark() {
		setUp();
		System.out.println("testAddValidPark");
		boolean expResult = true;
		oController.newPark("90", -10, 10, 0, "Park Test 1", 20, 10, 1.4,0.5);
		boolean result = oController.addPark();
		assertEquals(expResult, result);
	}
	
	@Test
	public void testAddInvalidPark() {
		setUp();
		System.out.println("testAddInvalidPark");
		boolean expResult = false;
		oController.newPark("2", 3, 4, 4, "Varzim", 1, 2, 3, 4);
		boolean result = oController.addPark();
		assertEquals(expResult, result);
	}
}