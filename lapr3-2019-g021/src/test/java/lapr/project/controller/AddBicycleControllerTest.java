package lapr.project.controller;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author roger40
 */
public class AddBicycleControllerTest {

        AddBicycleController oController;

        public void setUp() {
             System.out.println("setup");
                oController = new AddBicycleController(true);
        }

        public AddBicycleControllerTest() {
        }

        /**
         * Test of newValidBicycle method, of class AddBicycleController.
         */
        @Test
        public void testNewValidBicycle() {
                setUp();
                System.out.println("testNewValidBicycle");
                boolean expResult = true;
                boolean result = oController.newBicycle("VALID BIKE", 13, 2, 3, 2, 3, 2);
                assertEquals(expResult, result);
        }

        @Test
        public void testNewInvalidBicycle() {
                setUp();
                System.out.println("testNewInvalidBicycle");
                boolean expResult = false;
                boolean result = oController.newBicycle("INVALID BIKE", -13, -3, 3, 2, 3, 2);
                assertEquals(expResult, result);
        }

        /**
         * Test of addValidBicycle method, of class AddBicycleController.
         */
        @Test
        public void testAddValidBicycle() {
                setUp();
                System.out.println("testAddValidBicycle");
                boolean expResult = true;
                oController.newBicycle("BIKE099", 15, 2, 3, 20, 1.05, 15);
                boolean result = oController.addBicycle();
                assertEquals(expResult, result);
        }

        @Test
        public void testAddInvalidBicycle() {
                setUp();
                System.out.println("testAddInvalidBicycle");
                boolean expResult = false;
                oController.newBicycle("BIKE001", 15, -3, -3, 20, 1.05, 15);
                boolean result = oController.addBicycle();
                assertEquals(expResult, result);
        }

}
