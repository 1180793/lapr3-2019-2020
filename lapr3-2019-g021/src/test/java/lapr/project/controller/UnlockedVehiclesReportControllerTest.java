/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author roger40
 */
public class UnlockedVehiclesReportControllerTest {

    UnlockedVehiclesReportController uvc;

    public UnlockedVehiclesReportControllerTest() {
        uvc = new UnlockedVehiclesReportController(true);
    }

    @Test
    public void testGetUnlockedVehiclesReport() {

        int count = 0;
        for (String s : uvc.getUnlockedVehiclesReport().values()) { // verifica os valores das keys dos mapas
            if (count == 0) {
                assertEquals(s, "BIKE006");

            }
            if (count == 1) {
                assertEquals(s, "SCTR001");

            }
            count++;

        }

        count = 0;
        for (String s : uvc.getUnlockedVehiclesReport().keySet()) {  // verifica as keys do mapa
            if (count == 0) {
                assertEquals(s, "roger1@gmail.com");

            }
            if (count == 1) {
                assertEquals(s, "roger2@gmail.com");

            }
            count++;

        }
    }

}
