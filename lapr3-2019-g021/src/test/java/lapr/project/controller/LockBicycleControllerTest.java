/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author roger40
 */
public class LockBicycleControllerTest {
    LockBicycleController lbc;
    LockBicycleController lbc2;
      LockBicycleController lbc3;
     LockBicycleController lbc4;
   
    /**
     * Test of LockBicycle method, of class LockBicycleController.
     */
    
    

    
    @Test
    public void testLockBicycleGivenCords() {
        
        lbc = new LockBicycleController("roger1@gmail.com", 2, 3, true); // user tem bicicleta e o park de destino existe (o park de destino tem de existir ou na bd ou na parkMock)
     lbc2 = new LockBicycleController("rogerXXX@gmail.com", 3.0, -3, true); //user nao tem bicicleta
     lbc3 = new LockBicycleController("roger1@gmail.com", 4, 100, true);  // user tem bicicleta e o park de destino NAO existe  (o park de destino tem de existir ou na bd ou na parkMock)
     lbc4 = new LockBicycleController("roger1@gmail.com", 100, 300, true);  // user tem bicicleta e o park de destino  existe MAS o park nao tem capacidade para mais bikes 
        
        
        System.out.println("LockBicycle");
         assertEquals(lbc.lockBicycleGivenParkCords(), true);
         assertEquals(lbc2.lockBicycleGivenParkCords(), false);
           assertEquals(lbc3.lockBicycleGivenParkCords(), false);
             assertEquals(lbc4.lockBicycleGivenParkCords(), false);
       
    }
    
    @Test
    public void testLockBicycleGivenId() {
        lbc = new LockBicycleController("roger1@gmail.com", "1", true); // user tem bicicleta e o park de destino existe (o park de destino tem de existir ou na bd ou na parkMock)
     lbc2 = new LockBicycleController("rogerXXX@gmail.com", "1", true); //user nao tem bicicleta
     lbc3 = new LockBicycleController("roger1@gmail.com", "NOexists", true);  // user tem bicicleta e o park de destino NAO existe  (o park de destino tem de existir ou na bd ou na parkMock)
     lbc4 = new LockBicycleController("roger1@gmail.com", "5", true);  // user tem bicicleta e o park de destino  existe MAS o park nao tem capacidade para mais bikes 
        
        
        
        System.out.println("LockBicycle");
         assertEquals(lbc.lockBicycleGivenParkId(), true);
         assertEquals(lbc2.lockBicycleGivenParkId(), false);
           assertEquals(lbc3.lockBicycleGivenParkId(), false);
             assertEquals(lbc4.lockBicycleGivenParkId(), false);
       
    }
    
}
