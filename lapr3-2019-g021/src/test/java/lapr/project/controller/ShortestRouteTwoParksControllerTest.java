package lapr.project.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import lapr.project.model.Park;
import lapr.project.model.Path;
import lapr.project.model.PointOfInterest;
import lapr.project.model.graph.Graph;
import lapr.project.utils.GraphUtil;
import lapr.project.utils.Utils;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Francisco
 */
public class ShortestRouteTwoParksControllerTest {

    ShortestRouteTwoParksController controller;
    Park park1 = new Park("Park1", 41.123456, 9.153152, 10, "Park 1 for Graph test", 10, 6, 5, 5);
    Park park2 = new Park("Park2", 41.015153, 9.315251, 10, "Park 2 for Graph test", 10, 5, 0, 5);

    public void setUp() {
        controller = new ShortestRouteTwoParksController(true);
    }

    /**
     * Test of validateParks method, of class ShortestRouteTwoParksController.
     */
    @Test
    public void testValidateParks() throws Exception {
        System.out.println("validateParks");

        setUp();

        String startPark = "1";
        String endPark = "null";
        boolean expResult = false;
        boolean result = controller.validateParks(startPark, endPark);
        assertEquals(expResult, result);

        startPark = "null";
        endPark = "3";
        expResult = false;
        result = controller.validateParks(startPark, endPark);
        assertEquals(expResult, result);

        startPark = park1.getID();
        endPark = park2.getID();
        expResult = true;
        result = controller.validateParks(startPark, endPark);
        assertEquals(expResult, result);
    }

    /**
     * Test of getShortestRoute method, of class
     * ShortestRouteTwoParksController.
     */
    @Test
    public void testgetShortestRoute() throws SQLException {
        System.out.println("getShortestRoute");

        setUp();

        String startPark = park1.getID();
        String endPark = park2.getID();
        boolean res = controller.validateParks(startPark, endPark);

        List<PointOfInterest> expResult = new LinkedList<>();
        expResult.add(new PointOfInterest(park1.getLatitude(), park1.getLongitude()));
        expResult.add(new PointOfInterest(park2.getLatitude(), park2.getLongitude()));

        List<PointOfInterest> result = controller.getShortestRoute();

        System.out.println(result.size());

        assertEquals(expResult.get(0), result.get(0));
        assertEquals(expResult.get(1), result.get(1));
    }

    /**
     * Test of getMinDistance method, of class ShortestRouteTwoParksController.
     */
    @Test
    public void testGetMinDistance() throws SQLException {
        System.out.println("getMinDistance");

        setUp();

        String startPark = park1.getID();
        String endPark = park2.getID();
        boolean res = controller.validateParks(startPark, endPark);

        List<PointOfInterest> route = controller.getShortestRoute();

        double expResult = Utils.calculateDistance(new PointOfInterest(park1.getLatitude(), park1.getLongitude()), new PointOfInterest(park2.getLatitude(), park2.getLongitude())) / 1000;
        double result = controller.getMinDistance();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of insertCoordinates method, of class
     * ShortestRouteTwoParksController.
     */
    @Test
    public void testInsertCoordinates() {
        System.out.println("insertCoordinates");

        setUp();

        double originLatitudeInDegrees = park1.getLatitude();
        double originLongitudeInDegrees = park1.getLongitude();
        double destinyLatitudeInDegrees = park2.getLatitude();
        double destinyLongitudeInDegrees = park2.getLongitude();
        controller.insertCoordinates(originLatitudeInDegrees, originLongitudeInDegrees, destinyLatitudeInDegrees, destinyLongitudeInDegrees);

        List<PointOfInterest> route = controller.getShortestRoute();

        double expResult = Utils.calculateDistance(new PointOfInterest(park1.getLatitude(), park1.getLongitude()), new PointOfInterest(park2.getLatitude(), park2.getLongitude())) / 1000;
        double result = controller.getMinDistance();
        assertEquals(expResult, result, 0.0);
    }

}
