
package lapr.project.controller;

import lapr.project.model.User;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Francisco
 */
public class LoginControllerTest {
    
    LoginController controller;

    /**
     * Test of loginUser method, of class LoginController.
     */
    @Test
    public void testLoginUserSucessful() {
        System.out.println("loginUserSucessful");
        
        controller = new LoginController("email1@gmail.com", "12345678", true);
        
        User expResult = new User("email1@gmail.com", "Francisco Magalhaes", "12345678", "0000000000000001", 'm', 182, 90, 13);
        expResult.setAdmin(false);
                
        User result = controller.loginUser();
        assertEquals(expResult, result);
        
    }
    
    
    @Test
    public void testLoginUserFailed() {
        System.out.println("loginUserFailed");
        
        controller = new LoginController("email1@gmail.com", "password", true);
        
        User expResult = null;
                
        User result = controller.loginUser();
        assertEquals(expResult, result);
    }
    
}
