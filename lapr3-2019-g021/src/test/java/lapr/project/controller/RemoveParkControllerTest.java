package lapr.project.controller;

import lapr.project.model.Park;
import lapr.project.model.ParkRegistry;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 *
 * @author daniel
 */
public class RemoveParkControllerTest {

	RemoveParkController rpc;
	ParkRegistry pr;

	public RemoveParkControllerTest() {
		rpc = new RemoveParkController(true);
	}

	@Test
	public void testRemovePark() {
		Park park1 = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 220, 16);
		assertEquals(1, rpc.removePark(park1.getID()));
	}

	@Test
	public void testRemovePark1() {
		Park parkSet = new Park("6", 10, 30, 4, "Leça", 1, 2, 220, 16);
		assertEquals(0, rpc.removePark(parkSet.getID()));
	}
}
