/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.List;
import lapr.project.model.Bicycle;
import lapr.project.model.Scooter;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author roger40
 */
public class getNumberOfVehiclesAtParkControllerTest {
    
     getNumberOfVehiclesAtParkController gnv;
    
    public getNumberOfVehiclesAtParkControllerTest() {
        gnv = new getNumberOfVehiclesAtParkController(true);
    }
    
    

    /**
     * Test of getNumberOfBicyclesAtPark method, of class getNumberOfVehiclesAtParkController.
     */
    @Test
    public void testGetNumberOfBicyclesAtPark_String() {
        System.out.println("getNumberOfBicyclesAtPark");
        assertNotEquals(gnv.getNumberOfBicyclesAtPark("1"),0);
    }

    /**
     * Test of getNumberOfBicyclesAtPark method, of class getNumberOfVehiclesAtParkController.
     */
    @Test
    public void testGetNumberOfBicyclesAtPark_double_double() {
        System.out.println("getNumberOfBicyclesAtPark");
         assertNotEquals(gnv.getNumberOfBicyclesAtPark(3,4),0);
        
    }

    /**
     * Test of getNumberOfScootersAtPark method, of class getNumberOfVehiclesAtParkController.
     */
    @Test
    public void testGetNumberOfScootersAtPark_String() {
        System.out.println("getNumberOfScootersAtPark");
        assertNotEquals(gnv.getNumberOfScootersAtPark("1"),0);
    }

    /**
     * Test of getNumberOfScootersAtPark method, of class getNumberOfVehiclesAtParkController.
     */
    @Test
    public void testGetNumberOfScootersAtPark_double_double() {
        System.out.println("getNumberOfScootersAtPark");
        assertNotEquals(gnv.getNumberOfScootersAtPark(3,4),0);
    }
    
}
