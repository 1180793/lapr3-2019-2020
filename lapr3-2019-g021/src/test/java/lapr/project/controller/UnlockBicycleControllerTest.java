///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
package lapr.project.controller;
//
import java.util.List;
import lapr.project.data.mock.BicycleMock;
import lapr.project.data.mock.ScooterMock;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Scooter;
import lapr.project.model.User;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
//
///**
// *
// * @author roger40
// */
public class UnlockBicycleControllerTest {
//    
//    
//    
    UnlockBicycleController uvc;
   Park park1;
    User user1;
    User user2;
    User user3;
    BicycleMock mockBikes;
   // ScooterMock mockScooter;
    Bicycle bikex;
    List <Bicycle> listbikes;
    List <Scooter> listscot;
     Scooter scotx;
    public UnlockBicycleControllerTest() {
     mockBikes = new BicycleMock();
    // mockScooter = new ScooterMock();
    // listscot = mockScooter.getAllScooters();
     listbikes = mockBikes.getAllBicycles();
          park1 = new Park("1", 100.0, 210.0, 45, "parque teste1", 100, 100, 45, 25); 
         // park2 = new Park(2, 100.0, 210.0, 45.0, "parque teste1", 100, 100, 45, 25);// crio um park com um id igual a um parque que ja existe na mock, so vou extrair o id a este parque
           user1 = new User("nobikeuser@gmail.com", "roger", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);  //crio um user que nao tem bikes requesitadas
           user2 = new User("roger1@gmail.com", "roger1", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25); //crio um user que ja tem bikes requesitadas (email ja esta atribuido a uma bike
               user3 = new User("roger2@gmail.com", "roger2", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);
           
          bikex = new Bicycle("1", 15, park1, 20, 1.05,15);
             
          
        
            scotx = new Scooter("SCTR002", 25, 1, park1, 200.0, 100, 1.22, 2.1, 245);
           uvc = new UnlockBicycleController("email5@gmail.com", true);
    }

    
    @Test
    public void testSugestVehiclesBicycles() {
        System.out.println("sugestVehiclesBicycles");
       
     
     assertEquals(uvc.suggestBicycles("1").size(), 3); 
    
       
   }
   
   @Test
    public void testUnlockBicycle() {
        System.out.println("testUnlockBicycle");
       
       assertEquals(listbikes.get(0).getAssignedUser(),null);
        
        
     boolean test1=  uvc.unlockBicycleByUser("BIKE001");  // quero desbloquiar a bicicleta de id 1
        assertEquals(test1,true);
        
       
        UnlockBicycleController uvc2 = new UnlockBicycleController("roger1@gmail.com", true);
       
          boolean test2=  uvc2.unlockBicycleByUser("BIKE003"); 
        assertEquals(test2,false);  // user ja tem bicicleta requsitada e nao pode voltar a fazer);
        
      
        
    
        
        
        
        
    }

  
//    @Test
//    public void testUnlockScooter() {
//        System.out.println("testUnlockBicycle");
//       
//        assertEquals(listscot.get(0).getAssignedUser(),null);
//        
//        
//     boolean test1=  uvc.unlockScooterByUser(scotx, null);
//        assertEquals(test1,true);
//        
//     UnlockBicycleController uvc2 = new UnlockBicycleController(user2,   park1, true, true);
//        
//          boolean test2=  uvc2.unlockScooterByUser(scotx,null); 
//        assertEquals(test2,false);  // user ja tem bike requsitada e nao pode voltar a fazer);
//        
//        
//         UnlockBicycleController uvc3 = new UnlockBicycleController(user3,   park1, true, true); // este user ja tem uma scooter
//        
//          boolean test3=  uvc2.unlockScooterByUser(scotx,null); 
//        assertEquals(test3,false);  // user ja tem scooter requsitada e nao pode voltar a fazer);
//        
//        Scooter  scotx2 = new Scooter("SCTR0190", 25, 1, park1, 200.0, 100, 1.22, 2.1);
//        
//        
//        
//        
//         uvc = new UnlockBicycleController(user1,   park1, true, true);
//         boolean test4=  uvc.unlockScooterByUser(scotx2, null);  // esta scooter nao esta na lista de scooters eligiveis (sugested) para o user, o suggest user tambem filtra um pouco a escolha do user
//        assertEquals(test1,true);
//        
//    }
    
    
}
