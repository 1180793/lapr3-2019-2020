package lapr.project.controller;

import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class GetParkChargingStatusReportControllerTest {
	
	GetParkChargingStatusReportController oController;
	
	public void setUp() {
		oController = new GetParkChargingStatusReportController(true);
	}
	
	public GetParkChargingStatusReportControllerTest() {
	}

	/**
	 * Test of getChargingStatusReport method, of class UserRegistrationController.
	 */
	@Test
	public void testGetValidParkChargingStatusReport() {
		setUp();
		System.out.println("testGetValidChargingStatusReport");
		Map<String, Integer> result = oController.getChargingStatusReport("1");
		assertNotNull(result);
	}
	
	@Test
	public void testGetEmptyParkChargingStatusReport() {
		setUp();
		System.out.println("testGetEmptyParkChargingStatusReport");
		Map<String, Integer> result = oController.getChargingStatusReport("3");
		assertNull(result);
	}
	
	@Test
	public void testGetNullParkChargingStatusReport() {
		setUp();
		System.out.println("testGetNullParkChargingStatusReport");
		Map<String, Integer> result = oController.getChargingStatusReport("99");
		assertNull(result);
	}
}
