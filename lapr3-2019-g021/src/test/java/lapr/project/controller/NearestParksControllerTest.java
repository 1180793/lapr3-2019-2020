package lapr.project.controller;

import java.util.Iterator;
import java.util.Set;
import lapr.project.model.Park;
import lapr.project.model.PointOfInterest;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Francisco
 */
public class NearestParksControllerTest {

    NearestParksController controller1;
    NearestParksController controller2;

    public void setUp() {
        controller1 = new NearestParksController(true);
        controller1.newPointOfInterest(10, 40);

        controller2 = new NearestParksController(true, 10000);
        controller2.newPointOfInterest(10, 40);
    }

    /**
     * Test of newPointOfInterest method, of class NearestParksController.
     */
    @Test
    public void testNewPointOfInterest() {
        System.out.println("newPointOfInterest");

        double lat = 10.0;
        double lon = 20.0;
        NearestParksController instance = new NearestParksController(true);
        instance.newPointOfInterest(lat, lon);

        PointOfInterest expResult = new PointOfInterest(lat, lon);
        PointOfInterest result = instance.getPointOfInterest();

        assertEquals(expResult, result);
    }

    /**
     * Test of getNearestParksByCoordinates method, of class
     * NearestParksController.
     */
    @Test
    public void testGetNearestParksByCoordinates() {
        System.out.println("getNearestParksByCoordinates");

        this.setUp();

        Park park1 = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
        Park park2 = new Park("2", 3, 4, 4, "Varzim", 1, 2, 3, 4);
        Park park3 = new Park("3", 10, 10, 4, "Porto", 1, 2, 3, 4);
        Park park4 = new Park("4", 10, 30, 4, "Matosinhos", 1, 2, 3, 4);

//        Set<Park> result = controller1.getNearestParksByCoordinates().keySet();
//        Iterator<Park> it = result.iterator();
//
//        assertEquals(it.next(), park4);
//        assertEquals(it.next(), park3);
//        assertEquals(it.next(), park2);
//        assertEquals(it.next(), park1);

        Set<Park> result = controller2.getNearestParksByCoordinates().keySet();
        Iterator<Park> it = result.iterator();

        assertEquals(it.next(), park4);
        assertEquals(it.next(), park3);
        assertEquals(it.next(), park2);
        assertEquals(it.next(), park1);

    }

    /**
     * Test of getPointOfInterest method, of class NearestParksController.
     */
    @Test
    public void testGetPointOfInterest() {
        System.out.println("getPointOfInterest");

        NearestParksController instance = new NearestParksController(true);

        double lat = 10.0;
        double lon = 20.0;
        instance.newPointOfInterest(lat, lon);
        PointOfInterest expResult = new PointOfInterest(lat, lon);
        PointOfInterest result = instance.getPointOfInterest();
        assertEquals(expResult, result);

    }

}
