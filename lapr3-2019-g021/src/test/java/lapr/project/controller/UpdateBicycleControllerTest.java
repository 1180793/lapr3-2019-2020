package lapr.project.controller;

import lapr.project.model.Bicycle;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Joao Mata <1151352@isep.ipp.pt>
 */
public class UpdateBicycleControllerTest {

    UpdateBicycleController oController;

    public void setUp() {
        oController = new UpdateBicycleController(true);
    }

    public UpdateBicycleControllerTest() {
    }

    @Test
    public void testUpdateExistingBicycle() {
        setUp();
        System.out.println("testUpdateExistingBicycle");
        boolean expResult = true;
        Bicycle bike = oController.getBicycleById("BIKE001");
        assertNotNull(bike);

        boolean result = oController.updateBicycle("BIKE001", 5, 2, 3, 3, 2, 2);

        assertEquals(expResult, result);
    }

    @Test
    public void testUpdateToDbExistingBicycle() {
        setUp();
        System.out.println("testUpdateToDbExistingBicycle");
        boolean expResult = true;
        Bicycle bike = oController.getBicycleById("BIKE001");
        assertNotNull(bike);

        boolean result = oController.updateBicycle("BIKE001", 5, 2, 3, 3, 2, 2);

        result = oController.updateBicycle();
        assertEquals(expResult, result);

    }

    @Test
    public void testUpdateNonExistingBicycle() {
        setUp();
        System.out.println("testUpdateNonExistingBicycle");
        Bicycle bike = oController.getBicycleById("XPTO");
        assertNull(bike);
    }

    /**
     * Test of getBicycle method, of class UpdateBicycleController.
     */
    @Test
    public void testGetBicycle() {
        System.out.println("getBicycle");
        setUp();
        Bicycle expResult = null;
        Bicycle result = oController.getBicycle();
        assertEquals(expResult, result);
    }
    /**
     * Test of getScooter method, of class UpdateScooterController.
     */
    @Test
    public void testGetBicycleNotNull() {
        System.out.println("getScooterNotNull");
        setUp();
        oController.updateBicycle("BIKE001", 5, 2, 3, 3, 2, 2);
        Bicycle result = oController.getBicycle();
        assertNotNull(result);
    }

    @Test
    public void testUpdateBicycleInvalidWeight() {
        System.out.println("testUpdateBicycleInvalidWeight");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateBicycle("BIKE001", 0, 2, 3, 3, 2, 2);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateBicycleInvalidAeroCoeficient() {
        System.out.println("testUpdateBicycleInvalidAeroCoeficient");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateBicycle("BIKE001", 5, 2, 3, 0, 2, 2);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateBicycleInvalidFrontalArea() {
        System.out.println("testUpdateBicycleInvalidFrontalArea");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateBicycle("BIKE001", 5, 2, 3, 3, 0, 2);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateBicycleInvalidWheelSize() {
        System.out.println("testUpdateBicycleInvalidWheelSize");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateBicycle("BIKE001", 5, 2, 3, 3, 2, 0);
        assertEquals(result, expResult);
    }
    
    @Test
    public void testUpdateBicycleInvalidPark() {
        System.out.println("testUpdateBicycleInvalidPark");
        setUp();
        boolean expResult = false;
        boolean result = oController.updateBicycle("BIKE001", 5, 0, 0, 3, 2, 2);
        assertEquals(result, expResult);
    }
}
