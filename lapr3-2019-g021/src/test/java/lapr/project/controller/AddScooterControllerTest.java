package lapr.project.controller;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author roger40
 */
public class AddScooterControllerTest {

        AddScooterController oController;

        public void setUp() {
                oController = new AddScooterController(true);
        }
        
        /**
         * Test of newScooter method, of class AddScooterController.
         */
        @Test
        public void testNewValidScooter() {
                setUp();
                System.out.println("testNewValidScooter");
                boolean expResult = true;
                boolean result = oController.newScooter("SCTR001", 20, 1, 2, 3, 200.0, 75, 1.10, 0.3, 240);
                assertEquals(expResult, result);
        }

        @Test
        public void testNewInvalidBicycle() {
                setUp();
                System.out.println("testNewInvalidBicycle");
                boolean expResult = false;
                boolean result = oController.newScooter("SCTR001", -20, -1, -3, 3, 200.0, 75, 1.10, 0.3, 245);
                assertEquals(expResult, result);
        }
        
        /**
         * Test of addValidScooter method, of class AddScooterController.
         */
        @Test
        public void testAddValidScooter() {
                setUp();
                System.out.println("testAddValidScooter");
                boolean expResult = true;
                oController.newScooter("SCTR091", 20, 1, 2, 3, 200.0, 75, 1.10, 0.3, 245);
                boolean result = oController.addScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testAddInvalidScooter() {
                setUp();
                System.out.println("testAddInvalidScooter");
                boolean expResult = false;
                oController.newScooter("SCTR002", 20, 1, -3, 3, 200.0, 75, 1.10, 0.3, 245);
                boolean result = oController.addScooter();
                assertEquals(expResult, result);
        }
}