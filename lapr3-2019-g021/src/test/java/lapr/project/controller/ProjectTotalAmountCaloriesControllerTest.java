package lapr.project.controller;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Joao Mata <1151352@isep.ipp.pt>
 */
public class ProjectTotalAmountCaloriesControllerTest {

	ProjectTotalAmountCaloriesController oController;

	public void setUp() {
		oController = new ProjectTotalAmountCaloriesController(true);
	}

	public ProjectTotalAmountCaloriesControllerTest() {
	}

	//@Test
	public void testCalculateBurntCaloriesInvalidParkOrigin() {
		setUp();
		System.out.println("testCalculateBurntCaloriesInvalidParkOrigin");
		double expResult = -1;
		double result = oController.calculateTotalAmountCalories("BIKE001", -2, -3, 3, 4, "email1@gmail.com");

		assertEquals(expResult, result);
	}

	//@Test
	public void testCalculateBurntCaloriesInvalidParkDestination() {
		setUp();
		System.out.println("testCalculateBurntCaloriesInvalidParkDestination");
		double expResult = -1;
		double result = oController.calculateTotalAmountCalories("BIKE001", 2, 3, -3, -3, "email1@gmail.com");

		assertEquals(expResult, result);
	}

	//@Test
	public void testCalculateBurntCaloriesInvalidBicycle() {
		setUp();
		System.out.println("testCalculateBurntCaloriesInvalidBicycle");
		double expResult = -1;
		double result = oController.calculateTotalAmountCalories("xxx", 2, 3, 3, 4, "email1@gmail.com");

		assertEquals(expResult, result);
	}

	//@Test
	public void testCalculateBurntCaloriesInvalidUser() {
		setUp();
		System.out.println("testCalculateBurntCaloriesInvalidUser");
		double expResult = -1;
		double result = oController.calculateTotalAmountCalories("BIKE001", 2, 3, 3, 4, "xxx");

		assertEquals(expResult, result);
	}

	//@Test
	public void testCalculateBurntCaloriesOk() {
		setUp();
		System.out.println("testCalculateBurntCaloriesInvalidUser");
		double expResult = 178247.83;
		double result = oController.calculateTotalAmountCalories("BIKE001", 2, 3, 3, 4, "email1@gmail.com");

		assertEquals(expResult, result, 0.01);
	}
}
