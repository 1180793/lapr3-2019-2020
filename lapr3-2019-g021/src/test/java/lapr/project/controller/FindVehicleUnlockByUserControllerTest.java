/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.Bicycle;
import lapr.project.model.Scooter;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;

/**
 *
 * @author roger40
 */
public class FindVehicleUnlockByUserControllerTest {
    FindVehicleUnlockByUserController fvuc ;
           
    public FindVehicleUnlockByUserControllerTest() {
        fvuc = new FindVehicleUnlockByUserController(true);
    }
    
   
    @Test
    public void testFindUnlockedBicycleByUser() {
      
       assertNotNull( fvuc.findUnlockedBicycleByUser("roger1@gmail.com"));
       assertNull( fvuc.findUnlockedBicycleByUser("rogerXXXXX@gmail.com"));
    }

    /**
     * Test of findUnlockedScooterByUser method, of class FindVehicleUnlockByUserController.
     */
    @Test
    public void testFindUnlockedScooterByUser() {
         assertNotNull( fvuc.findUnlockedScooterByUser("roger2@gmail.com"));
       assertNull( fvuc.findUnlockedBicycleByUser("rogerXXXXX@gmail.com"));

       
    }
    
}
