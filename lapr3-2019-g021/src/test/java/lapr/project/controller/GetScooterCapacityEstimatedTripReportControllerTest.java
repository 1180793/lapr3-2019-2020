package lapr.project.controller;

import java.util.List;
import lapr.project.model.Scooter;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;


/**
 * @author Daniel
 */
public class GetScooterCapacityEstimatedTripReportControllerTest {
	
	GetScooterCapacityEstimatedTripReportController oController;
        	
	public void setUp() {
		oController = new GetScooterCapacityEstimatedTripReportController(true);
	}
	
	public GetScooterCapacityEstimatedTripReportControllerTest() {
	}
        
	@Test
	public void testGetScooterCapacityEstimatedTripReport() {
		setUp();
		System.out.println("testGetScooterCapacityEstimatedTripReport");
		List<Scooter> result = oController.GetScooterCapacityEstimatedTripReport(2000);
		assertNotNull(result);
                 assertEquals(result.size(), 6); //nenhuma scooter consegue fazer essa distancia
                
                setUp();
		System.out.println("testGetScooterCapacityEstimatedTripReport");
		List<Scooter> result2 = oController.GetScooterCapacityEstimatedTripReport(100);
		 assertEquals(result2.size(), 4); // ha 6 scooters na mock, 4 nao tem capacidade para fazer 100 kilometros, as outras têm
	}
}
