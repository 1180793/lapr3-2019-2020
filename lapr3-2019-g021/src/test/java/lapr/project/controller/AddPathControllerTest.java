package lapr.project.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AddPathControllerTest {

        AddPathController oController;

        public void setUp() {
                oController = new AddPathController(true);
        }

        public AddPathControllerTest() {
        }

        /**
         * Test of newValidPath method, of class AddPathController.
         */
        @Test
        public void testNewValidPath() {
                setUp();
                System.out.println("testNewValidPath");
                boolean expResult = true;
                boolean result = oController.newPath(41.152712, -8.609297, 42.110212, -8.234267, 3, 80, 13);
                assertEquals(expResult, result);
        }

        @Test
        public void testNewInvalidPath() {
                setUp();
                System.out.println("testNewInvalidPath");
                boolean expResult = false;
                boolean result = oController.newPath(941.152712, -8.609297, 42.110212, -8.234267, 3, 80, 13);
                assertEquals(expResult, result);
        }

        /**
         * Test of addValidPath method, of class AddPathController.
         */
        @Test
        public void testAddValidPath() {
                setUp();
                System.out.println("testAddValidPath");
                boolean expResult = true;
                oController.newPath(13.152712, -8.609297, 42.110212, -8.234267, 3, 80, 13);
                boolean result = oController.addPath();
                assertEquals(expResult, result);
        }

        @Test
        public void testAddInvalidPath() {
                setUp();
                System.out.println("testAddInvalidPath");
                boolean expResult = false;
                oController.newPath(41.123456, 9.153152, 41.015153, 9.315251, 0.8, 220, 4.1);
                boolean result = oController.addPath();
                assertEquals(expResult, result);
        }
}
