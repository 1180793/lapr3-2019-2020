package lapr.project.controller;

import java.util.LinkedList;
import java.util.List;
import lapr.project.model.Park;
import lapr.project.model.Path;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Francisco
 */
public class ShortestRouteTwoParksWithSequenceControllerTest {

    private ShortestRouteTwoParksWithSequenceController controller;
    Park park1 = new Park("Park1", 41.123456, 9.153152, 10, "Park 1 for Graph test", 10, 6, 5, 5);
    Park park2 = new Park("Park2", 41.015153, 9.315251, 10, "Park 2 for Graph test", 10, 5, 0, 5);
    Park park3 = new Park("Park3", 41.521512, 9.153120, 50, "Park 3 for Graph test", 10, 3, 0, 3);

    public void setUp() {
        controller = new ShortestRouteTwoParksWithSequenceController(true);
    }

    /**
     * Test of validateParks method, of class
     * ShortestRouteTwoParksWithSequenceController.
     */
    @Test
    public void testValidateParks() {
        System.out.println("validateParks");

        setUp();

        String startPark = park1.getID();
        String endPark = "null";
        List<String> sequence = new LinkedList<String>();
        sequence.add(park2.getID());

        int numRoutes = 1;
        boolean expResult = false;
        boolean result = controller.validateParks(startPark, endPark, sequence, numRoutes);
        assertEquals(expResult, result);

        startPark = "null";
        result = controller.validateParks(startPark, endPark, sequence, numRoutes);
        assertEquals(expResult, result);

        startPark = park1.getID();
        endPark = park3.getID();
        expResult = true;
        result = controller.validateParks(startPark, endPark, sequence, numRoutes);
        assertEquals(expResult, result);

        sequence = new LinkedList<String>();
        sequence.add("null");
        expResult = false;
        result = controller.validateParks(startPark, endPark, sequence, numRoutes);
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateShortestPath method, of class
     * ShortestRouteTwoParksWithSequenceController.
     */
    @Test
    public void testCalculateShortestPath() {
        System.out.println("calculateShortestPath");

        setUp();

        List<LinkedList<Path>> expResult = new LinkedList<>();
        LinkedList<Path> list = new LinkedList<>();
        list.add(new Path(park1.getLatitude(), park1.getLongitude(), park2.getLatitude(), park2.getLongitude(), 0.8, 220, 4.1));
        list.add(new Path(park2.getLatitude(), park2.getLongitude(), park3.getLatitude(), park3.getLongitude(), 0.8, 50, 3.2));
        expResult.add(list);

        List<String> sequence = new LinkedList<String>();
        sequence.add(park2.getID());
        String startPark = park1.getID();
        String endPark = park3.getID();
        boolean res = controller.validateParks(startPark, endPark, sequence, 1);

        List<LinkedList<Path>> result = controller.calculateShortestPath();
        assertEquals(expResult, result);
    }

    /**
     * Test of insertPointOfInterest method, of class ShortestRouteTwoParksWithSequenceController.
     */
    @Test
    public void testInsertPointOfInterest() {
        System.out.println("insertPointOfInterest");
        
        setUp();
        
        double originLatitudeInDegrees = park1.getLatitude();
        double originLongitudeInDegrees = park1.getLongitude();
        double destinationLatitudeInDegrees = park3.getLatitude();
        double destinationLongitudeInDegrees = park3.getLongitude();
        
        List<LinkedList<Path>> expResult = new LinkedList<>();
        LinkedList<Path> list = new LinkedList<>();
        list.add(new Path(park1.getLatitude(), park1.getLongitude(), park2.getLatitude(), park2.getLongitude(), 0.8, 220, 4.1));
        list.add(new Path(park2.getLatitude(), park2.getLongitude(), park3.getLatitude(), park3.getLongitude(), 0.8, 50, 3.2));
        expResult.add(list);

        List<String> sequence = new LinkedList<String>();
        sequence.add(park2.getID());
        controller.insertPointOfInterest(originLatitudeInDegrees, originLongitudeInDegrees, destinationLatitudeInDegrees, destinationLongitudeInDegrees, sequence);

        List<LinkedList<Path>> result = controller.calculateShortestPath();
        assertEquals(expResult, result);
    }

}
