package lapr.project.controller;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AddPOIControllerTest {
	
	AddPOIController oController;
	
	public void setUp() {
		oController = new AddPOIController(true);
	}
	
	public AddPOIControllerTest() {
	}

	/**
	 * Test of newValidPOI method, of class AddPOIController.
	 */
	@Test
	public void testNewValidPOI() {
		setUp();
		System.out.println("testNewValidPOI");
		boolean expResult = true;
		boolean result = oController.newPOI(20.003479, -3.854783, 3, "validPOI");
		assertEquals(expResult, result);
	}
	
	@Test
	public void testNewInvalidPOI() {
		setUp();
		System.out.println("testNewInvalidPOI");
		boolean expResult = false;
		boolean result = oController.newPOI(-500, -500, 3, "invalidPOI");
		assertEquals(expResult, result);
	}

	/**
	 * Test of addValidPOI method, of class AddPOIController.
	 */
	@Test
	public void testAddValidPOI() {
		setUp();
		System.out.println("testAddValidPOI");
		boolean expResult = true;
		oController.newPOI(20.003479, -3.854783, 3, "validPOI");
		boolean result = oController.addPOI();
		assertEquals(expResult, result);
	}
	
	@Test
	public void testAddInvalidPOI() {
		setUp();
		System.out.println("testAddInvalidPOI");
		boolean expResult = false;
		oController.newPOI(43.003479, -8.854783, 3, "invalidPOI");
		boolean result = oController.addPOI();
		assertEquals(expResult, result);
	}
}
