package lapr.project.controller;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Bicycle;
import lapr.project.model.Park;
import lapr.project.model.Scooter;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Omen
 */
public class AvailableVehiclesControllerTest {
    
    AvailableVehiclesController oController;
    
    public void setUp(){
        oController = new AvailableVehiclesController(true);
    }
    
    /**
     * Test of getAvailableBicycles method, of class AvailableVehiclesController.
     */
    @Test
    public void testGetAvailableBicyclesByID() {
        setUp();
        System.out.println("getAvailableBicyclesByID");
        
        List<Bicycle> expResult = new ArrayList<>();
        
        Park park1 = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 220, 16);

        expResult.add(new Bicycle("BIKE001", 15, park1, 20, 1.05, 15));
        expResult.add(new Bicycle("BIKE004", 17, park1, 20, 1.3, 17));
        expResult.add(new Bicycle("BIKE005", 18, park1, 20, 1.4, 15));
        
        List<Bicycle> result = oController.getAvailableBicycles("1");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetAvailableScootersByID() {
        setUp();
        System.out.println("getAvailableScootersByID");
        
        List<Scooter> expResult = new ArrayList<>();
        
        Park park1 = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 220, 16);
        
        expResult.add(new Scooter("SCTR002", 20, 1, park1, 1.0, 15, 1, 0.5, 245));
        expResult.add(new Scooter("SCTR003", 20, 2, park1, 1.5, 75, 0.90, 0.2, 235));
        expResult.add(new Scooter("SCTR004", 20, 2, park1, 2.0, 30, 1.2, 0.4, 230));
        expResult.add(new Scooter("SCTR005", 20, 2, park1, 2.2, 75, 1.30, 0.6, 225));      
        
        List<Scooter> result = oController.getAvailableScooters("1");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetAvailableBicyclesByCoord() {
        setUp();
        System.out.println("getAvailableBicyclesByCoord");
        
        List<Bicycle> expResult = new ArrayList<>();
        
        Park park1 = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 220, 16);

        expResult.add(new Bicycle("BIKE001", 15, park1, 20, 1.05, 15));
        expResult.add(new Bicycle("BIKE004", 17, park1, 20, 1.3, 17));
        expResult.add(new Bicycle("BIKE005", 18, park1, 20, 1.4, 15));
        
        List<Bicycle> result = oController.getAvailableBicycles(2, 3);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetAvailableScootersByCoord() {
        setUp();
        System.out.println("getAvailableScootersByCoord");
        
        List<Scooter> expResult = new ArrayList<>();
        
        Park park1 = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 220, 16);

        expResult.add(new Scooter("SCTR002", 20, 1, park1, 1.0, 15, 1, 0.5, 245));
        expResult.add(new Scooter("SCTR003", 20, 2, park1, 1.5, 75, 0.90, 0.2, 235));
        expResult.add(new Scooter("SCTR004", 20, 2, park1, 2.0, 30, 1.2, 0.4, 230));
        expResult.add(new Scooter("SCTR005", 20, 2, park1, 2.2, 75, 1.30, 0.6, 225)); 
        
        List<Scooter> result = oController.getAvailableScooters(2, 3);
        assertEquals(expResult, result);
    }   
}
