package lapr.project.controller;

import java.io.IOException;
import java.sql.SQLException;
import lapr.project.model.ParkRegistry;
import lapr.project.model.Park;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author daniel
 */
public class UpdateParkControllerTest {

    @Test
    public void UpdateParkControllerTest() {
        boolean teste = true;
        UpdateParkController upc = new UpdateParkController(teste);
        UpdateParkController upc1 = new UpdateParkController(teste);
        assertNotEquals(upc,upc1);
    }
        
    @Test
    public void testGetParkByID() throws IOException, SQLException{
        Park p = new Park("1", 2, 3, 4, "PÃ³voa", 1, 2, 3, 4);
        UpdateParkController upc = new UpdateParkController(true);
        Park p1 = upc.getParkById("1");
        assertEquals(p,p1);
    }
    /**
     * 
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    @Test
    public void testNewPark() throws IOException, SQLException{
        ParkRegistry pr = new ParkRegistry(true);
        String id = "1";
        double lat = 40;
        double lon = 10;
        int elev = 10;
        String desc = "1915";
        int maxBCap = 10;
        int maxSCap = 10;
        int volt = 0;
        int current = 0;
        UpdateParkController upc = new UpdateParkController(true);
        Park p1 = upc.getParkById(id);
        pr.getAllParks().add(p1);
        upc.newPark(id, lat, lon, elev, desc, maxBCap, maxSCap, volt, current);
        assertTrue(pr.getAllParks().contains(new Park(id, lat, lon, elev, desc, maxBCap, maxSCap, volt, current)));
    }
}
