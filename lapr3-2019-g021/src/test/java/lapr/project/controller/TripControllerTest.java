/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.List;
import lapr.project.model.Trip;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author roger40
 */
public class TripControllerTest {
    TripController tripcontr;
    
    public TripControllerTest() {
        tripcontr = new TripController(true); // true para usar a mpock e nao a db
    }
    
   
    @Test
    public void testGetAllTrips() {
        assertEquals(tripcontr.getAllTrips().size(), 4); // pna mock so ha 3 trips, e 2 estao a acontecer no momento, 1 esta concluida
     
    }
    
    
    
     @Test
    public void testUnlockedTime() {
    // Trip t1 = new Trip(bMock.getBicycle("BIKE006").getID(), "b", bMock.getBicycle("BIKE006").getAssignedPark(), bMock.getBicycle("BIKE006").getAssignedUser());
     
     long time = tripcontr.getUnlockedTime("BIKE006"); // veiculo existe e esta unlocked
      assertNotNull(time);
      
      
      long time2 = tripcontr.getUnlockedTime("BIKE009999"); // veiculo nao existe
      assertEquals(time2,0);
      
      
      
      long time3 = tripcontr.getUnlockedTime("SCTR002");  // veiculo existe mas nao esta unlocked
       assertEquals(time3,0);
    }
    
}
