
package lapr.project.utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import lapr.project.model.Park;
import lapr.project.model.Path;
import lapr.project.model.PointOfInterest;
import lapr.project.model.graph.Graph;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Francisco
 */
public class GraphUtilTest {

    GraphUtil completeMap = new GraphUtil(true);
    Park park1 = new Park("Park1", 41.123456, 9.153152, 10, "Park 1 for Graph test", 10, 6, 5, 5);
    Park park2 = new Park("Park2", 41.015153, 9.315251, 10, "Park 2 for Graph test", 10, 5, 0, 5);
    Park park3 = new Park("Park3", 41.521512, 9.153120, 50, "Park 3 for Graph test", 10, 3, 0, 3);
    Park park4 = new Park("Park4", 41.222552, 9.153210, 50, "Park 4 for Graph test", 10, 6, 0, 0);

    /**
     * Test of createDistanceGraph method, of class GraphAlgorithms.
     */
    @Test
    public void testCreateDistanceGraph() {
        System.out.println("createDistanceGraph");

        Graph<PointOfInterest, Path> result = completeMap.createDistanceGraph();
        PointOfInterest[] points = result.allkeyVerts();
        assertEquals(new PointOfInterest(park1.getLatitude(), park1.getLongitude()), points[0]);
        assertEquals(new PointOfInterest(park2.getLatitude(), park2.getLongitude()), points[1]);
        assertEquals(new PointOfInterest(park3.getLatitude(), park3.getLongitude()), points[2]);
        assertEquals(new PointOfInterest(park4.getLatitude(), park4.getLongitude()), points[3]);
    }


    /**
     * Test of getShortestPathSequence method, of class GraphUtil.
     */
    @Test
    public void testGetShortestPathSequence() {
        System.out.println("getShortestPathSequence");
        
        Graph<PointOfInterest, Path> g = completeMap.createDistanceGraph();
        PointOfInterest vOrig = new PointOfInterest(park1.getLatitude(), park1.getLongitude());
        PointOfInterest vDest = new PointOfInterest(park3.getLatitude(), park3.getLongitude());
        List<PointOfInterest> sequence = new ArrayList<>();
        sequence.add(new PointOfInterest(park2.getLatitude(), park2.getLongitude()));
        int numRoutes = 1;
        
        List<LinkedList<Path>> result = completeMap.getShortestPathSequence(g, vOrig, vDest, sequence, numRoutes);
        Path path1 = new Path(park1.getLatitude(), park1.getLongitude(), park2.getLatitude(), park2.getLongitude(), 0.8, 220, 4.1);
        assertEquals(path1, result.get(0).get(0));
        
    }
    
}
