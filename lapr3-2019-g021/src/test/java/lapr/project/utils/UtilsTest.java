package lapr.project.utils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lapr.project.data.mock.BicycleMock;
import lapr.project.data.mock.ParkMock;
import lapr.project.data.mock.ScooterMock;
import lapr.project.model.Park;
import lapr.project.model.PointOfInterest;
import lapr.project.model.Trip;
import lapr.project.model.TripRegistry;
import lapr.project.model.User;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UtilsTest {

    Trip trip;

    ParkMock pmcok;
    BicycleMock bMock;
    ScooterMock sMock;

    @Test
    public void testTripPointsUtilsTest() {

        bMock = new BicycleMock();
        sMock = new ScooterMock();

        User userT = new User("rogerTT@gmail.com", "roger1", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);

        Trip t1 = new Trip(bMock.getBicycle("BIKE006").getID(), "b", bMock.getBicycle("BIKE006").getAssignedPark(), bMock.getBicycle("BIKE006").getAssignedUser(), 1);
        Trip t2 = new Trip(sMock.getScooter("SCTR001").getID(), "s", sMock.getScooter("SCTR001").getAssignedPark(), sMock.getScooter("SCTR001").getAssignedUser(), 2);
        //INSIRO NA LISTA DE TRIPS, AS trips que estao a acontecer no momento

        Trip t3 = new Trip(sMock.getScooter("SCTR001").getID(), "s", sMock.getScooter("SCTR001").getAssignedPark(), userT, 3);
        t3.uppdateTrip(sMock.getScooter("SCTR001").getAssignedPark());
        //esta trip esta concluida  neste caso o user estacioneu a scooter no mesmo parque de onde saiu, o userT como nao tem viagens ativas ( viagem em curso), tambem nao pode fazer update a nenhuma viagem (para efeitos de teste)
        Trip t4 = new Trip(sMock.getScooter("SCTR002").getID(), "s", sMock.getScooter("SCTR001").getAssignedPark(), userT, 4);
        t4.uppdateTrip(sMock.getScooter("SCTR002").getAssignedPark());

        //Vou criar um time stamp para simular uma viagem de uma hora para melhorar o test covarage do get points
        Timestamp timestamp = new Timestamp(new Date().getTime());
        System.out.println(timestamp);

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp.getTime());

        cal.setTimeInMillis(timestamp.getTime());
        cal.add(Calendar.HOUR, -1);
        timestamp = new Timestamp(cal.getTime().getTime());
        //////////////

        assertEquals(Utils.calculateTripPoints(t4), 5); // 5 devido ao unlock time ser inferior a 15 minutos
        t4.setUnlockDate(timestamp); // simulo uma viagem de uma hora ao mudar o unlocktime

        assertEquals(Utils.calculateTripPoints(t4), 0);

        Park park4 = new Park("4", 10, 30, 400, "Matosinhos", 1, 2, 220, 16);

        Trip t6 = new Trip(sMock.getScooter("SCTR001").getID(), "s", sMock.getScooter("SCTR001").getAssignedPark(), userT, 3);
        t6.uppdateTrip(park4);

        assertEquals(Utils.calculateTripPoints(t1), 0); // 0 pois a trip nao esta atualizada com o lock
        assertEquals(Utils.calculateTripPoints(t3), 5);
        assertEquals(Utils.calculateTripPoints(t6), 20);

    }

    /**
     * Test of convertHoursToString method, of class Utils.
     */
    @Test
    public void testConvertHoursToString() {
        System.out.println("testConvertHoursToString");
        double time = 0.0;
        String result = Utils.convertHoursToString(time);
        String expResult = "00:00:00";
        assertEquals(expResult, result);
        time = 2.5;
        result = Utils.convertHoursToString(time);
        expResult = "02:30:00";
        assertEquals(expResult, result);
        time = 10.25;
        result = Utils.convertHoursToString(time);
        expResult = "10:15:00";
        assertEquals(expResult, result);
        time = 2.555;
        result = Utils.convertHoursToString(time);
        expResult = "02:33:18";
        assertEquals(expResult, result);
        time = 2.552222222;
        result = Utils.convertHoursToString(time);
        expResult = "02:33:07";
        assertEquals(expResult, result);
    }

    /**
     * Test of convertHoursToSeconds method, of class Utils.
     */
    @Test
    public void testConvertHoursToSeconds() {
        System.out.println("convertHoursToSeconds");
        double time = 0.0;
        int result = Utils.convertHoursToSeconds(time);
        int expResult = 0;
        assertEquals(expResult, result);
        time = 2.5;
        result = Utils.convertHoursToSeconds(time);
        expResult = 9000;
        assertEquals(expResult, result);
        time = 10.25;
        result = Utils.convertHoursToSeconds(time);
        expResult = 36900;
        assertEquals(expResult, result);
        time = 2.555;
        result = Utils.convertHoursToSeconds(time);
        expResult = 9198;
        assertEquals(expResult, result);
    }

    /**
     * Test of calculateDistance method, of class Utils.
     */
    @Test
    public void testCalculateDistance() {
        System.out.println("calculateDistance");
        double result = (int) Utils.calculateDistance(new PointOfInterest(41.148415, -8.610815), new PointOfInterest(41.152712, -8.609297));
        double expResult = 494;
        assertEquals(expResult, result, 0.1);

        result = (int) Utils.calculateDistance(new PointOfInterest(41.148415, -8.610815), new PointOfInterest(41.148415, -8.610815));
        expResult = 0;
        assertEquals(expResult, result, 0.1);
    }

    /**
     * Test of sortParkChargingReport method, of class Utils.
     */
    @Test
    public void testSortParkChargingReport() {
        System.out.println("sortParkChargingReport");
        Map<String, Integer> passedMap = new HashMap<>();
        passedMap.put("SCOOTER3", 0);
        passedMap.put("SCOOTER5", 99);
        passedMap.put("SCOOTER8", 89);
        passedMap.put("SCOOTER2", 180);
        passedMap.put("SCOOTER6", 0);
        passedMap.put("SCOOTER1", 0);
        passedMap.put("SCOOTER4", 300);
        Map<String, Integer> expResult = new LinkedHashMap<>();
        expResult.put("SCOOTER4", 300);
        expResult.put("SCOOTER2", 180);
        expResult.put("SCOOTER5", 99);
        expResult.put("SCOOTER8", 89);
        expResult.put("SCOOTER1", 0);
        expResult.put("SCOOTER3", 0);
        expResult.put("SCOOTER6", 0);
        Map<String, Integer> result = Utils.sortParkChargingReport(passedMap);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTimestampDifference method, of class Utils.
     */
    @Test
    public void testGetTimestampDifference() {
        System.out.println("getTimestampDifference");

        Timestamp lockTime = new Timestamp(1);
        Timestamp unlockTime = new Timestamp(1);
        long expResult = 0L;
        long result = Utils.getTimestampDifference(lockTime, unlockTime);
        assertEquals(expResult, result);
    }

}
