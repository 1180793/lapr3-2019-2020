package lapr.project.utils;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class MD5UtilsTest {

	/**
	 * Test of encrypt method, of class MD5Utils.
	 */
	@Test
	public void testEncrypt() {
		System.out.println("testEncrypt");
		String password = "";
		String expResult = "d41d8cd98f00b204e9800998ecf8427e";
		String result = MD5Utils.encrypt(password);
		assertEquals(expResult, result);
		
		password = "teste123";
		String otherPassword = "TeStE123";
		result = MD5Utils.encrypt(password);
		String otherResult = MD5Utils.encrypt(otherPassword);
		assertNotEquals(password, result);
		assertNotEquals(otherResult, result);
	}
	
}
