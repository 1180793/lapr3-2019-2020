package lapr.project.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ParkTest {

        String validID;

        double validLat;
        double invalidLat;

        double validLon;
        double invalidLon;

        int validElev;

        String validDesc;
        String invalidDesc;

        int validMaxBCap;
        int invalidMaxBCap;

        int validMaxSCap;
        int invalidMaxSCap;

        double validVolt;
        double invalidVolt;

        double validCurrent;
        double invalidCurrent;

        public void setUp() {
                validID = "100";

                validLat = 50.50;
                invalidLat = 100;

                validLon = 150.30;
                invalidLon = 200;

                validElev = 50;

                validDesc = "Parque válido";
                invalidDesc = "";

                validMaxBCap = 10;
                invalidMaxBCap = -5;

                validMaxSCap = 15;
                invalidMaxSCap = -20;

                validVolt = 200;
                invalidVolt = -100;

                validCurrent = 100;
                invalidCurrent = -100;
        }

        @Test
        public void ParkTest() {
                setUp();
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                assertEquals(park.getID(), validID);
                assertEquals(park.getLatitude(), validLat);
                assertEquals(park.getLongitude(), validLon);
                assertEquals(park.getElevation(), validElev);
                assertTrue(park.getDescription().equals(validDesc));
                assertEquals(park.getMaxBikeCapacity(), validMaxBCap);
                assertEquals(park.getMaxScooterCapacity(), validMaxSCap);
                assertEquals(park.getInputVoltage(), validVolt);
                assertEquals(park.getInputCurrent(), validCurrent);
        }

        @Test
        public void testInvalidLatPlus() {
                setUp();
                System.out.println("testInvalidLatPlus");
                Park park = new Park(validID, invalidLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                boolean expResult = false;
                boolean result = park.validatePark();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidLatMinus() {
                setUp();
                System.out.println("testInvalidLatMinus");
                Park park = new Park(validID, -100, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                boolean expResult = false;
                boolean result = park.validatePark();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidLonPlus() {
                setUp();
                System.out.println("testInvalidLonPlus");
                Park park = new Park(validID, validLat, invalidLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                boolean expResult = false;
                boolean result = park.validatePark();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidLonMinus() {
                setUp();
                System.out.println("testInvalidLonMinus");
                Park park = new Park(validID, validLat, -181, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                boolean expResult = false;
                boolean result = park.validatePark();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidDesc() {
                setUp();
                System.out.println("testInvalidDesc");
                Park park = new Park(validID, validLat, validLon, validElev, invalidDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                boolean expResult = false;
                boolean result = park.validatePark();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidMaxBCap() {
                setUp();
                System.out.println("testInvalidMaxBCap");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, invalidMaxBCap, validMaxSCap, validVolt, validCurrent);
                boolean expResult = false;
                boolean result = park.validatePark();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidMaxSCap() {
                setUp();
                System.out.println("testInvalidMaxSCap");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, invalidMaxSCap, validVolt, validCurrent);
                boolean expResult = false;
                boolean result = park.validatePark();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidVolt() {
                setUp();
                System.out.println("testInvalidVolt");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, invalidVolt, validCurrent);
                boolean expResult = false;
                boolean result = park.validatePark();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidCurrent() {
                setUp();
                System.out.println("testInvalidCurrent");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, invalidCurrent);
                boolean expResult = false;
                boolean result = park.validatePark();
                assertEquals(expResult, result);
        }

        /**
         * Test of getID method, of class Park.
         */
        @Test
        public void testGetParkID() {
                setUp();
                System.out.println("getParkID");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                String expResult = validID;
                String result = park.getID();
                assertEquals(expResult, result);
        }

        /**
         * Test of getLatitude method, of class Park.
         */
        @Test
        public void testGetParkLat() {
                setUp();
                System.out.println("getParkLat");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                double expResult = validLat;
                double result = park.getLatitude();
                assertEquals(expResult, result);
        }

        /**
         * Test of getLongitude method, of class Park.
         */
        @Test
        public void testGetParkLon() {
                setUp();
                System.out.println("getParkLon");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                double expResult = validLon;
                double result = park.getLongitude();
                assertEquals(expResult, result);
        }

        /**
         * Test of getElevation method, of class Park.
         */
        @Test
        public void testGetParkElev() {
                setUp();
                System.out.println("getParkElev");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                double expResult = validElev;
                double result = park.getElevation();
                assertEquals(expResult, result);
        }

        /**
         * Test of getDescription method, of class Park.
         */
        @Test
        public void testGetParkDesc() {
                setUp();
                System.out.println("getParkDesc");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                String expResult = validDesc;
                String result = park.getDescription();
                assertEquals(expResult, result);
        }

        /**
         * Test of getMaxBikeCapacity method, of class Park.
         */
        @Test
        public void testGetParkMaxBCap() {
                setUp();
                System.out.println("getParkMaxBCap");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                int expResult = validMaxBCap;
                int result = park.getMaxBikeCapacity();
                assertEquals(expResult, result);
        }

        /**
         * Test of getMaxScooterCapacity method, of class Park.
         */
        @Test
        public void testGetParkMaxSCap() {
                setUp();
                System.out.println("getParkMaxSCap");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                int expResult = validMaxSCap;
                int result = park.getMaxScooterCapacity();
                assertEquals(expResult, result);
        }

        /**
         * Test of getInputVoltage method, of class Park.
         */
        @Test
        public void testGetParkVolt() {
                setUp();
                System.out.println("getParkVolt");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                double expResult = validVolt;
                double result = park.getInputVoltage();
                assertEquals(expResult, result);
        }

        /**
         * Test of getInputCurrent method, of class Park.
         */
        @Test
        public void testGetParkCurrent() {
                setUp();
                System.out.println("getParkCurrent");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                double expResult = validCurrent;
                double result = park.getInputCurrent();
                assertEquals(expResult, result);
        }

        /**
         * Test of getAvailableBikes method, of class Park.
         */
        @Test
        public void testGetAvailableBikes() {
                setUp();
                System.out.println("getAvailableBikes");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                int expResult = 0;
                int result = park.getAvailableBikes();
                assertEquals(expResult, result);
        }

        /**
         * Test of getAvailableScooters method, of class Park.
         */
        @Test
        public void testGetAvailableScooters() {
                setUp();
                System.out.println("getAvailableScooters");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                int expResult = 0;
                int result = park.getAvailableScooters();
                assertEquals(expResult, result);
        }
	
	/**
         * Test of getPOI method, of class Park.
         */
        @Test
        public void testGetPOI() {
                setUp();
                System.out.println("getPOI");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                PointOfInterest poi = park.getPOI();
                assertEquals(validLat, poi.getLatitude());
                assertEquals(validLon, poi.getLongitude());
                assertEquals(validElev, poi.getAltitude());
                assertEquals(validDesc, poi.getName());
        }

        /**
         * Test of validatePark method, of class Park.
         */
        @Test
        public void testValidatePark() {
                setUp();
                System.out.println("validatePark");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                boolean expResult = true;
                boolean result = park.validatePark();
                assertEquals(expResult, result);
        }

        /**
         * Test of setAvailableBikes method, of class Park.
         */
        @Test
        public void testSetAvailableBikes() {
                setUp();
                System.out.println("setAvailableBikes");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                park.setAvailableBikes(2);
                assertEquals(2, park.getAvailableBikes());
        }

        /**
         * Test of setAvailableScooters method, of class Park.
         */
        @Test
        public void testSetAvailableScooters() {
                setUp();
                System.out.println("setAvailableScooters");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                park.setAvailableScooters(2);
                assertEquals(2, park.getAvailableScooters());
        }

        /**
         * Test of hashCode method, of class Park.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");

                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                Park park1 = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);

                assertEquals(park.hashCode(), park1.hashCode());
        }

        /**
         * Test of equals method, of class Park.
         */
        @Test
        public void testEqualsNull() {
                System.out.println("null");
                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);

                boolean expResult = false;
                boolean result = park.equals(null);

                assertEquals(expResult, result);
        }

//        @Test
//        public void testEqualsID() {
//                System.out.println("id diferente");
//                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
//                Park park1 = new Park("2", validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
//
//                boolean expResult = false;
//                boolean result = park.equals(park1);
//
//                assertEquals(expResult, result);
//        }

        @Test
        public void testEqualsNotSameClass() {
                System.out.println("NotSameClass");

                Park park = new Park(validID, validLat, validLon, validElev, validDesc, validMaxBCap, validMaxSCap, validVolt, validCurrent);
                int anotherClass = 3;

                boolean expResult = false;
                boolean result = park.equals(anotherClass);

                assertEquals(expResult, result);
        }

}
