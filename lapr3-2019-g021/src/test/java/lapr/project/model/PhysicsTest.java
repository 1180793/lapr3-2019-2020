/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author danie
 */
public class PhysicsTest {

    Scooter scooter1;
    Park park1;
    User user1;
    
    public PhysicsTest() {

        scooter1 = new Scooter("SCTR001", 20, 1, park1, 200.0, 75, 1.10, 0.3, 245);
        park1 = new Park("park", 50.0, 120.0, 65, "parque teste2", 85, 75, 35, 15);
        user1 = new User("roger@gmail.com", "roger", "12345", "1111111111111111", 'm', 188.7, 80.0, 0.1);

    }

    @Test
    public void testCalculateFAtrito() {
        Physics f = new Physics(scooter1.getAerodynamicCoefficient());
       
       
        
        double result = f.calculateFAtrito(scooter1.getWeight(), 25, user1.getWeight());
        double expResult = 996.9385657403151;
        assertEquals(expResult, result,0.00001);
    }

    @Test
    public void testCalculateFGravitica() {
        Physics f = new Physics(scooter1.getAerodynamicCoefficient());
       
        double result = f.calculateFGravitica(scooter1.getWeight(), 25, user1.getWeight());
        double expResult = 422.61826;
        assertEquals(expResult, result, 0.00001);
    }

            
//    @Test
//    public void testCalculateAutonomy() {
//        Physics f = new Physics();
//        f.setAeroDynCof(0.5);
//
//        int dist = 3;
//        double fr = f.calculateFAtrito(scooter1.getWeight(), 0, user1.getWeight()) + f.calculateFGravitica(scooter1.getWeight(), 0, user1.getWeight());
//        double autonomy = scooter1.getActualBatteryCapacity() / fr;
//        
//        boolean result = 
//        boolean expResult = true;
//        assertEquals(expResult, result, 0.01);
//    }
}
