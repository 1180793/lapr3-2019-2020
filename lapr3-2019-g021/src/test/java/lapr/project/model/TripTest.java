/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import lapr.project.data.mock.ParkMock;
import lapr.project.data.mock.TripMock;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 *
 * @author roger40
 */
public class TripTest {

    TripMock tripm;
    ParkMock pMock;

    public TripTest() {

        pMock = new ParkMock();
        tripm = new TripMock();
    }

    @Test
    public void testPrice() { 
        //TODAS AS TRIPS DA MOCK NAO PREÇO 0 
         assertEquals(tripm.getAllTrips().get(0).getPrice(), 0);
        assertEquals(tripm.getAllTrips().get(1).getPrice(), 0);
        assertEquals(tripm.getAllTrips().get(2).getPrice(), 0);
        assertEquals(tripm.getAllTrips().get(3).getPrice(), 0);
        
          User userT = new User("rogerTT@gmail.com", "roger1", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);
          Park park1 = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 220, 16);
      
        Trip t1 = new Trip("BIKE006", "b", park1, userT, 5);
          
          

        assertEquals(t1.getPrice(), 0); //preço é zero pois ainda esta unlocked

        Timestamp timestamp = new Timestamp(new Date().getTime());

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp.getTime());

        // subtract 10 hours
        cal.setTimeInMillis(timestamp.getTime());
        cal.add(Calendar.HOUR, -2);
        timestamp = new Timestamp(cal.getTime().getTime());
        
        t1.setUnlockDate(timestamp); // defino a data de unlock para 2 hora menos a atual
        t1.uppdateTrip(park1); // fecho a trip
        assertEquals(t1.getPrice(), 3);
        
        
    }
    
     @Test
    public void testGetTripId() {

        assertEquals(tripm.getAllTrips().get(0).getTripNumber(), 1);
        assertEquals(tripm.getAllTrips().get(1).getTripNumber(), 2);
          assertEquals(tripm.getAllTrips().get(2).getTripNumber(), 3);
            assertEquals(tripm.getAllTrips().get(3).getTripNumber(), 4);
    }

    @Test
    public void testGetVehicleId() {

        assertEquals(tripm.getAllTrips().get(0).getVehicleId(), "BIKE006");
    }

    /**
     * Test of getVehicleType method, of class Trip.
     */
    @Test
    public void testGetVehicleType() {
        assertEquals(tripm.getAllTrips().get(0).getVehicleType(), "b");

    }

    /**
     * Test of getDateUnlocked method, of class Trip.
     */
    @Test
    public void testGetDateUnlocked() {

        Date date = new Date();  //get current date in timestamp format
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        assertEquals(tripm.getAllTrips().get(0).getDateUnlocked().getDay(), ts.getDay());
        assertEquals(tripm.getAllTrips().get(0).getDateUnlocked().getHours(), ts.getHours());
        assertEquals(tripm.getAllTrips().get(0).getDateUnlocked().getMinutes(), ts.getMinutes());
        assertEquals(tripm.getAllTrips().get(0).getDateUnlocked().getSeconds(), ts.getSeconds());
    }

    /**
     * Test of getParkOrigin method, of class Trip.
     */
    @Test
    public void testGetParkOrigin() {
        assertEquals(tripm.getAllTrips().get(0).getParkOrigin().getID(), "1");
    }

    /**
     * Test of getAssignedUser method, of class Trip.
     */
    @Test
    public void testGetAssignedUser() {
        assertEquals(tripm.getAllTrips().get(0).getAssignedUser().getEmail(), "roger1@gmail.com");

    }

    /**
     * Test of getParkDestiny method, of class Trip.
     */
    @Test
    public void testGetParkDestiny() {
        assertEquals(tripm.getAllTrips().get(0).getParkDestiny(), null);

    }

    /**
     * Test of getDateLocked method, of class Trip.
     */
    @Test
    public void testGetDateLocked() {
        Date date = new Date();  //get current date in timestamp format
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        assertEquals(tripm.getAllTrips().get(0).getDateLocked(), null);

    }

    @Test
    public void testUpdateTrip() {

        assertNull(tripm.getAllTrips().get(0).getDateLocked());    //BEFOR UPDATE THIS FIELDS ARE NULL
        assertNull(tripm.getAllTrips().get(0).getParkDestiny());

        tripm.getAllTrips().get(0).uppdateTrip(pMock.getPark("3"));

        assertNotNull(tripm.getAllTrips().get(0).getDateLocked());
        assertEquals(tripm.getAllTrips().get(0).getParkDestiny().getID(), "3");

    }

    @Test
    public void testFullConstructor() {

        Date date = new Date();  //get current date in timestamp format
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);

        Date date2 = new Date();  //get current date in timestamp format
        long time2 = date.getTime();
        Timestamp ts2 = new Timestamp(time2);

        Trip tf = new Trip("bike1", "b", ts, pMock.getPark("2"), tripm.getAllTrips().get(0).getAssignedUser(), pMock.getPark("2"), ts2, 2, 3);
        assertNotNull(tf);
        assertEquals(tf.getDateUnlocked(), ts);

        assertEquals(tf.getDateLocked(), ts2);

    }

}
