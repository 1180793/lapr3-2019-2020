package lapr.project.model;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ParkRegistryTest {

        ParkRegistry oRegistry;
        Park test;
        Park test2;

        int validID = 9999;
        int invalidID;

        public ParkRegistryTest() {
                oRegistry = new ParkRegistry(true);
                test = new Park("2", 3, 4, 4, "Varzim", 1, 2, 3, 4);
                test2 = new Park("900", 3, 4, 4, "Varzim", 1, 2, 3, 4);
        }

        /**
         * Test of getPark method, of class ParkRegistry.
         */
        @Test
        public void testGetParkID() {
                System.out.println("testGetParkID");
                String id = "2";
                Park result = oRegistry.getPark(id);
                assertEquals(test, result);
        }

        /**
         * Test of getPark method, of class ParkRegistry.
         */
        @Test
        public void testGetPark_double_double() {
                System.out.println("getPark");
                double latitude = 3.0;
                double longitude = 4.0;
                Park result = oRegistry.getPark(latitude, longitude);
                assertEquals(test, result);
        }

        /**
         * Test of newPark method, of class ParkRegistry.
         */
        @Test
        public void testNewPark() {
                System.out.println("testNewPark");
                Park result = oRegistry.newPark("2", 3, 4, 4, "Varzim", 1, 2, 3, 4);
                assertEquals(test, result);
        }

        @Test
        public void testNewInvalidPark() {
                System.out.println("testNewPark");
                Park result = oRegistry.newPark("2", -900, -900, 4, "Varzim", 1, 2, 3, 4);
                assertNull(result);
        }

        /**
         * Test of addPark method, of class ParkRegistry.
         */
        @Test
        public void testAddPark() {
                System.out.println("testAddPark");
                boolean expResult = true;
                boolean result = oRegistry.addPark(test2);
                assertEquals(expResult, result);
        }

        @Test
        public void testAddInvalidPark() {
                System.out.println("testAddInvalidPark");
                boolean expResult = false;
                boolean result = oRegistry.addPark(test);
                assertEquals(expResult, result);
        }

        @Test
        public void testAddNullPark() {
                System.out.println("testAddInvalidPark");
                boolean expResult = false;
                Park parkNull = null;
                boolean result = oRegistry.addPark(parkNull);
                assertEquals(expResult, result);
        }

        /**
         * Test of getAllParks method, of class ParkRegistry.
         */
        @Test
        public void testGetAllParks() {
                System.out.println("getAllParks");

                ParkRegistry instance = new ParkRegistry(true);

                Park park1 = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
                Park park2 = new Park("2", 3, 4, 4, "Varzim", 1, 2, 3, 4);
                Park park3 = new Park("3", 10, 10, 4, "Porto", 1, 2, 3, 4);
                Park park4 = new Park("4", 10, 30, 4, "Matosinhos", 1, 2, 3, 4);
                Park park5 = new Park("5", 100, 300, 4, "Matosinhos", 0, 0, 220, 16);

                List<Park> expResult = new ArrayList<>();
                expResult.add(park1);
                expResult.add(park2);
                expResult.add(park3);
                expResult.add(park4);
                expResult.add(park5);
                
                // Parks with real values for Graph testing
                expResult.add(new Park("Park1", 41.123456, 9.153152, 10, "Park 1 for Graph test", 10, 6, 5, 5));
                expResult.add(new Park("Park2", 41.015153, 9.315251, 10, "Park 2 for Graph test", 10, 5, 0, 5));
                expResult.add(new Park("Park3", 41.521512, 9.153120, 50, "Park 3 for Graph test", 10, 3, 0, 3));
                expResult.add(new Park("Park4", 41.222552, 9.153210, 50, "Park 4 for Graph test", 10, 6, 0, 0));
                expResult.add(new Park("Park5", 41.545313, 9.135121, 90, "Park 5 for Graph test", 10, 6, 0, 6));
                expResult.add(new Park("Park6", 41.151313, 9.000000, 10, "Park 6 for Graph test", 10, 2, 10, 1));
                expResult.add(new Park("Park7", 45.000000, 10.014122, 35, "Park 7 for Graph test", 10, 2, 10, 1));
                expResult.add(new Park("Park8", 46.000000, 10.014100, 60, "Park 8 for Graph test", 10, 2, 10, 1));

                List<Park> result = instance.getAllParks();
                assertEquals(expResult, result);

        }

        /**
         * Test of getPark method, of class ParkRegistry.
         */
        @Test
        public void testGetPark_String() {
                System.out.println("getPark_string");

                String description = "Porto";
                Park expResult = new Park("3", 10, 10, 4, "Porto", 1, 2, 3, 4);
                Park result = oRegistry.getParkByDescription(description);
                assertEquals(expResult, result);
        }
        
         @Test
        public void removePark() {
                System.out.println("removePark");

                Park park1 = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 220, 16);    
                
                boolean expResult = true;
                boolean result = oRegistry.removePark(park1.getID());
                assertEquals(expResult, result);
        }
        
        @Test
        public void removePark1() {
                System.out.println("removeParkFalse");

                Park parkSet = new Park("6", 10, 30, 4, "Leça", 1, 2, 220, 16);   
                
                boolean expResult = false;
                boolean result = oRegistry.removePark(parkSet.getID());
                assertEquals(expResult, result);
        }

}
