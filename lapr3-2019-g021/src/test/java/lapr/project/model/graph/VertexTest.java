package lapr.project.model.graph;

import java.util.Iterator;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * @author DEI-ISEP
 */
public class VertexTest {

        Vertex<String, Integer> instance = new Vertex<>();

        @Test
        public void TestController() {
            System.out.println("controller test");
            
            String vInf = "Vertex1";
            instance.setElement(vInf);
            
            String vAdj1 = "VAdj1";
            Edge<String, Integer> edge = new Edge<>();
            instance.addAdjVert(vAdj1, edge);
            
            Vertex<String, Integer> clone = new Vertex<>(instance);
            assertEquals(clone.getKey(), instance.getKey());
            
        }

        /**
         * Test of getKey method, of class Vertex.
         */
        @Test
        public void testGetKey() {
                System.out.println("getKey");

                int expResult = -1;
                assertEquals(expResult, instance.getKey());
                
                Vertex<String,Integer> copy = new Vertex(instance);
                assertEquals(expResult, copy.getKey());

                Vertex<String, Integer> instance1 = new Vertex<>(1, "Vertex1");
                expResult = 1;
                assertEquals(expResult, instance1.getKey());
                
                copy = new Vertex(instance1);
                assertEquals(expResult, instance1.getKey());
        }

        /**
         * Test of setKey method, of class Vertex.
         */
        @Test
        public void testSetKey() {
                System.out.println("setKey");
                int key = 2;
                instance.setKey(key);
                int expResult = 2;
                assertEquals(expResult, instance.getKey());
        }

        /**
         * Test of getElement method, of class Vertex.
         */
        @Test
        public void testGetElement() {
                System.out.println("getElement");

                String expResult = null;
                assertEquals(expResult, instance.getElement());

                Vertex<String, Integer> instance1 = new Vertex<>(1, "Vertex1");
                expResult = "Vertex1";
                assertEquals(expResult, instance1.getElement());

        }

        /**
         * Test of setElement method, of class Vertex.
         */
        @Test
        public void testSetElement() {
                System.out.println("setElement");
                String vInf = "Vertex1";
                instance.setElement(vInf);
                assertEquals(vInf, instance.getElement());
        }

        /**
         * Test of addAdjVert method, of class Vertex.
         */
        @Test
        public void testAddAdjVert() {
                System.out.println("addAdjVert");

                assertTrue((instance.numAdjVerts() == 0));

                String vAdj1 = "VAdj1";
                Edge<String, Integer> edge = new Edge<>();

                instance.addAdjVert(vAdj1, edge);
                assertTrue((instance.numAdjVerts() == 1));

                String vAdj2 = "VAdj2";
                instance.addAdjVert(vAdj2, edge);
                assertTrue((instance.numAdjVerts() == 2));
        }

        /**
         * Test of getAdjVert method, of class Vertex.
         */
        @Test
        public void testGetAdjVert() {
                System.out.println("getAdjVert");

                Edge<String, Integer> edge = new Edge<>();
                Object expResult = null;
                assertEquals(expResult, instance.getAdjVert(edge));

                String vAdj = "VertexAdj";
                instance.addAdjVert(vAdj, edge);
                assertEquals(vAdj, instance.getAdjVert(edge));
        }

        /**
         * Test of remAdjVert method, of class Vertex.
         */
        @Test
        public void testRemAdjVert() {
                System.out.println("remAdjVert");

                Edge<String, Integer> edge1 = new Edge<>();
                String vAdj = "VAdj1";
                instance.addAdjVert(vAdj, edge1);

                Edge<String, Integer> edge2 = new Edge<>();
                vAdj = "VAdj2";
                instance.addAdjVert(vAdj, edge2);

                instance.remAdjVert(vAdj);
                assertTrue((instance.numAdjVerts() == 1));

                vAdj = "VAdj1";
                instance.remAdjVert(vAdj);
                assertTrue((instance.numAdjVerts() == 0));
        }

        /**
         * Test of getEdge method, of class Vertex.
         */
        @Test
        public void testGetEdge() {
                System.out.println("getEdge");

                Edge<String, Integer> edge1 = new Edge<>();
                String vAdj1 = "VAdj1";
                instance.addAdjVert(vAdj1, edge1);

                assertEquals(edge1, instance.getEdge(vAdj1));

                Edge<String, Integer> edge2 = new Edge<>();
                String vAdj2 = "VAdj2";
                instance.addAdjVert(vAdj2, edge2);

                assertEquals(edge2, instance.getEdge(vAdj2));
        }

        /**
         * Test of numAdjVerts method, of class Vertex.
         */
        @Test
        public void testNumAdjVerts() {
                System.out.println("numAdjVerts");

                Edge<String, Integer> edge1 = new Edge<>();
                String vAdj1 = "VAdj1";
                instance.addAdjVert(vAdj1, edge1);

                assertTrue((instance.numAdjVerts() == 1));

                Edge<String, Integer> edge2 = new Edge<>();
                String vAdj2 = "VAdj2";
                instance.addAdjVert(vAdj2, edge2);

                assertTrue((instance.numAdjVerts() == 2));

                instance.remAdjVert(vAdj1);

                assertTrue((instance.numAdjVerts() == 1));

                instance.remAdjVert(vAdj2);
                assertTrue((instance.numAdjVerts() == 0));

        }

        /**
         * Test of getAllAdjVerts method, of class Vertex.
         */
        @Test
        public void testGetAllAdjVerts() {
                System.out.println("getAllAdjVerts");

                Iterator<String> itVerts = instance.getAllAdjVerts().iterator();

                assertTrue(itVerts.hasNext() == false);

                Edge<String, Integer> edge1 = new Edge<>();
                String vAdj1 = "VAdj1";
                instance.addAdjVert(vAdj1, edge1);

                Edge<String, Integer> edge2 = new Edge<>();
                String vAdj2 = "VAdj2";
                instance.addAdjVert(vAdj2, edge2);

                itVerts = instance.getAllAdjVerts().iterator();

                assertTrue((itVerts.next().compareTo("VAdj1") == 0));
                assertTrue((itVerts.next().compareTo("VAdj2") == 0));

                instance.remAdjVert(vAdj1);

                itVerts = instance.getAllAdjVerts().iterator();
                assertTrue((itVerts.next().compareTo("VAdj2")) == 0);

                instance.remAdjVert(vAdj2);

                itVerts = instance.getAllAdjVerts().iterator();
                assertTrue(itVerts.hasNext() == false);
        }
}
