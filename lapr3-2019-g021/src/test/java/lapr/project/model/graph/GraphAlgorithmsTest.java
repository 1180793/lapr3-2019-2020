package lapr.project.model.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import lapr.project.model.graph.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author DEI-ISEP
 */
public class GraphAlgorithmsTest {

    Graph<String, String> completeMap = new Graph<>(false);
    Graph<String, String> incompleteMap = new Graph<>(false);

    public void setUp() throws Exception {

        completeMap.insertVertex("Porto");
        completeMap.insertVertex("Braga");
        completeMap.insertVertex("Vila Real");
        completeMap.insertVertex("Aveiro");
        completeMap.insertVertex("Coimbra");
        completeMap.insertVertex("Leiria");

        completeMap.insertVertex("Viseu");
        completeMap.insertVertex("Guarda");
        completeMap.insertVertex("Castelo Branco");
        completeMap.insertVertex("Lisboa");
        completeMap.insertVertex("Faro");

        completeMap.insertEdge("Porto", "Aveiro", "A1", 75);
        completeMap.insertEdge("Porto", "Braga", "A3", 60);
        completeMap.insertEdge("Porto", "Vila Real", "A4", 100);
        completeMap.insertEdge("Viseu", "Guarda", "A25", 75);
        completeMap.insertEdge("Guarda", "Castelo Branco", "A23", 100);
        completeMap.insertEdge("Aveiro", "Coimbra", "A1", 60);
        completeMap.insertEdge("Coimbra", "Lisboa", "A1", 200);
        completeMap.insertEdge("Coimbra", "Leiria", "A34", 80);
        completeMap.insertEdge("Aveiro", "Leiria", "A17", 120);
        completeMap.insertEdge("Leiria", "Lisboa", "A8", 150);

        completeMap.insertEdge("Aveiro", "Viseu", "A25", 85);
        completeMap.insertEdge("Leiria", "Castelo Branco", "A23", 170);
        completeMap.insertEdge("Lisboa", "Faro", "A2", 280);

        incompleteMap = completeMap.clone();

        incompleteMap.removeEdge("Aveiro", "Viseu");
        incompleteMap.removeEdge("Leiria", "Castelo Branco");
        incompleteMap.removeEdge("Lisboa", "Faro");
    }

    /**
     * Test of BreadthFirstSearch method, of class GraphAlgorithms.
     */
    @Test
    public void testBreadthFirstSearch() throws Exception {
        System.out.println("Test BreadthFirstSearch");

        setUp();

        //Should be null if vertex does not exist
        assertTrue(GraphAlgorithms.BreadthFirstSearch(completeMap, "LX") == null);

        LinkedList<String> path = GraphAlgorithms.BreadthFirstSearch(incompleteMap, "Faro");

        //Should be just one
        assertTrue(path.size() == 1);

        Iterator<String> it = path.iterator();
        //it should be Faro
        assertTrue(it.next().compareTo("Faro") == 0);

        path = GraphAlgorithms.BreadthFirstSearch(incompleteMap, "Porto");
        //Should give seven vertices 
        assertTrue(path.size() == 7);

        path = GraphAlgorithms.BreadthFirstSearch(incompleteMap, "Viseu");
        //Should give 3 vertices
        assertTrue(path.size() == 3);
    }

    /**
     * Test of DepthFirstSearch method, of class GraphAlgorithms.
     */
    @Test
    public void testDepthFirstSearch() throws Exception {
        System.out.println("Test of DepthFirstSearch");

        setUp();

        LinkedList<String> path;

        //Should be null if vertex does not exist
        assertTrue(GraphAlgorithms.DepthFirstSearch(completeMap, "LX") == null);

        path = GraphAlgorithms.DepthFirstSearch(incompleteMap, "Faro");
        //Should be just one
        assertTrue(path.size() == 1);

        Iterator<String> it = path.iterator();
        //it should be Faro
        assertTrue(it.next().compareTo("Faro") == 0);

        path = GraphAlgorithms.DepthFirstSearch(incompleteMap, "Porto");
        //Should give seven vertices 
        assertTrue(path.size() == 7);

        path = GraphAlgorithms.DepthFirstSearch(incompleteMap, "Viseu");
        //Should give 3 vertices
        assertTrue(path.size() == 3);

        it = path.iterator();
        //First in visit should be Viseu
        assertTrue(it.next().compareTo("Viseu") == 0);

        //then Guarda
        assertTrue(it.next().compareTo("Guarda") == 0);

        //then Castelo Branco
        assertTrue(it.next().compareTo("Castelo Branco") == 0);
    }

    /**
     * Test of allPaths method, of class GraphAlgorithms.
     */
    @Test
    public void testAllPaths() throws Exception {
        System.out.println("Test of all paths");

        setUp();

        ArrayList<LinkedList<String>> paths = new ArrayList<LinkedList<String>>();

        paths = GraphAlgorithms.allPaths(completeMap, "Porto", "LX");
        //There should not be paths if vertex does not exist
        assertTrue(paths.size() == 0);

        paths = GraphAlgorithms.allPaths(incompleteMap, "Porto", "Lisboa");
        //There should be 4 paths
        assertTrue(paths.size() == 4);

        paths = GraphAlgorithms.allPaths(incompleteMap, "Porto", "Faro");
        //There should not be paths between Porto and Faro in the incomplete map
        assertTrue(paths.size() == 0);
    }

    /**
     * Test of shortestPath method, of class GraphAlgorithms.
     */
    @Test
    public void testShortestPath() throws Exception {
        System.out.println("Test of shortest path");

        setUp();

        LinkedList<String> shortPath = new LinkedList<String>();
        double lenpath = 0;
        lenpath = GraphAlgorithms.shortestPath(completeMap, "Porto", "LX", shortPath);
        //Length path should be 0 if vertex does not exist
        assertTrue(lenpath == 0);

        lenpath = GraphAlgorithms.shortestPath(incompleteMap, "Porto", "Faro", shortPath);
        //Length path should be 0 if there is no path
        assertTrue(lenpath == 0);

        lenpath = GraphAlgorithms.shortestPath(completeMap, "Porto", "Porto", shortPath);
        //Number of nodes should be 1 if source and vertex are the same
        assertTrue(shortPath.size() == 1);

        lenpath = GraphAlgorithms.shortestPath(incompleteMap, "Porto", "Lisboa", shortPath);
        //Path between Porto and Lisboa should be 335 Km
        assertTrue(lenpath == 335);

        Iterator<String> it = shortPath.iterator();

        //First in path should be Porto
        assertTrue(it.next().compareTo("Porto") == 0);
        assertTrue(it.next().compareTo("Aveiro") == 0);

        //then Coimbra
        assertTrue(it.next().compareTo("Coimbra") == 0);

        //then Lisboa
        assertTrue(it.next().compareTo("Lisboa") == 0);

        lenpath = GraphAlgorithms.shortestPath(incompleteMap, "Braga", "Leiria", shortPath);
        //Path between Braga and Leiria should be 255 Km
        assertTrue(lenpath == 255);

        it = shortPath.iterator();

        //First in path should be Braga
        assertTrue(it.next().compareTo("Braga") == 0);
        assertTrue(it.next().compareTo("Porto") == 0);

        //then Aveiro
        assertTrue(it.next().compareTo("Aveiro") == 0);

        //then Leiria
        assertTrue(it.next().compareTo("Leiria") == 0);

        shortPath.clear();
        lenpath = GraphAlgorithms.shortestPath(completeMap, "Porto", "Castelo Branco", shortPath);

        //Path between Porto and Castelo Branco should be 335 Km
        assertTrue(lenpath == 335);

        //N. cities between Porto and Castelo Branco should be 5 
        assertTrue(shortPath.size() == 5);

        it = shortPath.iterator();

        //First in path should be Porto
        assertTrue(it.next().compareTo("Porto") == 0);

        //then Aveiro
        assertTrue(it.next().compareTo("Aveiro") == 0);

        //then Viseu
        assertTrue(it.next().compareTo("Viseu") == 0);

        //then Guarda
        assertTrue(it.next().compareTo("Guarda") == 0);

        //then Castelo Branco
        assertTrue(it.next().compareTo("Castelo Branco") == 0);

        //Changing Edge: Aveiro-Viseu with Edge: Leiria-C.Branco 
        //should change shortest path between Porto and Castelo Branco
        completeMap.removeEdge("Aveiro", "Viseu");
        completeMap.insertEdge("Leiria", "Castelo Branco", "A23", 170);
        shortPath.clear();
        lenpath = GraphAlgorithms.shortestPath(completeMap, "Porto", "Castelo Branco", shortPath);

        //Path between Porto and Castelo Branco should now be 365 Km
        assertTrue(lenpath == 365);

        //Path between Porto and Castelo Branco should be 4 cities
        assertTrue(shortPath.size() == 4);

        it = shortPath.iterator();

        //First in path should be Porto
        assertTrue(it.next().compareTo("Porto") == 0);

        // then Aveiro
        assertTrue(it.next().compareTo("Aveiro") == 0);

        //then Leiria
        assertTrue(it.next().compareTo("Leiria") == 0);

        //then Castelo Branco
        assertTrue(it.next().compareTo("Castelo Branco") == 0);

    }

    /**
     * Test of shortestPaths method, of class GraphAlgorithms.
     */
    @Test
    public void testShortestPaths() throws Exception {
        System.out.println("Test of shortest path");

        setUp();

        ArrayList<LinkedList<String>> paths = new ArrayList<>();
        ArrayList<Double> dists = new ArrayList<>();

        GraphAlgorithms.shortestPaths(completeMap, "Porto", paths, dists);

        //There should be as many paths as sizes
        assertEquals(paths.size(), dists.size());

        //There should be a path to every vertex
        assertEquals(completeMap.numVertices(), paths.size());

        //Number of nodes should be 1 if source and vertex are the same
        assertEquals(1, paths.get(completeMap.getKey("Porto")).size());

        //Path to Lisbon
        assertEquals(Arrays.asList("Porto", "Aveiro", "Coimbra", "Lisboa"), paths.get(completeMap.getKey("Lisboa")));

        //Path to Castelo Branco
        assertEquals(Arrays.asList("Porto", "Aveiro", "Viseu", "Guarda", "Castelo Branco"), paths.get(completeMap.getKey("Castelo Branco")));

        //Path between Porto and Castelo Branco should be 335 Km
        assertEquals(335, dists.get(completeMap.getKey("Castelo Branco")), 0.01);

        //Changing Edge: Aveiro-Viseu with Edge: Leiria-C.Branco 
        //should change shortest path between Porto and Castelo Branco        
        completeMap.removeEdge("Aveiro", "Viseu");
        completeMap.insertEdge("Leiria", "Castelo Branco", "A23", 170);
        GraphAlgorithms.shortestPaths(completeMap, "Porto", paths, dists);

        //Path between Porto and Castelo Branco should now be 365 Km
        assertEquals(365, dists.get(completeMap.getKey("Castelo Branco")), 0.01);

        //Path to Castelo Branco
        assertEquals(Arrays.asList("Porto", "Aveiro", "Leiria", "Castelo Branco"), paths.get(completeMap.getKey("Castelo Branco")));

        GraphAlgorithms.shortestPaths(incompleteMap, "Porto", paths, dists);

        //Length path should be Double.MAX_VALUE if there is no path
        assertEquals(Double.MAX_VALUE, dists.get(completeMap.getKey("Faro")), 0.01);

        //Path between Porto and Lisboa should be 335 Km
        assertEquals(335, dists.get(completeMap.getKey("Lisboa")), 0.01);

        //Path to Lisboa
        assertEquals(Arrays.asList("Porto", "Aveiro", "Coimbra", "Lisboa"), paths.get(completeMap.getKey("Lisboa")));

        //Path between Porto and Lisboa should be 335 Km
        assertEquals(335, dists.get(completeMap.getKey("Lisboa")), 0.01);

        GraphAlgorithms.shortestPaths(incompleteMap, "Braga", paths, dists);

        //Path between Braga and Leiria should be 255 Km
        assertEquals(255, dists.get(completeMap.getKey("Leiria")), 0.01);

        //Path to Leiria
        assertEquals(Arrays.asList("Braga", "Porto", "Aveiro", "Leiria"), paths.get(completeMap.getKey("Leiria")));
    }
}
