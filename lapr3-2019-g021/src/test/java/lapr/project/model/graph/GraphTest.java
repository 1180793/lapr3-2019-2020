package lapr.project.model.graph;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * @author DEI-ISEP
 */
public class GraphTest {

    Graph<String, String> instance = new Graph<>(true);

    public GraphTest() {
    }

    /**
     * Test of numVertices method, of class Graph.
     */
    @Test
    public void testNumVertices() {
        System.out.println("Test numVertices");

        assertTrue((instance.numVertices() == 0));

        instance.insertVertex("A");
        assertTrue((instance.numVertices() == 1));

        instance.insertVertex("B");
        assertTrue((instance.numVertices() == 2));

        instance.removeVertex("A");
        assertTrue((instance.numVertices() == 1));

        instance.removeVertex("B");
        assertTrue((instance.numVertices() == 0));
    }

    /**
     * Test of vertices method, of class Graph.
     */
    @Test
    public void testVertices() {
        System.out.println("Test vertices");

        Iterator<String> itVerts = instance.vertices().iterator();

        assertTrue(itVerts.hasNext() == false);

        instance.insertVertex("A");
        instance.insertVertex("B");

        itVerts = instance.vertices().iterator();

        assertTrue((itVerts.next().compareTo("A") == 0));
        assertTrue((itVerts.next().compareTo("B") == 0));

        instance.removeVertex("A");

        itVerts = instance.vertices().iterator();
        assertTrue((itVerts.next().compareTo("B")) == 0);

        instance.removeVertex("B");

        itVerts = instance.vertices().iterator();
        assertTrue(itVerts.hasNext() == false);
    }

    /**
     * Test of numEdges method, of class Graph.
     */
    @Test
    public void testNumEdges() {
        System.out.println("Test numEdges");

        assertTrue((instance.numEdges() == 0));

        instance.insertEdge("A", "B", "Edge1", 6);
        assertTrue((instance.numEdges() == 1));

        instance.insertEdge("A", "C", "Edge2", 1);
        assertTrue((instance.numEdges() == 2));

        instance.removeEdge("A", "B");
        assertTrue((instance.numEdges() == 1));

        instance.removeEdge("A", "C");
        assertTrue((instance.numEdges() == 0));
    }

    /**
     * Test of edges method, of class Graph.
     */
    @Test
    public void testEdges() {
        System.out.println("Test Edges");

        Iterator<Edge<String, String>> itEdge = instance.edges().iterator();

        assertTrue((itEdge.hasNext() == false));

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        itEdge = instance.edges().iterator();

        itEdge.next();
        itEdge.next();
        assertTrue(itEdge.next().getElement().equals("Edge3") == true);

        itEdge.next();
        itEdge.next();
        assertTrue(itEdge.next().getElement().equals("Edge6") == true);

        instance.removeEdge("A", "B");

        itEdge = instance.edges().iterator();
        assertTrue(itEdge.next().getElement().equals("Edge2") == true);

        instance.removeEdge("A", "C");
        instance.removeEdge("B", "D");
        instance.removeEdge("C", "D");
        instance.removeEdge("C", "E");
        instance.removeEdge("D", "A");
        instance.removeEdge("E", "D");
        instance.removeEdge("E", "E");
        itEdge = instance.edges().iterator();
        assertTrue((itEdge.hasNext() == false));
    }

    /**
     * Test of getEdge method, of class Graph.
     */
    @Test
    public void testGetEdge() {
        System.out.println("Test getEdge");

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        assertTrue(instance.getEdge("A", "E") == null);

        assertTrue(instance.getEdge("B", "D").getElement().equals("Edge3") == true);
        assertTrue(instance.getEdge("D", "B") == null);

        instance.removeEdge("D", "A");
        assertTrue(instance.getEdge("D", "A") == null);

        assertTrue(instance.getEdge("E", "E").getElement().equals("Edge8") == true);

    }

    /**
     * Test of endVertices method, of class Graph.
     */
    @Test
    public void testEndVertices() {
        System.out.println("Test endVertices");

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        Edge<String, String> edge0 = new Edge<>();

        String[] vertices = new String[2];

        //assertTrue("endVertices should be null", instance.endVertices(edge0)==null);
        Edge<String, String> edge1 = instance.getEdge("A", "B");
        //vertices = instance.endVertices(edge1);
        assertTrue(instance.endVertices(edge1)[0].equals("A"));
        assertTrue(instance.endVertices(edge1)[1].equals("B"));
        
        Graph<String, String> instance = new Graph<>(true);
        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertVertex("C");
        
        Edge<String,String> edge = null;
        assertEquals(null, instance.endVertices(edge));
        
        Vertex<String, String> vertex1 = new Vertex<>(1, "A");
        edge = new Edge<>();
        edge.setVOrig(vertex1);
        edge.setVDest(new Vertex<>());
        assertEquals(null, instance.endVertices(edge));
        
        Vertex<String, String> vertex2 = new Vertex<>(2, "B");
        edge = new Edge<>();
        edge.setVOrig(new Vertex<>());
        edge.setVDest(vertex2);
        assertEquals(null, instance.endVertices(edge));
        
        Vertex<String, String> vertex3 = new Vertex<>(3, "C");
        edge = new Edge<>();
        edge.setVOrig(vertex1);
        edge.setVDest(vertex3);
        assertEquals(null, instance.endVertices(edge));
        
    }

    /**
     * Test of opposite method, of class Graph.
     */
    @Test
    public void testOpposite() {
        System.out.println("Test opposite");

        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        Edge<String, String> edge5 = instance.getEdge("C", "E");
        String vert = instance.opposite("A", edge5);
        assertTrue(vert == null);

        Edge<String, String> edge1 = instance.getEdge("A", "B");
        vert = instance.opposite("A", edge1);
        assertTrue(vert.equals("B") == true);

        Edge<String, String> edge8 = instance.getEdge("E", "E");
        vert = instance.opposite("E", edge8);
        assertTrue(vert.equals("E") == true);
        
        assertEquals(null, instance.opposite("F", edge8));
    }

    /**
     * Test of outDegree method, of class Graph.
     */
    @Test
    public void testOutDegree() {
        System.out.println("Test outDegree");

        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        int outdeg = instance.outDegree("G");
        assertTrue(outdeg == -1);

        outdeg = instance.outDegree("A");
        assertTrue(outdeg == 2);

        outdeg = instance.outDegree("B");
        assertTrue(outdeg == 1);

        outdeg = instance.outDegree("E");
        assertTrue(outdeg == 2);
    }

    /**
     * Test of inDegree method, of class Graph.
     */
    @Test
    public void testInDegree() {
        System.out.println("Test inDegree");

        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        int indeg = instance.inDegree("G");
        assertTrue(indeg == -1);

        indeg = instance.inDegree("A");
        assertTrue(indeg == 1);

        indeg = instance.inDegree("D");
        assertTrue(indeg == 3);

        indeg = instance.inDegree("E");
        assertTrue(indeg == 2);
    }

    /**
     * Test of outgoingEdges method, of class Graph.
     */
    @Test
    public void testOutgoingEdges() {
        System.out.println(" Test outgoingEdges");

        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        Iterator<Edge<String, String>> itEdge = instance.outgoingEdges("C").iterator();
        Edge<String, String> first = itEdge.next();
        Edge<String, String> second = itEdge.next();
        assertTrue(
                ((first.getElement().equals("Edge4") == true && second.getElement().equals("Edge5") == true)
                || (first.getElement().equals("Edge5") == true && second.getElement().equals("Edge4") == true)));

        instance.removeEdge("E", "E");

        itEdge = instance.outgoingEdges("E").iterator();
        assertTrue((itEdge.next().getElement().equals("Edge7") == true));

        instance.removeEdge("E", "D");

        itEdge = instance.outgoingEdges("E").iterator();
        assertTrue((itEdge.hasNext() == false));
        
        assertEquals(null, instance.outgoingEdges("F"));
    }

    /**
     * Test of incomingEdges method, of class Graph.
     */
    @Test
    public void testIncomingEdges() {

        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        Iterator<Edge<String, String>> itEdge = instance.incomingEdges("D").iterator();

        assertTrue((itEdge.next().getElement().equals("Edge3") == true));
        assertTrue((itEdge.next().getElement().equals("Edge4") == true));
        assertTrue((itEdge.next().getElement().equals("Edge7") == true));

        itEdge = instance.incomingEdges("E").iterator();

        assertTrue((itEdge.next().getElement().equals("Edge5") == true));
        assertTrue((itEdge.next().getElement().equals("Edge8") == true));

        instance.removeEdge("E", "E");

        itEdge = instance.incomingEdges("E").iterator();

        assertTrue((itEdge.next().getElement().equals("Edge5") == true));

        instance.removeEdge("C", "E");

        itEdge = instance.incomingEdges("E").iterator();
        assertTrue((itEdge.hasNext() == false));
        
        Graph<String, String> instance = new Graph<>(false);
        assertEquals(null, instance.incomingEdges("F"));
    }

    /**
     * Test of insertVertex method, of class Graph.
     */
    @Test
    public void testInsertVertex() {
        System.out.println("Test insertVertex");

        assertTrue(instance.insertVertex("A"));
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");

        Iterator<String> itVert = instance.vertices().iterator();
        assertFalse(instance.insertVertex("A"));
        assertTrue((itVert.next().equals("A") == true));
        assertTrue((itVert.next().equals("B") == true));
        assertTrue((itVert.next().equals("C") == true));
        assertTrue((itVert.next().equals("D") == true));
        assertTrue((itVert.next().equals("E") == true));
    }

    /**
     * Test of insertEdge method, of class Graph.
     */
    @Test
    public void testInsertEdge() {
        System.out.println("Test insertEdge");

        assertTrue((instance.numEdges() == 0));

        instance.insertEdge("A", "B", "Edge1", 6);
        assertTrue((instance.numEdges() == 1));

        assertFalse(instance.insertEdge("A", "B", "Edge1", 6));

        instance.insertEdge("A", "C", "Edge2", 1);
        assertTrue((instance.numEdges() == 2));

        instance.insertEdge("B", "D", "Edge3", 3);
        assertTrue((instance.numEdges() == 3));

        instance.insertEdge("C", "D", "Edge4", 4);
        assertTrue((instance.numEdges() == 4));

        instance.insertEdge("C", "E", "Edge5", 1);
        assertTrue((instance.numEdges() == 5));

        instance.insertEdge("D", "A", "Edge6", 2);
        assertTrue((instance.numEdges() == 6));

        instance.insertEdge("E", "D", "Edge7", 1);
        assertTrue((instance.numEdges() == 7));

        instance.insertEdge("E", "E", "Edge8", 1);
        assertTrue((instance.numEdges() == 8));

        Iterator<Edge<String, String>> itEd = instance.edges().iterator();

        itEd.next();
        itEd.next();
        assertTrue((itEd.next().getElement().equals("Edge3") == true));
        itEd.next();
        itEd.next();
        assertTrue((itEd.next().getElement().equals("Edge6") == true));
    }

    /**
     * Test of removeVertex method, of class Graph.
     */
    @Test
    public void testRemoveVertex() {
        System.out.println("Test removeVertex");

        instance.insertVertex("A");
        instance.insertVertex("B");
        instance.insertVertex("C");
        instance.insertVertex("D");
        instance.insertVertex("E");

        instance.removeVertex("C");
        assertTrue((instance.numVertices() == 4));

        Iterator<String> itVert = instance.vertices().iterator();
        assertTrue((itVert.next().equals("A") == true));
        assertTrue((itVert.next().equals("B") == true));
        assertTrue((itVert.next().equals("D") == true));
        assertTrue((itVert.next().equals("E") == true));

        instance.removeVertex("A");
        assertTrue((instance.numVertices() == 3));

        itVert = instance.vertices().iterator();
        assertTrue((itVert.next().equals("B") == true));
        assertTrue((itVert.next().equals("D") == true));
        assertTrue((itVert.next().equals("E") == true));

        instance.removeVertex("E");
        assertTrue((instance.numVertices() == 2));

        itVert = instance.vertices().iterator();

        assertTrue(itVert.next().equals("B") == true);
        assertTrue(itVert.next().equals("D") == true);

        instance.removeVertex("B");
        instance.removeVertex("D");
        assertTrue((instance.numVertices() == 0));
        
        Graph<String,String> ins = new Graph<>(false);
        
    }

    /**
     * Test of removeEdge method, of class Graph.
     */
    @Test
    public void testRemoveEdge() {
        System.out.println("Test removeEdge");

        assertTrue((instance.numEdges() == 0));

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        assertTrue((instance.numEdges() == 8));

        instance.removeEdge("E", "E");
        assertTrue((instance.numEdges() == 7));

        Iterator<Edge<String, String>> itEd = instance.edges().iterator();

        itEd.next();
        itEd.next();
        assertTrue((itEd.next().getElement().equals("Edge3") == true));
        itEd.next();
        itEd.next();
        assertTrue((itEd.next().getElement().equals("Edge6") == true));

        instance.removeEdge("C", "D");
        assertTrue((instance.numEdges() == 6));

        itEd = instance.edges().iterator();
        itEd.next();
        itEd.next();
        assertTrue((itEd.next().getElement().equals("Edge3") == true));
        assertTrue((itEd.next().getElement().equals("Edge5") == true));
        assertTrue((itEd.next().getElement().equals("Edge6") == true));
        assertTrue((itEd.next().getElement().equals("Edge7") == true));
        assertFalse(instance.removeEdge("Z", "A"));
        assertFalse(instance.removeEdge("E", "E"));
    }

    /**
     * Test of toString method, of class Graph.
     */
    @Test
    public void testClone() {
        System.out.println("Test Clone");

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        Graph<String, String> instClone = instance.clone();

        assertTrue(instance.numVertices() == instClone.numVertices());
        assertTrue(instance.numEdges() == instClone.numEdges());

        //vertices should be equal
        Iterator<String> itvertClone = instClone.vertices().iterator();
        Iterator<String> itvertSource = instance.vertices().iterator();
        while (itvertSource.hasNext()) {
            assertTrue((itvertSource.next().equals(itvertClone.next()) == true));
        }
    }

    /**
     * Test of validVertex method, of class Graph.
     */
    @Test
    public void testValidVertex() {
        System.out.println("Test validVertex");

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        Object vert = "F";
        boolean expResult = false;
        boolean result = instance.validVertex((String) vert);
        assertEquals(expResult, result);

        vert = "A";
        expResult = true;
        result = instance.validVertex((String) vert);
        assertEquals(expResult, result);

    }

    /**
     * Test of getKey method, of class Graph.
     */
    @Test
    public void testGetKey() {
        System.out.println("Test getKey");

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        Object vert = "A";
        int expResult = 0;
        int result = instance.getKey((String) vert);
        assertEquals(expResult, result);
    }

    /**
     * Test of allkeyVerts method, of class Graph.
     */
    @Test
    public void testAllkeyVerts() {
        System.out.println("Test allkeyVerts");

        Object[] expResult = null;
        Object[] result = instance.allkeyVerts();
        assertArrayEquals(expResult, result);

        instance.insertEdge("A", "B", "Edge1", 6);
        instance.insertEdge("A", "C", "Edge2", 1);
        instance.insertEdge("B", "D", "Edge3", 3);
        instance.insertEdge("C", "D", "Edge4", 4);
        instance.insertEdge("C", "E", "Edge5", 1);
        instance.insertEdge("D", "A", "Edge6", 2);
        instance.insertEdge("E", "D", "Edge7", 1);
        instance.insertEdge("E", "E", "Edge8", 1);

        expResult = new Object[5];
        expResult[0] = "A";
        expResult[1] = "B";
        expResult[2] = "C";
        expResult[3] = "D";
        expResult[4] = "E";

        result = instance.allkeyVerts();
        assertArrayEquals(expResult, result);

    }

    /**
     * Test of adjVertices method, of class Graph.
     */
    @Test
    public void testAdjVertices() {
        System.out.println("Test adjVertices");

        String vert = null;
        Iterable expResult = null;
        Iterable result = instance.adjVertices(vert);
        assertEquals(expResult, result);

        instance.insertEdge("A", "B", "Edge1", 6);
        vert = "A";
        Set<String> expResult2 = new HashSet<>();
        expResult2.add("B");
        result = instance.adjVertices((String) vert);
        assertEquals(expResult2, (Set<String>) result);
    }
}
