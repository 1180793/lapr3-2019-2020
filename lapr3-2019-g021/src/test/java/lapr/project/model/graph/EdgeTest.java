package lapr.project.model.graph;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * @author DEI-ISEP
 */
public class EdgeTest {

        Edge<String, String> instance = new Edge<>();

        public EdgeTest() {
        }

        /**
         * Test of getElement method, of class Edge.
         */
        @Test
        public void testGetElement() {
                System.out.println("getElement");

                String expResult = null;
                assertEquals(expResult, instance.getElement());

                Edge<String, String> instance1 = new Edge<>("edge1", 1.0, null, null);
                expResult = "edge1";
                assertEquals(expResult, instance1.getElement());
        }

        /**
         * Test of setElement method, of class Edge.
         */
        @Test
        public void testSetElement() {
                System.out.println("setElement");

                String eInf = "edge1";
                instance.setElement(eInf);

                assertEquals("edge1", instance.getElement());
        }

        /**
         * Test of getWeight method, of class Edge.
         */
        @Test
        public void testGetWeight() {
                System.out.println("getWeight");

                double expResult = 0.0;
                assertEquals(expResult, instance.getWeight(), 0.1);
        }

        /**
         * Test of setWeight method, of class Edge.
         */
        @Test
        public void testSetWeight() {
                System.out.println("setWeight");
                double ew = 2.0;
                instance.setWeight(ew);

                double expResult = 2.0;
                assertEquals(expResult, instance.getWeight(), 2.0);
        }

        /**
         * Test of getVOrig method, of class Edge.
         */
        @Test
        public void testGetVOrig() {
                System.out.println("getVOrig");

                Object expResult = null;
                assertEquals(expResult, instance.getVOrig());

                Vertex<String, String> vertex1 = new Vertex<>(1, "Vertex1");
                Edge<String, String> otherEdge = new Edge<>("edge1", 1.0, vertex1, vertex1);
                assertEquals(vertex1.getElement(), otherEdge.getVOrig());
        }

        /**
         * Test of setVOrig method, of class Edge.
         */
        @Test
        public void testSetVOrig() {
                System.out.println("setVOrig");

                Vertex<String, String> vertex1 = new Vertex<>(1, "Vertex1");
                instance.setVOrig(vertex1);
                assertEquals(vertex1.getElement(), instance.getVOrig());
        }

        /**
         * Test of getVDest method, of class Edge.
         */
        @Test
        public void testGetVDest() {
                System.out.println("getVDest");

                Object expResult = null;
                assertEquals(expResult, instance.getVDest());

                Vertex<String, String> vertex1 = new Vertex<>(1, "Vertex1");
                Edge<String, String> otherEdge = new Edge<>("edge1", 1.0, vertex1, vertex1);
                assertEquals(vertex1.getElement(), otherEdge.getVDest());
        }

        /**
         * Test of setVDest method, of class Edge.
         */
        @Test
        public void testSetVDest() {
                System.out.println("setVDest");

                Vertex<String, String> vertex1 = new Vertex<>(1, "Vertex1");
                instance.setVDest(vertex1);
                assertEquals(vertex1.getElement(), instance.getVDest());
        }

        /**
         * Test of getEndpoints method, of class Edge.
         */
        @Test
        public void testGetEndpoints() {
                System.out.println("getEndpoints");

                String[] expResult = null;
                String[] result = instance.getEndpoints();
                assertArrayEquals(expResult, result);

                Vertex<String, String> vertex1 = new Vertex<>(1, "Vertex1");
                instance.setVOrig(vertex1);
                instance.setVDest(vertex1);

                String[] expResult1 = {"Vertex1", "Vertex1"};
                assertArrayEquals(expResult1, instance.getEndpoints());
                
                Edge<String, String> edge1 = new Edge<>();
                edge1.setVOrig(vertex1);
                String[] expResult2 = {"Vertex1", null};
                assertArrayEquals(expResult2, edge1.getEndpoints());
                
                Edge<String, String> edge2 = new Edge<>();
                edge2.setVDest(vertex1);
                String[] expResult3 = {null, "Vertex1"};
                assertArrayEquals(expResult3, edge2.getEndpoints());
        }

}
