/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;
import lapr.project.data.mock.BicycleMock;
import lapr.project.data.mock.ParkMock;
import lapr.project.data.mock.ScooterMock;
import lapr.project.data.mock.UsersMock;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 *
 * @author roger40
 */
public class TripRegistryTest {

	TripRegistry tripreg;
	BicycleMock bMock;
	ScooterMock sMock;
	UsersMock uMock;
	ParkMock pMock;

	public TripRegistryTest() {
		pMock = new ParkMock();
		uMock = new UsersMock();
		bMock = new BicycleMock();
		sMock = new ScooterMock();
		tripreg = new TripRegistry(true); //True para usar a mock
	}

	@Test
	public void testGetAllTrips() {
		assertEquals(tripreg.getAllTrips().size(), 4); // pna mock so ha 2 trips, e ambas estao a acontecer no momento

	}

	/**
	 * Test of addTrip method, of class TripRegistry.
	 */
	@Test
	public void testAddTrip() {

		boolean t1 = tripreg.addTrip(bMock.getBicycle("BIKE001").getID(), "b", bMock.getBicycle("BIKE001").getAssignedPark(), uMock.getUserById("email6@gmail.com"));

		boolean t2 = tripreg.addTrip(sMock.getScooter("SCTR002").getID(), "s", sMock.getScooter("SCTR002").getAssignedPark(), uMock.getUserById("email7@gmail.com"));

		assertEquals(t1, true);
		assertEquals(t2, true);

	}

	/**
	 * Test of updateTrip method, of class TripRegistry.
	 */
	@Test
	public void testUpdateTrip() {

		for (Trip t : tripreg.getAllTrips()) {
			if (t.getAssignedUser().getEmail().equals("roger1@gmail.com")) {
				assertEquals(t.getParkDestiny(), null);   //Verifico realmente que a trip do user "roger1@gmail.com" ainda nao foi atualizada e ainda se encontra em curso ( sem park de destino)
			}

			User user = new User("roger1@gmail.com", "roger1", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25); // é este user que tem uma viagem em curso, tive de criar um user pois teno de o passar por parametro

			boolean t1 = tripreg.updateTrip(pMock.getPark("3"), user);
			assertEquals(t1, true);
			assertEquals(t1, true);

			User user2 = new User("rogerNOTRIP@gmail.com", "roger1", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25); // este user nao tem nenhuma viagem em curso logo a sua trip nao pode ser atualizada
			boolean t2 = tripreg.updateTrip(pMock.getPark("3"), user2);
			assertEquals(t2, false);

			User userT = new User("rogerTT@gmail.com", "roger1", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25); // este user nao tem nenhuma viagem em curso logo a sua trip nao pode ser atualizada
			boolean t3 = tripreg.updateTrip(pMock.getPark("3"), userT);
			assertEquals(t3, false); // false porque o user embora tenha uma trip concluida, nao tem trips ativas

		}

	}

	@Test
	public void testGetListByUserAndMonth() {
		Date date = new Date();

		long time = date.getTime();

		Timestamp ts = new Timestamp(time);
		//assertEquals( tripreg.getAlltripsFromUserInMonth("rogerTT@gmail.com", ts.getMonth()+1).size(), 2); // no mes atual ha duas trips com esse user

		assertEquals(tripreg.getUserMonthTrips("rogerTTTTTT@gmail.com", ts.getMonth() + 1).size(), 0); // user nao tem trips

		assertEquals(tripreg.getUserMonthTrips("roger1@gmail.com", ts.getMonth() + 1).size(), 0); // user tem trips, mas nao estao termiandas

	}
}
