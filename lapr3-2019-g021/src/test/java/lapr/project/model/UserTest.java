package lapr.project.model;

import lapr.project.utils.MD5Utils;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UserTest {

	String validEmail;
	String invalidEmail;

	String validName;
	String invalidName;

	String validPassword;
	String invalidPassword;

	String validCreditCard;
	String invalidCreditCard;

	char validGender;
	char invalidGender;

	double validHeight;
	double invalidHeight;

	double validWeight;
	double invalidWeight;
	
	double validAvgSpeed;
	double invalidAvgSpeed;

	public void setUp() {
		validEmail = "test@isep.ipp.pt";
		invalidEmail = "isep.pt";
		validName = "testeUser";
		invalidName = "";
		validPassword = "He3232JFll2";
		invalidPassword = "";
		validCreditCard = "1234567891234567";
		invalidCreditCard = "8576432jjhf238ji";
		validGender = 'M';
		invalidGender = 'g';
		validHeight = 1.79;
		invalidHeight = -2.4;
		validWeight = 65.3;
		invalidWeight = -2.3;
		validAvgSpeed = 30.2;
		invalidAvgSpeed = -2.3;
	}

	@Test
	public void UserTest() {
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		assertTrue(oUser.getName().equals(validName));
		assertTrue(oUser.getEmail().equals(validEmail));
		assertTrue(oUser.getPassword().equals(MD5Utils.encrypt(validPassword)));
		assertTrue(oUser.getCreditCard().equals(validCreditCard));
		assertTrue(oUser.getGender() == validGender);
		assertEquals(oUser.getHeight(), validHeight);
		assertEquals(oUser.getWeight(), validWeight);
		assertFalse(oUser.isAdmin());
		assertEquals(oUser.getPoints(), 0);
		assertEquals(oUser.getAverageSpeed(), validAvgSpeed);
	}

	/**
	 * Test of validate method, of class User.
	 */
	@Test
	public void testInvalidName() {
		setUp();
		System.out.println("testInvalidName");
		User oUser = new User(validEmail, invalidName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidEmail() {
		setUp();
		System.out.println("testInvalidEmail");
		User oUser = new User(invalidEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidEmail2() {
		setUp();
		System.out.println("testInvalidEmail2");
		User oUser = new User("testEmail@isep@ipp.pt", validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidEmail3() {
		setUp();
		System.out.println("testInvalidEmail3");
		User oUser = new User("testEmail@isepipppt", validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidPassword() {
		setUp();
		System.out.println("testInvalidPassword");
		User oUser = new User(validEmail, validName, invalidPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidCreditCard() {
		setUp();
		System.out.println("testInvalidCreditCard");
		User oUser = new User(validEmail, validName, validPassword, invalidCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);

	}

	@Test
	public void testInvalidCreditCard2() {
		setUp();
		System.out.println("testInvalidCreditCard2");
		User oUser = new User(validEmail, validName, validPassword, "1112", validGender, validHeight, validWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidGender() {
		setUp();
		System.out.println("testInvalidGender");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, invalidGender, invalidHeight, validWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testValidateGenderF() {
		setUp();
		System.out.println("testValidateGenderF");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, 'F', validHeight, validWeight, validAvgSpeed);
		boolean expResult = true;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidHeight() {
		setUp();
		System.out.println("testInvalidHeight");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, invalidHeight, validWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidHeight0() {
		setUp();
		System.out.println("testInvalidHeight0");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, 0, validWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidWeight() {
		setUp();
		System.out.println("testInvalidWeight");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, invalidWeight, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidWeight0() {
		setUp();
		System.out.println("testInvalidWeight0");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, 0, validAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}
	
	@Test
	public void testInvalidAverageSpeed() {
		setUp();
		System.out.println("testInvalidAverageSpeed");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, invalidAvgSpeed);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testInvalidAverageSpeed0() {
		setUp();
		System.out.println("testInvalidAverageSpeed0");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, 0);
		boolean expResult = false;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	@Test
	public void testValidateUser() {
		setUp();
		System.out.println("testValidateUser");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		boolean expResult = true;
		boolean result = oUser.validateUser();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getEmail method, of class User.
	 */
	@Test
	public void testGetEmail() {
		System.out.println("testGetEmail");
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		String expResult = validEmail;
		String result = oUser.getEmail();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getName method, of class User.
	 */
	@Test
	public void testGetName() {
		System.out.println("testGetName");
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		String expResult = validName;
		String result = oUser.getName();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getPassword method, of class User.
	 */
	@Test
	public void testGetPassword() {
		System.out.println("testGetPassword");
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		String expResult = MD5Utils.encrypt(validPassword);
		String result = oUser.getPassword();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getCreditCard method, of class User.
	 */
	@Test
	public void testGetCreditCard() {
		System.out.println("testGetCreditCard");
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		String expResult = validCreditCard;
		String result = oUser.getCreditCard();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getGender method, of class User.
	 */
	@Test
	public void testGetGender() {
		System.out.println("testGetGender");
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		char expResult = validGender;
		char result = oUser.getGender();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getHeight method, of class User.
	 */
	@Test
	public void testGetHeight() {
		System.out.println("testGetHeight");
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		double expResult = validHeight;
		double result = oUser.getHeight();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getWeight method, of class User.
	 */
	@Test
	public void testGetWeight() {
		System.out.println("testGetWeight");
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		double expResult = validWeight;
		double result = oUser.getWeight();
		assertEquals(expResult, result);
	}
	
	/**
	 * Test of getWeight method, of class User.
	 */
	@Test
	public void testGetAverageSpeed() {
		System.out.println("testGetAverageSpeed");
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		double expResult = validAvgSpeed;
		double result = oUser.getAverageSpeed();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getPoints method, of class User.
	 */
	@Test
	public void testGetPoints() {
		System.out.println("getPoints");
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		int expResult = 0;
		int result = oUser.getPoints();
		assertEquals(expResult, result);
	}

	/**
	 * Test of setPoints and getPoints method, of class User.
	 */
	@Test
	public void testSetPoints() {
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		System.out.println("setPoints");
		int points = 10;
		oUser.setPoints(points);
		int expResult = 10;
		int result = oUser.getPoints();
		assertEquals(expResult, result);
	}

	/**
	 * Test of setIsAdmin method, of class User.
	 */
	@Test
	public void testIsAdmin() {
		setUp();
		System.out.println("setAdmin");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		oUser.setAdmin(true);
		assertTrue(oUser.isAdmin());
	}

	/**
	 * + * Test of setAdmin method, of class User. +
	 */
	@Test

	public void testSetAdmin_char() {
		System.out.println("setAdmin_char");
		User oUser = new User("email1@gmail.com", "Francisco Magalhaes", "1234567", "0000000000000001", 'm', 182, 90, validAvgSpeed);
		oUser.setAdmin(true);
		assertEquals(oUser.isAdmin(), true);
	}

	/**
	 * Test of hashCode method, of class User.
	 */
	@Test
	public void testHashCodeDifferentObjects() {
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		User anotherUser = new User("testeEmailDiferente@isep.ipp.pt", validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validAvgSpeed);
		assertNotEquals(oUser.hashCode(), anotherUser.hashCode());
	}

	public void testHashCodeSameObjects() {
		setUp();
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validWeight);
		User anotherUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validWeight);
		assertNotEquals(oUser.hashCode(), anotherUser.hashCode());
		assertEquals(oUser.hashCode(), anotherUser.hashCode());
	}

	/**
	 * Test of equals method, of class User.
	 */
	@Test
	public void testEqualsSameObject() {
		setUp();
		System.out.println("testEqualsSameObject");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validWeight);
		assertEquals(oUser, oUser);

	}

	@Test
	public void testEqualsNotSameClass() {
		setUp();
		System.out.println("testEqualsNotSameClass");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validWeight);
		int anotherClass = 3;
		assertFalse(oUser.equals(anotherClass));
	}

	@Test
	public void testEqualsNull() {
		setUp();
		System.out.println("testEqualsNull");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validWeight);
		User anotherUser = null;
		assertFalse(oUser.equals(anotherUser));
	}

	@Test
	public void testEqualsNotEmail() {
		setUp();
		System.out.println("testEqualsNotEmail");
		User oUser = new User(validEmail, validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validWeight);
		User anotherUser = new User("testeOtherEmail@isep.ipp.pt", validName, validPassword, validCreditCard, validGender, validHeight, validWeight, validWeight);
		assertNotEquals(oUser, anotherUser);
	}

}
