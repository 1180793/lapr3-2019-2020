package lapr.project.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Francisco
 */
public class InvoiceTest {

    Invoice invoiceOne;
    Park park1 = new Park("Park1", 41.123456, 9.153152, 10, "Park 1 for Graph test", 10, 6, 5, 5);
    Park park2 = new Park("Park2", 41.015153, 9.315251, 10, "Park 2 for Graph test", 10, 5, 0, 5);
    Park park3 = new Park("Park3", 41.521512, 9.153120, 50, "Park 3 for Graph test", 10, 3, 0, 3);
    Park park4 = new Park("Park4", 41.222552, 9.153210, 50, "Park 4 for Graph test", 10, 6, 0, 0);

    public void setUp() {

        User userOne = new User("email1@gmail.com", "Francisco Magalhaes", "123", "0000000000000001", 'm', 182, 90, 13);
        userOne.setPoints(35);

        Timestamp tripStartOne = new Timestamp(2019, 10, 3, 10, 0, 0, 0);
        Timestamp tripEndOne = new Timestamp(2019, 10, 3, 12, 0, 0, 0);

        Timestamp tripStartTwo = new Timestamp(2019, 10, 24, 10, 0, 0, 0);
        Timestamp tripEndTwo = new Timestamp(2019, 10, 24, 19, 0, 0, 0);

        Timestamp tripStartThree = new Timestamp(2019, 11, 3, 10, 0, 0, 0);
        Timestamp tripEndThree = new Timestamp(2019, 11, 3, 12, 0, 0, 0);

        Timestamp tripStartFour = new Timestamp(2019, 11, 24, 10, 0, 0, 0);
        Timestamp tripEndFour = new Timestamp(2019, 11, 24, 19, 0, 0, 0);

        Trip tripOne = new Trip("BIKE001", "b", tripStartOne, park1, userOne, park2, tripEndOne, 111, 2.23);

        Trip tripTwo = new Trip("BIKE001", "b", tripStartTwo, park3, userOne, park4, tripEndTwo, 112, 18.23);

        List<Trip> lstTripsOne = new ArrayList<>();
        lstTripsOne.add(tripOne);
        lstTripsOne.add(tripTwo);
        invoiceOne = new Invoice(1, userOne.getName(), 55, 30, 80, 5, 12.46, lstTripsOne);
    }

    /**
     * Test of getID method, of class Invoice.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");

        setUp();

        int expResult = 1;
        int result = invoiceOne.getID();
        assertEquals(expResult, result);

    }

    /**
     * Test of getUsername method, of class Invoice.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername");

        setUp();

        String expResult = "Francisco Magalhaes";
        String result = invoiceOne.getUsername();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPreviousMonthPoints method, of class Invoice.
     */
    @Test
    public void testGetPreviousMonthPoints() {
        System.out.println("getPreviousMonthPoints");

        setUp();

        int expResult = 55;
        int result = invoiceOne.getPreviousMonthPoints();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointsEarnedThisMonth method, of class Invoice.
     */
    @Test
    public void testGetPointsEarnedThisMonth() {
        System.out.println("getPointsEarnedThisMonth");

        setUp();

        int expResult = 30;
        int result = invoiceOne.getPointsEarnedThisMonth();
        assertEquals(expResult, result);

    }

    /**
     * Test of getPointsUsed method, of class Invoice.
     */
    @Test
    public void testGetPointsUsed() {
        System.out.println("getPointsUsed");

        setUp();

        int expResult = 80;
        int result = invoiceOne.getPointsUsed();
        assertEquals(expResult, result);

    }

    /**
     * Test of getPointsAvailableForNextMonth method, of class Invoice.
     */
    @Test
    public void testGetPointsAvailableForNextMonth() {
        System.out.println("getPointsAvailableForNextMonth");

        setUp();

        int expResult = 5;
        int result = invoiceOne.getPointsAvailableForNextMonth();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTotalPrice method, of class Invoice.
     */
    @Test
    public void testGetTotalPrice() {
        System.out.println("getTotalPrice");

        setUp();

        double expResult = 12.46;
        double result = invoiceOne.getTotalPrice();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getTripList method, of class Invoice.
     */
    @Test
    public void testGetTripList() {
        System.out.println("getTripList");

        setUp();

        Timestamp tripStartOne = new Timestamp(2019, 10, 3, 10, 0, 0, 0);
        Timestamp tripEndOne = new Timestamp(2019, 10, 3, 12, 0, 0, 0);

        Timestamp tripStartTwo = new Timestamp(2019, 10, 24, 10, 0, 0, 0);
        Timestamp tripEndTwo = new Timestamp(2019, 10, 24, 19, 0, 0, 0);

        User userOne = new User("email1@gmail.com", "Francisco Magalhaes", "123", "0000000000000001", 'm', 182, 90, 13);
        Trip tripOne = new Trip("BIKE001", "b", tripStartOne, park1, userOne, park2, tripEndOne, 111, 2.23);
        Trip tripTwo = new Trip("BIKE001", "b", tripStartTwo, park3, userOne, park4, tripEndTwo, 112, 18.23);

        List<Trip> expResult = new ArrayList<>();
        expResult.add(tripOne);
        expResult.add(tripTwo);

        List<Trip> result = invoiceOne.getTripList();
        assertEquals(tripOne.getInvoiceId(), result.get(0).getInvoiceId());

    }

    /**
     * Test of hashCode method, of class Invoice.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");

        setUp();

        int expResult = 1;
        int result = invoiceOne.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Invoice.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        
        setUp();
        
        Object obj = null;
        boolean expResult = false;
        boolean result = invoiceOne.equals(obj);
        assertEquals(expResult, result);
        
        obj = invoiceOne;
        expResult = true;
        result = invoiceOne.equals(obj);
        assertEquals(expResult, result);
        
        obj = 1;
        expResult = false;
        result = invoiceOne.equals(obj);
        assertEquals(expResult, result);
        
        
        User userOne = new User("email1@gmail.com", "Francisco Magalhaes", "123", "0000000000000001", 'm', 182, 90, 13);
        userOne.setPoints(35);
        Timestamp tripStartOne = new Timestamp(2019, 10, 3, 10, 0, 0, 0);
        Timestamp tripEndOne = new Timestamp(2019, 10, 3, 12, 0, 0, 0);
        Timestamp tripStartTwo = new Timestamp(2019, 10, 24, 10, 0, 0, 0);
        Timestamp tripEndTwo = new Timestamp(2019, 10, 24, 19, 0, 0, 0);
        Timestamp tripStartThree = new Timestamp(2019, 11, 3, 10, 0, 0, 0);
        Timestamp tripEndThree = new Timestamp(2019, 11, 3, 12, 0, 0, 0);
        Timestamp tripStartFour = new Timestamp(2019, 11, 24, 10, 0, 0, 0);
        Timestamp tripEndFour = new Timestamp(2019, 11, 24, 19, 0, 0, 0);
        Trip tripOne = new Trip("BIKE001", "b", tripStartOne, park1, userOne, park2, tripEndOne, 111, 2.23);
        Trip tripTwo = new Trip("BIKE001", "b", tripStartTwo, park3, userOne, park4, tripEndTwo, 112, 18.23);
        List<Trip> lstTripsOne = new ArrayList<>();
        lstTripsOne.add(tripOne);
        lstTripsOne.add(tripTwo);
        Invoice invoiceTwo = new Invoice(1, userOne.getName(), 55, 30, 80, 5, 12.46, lstTripsOne);
        obj = invoiceTwo;
        expResult = true;
        result = invoiceOne.equals(obj);
        assertEquals(expResult, result);
        
        invoiceTwo = new Invoice(2, userOne.getName(), 55, 30, 80, 5, 12.46, lstTripsOne);
        obj = invoiceTwo;
        expResult = false;
        result = invoiceOne.equals(obj);
        assertEquals(expResult, result);
        
    }


    /**
     * Test of toString method, of class Invoice.
     */
    //@Test
    public void testToString() {
        System.out.println("toString");

        setUp();

        String expResult = "Francisco Magalhaes\n"
                + "Previous points:55\n"
                + "Earned points:30\n"
                + "Discounted points:80\n"
                + "Actual points:5\n"
                + "Charged Value:12.46\n"
                + "BIKE001;61530919200000;61530926400000;41.123456;9.153152;41.015153;9.315251;7200;2.23\n"
                + "BIKE001;61532733600000;61532766000000;41.521512;9.15312;41.222552;9.15321;32400;18.23";
        String result = invoiceOne.toString();
        assertEquals(expResult, result);
    }

}
