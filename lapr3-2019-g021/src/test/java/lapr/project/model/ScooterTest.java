package lapr.project.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author roger40
 */
public class ScooterTest {

        Scooter scooter1;
        Scooter scooter2;
        Park park1;
        User user1;

        public ScooterTest() {

                park1 = new Park("2", 50.0, 120.0, 65, "parque teste2", 85, 75, 35, 15);

                user1 = new User("roger@gmail.com", "roger", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.1);

                scooter1 = new Scooter("SCTR001", 20, 1, park1, 200.0, 75, 1.10, 0.3, 200);

                scooter2 = new Scooter("SCTR002", 20, 1, park1, 200.0, 75, 1.10, 0.3, 215);
                scooter2.setAssignedUser(user1);
        }

        /**
         * Test of getID method, of class Scooter.
         */
        @Test
        public void testGetID() {
                System.out.println("getID");
                String expResult = "SCTR001";
                String result = scooter1.getID();
                assertEquals(expResult, result);
        }

        /**
         * Test of getWeight method, of class Scooter.
         */
        @Test
        public void testGetWeight() {
                System.out.println("getWeight");
                int expResult = 20;
                int result = scooter1.getWeight();
                assertEquals(expResult, result);
        }

        /**
         * Test of getType method, of class Scooter.
         */
        @Test
        public void testGetType() {
                System.out.println("getType");
                int expResult = 1;
                int result = scooter1.getType();
                assertEquals(expResult, result);
        }

        /**
         * Test of getAssignedPark method, of class Scooter.
         */
        @Test
        public void testGetAssignedPark() {
                System.out.println("getAssignedPark");
                Park result = scooter1.getAssignedPark();
                assertEquals(park1, result);
        }

        /**
         * Test of getMaxBatteryCapacity method, of class Scooter.
         */
        @Test
        public void testGetMaxBatteryCapacity() {
                System.out.println("testGetMaxBatteryCapacity");
                Double expRes = scooter1.getMaxBatteryCapacity();
                assertEquals(expRes, 200);
        }

        /**
         * Test of getActualBatteryCapacity method, of class Scooter.
         */
        @Test
        public void testGetActualBatteryCapacity() {
                System.out.println("getActualBatteryCapacity");
                int result = scooter1.getActualBatteryCapacity();
                assertEquals(75, result);
        }

        /**
         * Test of getAerodynamicCoefficient method, of class Scooter.
         */
        @Test
        public void testGetAerodynamicCoefficient() {
                System.out.println("getAerodynamicCoefficient");
                double expResult = 1.10;
                double result = scooter1.getAerodynamicCoefficient();
                assertEquals(expResult, result);
        }

        /**
         * Test of getFrontalArea method, of class Scooter.
         */
        @Test
        public void testGetFrontalArea() {
                System.out.println("getFrontalArea");
                double expResult = 0.30;
                double result = scooter1.getFrontalArea();
                assertEquals(expResult, result);
        }

        /**
         * Test of getAssignedUser method, of class Scooter.
         */
        @Test
        public void testGetUser() {
                System.out.println("getUser");
                assertEquals(user1, scooter2.getAssignedUser());
                assertNull(scooter1.getAssignedUser());
        }

        /**
         * Test of setAssignedUser method, of class Scooter.
         */
        @Test
        public void testSetAssignedUser() {
                System.out.println("setAssignedUser");
                User user2 = new User("test@gmail.com", "teste", "12345", "2221111111111111", 'm', 188.7, 90.0, 0.25);
                scooter1.setAssignedUser(user2);
                assertEquals(user2, scooter1.getAssignedUser());
        }

        /**
         * Test of setAssignedPark method, of class Scooter.
         */
        @Test
        public void testSetPark() {
                System.out.println("setAssignedPark");
                Park park2 = new Park("7", 70.0, 120.0, 65, "parque testex", 85, 75, 35, 15);
                scooter1.setAssignedPark(park2);
                assertEquals(park2, scooter1.getAssignedPark());
        }
        
        
          @Test
        public void testGetMotor() {
                System.out.println("testGetMotor");
                
                assertEquals(200, scooter1.getMotor());
        }
        
         @Test
        public void testSetMotor() {
                System.out.println("testSetMotor");
                scooter2.setMotor(150);
                assertEquals(150, scooter2.getMotor());
        }
        
        @Test
        public void testInvalidWeight() {
                System.out.println("testInvalidWeight");
                Scooter scooter = new Scooter("SCTR001", -20, 1, park1, 200.0, 75, 1.10, 0.3, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidWeight0() {
                System.out.println("testInvalidWeight0");
                Scooter scooter = new Scooter("SCTR001", 0, 1, park1, 200.0, 75, 1.10, 0.3, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }

        @Test
        public void testValidType1() {
                System.out.println("testValidType1");
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, 200.0, 75, 1.10, 0.3, 215);
                boolean expResult = true;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testValidType2() {
                System.out.println("testValidType2");
                Scooter scooter = new Scooter("SCTR001", 20, 2, park1, 200.0, 75, 1.10, 0.3, 215);
                boolean expResult = true;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidType() {
                System.out.println("testInvalidType");
                Scooter scooter = new Scooter("SCTR001", 20, 0, park1, 200.0, 75, 1.10, 0.3, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidMaxBatteryCapacity() {
                System.out.println("testInvalidMaxBatteryCapacity");
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, -200, 75, 1.10, 0.3, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidMaxBatteryCapacity0() {
                System.out.println("testInvalidMaxBatteryCapacity0");
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, 0, 75, 1.10, 0.3, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidBatteryActualCapacapity1() {
                System.out.println("testInvalidBatteryActualCapacapity1");
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, 200, -30, 1.10, 0.3, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidBatteryActualCapacapity2() {
                System.out.println("testInvalidBatteryActualCapacapity2");
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, 200, 120, 1.10, 0.3, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidAerodynamicCoefficient() {
                System.out.println("testInvalidAerodynamicCoefficient");
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, 200.0, 75, -1.10, 0.3, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidAerodynamicCoefficient0() {
                System.out.println("testInvalidAerodynamicCoefficient0");
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, 200.0, 75, 0, 0.3, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidFrontalArea() {
                System.out.println("testInvalidFrontalArea");
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, 200.0, 75, 1.10, -0.3, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testInvalidFrontalArea0() {
                System.out.println("testInvalidFrontalArea0");
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, 200.0, 75, 1.10, 0, 215);
                boolean expResult = false;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }
        
        @Test
        public void testValidateScooter() {
                System.out.println("testInvalidFrontalArea0");
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, 200.0, 75, 1.10, 0.3, 215);
                boolean expResult = true;
                boolean result = scooter.validateScooter();
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class Scooter.
         */
        @Test
        public void testHashCodeDifferentObjects() {
                assertNotEquals(scooter1.hashCode(), scooter2.hashCode());
        }

        public void testHashCodeSameObjects() {
                Scooter scooter = new Scooter("SCTR001", 20, 1, park1, 200.0, 75, 1.10, 0.3, 215);
                assertNotEquals(scooter.hashCode(), scooter.hashCode());
                assertEquals(scooter.hashCode(), scooter.hashCode());
        }

        /**
         * Test of equals method, of class Scooter.
         */
        @Test
        public void testEqualsSameObject() {
                System.out.println("testEqualsSameObject");
                assertEquals(scooter1, scooter1);
        }

        @Test
        public void testEqualsNotSameClass() {
                System.out.println("testEqualsNotSameClass");
                int anotherClass = 3;
                assertFalse(scooter1.equals(anotherClass));
        }

        @Test
        public void testEqualsNull() {
                System.out.println("testEqualsNull");
                Bicycle bike = null;
                assertFalse(scooter1.equals(bike));
        }

        @Test
        public void testEqualsNotID() {
                System.out.println("testEqualsNotID");
                assertNotEquals(scooter1, scooter2);
        }
}
