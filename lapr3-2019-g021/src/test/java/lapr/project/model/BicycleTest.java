package lapr.project.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author roger
 */
public class BicycleTest {

        Bicycle bike1;
        Bicycle bike2;
        Park park1;
        User user1;

        public BicycleTest() {
                park1 = new Park("2", 50.0, 120.0, 65, "parque teste2", 85, 75, 35, 15);

                user1 = new User("roger@gmail.com", "roger", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);

                bike1 = new Bicycle("BIKE001", 15, park1, 7, 1.1, 15);
                bike2 = new Bicycle("BIKE002", 20, park1, 7, 1.44, 15);
        }

        @Test
        public void testGetSize() {
                System.out.println("getWheelSize");
                int expResult = 15;
                int result = bike1.getWheelSize();
                int expResult2 = 15;
                int result2 = bike2.getWheelSize();
                assertEquals(expResult, result);
                assertEquals(expResult2, result2);
        }

        /**
         * Test of getID method, of class Bicycle.
         */
        @Test
        public void testGetID() {
                System.out.println("getID");
                String expResult = "BIKE001";
                String result = bike1.getID();
                String expResult2 = "BIKE002";
                String result2 = bike2.getID();
                assertEquals(expResult, result);
                assertEquals(expResult2, result2);

        }

        /**
         * Test of getWeight method, of class Bicycle.
         */
        @Test
        public void testGetWeight() {
                System.out.println("getWeight");
                int expResult = 15;
                int result = bike1.getWeight();
                int expResult2 = 20;
                int result2 = bike2.getWeight();
                assertEquals(expResult, result);
                assertEquals(expResult2, result2);

        }

        /**
         * Test of getUser method, of class Bicycle.
         */
        @Test
        public void testGetUser() {
                System.out.println("getUser");
                bike1.setAssignedUser(user1);
                assertEquals(bike1.getAssignedUser(), user1);
                User expUser2 = bike2.getAssignedUser();
                assertNull(expUser2);
        }

        /**
         * Test of getAssignedPark method, of class Bicycle.
         */
        @Test
        public void testGetPark() {
                Park expPark = bike1.getAssignedPark();
                Park expPark2 = bike2.getAssignedPark();
                assertEquals(expPark, park1);
                assertEquals(expPark2, park1);
        }
        
        /**
         * Test of getAerodynamicCoefficient method, of class Scooter.
         */
        @Test
        public void testGetAerodynamicCoefficient() {
                System.out.println("getAerodynamicCoefficient");
                double expResult = 7;
                double result = bike1.getAerodynamicCoefficient();
                assertEquals(expResult, result);
        }

        /**
         * Test of getFrontalArea method, of class Scooter.
         */
        @Test
        public void testGetFrontalArea() {
                System.out.println("getFrontalArea");
                double expResult = 1.1;
                double result = bike1.getFrontalArea();
                assertEquals(expResult, result);
        }

        /**
         * Test of setAssignedUser method, of class Bicycle.
         */
        @Test
        public void testSetAssignedUser() {
                System.out.println("setAssignedUser");
                User user2 = new User("teste@gmail.com", "teste", "21312", "1111111111111111", 'm', 150.7, 100.0, 0.25);
                bike1.setAssignedUser(user2);
                assertEquals(user2, bike1.getAssignedUser());
        }

        /**
         * Test of setAssignedPark method, of class Bicycle.
         */
        @Test
        public void testSetAssignedPark() {
                System.out.println("setAssignedPark");
                Park park2 = new Park("3", 30.0, 20.0, 5, "parque testett2", 5, 5, 5, 5);
                bike1.setAssignedPark(park2);
                assertEquals(park2, bike1.getAssignedPark());
        }

        @Test
        public void testInvalidWeight() {
                System.out.println("testInvalidWeight");
                Bicycle bike = new Bicycle("BIKE001", -3, park1, 7, 1.1, 15);
                boolean expResult = false;
                boolean result = bike.validateBicycle();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidWeight0() {
                System.out.println("testInvalidWeight0");
                Bicycle bike = new Bicycle("BIKE001", 0, park1, 7, 1.1, 15);
                boolean expResult = false;
                boolean result = bike.validateBicycle();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidAerodynamicCoefficient() {
                System.out.println("testInvalidAerodynamicCoefficient");
                Bicycle bike = new Bicycle("BIKE001", 3, park1, -7, 1.1, 15);
                boolean expResult = false;
                boolean result = bike.validateBicycle();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidAerodynamicCoefficient0() {
                System.out.println("testInvalidAerodynamicCoefficient0");
                Bicycle bike = new Bicycle("BIKE001", 3, park1, 0, 1.1, 15);
                boolean expResult = false;
                boolean result = bike.validateBicycle();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidFrontalArea() {
                System.out.println("testInvalidFrontalArea");
                Bicycle bike = new Bicycle("BIKE001", 3, park1, 7, -1, 15);
                boolean expResult = false;
                boolean result = bike.validateBicycle();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidFrontalArea0() {
                System.out.println("testInvalidFrontalArea0");
                Bicycle bike = new Bicycle("BIKE001", 3, park1, 7, 0, 15);
                boolean expResult = false;
                boolean result = bike.validateBicycle();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidSize() {
                System.out.println("testInvalidSize");
                Bicycle bike = new Bicycle("BIKE001", 3, park1, 7, 1, -1);
                boolean expResult = false;
                boolean result = bike.validateBicycle();
                assertEquals(expResult, result);
        }

        @Test
        public void testInvalidSize0() {
                System.out.println("testInvalidSize0");
                Bicycle bike = new Bicycle("BIKE001", 3, park1, 7, 1, 0);
                boolean expResult = false;
                boolean result = bike.validateBicycle();
                assertEquals(expResult, result);
        }

        @Test
        public void testValidateBicycle() {
                System.out.println("testValidateBicycle");
                Bicycle bike = new Bicycle("BIKE001", 3, park1, 7, 1, 3);
                boolean expResult = true;
                boolean result = bike.validateBicycle();
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class Bicycle.
         */
        @Test
        public void testHashCodeDifferentObjects() {
                assertNotEquals(bike1.hashCode(), bike2.hashCode());
        }

        public void testHashCodeSameObjects() {
                Bicycle bike = new Bicycle("BIKE001", 15, park1, 7, 1.1, 15);
                assertNotEquals(bike.hashCode(), bike.hashCode());
                assertEquals(bike.hashCode(), bike.hashCode());
        }

        /**
         * Test of equals method, of class Bicycle.
         */
        @Test
        public void testEqualsSameObject() {
                System.out.println("testEqualsSameObject");
                assertEquals(bike1, bike1);
        }

        @Test
        public void testEqualsNotSameClass() {
                System.out.println("testEqualsNotSameClass");
                int anotherClass = 3;
                assertFalse(bike1.equals(anotherClass));
        }

        @Test
        public void testEqualsNull() {
                System.out.println("testEqualsNull");
                Bicycle bike = null;
                assertFalse(bike1.equals(bike));
        }

        @Test
        public void testEqualsNotID() {
                System.out.println("testEqualsNotID");
                assertNotEquals(bike1, bike2);
        }

}
