package lapr.project.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UserRegistryTest {
	
	UserRegistry oRegistry;
	
	public void setUp() {
		oRegistry = new UserRegistry(true);
	}

	/**
	 * Test of getUserByEmail method, of class UserRegistry.
	 */
	@Test
	public void testGetUserByEmail() {
		setUp();
		System.out.println("testGetUserByEmail");
		User user1 = new User("email1@gmail.com", "Francisco Magalhaes", "teste123", "0000000000000001", 'm', 182, 90, 13);
		String email = "email1@gmail.com";
		User result = oRegistry.getUserByEmail(email);
		assertEquals(user1, result);
	}
	
	@Test
	public void testGetUserNullByEmail() {
		setUp();
		System.out.println("testGetUserNullByEmail");
		String email = "notExistingUser@gmail.com";
		User result = oRegistry.getUserByEmail(email);
		assertNull(result);
	}

	/**
	 * Test of validateLogin method, of class UserRegistry.
	 */
	@Test
	public void testValidateLogin() {
		setUp();
		System.out.println("testValidateLogin");
		String email = "email1@gmail.com";
		String password = "12345678";
		boolean expResult = true;
		boolean result = oRegistry.validateLogin(email, password);
		assertEquals(expResult, result);
	}
	
	@Test
	public void testValidateInvalidLogin() {
		setUp();
		System.out.println("testValidateInvalidLogin");
		String email = "email1@gmail.com";
		String password = "invalidPassword";
		boolean expResult = false;
		boolean result = oRegistry.validateLogin(email, password);
		assertEquals(expResult, result);
	}

	/**
	 * Test of newUser method, of class UserRegistry.
	 */
	@Test
	public void testNewValidUser() {
		setUp();
		System.out.println("testNewValidUser");
		User user1 = new User("email1@gmail.com", "Francisco Magalhaes", "teste123", "0000000000000001", 'm', 182, 90, 13);
		User result = oRegistry.newUser("email1@gmail.com", "Francisco Magalhaes", "teste123", "0000000000000001", 'm', 182, 90, 13);
		assertEquals(user1, result);
	}
	
	@Test
	public void testNewInvalidUser() {
		setUp();
		System.out.println("testNewInvalidUser");
		User result = oRegistry.newUser("invalidEmailgmail.com", "Francisco Magalhaes", "teste123", "0000000000000001", 'm', 182, 90, 13);
		assertNull(result);
	}

	/**
	 * Test of registerUser method, of class UserRegistry.
	 */
	@Test
	public void testRegisterUser() {
		setUp();
		System.out.println("testRegisterUser");
		User oUser = new User("newEmail@gmail.com", "Francisco Magalhaes", "teste123", "0000000000000001", 'm', 182, 90, 13);
		boolean expResult = true;
		boolean result = oRegistry.registerUser(oUser);
		assertEquals(expResult, result);
	}
	
	@Test
	public void testRegisterInvalidUser() {
		setUp();
		System.out.println("testRegisterInvalidUser");
		User oUser = new User("email1@gmail.com", "Francisco Magalhaes", "teste123", "0000000000000001", 'm', 182, 90, 13);
		boolean expResult = false;
		boolean result = oRegistry.registerUser(oUser);
		assertEquals(expResult, result);
	}
	
	@Test
	public void testRegisterNullUser() {
		setUp();
		System.out.println("testRegisterNullUser");
		User oUser = null;
		boolean expResult = false;
		boolean result = oRegistry.registerUser(oUser);
		assertEquals(expResult, result);
	}
}
