package lapr.project.model;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PathRegistryTest {

	PathRegistry oRegistry;
	Path path1;

	public PathRegistryTest() {
		oRegistry = new PathRegistry(true);
		path1 = new Path(41.123456, 9.153152, 41.015153, 9.315251, 0.8, 220, 4.1);
	}

	/**
	 * Test of getPath method, of class PathRegistry.
	 */
	@Test
	public void testGetPathByPOIs() {
		System.out.println("testGetPathByPOIs");
		Path result = oRegistry.getPath(new PointOfInterest(41.123456, 9.153152), new PointOfInterest(41.015153, 9.315251));
		assertEquals(path1, result);
	}
	
	@Test
	public void testGetPathByCoordinates() {
		System.out.println("testGetPathByCoordinates");
		Path result = oRegistry.getPath(41.123456, 9.153152, 41.015153, 9.315251);
		assertEquals(path1, result);
	}
	
	/**
	 * Test of newPath method, of class PathRegistry.
	 */
	@Test
	public void testNewPath() {
		System.out.println("testNewPath");
		Path result = oRegistry.newPath(41.123456, 9.153152, 41.015153, 9.315251, 0.8, 220, 4.1);
		assertEquals(path1, result);
	}

	@Test
	public void testNewInvalidPath() {
		System.out.println("testNewPath");
		Path result = oRegistry.newPath(420.110212, -8.234267, 42.947574, -8.743839, 2, 90, 12);
		assertNull(result);
	}

	/**
	 * Test of addPath method, of class PathRegistry.
	 */
	@Test
	public void testAddPath() {
		System.out.println("testAddPath");
		boolean expResult = true;
		Path path2 = new Path(50.110212, -8.234267, 42.947574, -8.743839, 2, 90, 12);
		boolean result = oRegistry.addPath(path2);
		assertEquals(expResult, result);
	}

	@Test
	public void testAddInvalidPath() {
		System.out.println("testAddInvalidPath");
		boolean expResult = false;
		boolean result = oRegistry.addPath(path1);
		assertEquals(expResult, result);
	}

	@Test
	public void testAddNullPath() {
		System.out.println("testAddInvalidPath");
		boolean expResult = false;
		Path pathNull = null;
		boolean result = oRegistry.addPath(pathNull);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getAllPaths method, of class PathRegistry.
	 */
	@Test
	public void testGetAllPaths() {
		PathRegistry instance = new PathRegistry(true);
		System.out.println("getAllPaths");

		List<Path> expResult = new ArrayList<>();
		
                Path path2 = new Path(41.015153, 9.315251, 41.521512, 9.153120, 0.8, 50, 3.2);
                Path path3 = new Path(41.521512, 9.153120, 41.015153, 9.315251, 0.8, 230, 3.3);
                Path path4 = new Path(41.521512, 9.153120, 41.222552, 9.153210, 0.8, 20, 5.4);
                Path path5 = new Path(41.222552, 9.153210, 41.521512, 9.153120, 0.8, 200, 5.5);
                Path path6 = new Path(41.123456, 9.153152, 41.521512, 9.153120, 0.8, 100, 4.6);
                Path path7 = new Path(41.521512, 9.153120, 41.123456, 9.153152, 0.8, 80, 4.7);
                Path path8 = new Path(41.015153, 9.315251, 41.123456, 9.153152, 0.8, 40, 4.8);
                
		expResult.add(path1);
		expResult.add(path2);
		expResult.add(path3);
		expResult.add(path4);
		expResult.add(path5);
		expResult.add(path6);
		expResult.add(path7);
		expResult.add(path8);

		List<Path> result = instance.getAllPaths();
		assertEquals(expResult, result);
	}

}
