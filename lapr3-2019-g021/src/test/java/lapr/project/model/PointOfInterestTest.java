package lapr.project.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author pedro
 */
public class PointOfInterestTest {

    PointOfInterest test;           //Constructor
    PointOfInterest test2;          //Constructor without name
    PointOfInterest test3;          //Constructor without altitude
    PointOfInterest test4;          //Constructor withou name and altitude

    double validLatitude = 41.152712;
    double invalidLatitude = -200;
    double invalidLatitude2 = 200;

    double validLongitude = -8.609297;
    double invalidLongitude = -200;
    double invalidLongitude2 = 200;

    int validAltitude = 13;

    public void setUp() {
        test = new PointOfInterest(validLatitude, validLongitude, validAltitude, "Test POI");
        test2 = new PointOfInterest(validLatitude, validLongitude, validAltitude);
        test3 = new PointOfInterest(validLatitude, validLongitude, "Test POI");
        test4 = new PointOfInterest(validLatitude, validLongitude);
    }

    /**
     * Test of getLatitude method, of class Point.
     */
    @Test
    public void testGetLatitude() {
        setUp();
        System.out.println("testGetLatitude");
        double result = test.getLatitude();
        assertTrue(validLatitude == result);
    }

    /**
     * Test of getLongitude method, of class Point.
     */
    @Test
    public void testGetLongitude() {
        setUp();
        System.out.println("testGetLongitude");
        double result = test.getLongitude();
        assertTrue(validLongitude == result);
    }

    /**
     * Test of getAltitude method, of class Point.
     */
    @Test
    public void testGetAltitude() {
        setUp();
        System.out.println("testGetAltitude");
        double result = test.getAltitude();
        assertTrue(validAltitude == result);
    }

    /**
     * Test of getName method, of class Point.
     */
    @Test
    public void testGetName() {
        setUp();
        System.out.println("testGetName");
        String expResult = "Test POI";
        String result = test.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of validatePOI method, of class Point.
     */
    @Test
    public void testValidatePOI() {
        setUp();
        System.out.println("testValidatePOI");
        boolean expResult = true;
        boolean result = test.validatePOI();
        assertEquals(expResult, result);
    }

    @Test
    public void testInvalidLatitude() {
        test = new PointOfInterest(invalidLatitude, validLongitude);
        System.out.println("testInvalidLatitude");
        boolean expResult = false;
        boolean result = test.validatePOI();
        assertEquals(expResult, result);
    }

    @Test
    public void testInvalidLatitude2() {
        test = new PointOfInterest(invalidLatitude2, validLongitude);
        System.out.println("testInvalidLatitude2");
        boolean expResult = false;
        boolean result = test.validatePOI();
        assertEquals(expResult, result);
    }

    @Test
    public void testInvalidLongitude() {
        test = new PointOfInterest(validLatitude, invalidLongitude);
        System.out.println("testInvalidLongitude");
        boolean expResult = false;
        boolean result = test.validatePOI();
        assertEquals(expResult, result);
    }

    @Test
    public void testInvalidLongitude2() {
        test = new PointOfInterest(validLatitude, invalidLongitude2);
        System.out.println("testInvalidLongitude2");
        boolean expResult = false;
        boolean result = test.validatePOI();
        assertEquals(expResult, result);
    }

    @Test
    public void testHashCodeDifferentObjects() {
        setUp();
        System.out.println("testHashCodeDifferentObjects");
        PointOfInterest otherObj = new PointOfInterest(validLatitude - 0.1, validLongitude - 0.1, validAltitude, "Test POI");
        assertNotEquals(test.hashCode(), otherObj.hashCode());
    }

    @Test
    public void testHashCodeSameObjects() {
        setUp();
        System.out.println("testHashCodeSameObjects");
        PointOfInterest otherObj = new PointOfInterest(validLatitude, validLongitude, validAltitude, "Test POI");
        assertEquals(test.hashCode(), otherObj.hashCode());
    }

    /**
     * Test of equals method, of class Points Of Interest.
     */
    @Test
    public void testEqualsSameObject() {
        setUp();
        System.out.println("testEqualsSameObject");
        assertEquals(test, test);
    }

    @Test
    public void testEqualsNotSameClass() {
        setUp();
        System.out.println("testEqualsNotSameClass");
        int anotherClass = 3;
        assertFalse(test.equals(anotherClass));
    }

    @Test
    public void testEqualsNull() {
        setUp();
        System.out.println("testEqualsNull");
        PointOfInterest otherObj = null;
        assertFalse(test.equals(otherObj));
    }

    @Test
    public void testEqualsDifferentCoordenates() {
        setUp();
        System.out.println("testEqualsDifferentCoordenates");
        PointOfInterest otherObj = new PointOfInterest(validLatitude - 0.1, validLongitude, validAltitude, "Test POI");
        assertNotEquals(test, otherObj);
        otherObj = new PointOfInterest(validLatitude, validLongitude - 0.1, validAltitude, "Test POI");
        assertNotEquals(test, otherObj);
        otherObj = new PointOfInterest(validLatitude - 0.1, validLongitude - 0.1, validAltitude, "Test POI");
        assertNotEquals(test, otherObj);
    }

    /**
     * Test of calculateDistance method, of class PointOfInterest.
     */
    @Test
    public void testCalculateDistance() {
        System.out.println("calculateDistance");

        setUp();
        PointOfInterest point = test;
        PointOfInterest instance = new PointOfInterest(50, -15);
        double expResult = 1101.3883029304568;
        double result = instance.calculateDistance(point);
        assertEquals(expResult, result, 0.0);

        instance = test;
        expResult = 0;
        result = instance.calculateDistance(point);
        assertEquals(expResult, result, 0.0);

    }
}
