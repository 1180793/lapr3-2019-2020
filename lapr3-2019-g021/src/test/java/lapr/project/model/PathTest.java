package lapr.project.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PathTest {

	double latitudeA;

	double longitudeA;

	double latitudeB;

	double longitudeB;

	double kineticCoefficient;

	double windDirection;

	double windSpeed;

	public void setUp() {
		latitudeA = 50.50;
		longitudeA = 150.30;
		latitudeB = 60;
		longitudeB = 160;
		kineticCoefficient = 0.14;
		windDirection = 90;
		windSpeed = 16000;
	}

	/**
	 * Test of getLatitudeA method, of class Path.
	 */
	@Test
	public void testGetLatitudeA() {
		setUp();
		System.out.println("getLatitudeA");
		Path instance = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		double expResult = latitudeA;
		double result = instance.getLatitudeA();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getLongitudeA method, of class Path.
	 */
	@Test
	public void testGetLongitudeA() {
		setUp();
		System.out.println("getLongitudeA");
		Path instance = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		double expResult = longitudeA;
		double result = instance.getLongitudeA();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getLatitudeB method, of class Path.
	 */
	@Test
	public void testGetLatitudeB() {
		setUp();
		System.out.println("getLatitudeB");
		Path instance = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		double expResult = latitudeB;
		double result = instance.getLatitudeB();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getLongitudeB method, of class Path.
	 */
	@Test
	public void testGetLongitudeB() {
		setUp();
		System.out.println("getLongitudeB");
		Path instance = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		double expResult = longitudeB;
		double result = instance.getLongitudeB();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getKineticCoefficient method, of class Path.
	 */
	@Test
	public void testGetKineticCoefficient() {
		setUp();
		System.out.println("getKineticCoefficient");
		Path instance = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		double expResult = kineticCoefficient;
		double result = instance.getKineticCoefficient();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getWindDirection method, of class Path.
	 */
	@Test
	public void testGetWindDirection() {
		setUp();
		System.out.println("getWindDirection");
		Path instance = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		double expResult = windDirection;
		double result = instance.getWindDirection();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getWindSpeed method, of class Path.
	 */
	@Test
	public void testGetWindSpeed() {
		setUp();
		System.out.println("getWindSpeed");
		Path instance = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		double expResult = windSpeed;
		double result = instance.getWindSpeed();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of validatePath method, of class Path.
	 */
	@Test
	public void testValidatePathInvalidLatitudeA() {
		setUp();
		System.out.println("validatePathInvalidLatitudeA");
		Path path = new Path(900, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertFalse(path.validatePath());
		path = new Path(-900, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertFalse(path.validatePath());
	}
	
	@Test
	public void testValidatePathInvalidLatitudeB() {
		setUp();
		System.out.println("validatePathInvalidLatitudeB");
		Path path = new Path(latitudeA, longitudeA, 900, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertFalse(path.validatePath());
		path = new Path(latitudeA, longitudeA, -900, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertFalse(path.validatePath());
	}
	
	@Test
	public void testValidatePathInvalidLongitudeA() {
		setUp();
		System.out.println("validatePathInvalidLongitudeA");
		Path path = new Path(latitudeA, 1800, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertFalse(path.validatePath());
		path = new Path(latitudeA, -1800, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertFalse(path.validatePath());
	}
	
	@Test
	public void testValidatePathInvalidLongitudeB() {
		setUp();
		System.out.println("validatePathInvalidLongitudeB");
		Path path = new Path(latitudeA, longitudeA, latitudeB, 1800, kineticCoefficient, windDirection, windSpeed);
		assertFalse(path.validatePath());
		path = new Path(latitudeA, longitudeA, latitudeB, -1800, kineticCoefficient, windDirection, windSpeed);
		assertFalse(path.validatePath());
	}
	
	@Test
	public void testValidatePath() {
		setUp();
		System.out.println("validatePath");
		Path path = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertTrue(path.validatePath());
	}

	/**
	 * Test of hashCode method, of class Path.
	 */
	@Test
	public void testHashCodeDifferentObjects() {
		setUp();
		Path path = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		Path anotherPath = new Path(latitudeA + 1, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertNotEquals(path.hashCode(), anotherPath.hashCode());
	}

	public void testHashCodeSameObjects() {
		setUp();
		Path path = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		Path anotherPath = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertNotEquals(path.hashCode(), anotherPath.hashCode());
		assertEquals(path.hashCode(), anotherPath.hashCode());
	}

	/**
	 * Test of equals method, of class Path.
	 */
	@Test
	public void testEqualsSameObject() {
		setUp();
		System.out.println("testEqualsSameObject");
		Path path = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertEquals(path, path);

	}

	@Test
	public void testEqualsNotSameClass() {
		setUp();
		System.out.println("testEqualsNotSameClass");
		Path path = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		int anotherClass = 3;
		assertFalse(path.equals(anotherClass));
	}

	@Test
	public void testEqualsNull() {
		setUp();
		System.out.println("testEqualsNull");
		Path path = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		Path anotherPath = null;
		assertFalse(path.equals(anotherPath));
	}

	@Test
	public void testNotEquals() {
		setUp();
		System.out.println("testNotEquals");
		Path path = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		Path anotherPath = new Path(latitudeA + 1, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertNotEquals(path, anotherPath);
		anotherPath = new Path(latitudeA, longitudeA + 1, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertNotEquals(path, anotherPath);
		anotherPath = new Path(latitudeA, longitudeA, latitudeB + 1, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertNotEquals(path, anotherPath);
		anotherPath = new Path(latitudeA, longitudeA, latitudeB, longitudeB + 1, kineticCoefficient, windDirection, windSpeed);
		assertNotEquals(path, anotherPath);
		anotherPath = new Path(latitudeA, longitudeA, latitudeB, longitudeB, kineticCoefficient, windDirection, windSpeed);
		assertEquals(path, anotherPath);
	}

}
