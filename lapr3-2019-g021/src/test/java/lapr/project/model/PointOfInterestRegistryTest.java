package lapr.project.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PointOfInterestRegistryTest {

        PointOfInterestRegistry oRegistry;
        PointOfInterest test;
        PointOfInterest test2;

        double validLatitude = 41.152712;
        double invalidLatitude = -900;
        double validLongitude = -8.609297;
        double invalidLongitude = -900;

        public void setUp() {
                oRegistry = new PointOfInterestRegistry(true);
                test = new PointOfInterest(validLatitude, validLongitude, 13, "Point 1");
                test2 = new PointOfInterest(validLatitude + 0.2, validLongitude + 0.2);
        }

        /**
         * Test of getPOI method, of class PointOfInterestRegistry.
         */
        @Test
        public void testGetPOI() {
                setUp();
                System.out.println("testGetPOI");
                PointOfInterest result = oRegistry.getPOI(validLatitude, validLongitude);
                assertEquals(test, result);
        }

        @Test
        public void testGetPOINull() {
                setUp();
                System.out.println("testGetPOINull");
                PointOfInterest result = oRegistry.getPOI(0, 0);
                assertNull(result);
        }

        /**
         * Test of newUser method, of class PointOfInterestRegistry.
         */
        @Test
        public void testNewPOI() {
                setUp();
                System.out.println("testNewPOI");
                PointOfInterest result = oRegistry.newPOI(validLatitude, validLongitude, 13, "Point 1");
                assertEquals(test, result);
        }

        @Test
        public void testNewInvalidPOI() {
                setUp();
                System.out.println("testNewInvalidPOI");
                PointOfInterest result = oRegistry.newPOI(invalidLatitude, invalidLongitude, 13, "Point 1");
                assertNull(result);
        }

        /**
         * Test of addPOI method, of class PointOfInterestRegistry.
         */
        @Test
        public void testAddPOI() {
                setUp();
                System.out.println("testAddPOI");
                boolean expResult = true;
                boolean result = oRegistry.addPOI(test2);
                assertEquals(expResult, result);
        }

        @Test
        public void testAddInvalidPOI() {
                setUp();
                System.out.println("testAddInvalidPOI");
                boolean expResult = false;
                boolean result = oRegistry.addPOI(test);
                assertEquals(expResult, result);
        }

        @Test
        public void testAddNullPOI() {
                setUp();
                System.out.println("testAddNullPOI");
                PointOfInterest oPoi = null;
                boolean expResult = false;
                boolean result = oRegistry.addPOI(oPoi);
                assertEquals(expResult, result);
        }
}
