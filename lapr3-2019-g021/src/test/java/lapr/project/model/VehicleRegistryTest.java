package lapr.project.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.data.mock.BicycleMock;
import lapr.project.data.mock.ScooterMock;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author roger40
 */
public class VehicleRegistryTest {

        VehicleRegistry ver;
        VehicleRegistry ver2;

        public VehicleRegistryTest() {
                ver = new VehicleRegistry(true);
                ver2 = new VehicleRegistry(true);
        }

        /**
         * Test of getAllBicycles method, of class VehicleRegistry.
         */
        @Test
        public void testGetAllBicycles() {
                List<Bicycle> listBikesTest = ver.getAllBicycles();
                assertTrue(listBikesTest.size() > 0);
        }

        /**
         * Test of getAllScooters method, of class VehicleRegistry.
         */
        @Test
        public void testGetAllScooters() {
                List<Scooter> listScooterTest = ver.getAllScooters();
                assertTrue(listScooterTest.size() > 0);
        }

        /**
         * Test of getBicycle method, of class VehicleRegistry.
         *
         */
        @Test
        public void testGetBicycle() {
                System.out.println("getBicycle");
                String id = "BIKE001";
                assertEquals(ver.getBicycle(id).getID(), id);
                id = "BIKE002";
                assertEquals(ver.getBicycle(id).getID(), id);
                id = "SCT3123";
                assertNull(ver.getBicycle(id));
        }

        /**
         * Test of getScooter method, of class VehicleRegistry.
         *
         */
        @Test
        public void testGetScooter() {
                System.out.println("getScooter");
                String id = "SCTR001";
                assertEquals(ver.getScooter(id).getID(), id);
                id = "SCTR001";
                assertEquals(ver.getScooter(id).getID(), id);
                id = "SCT3123";
                assertNull(ver.getScooter(id));
        }

        /**
         * Test of newBicycle method, of class VehicleRegistry.
         */
        @Test
        public void testNewValidBicycle() {
                System.out.println("newValidBicycle");
                Park park = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
                Bicycle bike = new Bicycle("BIKE001", 15, park, 7, 1.1, 15);
                Bicycle bikeTest = ver.newBicycle("BIKE001", 15, 2, 3, 7, 1.1, 15);
                assertEquals(bike, bikeTest);
        }

        @Test
        public void testNewInvalidBicycle() {
                System.out.println("newInvalidBicycle");
                Bicycle bikeTest = ver.newBicycle("BIKE001", -15, 89, -89, 7, 1.1, 15);
                assertNull(bikeTest);
        }

        /**
         * Test of newScooter method, of class VehicleRegistry.
         */
        @Test
        public void testNewValidScooter() {
                System.out.println("newValidScooter");
                Park park = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
                Scooter scooter = new Scooter("SCTR001", 20, 1, park, 200.0, 75, 1.10, 0.3, 250);
                Scooter scooterTest = ver.newScooter("SCTR001", 20, 1, 2, 3, 200.0, 75, 1.10, 0.3, 250);
                assertEquals(scooter, scooterTest);
        }

        @Test
        public void testNewInvalidScooter() {
                System.out.println("newInvalidScooter");
                Scooter scooterTest = ver.newScooter("SCTR001", -20, 1, 89, -89, 200.0, 75, 1.10, 0.3, 250);
                assertNull(scooterTest);
        }

        /**
         * Test of addBicycle method, of class VehicleRegistry.
         */
        @Test
        public void testAddBicycle() {
                System.out.println("addBicycle");
                boolean expResult = true;
                Park park = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
                Bicycle bike = new Bicycle("BIKE099", 15, park, 7, 1.1, 15);
                boolean result = ver.addBicycle(bike);
                assertEquals(expResult, result);
        }

        @Test
        public void testAddInvalidBicycle() {
                System.out.println("addInvalidBicycle");
                boolean expResult = false;
                Park park = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
                Bicycle bike = new Bicycle("BIKE001", 15, park, 7, 1.1, 15);
                boolean result = ver.addBicycle(bike);
                assertEquals(expResult, result);
        }

        @Test
        public void testAddNullBicycle() {
                System.out.println("addNullBicycle");
                Bicycle bike = null;
                boolean expResult = false;
                boolean result = ver.addBicycle(bike);
                assertEquals(expResult, result);
        }

        /**
         * Test of addScooter method, of class VehicleRegistry.
         */
        @Test
        public void testAddScooter() {
                System.out.println("addScooter");
                boolean expResult = true;
                Park park = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
                Scooter scooter = new Scooter("SCTR099", 20, 1, park, 200.0, 75, 1, 0.5, 250);
                boolean result = ver.addScooter(scooter);
                assertEquals(expResult, result);
        }

        @Test
        public void testAddInvalidScooter() {
                System.out.println("addInvalidScooter");
                boolean expResult = false;
                Park park = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
                Scooter scooter = new Scooter("SCTR002", 20, 1, park, 200.0, 75, 1, 0.5, 250);
                boolean result = ver.addScooter(scooter);
                assertEquals(expResult, result);
        }

        @Test
        public void testAddNullScooter() {
                System.out.println("addNullScooter");
                Scooter scooter = null;
                boolean expResult = false;
                boolean result = ver.addScooter(scooter);
                assertEquals(expResult, result);
        }

        @Test
        public void testRemoveBicycle() throws Exception {
                boolean testRemove = ver.removeBicycle("BIKE001");
                assertEquals(testRemove, true);
                testRemove = ver.removeBicycle("BIKE099");
                assertEquals(testRemove, false);
        }

        @Test
        public void testRemoveScooter() throws Exception {
                boolean testRemove = ver.removeScooter("SCTR002");
                assertEquals(testRemove, true);
                testRemove = ver.removeScooter("SCTR099");
                assertEquals(testRemove, false);
        }
        
          @Test
    public void testSugestVehiclesBicycles() {
        System.out.println("sugestVehiclesBicycles");
       
     Park parkOrigin = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
     assertEquals(ver.getAvailableBicycles(parkOrigin.getID()).size(),3);
      
     
        
    }
    
    @Test
    public void testUnlockBicycle() {
        System.out.println("testUnlockBicycle");
       List <Bicycle> listbikes = new ArrayList();
       BicycleMock bikemock = new BicycleMock();
       listbikes = bikemock.getAllBicycles();
       
       
        assertEquals(listbikes.get(0).getAssignedUser(),null); //para ja nao ha nenhum user atr4ibuido a primeira bicicleta da lista da mock
        
        Park parkOrigin = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
         User user1 = new User("elegible@gmail.com", "roger", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);
          User user2 = new User("roger1@gmail.com", "roger1", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);
          User user3 = new User("roger2@gmail.com", "roger2", "12345", "1111111111111111", 'm', 188.7, 90.0, 0.25);
       
     boolean test1=  ver.unlockBicycle(bikemock.getAvailableBicycles("1").get(0).getID(),  "email5@gmail.com" ); 
        assertEquals(test1,true);
        
       
       boolean test2=  ver.unlockBicycle(bikemock.getAvailableBicycles("1").get(2).getID(), user2.getEmail() ); //o user com esse email ja tem bike atribuido a ele
        assertEquals(test2,false);
        
        
           
       boolean test3=  ver.unlockBicycle(bikemock.getAvailableBicycles("1").get(0).getID(), user3.getEmail() ); //o user com esse email ja tem scooter atribuido a ele
        assertEquals(test2,false);
        
        
         
        
        
        
        
    }
    
//       @Test
//    public void testSugestScooterBicycles() {
//        System.out.println("sugestVehiclesScooters");
//       
//     Park parkOrigin = new Park("1", 2, 3, 4, "Póvoa", 1, 2, 3, 4);
//     assertEquals(ver.getSuggestedScooters(parkOrigin,null).size(),1);
//      
//     
//        
//    }
    
    
     @Test
    public void testUnlockScooter() {
        System.out.println("testUnlockScooter");
      
       ScooterMock scotmock = new ScooterMock();
      
       
       
        
        
       
       
          
         
     boolean test1=  ver.unlockAnyScooterAtPark("1", "email6@gmail.com" ); 
        assertEquals(test1,true);
        
       
        boolean test2=  ver.unlockAnyScooterAtPark("1", "roger1@gmail.com" );   //user ja tem veiculo
        assertEquals(test2,false);
        
        
           
    
        
         
        
        
        
        
    }
    
    @Test
    public void testReportUnlockedScooter() {
       int count = 0;
      for ( String s: ver.GetCurrentUnlockedScootersReport().values()){
              assertEquals(s,"SCTR001");
             count ++;
          
      }
      count = 0;
        for ( String s: ver.GetCurrentUnlockedScootersReport().keySet()){
              assertEquals(s,"roger2@gmail.com");
             count ++;
          
      }
    }
    
     @Test
    public void testReportUnlockedBikes() {
       int count = 0;
      for ( String s: ver.GetCurrentUnlockedBicyclesReport().values()){
              assertEquals(s,"BIKE006");
             count ++;
          
      }
      count = 0;
        for ( String s: ver.GetCurrentUnlockedBicyclesReport().keySet()){
              assertEquals(s,"roger1@gmail.com");
             count ++;
          
      }
    }
    
    
    
     @Test
    public void testLockBicycle() {
      assertEquals(ver.lockBicycle("roger1@gmail.com", 3, 4), true); //USER tem bicicleta e Park existe
      assertEquals(ver.lockBicycle("rogerXX@gmail.com", 3, 4), false); //User nao tem bicicleta
      assertEquals(ver.lockBicycle("roger1@gmail.com", 3, 400), false); //USER tem bicicleta Mas  Park Nao existe
       assertEquals(ver2.lockBicycle("roger1@gmail.com", 100, 300), false); //USER tem bicicleta MAS  Park Nao tem capacidade para mais bikes

}
}