DELETE FROM invoices;
DELETE FROM trips;
DELETE FROM bicycles;
DELETE FROM scooters;
DELETE FROM parks;
DELETE FROM users;
DELETE FROM paths;
DELETE FROM pointsofinterest;

SELECT * FROM invoices;
SELECT * FROM trips;
SELECT * FROM bicycles;
SELECT * FROM scooters;
SELECT * FROM parks;
SELECT * FROM users;
SELECT * FROM paths;
SELECT * FROM pointsofinterest;