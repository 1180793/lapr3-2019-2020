INSERT INTO trips (
    trip_number,
    vehicleid,
    typevehicle,
    dateunlocked,
    parkoriginid,
    assigneduser,
    parkdestinyid,
    datelocked,
    price,
    invoice_id
) VALUES (
    20,
    'BIKE001',
    'b',
    ( to_timestamp('2020-01-03 10:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    'Trindade',
    'email1@gmail.com',
    'Castelo do Queijo',
    ( to_timestamp('2020-01-03 18:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    14.3,
    0
);

INSERT INTO trips (
    trip_number,
    vehicleid,
    typevehicle,
    dateunlocked,
    parkoriginid,
    assigneduser,
    parkdestinyid,
    datelocked,
    price,
    invoice_id
) VALUES (
    21,
    'BIKE001',
    'b',
    ( to_timestamp('2020-01-04 10:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    'Trindade',
    'email1@gmail.com',
    'Castelo do Queijo',
    ( to_timestamp('2020-01-04 14:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    9.5,
    0
);

INSERT INTO trips (
    trip_number,
    vehicleid,
    typevehicle,
    dateunlocked,
    parkoriginid,
    assigneduser,
    parkdestinyid,
    datelocked,
    price,
    invoice_id
) VALUES (
    22,
    'BIKE001',
    'b',
    ( to_timestamp('2020-01-06 10:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    'Trindade',
    'email1@gmail.com',
    'Castelo do Queijo',
    ( to_timestamp('2020-01-06 14:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    1.5,
    0
);

INSERT INTO trips (
    trip_number,
    vehicleid,
    typevehicle,
    dateunlocked,
    parkoriginid,
    assigneduser,
    parkdestinyid,
    datelocked,
    price,
    invoice_id
) VALUES (
    23,
    'SCOOTER001',
    's',
    ( to_timestamp('2020-01-05 11:00:00', 'YYYY-MM-DD HH24:MI:SS') ),
    'Trindade',
    'email1@gmail.com',
    'Castelo do Queijo',
    ( to_timestamp('2020-01-05 14:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    20.3,
    0
);

INSERT INTO trips (
    trip_number,
    vehicleid,
    typevehicle,
    dateunlocked,
    parkoriginid,
    assigneduser,
    parkdestinyid,
    datelocked,
    price,
    invoice_id
) VALUES (
    24,
    'SCOOTER001',
    's',
    ( to_timestamp('2020-01-08 11:00:00', 'YYYY-MM-DD HH24:MI:SS') ),
    'Trindade',
    'email1@gmail.com',
    'Castelo do Queijo',
    ( to_timestamp('2020-01-08 14:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    20.3,
    1
);