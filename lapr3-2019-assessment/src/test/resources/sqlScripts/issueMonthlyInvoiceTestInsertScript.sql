INSERT INTO trips (
    trip_number,
    vehicleid,
    typevehicle,
    dateunlocked,
    parkoriginid,
    assigneduser,
    parkdestinyid,
    datelocked,
    price,
    invoice_id
) VALUES (
    40,
    'BIKE001',
    'b',
    ( to_timestamp('2020-01-12 10:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    'Trindade',
    'email1@gmail.com',
    'Castelo do Queijo',
    ( to_timestamp('2020-01-12 18:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    14.3,
    0
);

INSERT INTO trips (
    trip_number,
    vehicleid,
    typevehicle,
    dateunlocked,
    parkoriginid,
    assigneduser,
    parkdestinyid,
    datelocked,
    price,
    invoice_id
) VALUES (
    41,
    'BIKE001',
    'b',
    ( to_timestamp('2020-01-13 10:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    'Trindade',
    'email1@gmail.com',
    'Castelo do Queijo',
    ( to_timestamp('2020-01-13 14:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    9.5,
    0
);

INSERT INTO trips (
    trip_number,
    vehicleid,
    typevehicle,
    dateunlocked,
    parkoriginid,
    assigneduser,
    parkdestinyid,
    datelocked,
    price,
    invoice_id
) VALUES (
    42,
    'BIKE001',
    'b',
    ( to_timestamp('2020-01-14 10:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    'Trindade',
    'email1@gmail.com',
    'Castelo do Queijo',
    ( to_timestamp('2020-01-14 14:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    1.5,
    0
);

INSERT INTO trips (
    trip_number,
    vehicleid,
    typevehicle,
    dateunlocked,
    parkoriginid,
    assigneduser,
    parkdestinyid,
    datelocked,
    price,
    invoice_id
) VALUES (
    43,
    'SCOOTER001',
    's',
    ( to_timestamp('2020-01-15 11:00:00', 'YYYY-MM-DD HH24:MI:SS') ),
    'Trindade',
    'email1@gmail.com',
    'Castelo do Queijo',
    ( to_timestamp('2020-01-15 14:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    20.3,
    0
);

INSERT INTO trips (
    trip_number,
    vehicleid,
    typevehicle,
    dateunlocked,
    parkoriginid,
    assigneduser,
    parkdestinyid,
    datelocked,
    price,
    invoice_id
) VALUES (
    44,
    'SCOOTER001',
    's',
    ( to_timestamp('2020-01-16 11:00:00', 'YYYY-MM-DD HH24:MI:SS') ),
    'Trindade',
    'email1@gmail.com',
    'Castelo do Queijo',
    ( to_timestamp('2020-01-16 14:30:00', 'YYYY-MM-DD HH24:MI:SS') ),
    20.3,
    0
);

UPDATE users u SET u.points = 55 WHERE u.email = 'email1@gmail.com';