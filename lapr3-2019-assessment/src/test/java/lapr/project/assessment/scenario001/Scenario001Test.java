package lapr.project.assessment.scenario001;

import lapr.project.assessment.ProjectUtils;
import lapr.project.assessment.Serviceable;
import lapr.project.assessment.TargetProject;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import junitx.framework.FileAssert;
import lapr.project.assessment.DataHandler;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)
public class Scenario001Test {

	private String getSeparator() {
		return File.separator;
	}

	private String getScenarioID() {
		return this.getClass().getSimpleName().substring(8, 11);
	}

	private final Serviceable facade = TargetProject.getServiceableObject();

	private final DataHandler dh = new DataHandler();

	private final String scriptFolder
		= ProjectUtils.getApplicationFolder()
		+ File.separator + "target" + File.separator + "test"
		+ "-classes" + File.separator + "sqlScripts"
		+ File.separator;

	private String inputFileName
		= ProjectUtils.getApplicationFolder()
		+ File.separator + "target" + File.separator + "test"
		+ "-classes" + File.separator + "fileTemplates"
		+ File.separator;

	private String resultFileName
		= ProjectUtils.getApplicationFolder()
		+ File.separator + "target" + File.separator + "test"
		+ "-classes" + getSeparator() + "fileTemplates"
		+ File.separator + "output" + File.separator;

	@Test
	@Order(7)
	@DisplayName("Test addScooters")
	public void testAddScooters() throws IOException, SQLException {

		/* Test Add Scooters */
		facade.getParkChargingReport("Trindade", resultFileName + "parkChargingReport.csv");

	}
	
//	@Test
//	@Order(1)
//	@DisplayName("Clear SQL Tables")
//	public void clearSQLTables() throws IOException, SQLException {
//
//		/* Clears Database Tables */
//		dh.scriptRunner(scriptFolder + "clearTables.sql");
//		System.out.println("INFO: SQL Tables Cleared");
//
//	}
//
//	@Test
//	@Order(2)
//	@DisplayName("Test addUsers")
//	public void testAddUsers() throws IOException, SQLException {
//
//		/* Test Add Users */
//		int nUsers = facade.addUsers(inputFileName + "users.csv");
//		System.out.println("There were added " + nUsers + " users to the App.");
//		assertEquals(6, nUsers);
//
//	}
//
//	@Test
//	@Order(3)
//	@DisplayName("Test addParks")
//	public void testAddParks() throws IOException, SQLException {
//
//		/* Test Add Parks */
//		int nParks = facade.addParks(inputFileName + "parks.csv");
//		System.out.println("There were added " + nParks + " Parks to the App.");
//		assertEquals(3, nParks);
//
//	}
//
//	@Test
//	@Order(4)
//	@DisplayName("Test addPOIs")
//	public void testAddPOIs() throws IOException, SQLException {
//
//		/* Test Add POIs */
//		int nPOIs = facade.addPOIs(inputFileName + "pois.csv");
//		System.out.println("There were added " + nPOIs + " POIs to the App.");
//		assertEquals(5, nPOIs);
//
//	}
//
//	@Test
//	@Order(5)
//	@DisplayName("Test addPaths")
//	public void testAddPaths() throws IOException, SQLException {
//
//		/* Test Add Paths */
//		int nPaths = facade.addPaths(inputFileName + "paths.csv");
//		System.out.println("There were added " + nPaths + " Paths to the App.");
//		assertEquals(7, nPaths);
//
//	}
//
//	@Test
//	@Order(6)
//	@DisplayName("Test addBicycles")
//	public void testAddBicycles() throws IOException, SQLException {
//
//		/* Test Add Bicycles */
//		int nBicycles = facade.addBicycles(inputFileName + "bicycles.csv");
//		System.out.println("There were added " + nBicycles + " Bicycles to the App.");
//		assertEquals(2, nBicycles);
//
//	}
//
//	@Test
//	@Order(7)
//	@DisplayName("Test addScooters")
//	public void testAddScooters() throws IOException, SQLException {
//
//		/* Test Add Scooters */
//		int nScooters = facade.addEscooters(inputFileName + "escooters.csv");
//		System.out.println("There were added " + nScooters + " Scooters to the App.");
//		assertEquals(6, nScooters);
//
//	}
//
//	@Test
//	@Order(8)
//	@DisplayName("Test linearDistanceTo")
//	public void testLinearDistanceTo() throws IOException, SQLException {
//
//		/* Test Linear Distance To */
//		// Clérigos(41.14582, -8.61398) - Majestic(41.14723, -8.60657): 640m
//		int linearDistanceTo = facade.linearDistanceTo(41.14582, -8.61398, 41.14723, -8.60657);
//		assertEquals(640, linearDistanceTo);
//		// Clérigos(41.14582, -8.61398) - Bolhão(41.14871, -8.60746): 634m
//		linearDistanceTo = facade.linearDistanceTo(41.14582, -8.61398, 41.14871, -8.60746);
//		assertEquals(634, linearDistanceTo);
//		// Bolhão(41.14871, -8.60746) - Cais da Ribeira(41.14063, -8.61118): 951m
//		linearDistanceTo = facade.linearDistanceTo(41.14871, -8.60746, 41.14063, -8.61118);
//		assertEquals(951, linearDistanceTo);
//		// Majestic(41.14723, -8.60657) - Cais da Ribeira(41.14063, -8.61118): 829m
//		linearDistanceTo = facade.linearDistanceTo(41.14723, -8.60657, 41.14063, -8.61118);
//		assertEquals(829, linearDistanceTo);
//		// Clérigos(41.14582, -8.61398) - Majestic(41.14723, -8.60657) - Cais da Ribeira(41.14063, -8.61118): 1469m
//		linearDistanceTo = facade.linearDistanceTo(41.14582, -8.61398, 41.14723, -8.60657) + facade.linearDistanceTo(41.14723, -8.60657, 41.14063, -8.61118);
//		assertEquals(1469, linearDistanceTo);
//		// Clérigos(41.14582, -8.61398) - Bolhão(41.14871, -8.60746) - Cais da Ribeira(41.14063, -8.61118): 1585m
//		linearDistanceTo = facade.linearDistanceTo(41.14582, -8.61398, 41.14871, -8.60746) + facade.linearDistanceTo(41.14871, -8.60746, 41.14063, -8.61118);
//		assertEquals(1585, linearDistanceTo);
//
//	}
//
//	@Test
//	@Order(9)
//	@DisplayName("Test registerUser")
//	public void testRegisterUser() {
//
//		/* Test Register User */
//		// Fails Local Validation
//		int result = facade.registerUser("ana_santos07", "invalidEmail.com@gmail@outlook.com", "teste123", "0000000000000007", 160, 60, BigDecimal.valueOf(4.17), "female");
//		assertEquals(0, result);
//		// Failed Global Validation
//		result = facade.registerUser("ana_santos07", "email1@gmail.com", "teste123", "0000000000000007", 160, 60, BigDecimal.valueOf(4.17), "female");
//		assertEquals(0, result);
//		// Sucess: User Registered
//		result = facade.registerUser("ana_santos07", "email7@gmail.com", "teste123", "0000000000000007", 160, 60, BigDecimal.valueOf(4.17), "female");
//		assertEquals(1, result);
//
//	}
//
//	@Test
//	@Order(10)
//	@DisplayName("Test getUserCurrentDebt")
//	public void testGetUserCurrentDebt() throws IOException, SQLException {
//
//		/* Test User Debt */
//		// Adds data for the User Debt Test
//		dh.scriptRunner(scriptFolder + "userDebtTestInsertScript.sql");
//		double userDebtValue = facade.getUserCurrentDebt("f_magalhaes01", resultFileName + "userDebt.csv");
//		assertEquals(45.60, userDebtValue);
//		File expectedUserDebt = new File(resultFileName + "expectedUserDebt.csv");
//		File userDebt = new File(resultFileName + "userDebt.csv");
//		FileAssert.assertBinaryEquals(expectedUserDebt, userDebt);
//		// Removes the added data for the User Debt Test
//		dh.scriptRunner(scriptFolder + "userDebtTestRemoveScript.sql");
//
//	}
//
//	@Test
//	@Order(11)
//	@DisplayName("Test getUserCurrentPoints")
//	public void testGetUserCurrentPoints() throws IOException, SQLException {
//
//		/* Test User Points */
//		// Adds data for the User Points Test
//		dh.scriptRunner(scriptFolder + "userPointsTestInsertScript.sql");
//		double userPointsValue = facade.getUserCurrentPoints("f_magalhaes01", resultFileName + "userPoints.csv");
//		assertEquals(55, userPointsValue);
//		File expectedUserPoints = new File(resultFileName + "expectedUserPoints.csv");
//		File userPoints = new File(resultFileName + "userPoints.csv");
//		FileAssert.assertBinaryEquals(expectedUserPoints, userPoints);
//		// Removes the added data for the User Points Test
//		dh.scriptRunner(scriptFolder + "userPointsTestRemoveScript.sql");
//
//	}
//
//	@Test
//	@Order(12)
//	@DisplayName("Test issueMonthlyInvoice")
//	public void testIssueMonthlyInvoice() throws IOException, SQLException {
//
//		/* Test Issue Monthly Invoice */
//		// Adds data for the Issue Monthly Invoice
//		dh.scriptRunner(scriptFolder + "issueMonthlyInvoiceTestInsertScript.sql");
//		System.out.println("Added Trips to the Database to Test Invoices");
//		double invoicePrice = facade.getInvoiceForMonth(1, "f_magalhaes01", resultFileName + "invoice.csv");
//		System.out.println("Price: " + invoicePrice);
//		assertEquals(60.90, invoicePrice);
//		File expectedInvoice = new File(resultFileName + "expectedInvoice.csv");
//		File invoice = new File(resultFileName + "invoice.csv");
//		FileAssert.assertBinaryEquals(expectedInvoice, invoice);
//		// Removes the added data for the Issue Monthly Invoice Test
//		dh.scriptRunner(scriptFolder + "issueMonthlyInvoiceTestRemoveScript.sql");
//		System.out.println("Removed Trips from the Database from Test Invoices");
//
//	}
//
//	@Order(13)
//	@Test
//	public void testUnlockBike() throws IOException, SQLException {
//
//		dh.scriptRunner(scriptFolder + "clearTripsAndInvoices.sql");
//		long time = facade.unlockBicycle("email1@gmail.com", "BIKE001");
//		assertTrue(time > 0);
//
//	}
//
//	@Order(14)
//	@Test
//	public void testUnlockAnyScooter() throws IOException, SQLException {
//
//		// String parkIdentification, String username, String outputFileName
//		long time2 = facade.unlockAnyEscooterAtPark("Trindade", "email2@gmail.com", "scooter.csv");
//		assertTrue(time2 > 0);
//
//	}
//
//	@Order(15)
//	@Test
//	public void testUnlockScooter() throws IOException, SQLException {
//
//		// String parkIdentification, String username, String outputFileName
//		long time3 = facade.unlockEscooter("email3@gmail.com", "SCOOTER001");
//		assertTrue(time3 > 0);
//
//	}
//
//	@Order(16)
//	@Test
//	public void testUnlockScooterGivenDestiny() throws IOException, SQLException {
//
//		// String parkIdentification, String username, double destinyLatitudeInDegrees, double destinyLongitudeInDegrees, String outputFileName
//		long time4 = facade.unlockAnyEscooterAtParkForDestination("trindade", "email4@gmail.com", 41.15227, -8.60929, "scooter.csv");
//		assertTrue(time4 > 0);
//		long time5 = facade.unlockAnyEscooterAtParkForDestination("trindade", "email5@gmail.com", 41.16875, -8.68995, "scooter.csv");
//		assertTrue(time5 == 0); // There is no path between the Parks
//
//	}
//
//	@Order(17)
//	@Test
//	public void testLockBicycle() throws IOException, SQLException {
//
//		long time5 = facade.lockBicycle("BIKE001", "Cais da Ribeira", "email1@gmail.com"); // Only uses User to get the Vehicle
//		assertTrue(time5 > 0);
//
//	}
//
//	@Order(18)
//	@Test
//	public void testLockBicycleGivenCords() throws IOException, SQLException {
//
//		long time = facade.unlockBicycle("email1@gmail.com", "BIKE001");
//		assertTrue(time > 0);
//		long time5 = facade.lockBicycle("BIKE001", 41.16875, -8.68995, "email1@gmail.com");
//		assertTrue(time5 > 0);
//
//	}
//
//	@Order(19)
//	@Test
//	public void howLongUnlocked() throws IOException, SQLException {
//
//		long time5 = facade.forHowLongAVehicleIsUnlocked("SCOOTER001");
//		assertTrue(time5 > 0);
//
//	}
//
//	@Order(20)
//	@Test
//	public void testLockScooter() throws IOException, SQLException {
//
//		long time5 = facade.lockEscooter("SCOOTER001", "Trindade", "email3@gmail.com");
//		assertTrue(time5 > 0);
//
//	}
//
//	@Order(21)
//	@Test
//	public void testLockScooterGivenParkCords() throws IOException, SQLException {
//		
//		long time = facade.unlockEscooter("email3@gmail.com", "SCOOTER001");
//		assertTrue(time > 0);
//		long time5 = facade.lockEscooter("SCOOTER001", 41.16875, -8.68995, "email3@gmail.com");
//		assertTrue(time5 > 0);
//
//	}

}
